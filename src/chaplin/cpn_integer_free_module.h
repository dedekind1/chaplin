#pragma once

#include "cpn_integer_ring.h"
#include "cpn_integer_module.h"
#include "cpn_free_module.h"

namespace cpn
{

template<ring_model R, size_t Dim>
using IntegerFreeModule_n = PowFreeModule<IntegerModule<R>,Dim>;

template<ring_model R = IntegerRing>
using IntegerFreeModule_1 = IntegerFreeModule_n<R,1>;
template<ring_model R = IntegerRing>
using IntegerFreeModule_2 = IntegerFreeModule_n<R,2>;
template<ring_model R = IntegerRing>
using IntegerFreeModule_3 = IntegerFreeModule_n<R,3>;
template<ring_model R = IntegerRing>
using IntegerFreeModule_4 = IntegerFreeModule_n<R,4>;

template<ring_model R, size_t Dim>
using integer_free_module_n = pow_free_module<integer_module<R>,reify_model<integer,R>,Dim>;

template<ring_model R = IntegerRing>
using integer_free_module_1 = integer_free_module_n<R,1>;
template<ring_model R = IntegerRing>
using integer_free_module_2 = integer_free_module_n<R,2>;
template<ring_model R = IntegerRing>
using integer_free_module_3 = integer_free_module_n<R,3>;
template<ring_model R = IntegerRing>
using integer_free_module_4 = integer_free_module_n<R,4>;

}