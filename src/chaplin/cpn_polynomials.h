#pragma once

#include "cpn_polynomial_algebra.h"
#include "cpn_algebraic_polynomial_concepts.h"

namespace cpn
{

template<ring_model T, typename Order = lexicographic_order, typename Allocator = ddk::system_allocator>
using Polynomial = typename make_algebraic_structure<polynomial<T,Order,Allocator>,typename concepts::holds_operations_impl<polynomial<T,Order,Allocator>>::type>::type;

}

#include "cpn_polynomial_ops.h"
#include "cpn_taylor_series.h"