#pragma once

#include "cpn_complex_ring.h"
#include "cpn_field.h"

namespace cpn
{

struct complex_division
{
    PUBLISH_OPERATION_PROPERTIES(complex_division,field_operation,commutative,associative,distributive);

	template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, typename ... Operations>
	friend inline auto operator/(const algebraic_structure<complex<T>,Operations...>& i_lhs, const algebraic_structure<complex<TT>,Operations...>& i_rhs)
	{
        typedef decltype(div(i_lhs,i_rhs)) ret_type;

		return algebraic_structure<ret_type,Operations...>{ div(i_lhs,i_rhs) };
	}

private:
	template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, typename ... Operations>
	static inline auto div(const complex<T>& i_lhs, const complex<TT>& i_rhs)
    {
        return complex{ (i_lhs.re().number() * i_rhs.re().number() + i_lhs.im().number() * i_rhs.im().number())/(i_rhs.re().number() * i_rhs.re().number() + i_rhs.im().number() * i_rhs.im().number()),
                        (i_lhs.im().number() * i_rhs.re().number() - i_lhs.re().number() * i_rhs.im().number())/(i_rhs.re().number() * i_rhs.re().number() + i_rhs.im().number() * i_rhs.im().number()) };
    }
};

using ComplexField = Field<ComplexRing,complex_division>;
template<solvable_literal_pack<2> T>
using complex_field = field<complex_ring<T>,complex_division,complex<T>>;

template<size_t ... Dims>
using ComplexField_n = times_model<ComplexField,Dims...>;

using ComplexField_1 = ComplexField_n<1>;
using ComplexField_2 = ComplexField_n<2>;
using ComplexField_3 = ComplexField_n<3>;
using ComplexField_4 = ComplexField_n<4>;

template<solvable_literal_pack<2> ... T>
using complex_field_n = prod_struct<complex_field<T>...>;

template<solvable_literal_pack<2> T>
using complex_field_1 = complex_field_n<T>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT>
using complex_field_2 = complex_field_n<T,TT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT>
using complex_field_3 = complex_field_n<T,TT,TTT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT, solvable_literal_pack<2> TTTT>
using complex_field_4 = complex_field_n<T,TT,TTT,TTTT>;

}