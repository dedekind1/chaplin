#pragma once

namespace cpn
{

template<callable_type Function>
struct operator_functor<Function,operator_tags<ring_operation>>
{
	PUBLISH_OPERATION_PROPERTIES(operator_functor,ring_operation,binary,commutative,associative,distributive);

	template<typename ... Operations>
	friend inline algebraic_structure<Function,Operations...> operator*(const algebraic_structure<Function,Operations...>& i_lhs, const algebraic_structure<Function,Operations...>& i_rhs)
	{
		return static_cast<const Function&>(i_lhs) * static_cast<const Function&>(i_rhs);
	}
};

}