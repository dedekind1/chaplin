#pragma once

#include "cpn_algebraic_operators.h"

namespace cpn
{

template<typename ... Operators>
using algebraic_model = algebraic_operators<Operators...>;

template<typename>
struct initial_object_impl;

template<set_model T>
struct initial_object_impl<T>
{
private:
    typedef typename algebraic_operator<T,set_operation>::ascend_t ascend_t;

    template<typename ... Operators>
    static algebraic_structure<ascend_t,Operators...> resolve(const algebraic_operators<Operators...>&);

public:
    typedef decltype(resolve(std::declval<T>())) type;
};

template<set_type T>
struct initial_object_impl<T>
{
    typedef T type;
};

template<typename>
struct final_object_impl;

template<set_model T>
struct final_object_impl<T>
{
private:
    typedef typename algebraic_operator<T,set_operation>::decay_t decay_t;

    template<typename ... Operators>
    static algebraic_structure<decay_t,Operators...> resolve(const algebraic_operators<Operators...>&);

public:
    typedef decltype(resolve(std::declval<T>())) type;
};

template<set_type T>
struct final_object_impl<T>
{
    typedef T type;
};

}