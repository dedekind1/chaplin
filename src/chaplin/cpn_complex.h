#pragma once

#include "cpn_complex_field.h"
#include "cpn_metric_space.h"
#include "cpn_real_field.h"

namespace cpn
{

struct complex_metric
{
	typedef real_field<float> valuation_type;

	template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT>
	inline valuation_type operator()(const complex<T>& i_lhs,const complex<TT>& i_rhs) const
	{
        const complex subs = i_lhs - i_rhs;

		return std::sqrt(subs.re().number() * subs.re().number() + subs.im().number() + subs.im().number());
	}
	template<solvable_literal_pack<2> T>
	inline auto operator()(const complex<T>& i_lhs, const valuation_type& i_rhs) const
	{
        return i_lhs + complex{i_rhs,i_rhs};
	}
};

using Complexs = MetricSpace<ComplexField,complex_metric>;

template<solvable_literal_pack<2> T>
using complexs = algebraic_structure<complex<T>,Complexs>;

template<size_t ... Dims>
using Complexs_n = pow_model<Complexs,Dims...>;

using Complexs1 = Complexs_n<1>;
using Complexs2 = Complexs_n<2>;
using Complexs3 = Complexs_n<3>;
using Complexs4 = Complexs_n<4>;

template<solvable_literal_pack<2> ... T>
using complexs_n = prod_struct<complexs<T>...>;

template<solvable_literal_pack<2> T>
using complexs1 = complexs_n<T>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT>
using complexs2 = complexs_n<T,TT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT>
using complexs3 = complexs_n<T,TT,TTT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT, solvable_literal_pack<2> TTTT>
using complexs4 = complexs_n<T,TT,TTT,TTTT>;

using C = Complexs;
template<solvable_literal_pack<2> T>
using c = complexs<T>;

}

#include "cpn_complex_vector_space.h"