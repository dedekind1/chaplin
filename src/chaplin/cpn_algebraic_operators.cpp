#include "cpn_algebraic_operators.h"

namespace cpn
{

operator_context::operator_context(const operator_tag_id& i_id)
: m_id(i_id)
{
}
operator_tag_id operator_context::id() const
{
    return m_id;
}

}