#pragma once

#include "cpn_algebraic_concepts.h"
#include "cpn_builtin_expression_ops.h"
#include "cpn_module.h"

namespace cpn
{

template<set_model T, typename Transporter, size_t Lattice = 10000>
struct point_convergence_operation
{
	PUBLISH_OPERATION_PROPERTIES(point_convergence_operation,point_conv_operation);

	typedef Transporter transporter;
	typedef typename Transporter::valuation_type valuation_type;
	static constexpr valuation_type k_epsilon = valuation_type(1.f/Lattice);

	static_assert(solvable_type<valuation_type>,"You shall provide through Transporter of a solvable type");

	TEMPLATE(set_type TT, typename ... Operations)
	REQUIRES(implements_model<TT,T>)
	friend inline auto convergence(const TT& i_targetPoint, size_t i_index)
	{
		if(i_index < Lattice)
		{
			static const Transporter s_transporter;

			return s_transporter(i_targetPoint,valuation_type{ (Lattice - i_index) * resolve(k_epsilon) });
		}
		else
		{
			return i_targetPoint;
		}			
	}
};

template<complete_space_model T, size_t ... Dims>
struct pow_operation<point_conv_operation,T,Dims...>
{
    PUBLISH_OPERATION_PROPERTIES(pow_operation,point_conv_operation);

	static const size_t k_cardinality = ddk::mpl::prod_ranks<Dims...>;
	typedef typename algebraic_operator<T,point_conv_operation>::valuation_type valuation_type;
	static constexpr std::array<valuation_type,k_cardinality> k_epsilon = { algebraic_operator<T,point_conv_operation>::k_epsilon };

	static_assert(solvable_type<valuation_type>,"You shall provide through Transporter of a solvable type");

	TEMPLATE(complete_space_type TT, typename ... Operations)
	REQUIRES(implements_model<TT,T>)
	friend inline auto convergence(const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_targetPoint, const std::array<size_t,k_cardinality>& i_indexs)
	{
		typedef typename ddk::mpl::make_sequence<0,k_cardinality>::type seq_t;

		return _convergence(seq_t{},i_targetPoint,i_indexs);
	}

private:
	TEMPLATE(size_t ... Indexs, complete_space_type TT, typename ... Operations)
	REQUIRES(implements_model<TT,T>)
	static inline auto _convergence(const ddk::mpl::sequence<Indexs...>&, const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_targetPoint, const std::array<size_t,k_cardinality>& i_indexs)
	{
		typedef pow_type<decltype(convergence(std::declval<TT>(),std::declval<size_t>())),Dims...> pow_t;

		return algebraic_structure<pow_t,Operations...>{ pow_type{ convergence(project_onto<TT>(i_targetPoint,Indexs),i_indexs[Indexs])... } };
	}
};

template<complete_space_model ... T>
struct prod_operation<point_conv_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(prod_operation,point_conv_operation);

	static const size_t k_cardinality = ddk::mpl::num_types<T...>;
	typedef ddk::mpl::homogeneous_type<typename algebraic_operator<T,point_conv_operation>::valuation_type...> valuation_type;
	static constexpr std::array<valuation_type,k_cardinality> k_epsilon = { algebraic_operator<T,point_conv_operation>::k_epsilon ... };

	static_assert(solvable_type<valuation_type>,"You shall provide through Transporter of a solvable type");

	TEMPLATE(complete_space_type ... TT, typename ... Operations)
	REQUIRES(implements_model<prod_type<TT...>,T...>)
	friend inline auto convergence(const algebraic_structure<prod_type<TT...>,Operations...>& i_targetPoint, const std::array<size_t,k_cardinality>& i_indexs)
	{
		typedef typename ddk::mpl::make_sequence<0,k_cardinality>::type seq_t;

		return _convergence(seq_t{},i_targetPoint,i_indexs);
	}

private:
	TEMPLATE(size_t ... Indexs, complete_space_type ... TT, typename ... Operations)
	REQUIRES(implements_model<prod_type<TT...>,T...>)
	static inline auto _convergence(const ddk::mpl::sequence<Indexs...>&, const algebraic_structure<prod_type<TT...>,Operations...>& i_targetPoint, const std::array<size_t,k_cardinality>& i_indexs)
	{
		typedef prod_type<decltype(convergence(std::declval<TT>(),std::declval<size_t>()))...> prod_t;

		return algebraic_structure<prod_t,Operations...>{ prod_type{ convergence(project_onto<TT>(i_targetPoint,Indexs),i_indexs[Indexs])... } };
	}
};

template<group_model T, typename Transporter>
using complete_space = typename T::template equip_with<point_convergence_operation<T,Transporter>>;

}