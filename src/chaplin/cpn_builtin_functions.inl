
#include "cpn_builtin_operators.h"
#include "ddkFramework/ddk_iterable.h"

namespace cpn
{
namespace detail
{

template<builtin_function_type Function>
builtin_function_id get_builtin_function_id()
{
    static builtin_function_id _id = get_next_builtin_function_id();

    return _id;
}

template<typename SuperClass, set_model Im, set_model Dom, typename Allocator>
const std::array<typename builtin_function<SuperClass,Im,Dom,Allocator>::binary_operator_type,builtin_function<SuperClass,Im,Dom,Allocator>::s_numOps> builtin_function<SuperClass,Im,Dom,Allocator>::s_binary_operators = builtin_operators_init<SuperClass>::binary(function_operator_tags<function<Im(const Dom&),Allocator>>{});
template<typename SuperClass, set_model Im, set_model Dom, typename Allocator>
const std::array<typename builtin_function<SuperClass,Im,Dom,Allocator>::unary_operator_type,builtin_function<SuperClass,Im,Dom,Allocator>::s_numOps> builtin_function<SuperClass,Im,Dom,Allocator>::s_unary_operators = builtin_operators_init<SuperClass>::unary(function_operator_tags<function<Im(const Dom&),Allocator>>{});

template<typename SuperClass, set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> builtin_function<SuperClass,Im,Dom,Allocator>::clone() const
{
    return { static_cast<const SuperClass&>(*this) };
}
template<typename SuperClass, set_model Im, set_model Dom, typename Allocator>
builtin_function_id builtin_function<SuperClass,Im,Dom,Allocator>::id() const
{
    return __builtin_function_id(static_cast<const SuperClass&>(*this));
}
template<typename SuperClass, set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> builtin_function<SuperClass,Im,Dom,Allocator>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& other, size_t i_depth) const
{
    if(i_context.id().getValue() < s_numOps)
    {
        return (*s_binary_operators[i_context.id().getValue()])(i_context,*this,other,i_depth);
    }
    else
    {
        throw;
    }
}
template<typename SuperClass, set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> builtin_function<SuperClass,Im,Dom,Allocator>::unary_operator(const operator_context& i_context) const
{
    if(i_context.id().getValue() < s_numOps)
    {
        return (*s_unary_operators[i_context.id().getValue()])(i_context,*this);
    }
    else
    {
        throw;
    }
}

template<set_model Im,set_model Dom, typename Allocator>
final_object<Im> builtin_null_function<Im(const Dom&),Allocator>::operator()(final_object<const Dom&> i_args) const
{
    if constexpr (concepts::contains_algebraic_structure<Im,ring_operation>)
    {
        return algebraic_operator<Im,ring_operation>::annihilator;
    }
    else if constexpr (concepts::contains_algebraic_structure<Im,semi_group_operation>)
    {
        return algebraic_operator<Im,semi_group_operation>::identity;
    }
    else
    {
        throw;
    }
}

template<set_model Im,set_model Dom, typename Allocator>
final_object<Im> builtin_unity_function<Im(const Dom&),Allocator>::operator()(final_object<const Dom&> i_args) const
{
    if constexpr (concepts::contains_algebraic_structure<Im,ring_operation>)
    {
        return algebraic_operator<Im,ring_operation>::identity;
    }
    else if constexpr (concepts::contains_algebraic_structure<Im,semi_group_operation>)
    {
        return algebraic_operator<Im,semi_group_operation>::identity;
    }
    else
    {
        throw;        
    }
}

template<intersection_model Im,set_model Dom, typename Allocator>
TEMPLATE(typename ... CCallables)
REQUIRED(ddk::is_num_of_args_equal<Im::rank(),CCallables...>,ddk::is_constructible<typename builtin_fusioned_function<Im(const Dom&),Allocator>::function_t,CCallables>...)
builtin_fusioned_function<Im(const Dom&),Allocator>::builtin_fusioned_function(CCallables&& ... i_callables)
: m_callables({ std::forward<CCallables>(i_callables)... })
{
}
template<intersection_model Im,set_model Dom, typename Allocator>
final_object<Im> builtin_fusioned_function<Im(const Dom&),Allocator>::operator()(final_object<const Dom&> i_args) const
{
    return execute(typename ddk::mpl::make_sequence<0,Im::num_places>::type{},i_args);
}
template<intersection_model Im,set_model Dom, typename Allocator>
const typename builtin_fusioned_function<Im(const Dom&),Allocator>::function_t& builtin_fusioned_function<Im(const Dom&),Allocator>::operator[](size_t i_index) const
{
    return m_callables[i_index];
}
template<intersection_model Im,set_model Dom, typename Allocator>
template<size_t ... Indexs>
Im builtin_fusioned_function<Im(const Dom&),Allocator>::execute(const ddk::mpl::sequence<Indexs...>&, const Dom& i_args) const
{
    return { ddk::eval(m_callables[Indexs],i_args) ...};
}

template<set_model Im,set_model Dom, typename Allocator>
TEMPLATE(typename Function,typename FFunction)
REQUIRED(ddk::is_constructible<typename builtin_composed_function<Im(const Dom&),Allocator>::function_lhs_t,Function>,
         ddk::is_constructible<typename builtin_composed_function<Im(const Dom&),Allocator>::function_rhs_t,FFunction>)
builtin_composed_function<Im(const Dom&),Allocator>::builtin_composed_function(Function&& i_lhs, FFunction&& i_rhs)
: m_lhs(std::forward<Function>(i_lhs))
, m_rhs(std::forward<FFunction>(i_rhs))
{
}
template<set_model Im,set_model Dom, typename Allocator>
final_object<Im> builtin_composed_function<Im(const Dom&),Allocator>::operator()(final_object<const Dom&> i_args) const
{
    return ddk::eval(m_lhs,ddk::eval(m_rhs,i_args));
}
template<set_model Im,set_model Dom, typename Allocator>
const typename builtin_composed_function<Im(const Dom&),Allocator>::function_lhs_t& builtin_composed_function<Im(const Dom&),Allocator>::lhs_function() const
{
    return m_lhs;
}
template<set_model Im,set_model Dom, typename Allocator>
const typename builtin_composed_function<Im(const Dom&),Allocator>::function_rhs_t& builtin_composed_function<Im(const Dom&),Allocator>::rhs_function() const
{
    return m_rhs;
}

template<set_model Im,set_model Dom, typename Allocator>
template<size_t Comp>
builtin_component_function<Im(const Dom&),Allocator>::builtin_component_function(const ddk::mpl::static_number<Comp>&)
: m_component(Comp)
{
}
template<set_model Im,set_model Dom, typename Allocator>
final_object<Im> builtin_component_function<Im(const Dom&),Allocator>::operator()(final_object<const Dom&> i_arg) const
{
    return forward_arg(i_arg);
}
template<set_model Im,set_model Dom, typename Allocator>
size_t builtin_component_function<Im(const Dom&),Allocator>::component() const
{
    return m_component;
}
template<set_model Im,set_model Dom, typename Allocator>
template<intersection_type DDom>
final_object<Im> builtin_component_function<Im(const Dom&),Allocator>::forward_arg(const DDom& i_arg) const
{
    return project_onto<final_object<Im>>(i_arg,m_component);
}
template<set_model Im,set_model Dom, typename Allocator>
template<flat_type DDom>
final_object<Im> builtin_component_function<Im(const Dom&),Allocator>::forward_arg(const DDom& i_arg) const
{
    return { i_arg };
}

template<set_model Im,set_model Dom, typename Allocator>
builtin_number_function<Im(const Dom&),Allocator>::builtin_number_function(const final_object<Im>& i_number)
: m_number(i_number)
{
}
template<set_model Im,set_model Dom, typename Allocator>
template<solvable_literal_type IIm>
builtin_number_function<Im(const Dom&),Allocator>::builtin_number_function(const IIm& i_number)
: m_number(i_number)
{
}
template<set_model Im,set_model Dom, typename Allocator>
final_object<Im> builtin_number_function<Im(const Dom&),Allocator>::operator()(final_object<const Dom&> i_args) const
{
    return m_number;
}
template<set_model Im,set_model Dom, typename Allocator>
const final_object<Im>& builtin_number_function<Im(const Dom&),Allocator>::number() const
{
    return m_number;
}

template<group_model Im,set_model Dom, typename Allocator>
TEMPLATE(typename Function)
REQUIRED(ddk::is_constructible<typename builtin_minus_function<Im(const Dom&),Allocator>::function_t,Function>)
builtin_minus_function<Im(const Dom&),Allocator>::builtin_minus_function(Function&& i_function)
: m_function(std::forward<Function>(i_function))
{
}
template<group_model Im,set_model Dom, typename Allocator>
const typename builtin_minus_function<Im(const Dom&),Allocator>::function_t& builtin_minus_function<Im(const Dom&),Allocator>::func() const
{
    return m_function;
}
template<group_model Im,set_model Dom, typename Allocator>
final_object<Im> builtin_minus_function<Im(const Dom&),Allocator>::operator()(final_object<const Dom&> i_args) const
{
    return -ddk::eval(m_function,i_args);
}

template<field_model Im,set_model Dom, typename Allocator>
TEMPLATE(typename Function)
REQUIRED(ddk::is_constructible<typename builtin_inverted_function<Im(const Dom&),Allocator>::function_t,Function>)
builtin_inverted_function<Im(const Dom&),Allocator>::builtin_inverted_function(Function&& i_function)
: m_function(std::forward<Function>(i_function))
{
}
template<field_model Im,set_model Dom, typename Allocator>
const typename builtin_inverted_function<Im(const Dom&),Allocator>::function_t& builtin_inverted_function<Im(const Dom&),Allocator>::func() const
{
    return m_function;
}
template<field_model Im,set_model Dom, typename Allocator>
final_object<Im> builtin_inverted_function<Im(const Dom&),Allocator>::operator()(final_object<const Dom&> i_args) const
{
    mpfr_t res,den;

    mpfr_init(res);
    mpfr_init(den);

    mpfr_ui_div(res,1,den,MPFR_RNDD);

    return { mpfr_get_d(res,MPFR_RNDD) };
}

template<ring_model Im,set_model Dom, typename Allocator>
TEMPLATE(typename Function, typename FFunction)
REQUIRED(ddk::is_constructible<typename builtin_pow_function<Im(const Dom&),Allocator>::function_t,Function>,
         ddk::is_constructible<typename builtin_pow_function<Im(const Dom&),Allocator>::function_t,FFunction>)
builtin_pow_function<Im(const Dom&),Allocator>::builtin_pow_function(Function&& i_base, FFunction&& i_exp)
: m_base(std::forward<Function>(i_base))
, m_exp(std::forward<FFunction>(i_exp))
{
}
template<ring_model Im,set_model Dom, typename Allocator>
const typename builtin_pow_function<Im(const Dom&),Allocator>::function_t& builtin_pow_function<Im(const Dom&),Allocator>::base() const
{
    return m_base;
}
template<ring_model Im,set_model Dom, typename Allocator>
const typename builtin_pow_function<Im(const Dom&),Allocator>::function_t& builtin_pow_function<Im(const Dom&),Allocator>::exp() const
{
    return m_exp;
}
template<ring_model Im,set_model Dom, typename Allocator>
final_object<Im> builtin_pow_function<Im(const Dom&),Allocator>::operator()(final_object<const Dom&> i_args) const
{
    mpfr_t res,base,degree;

    mpfr_init(res);
    mpfr_init(base);
    mpfr_init(degree);

    mpfr_set_d(base,ddk::eval(m_base,i_args),MPFR_RNDD);
    mpfr_set_d(degree,ddk::eval(m_exp,i_args),MPFR_RNDD);

    mpfr_pow(res,base,degree,MPFR_RNDD);

    return mpfr_get_d(res,MPFR_RNDD);
}

template<set_model Im,set_model Dom, typename Allocator, typename Functor>
builtin_functor_function<Im(const Dom&),Allocator,Functor>::builtin_functor_function(const builtin_functor_expression<Functor>& i_functor)
: m_functor(i_functor.functor())
{
}
template<set_model Im,set_model Dom, typename Allocator, typename Functor>
const Functor& builtin_functor_function<Im(const Dom&),Allocator,Functor>::functor() const
{
    return m_functor;
}
template<set_model Im,set_model Dom, typename Allocator, typename Functor>
final_object<Im> builtin_functor_function<Im(const Dom&),Allocator,Functor>::operator()(final_object<const Dom&> i_args) const
{
    return ddk::eval(m_functor,i_args);
}

template<set_model Im, set_model Dom, typename Allocator>
template<typename ... Functions>
builtin_nary_function<Im(const Dom&),Allocator>::builtin_nary_function(const Functions& ... i_functions)
: m_functions({i_functions...})
{
}
template<set_model Im, set_model Dom, typename Allocator>
builtin_nary_function<Im(const Dom&),Allocator>::builtin_nary_function(const container_func& i_functions)
: m_functions(i_functions)
{
}
template<set_model Im, set_model Dom, typename Allocator>
builtin_nary_function<Im(const Dom&),Allocator>::builtin_nary_function(container_func&& i_functions)
: m_functions(std::move(i_functions))
{
}
template<set_model Im, set_model Dom, typename Allocator>
bool builtin_nary_function<Im(const Dom&),Allocator>::empty() const
{
    return m_functions.empty();
}
template<set_model Im, set_model Dom, typename Allocator>
size_t builtin_nary_function<Im(const Dom&),Allocator>::size() const
{
    return m_functions.size();
}
template<set_model Im, set_model Dom, typename Allocator>
template<typename Callable>
void builtin_nary_function<Im(const Dom&),Allocator>::enumerate(Callable&& i_callable) const
{
    for(const auto& func : m_functions)
    {
        ddk::eval(std::forward<Callable>(i_callable),func);
    }
}
template<set_model Im, set_model Dom, typename Allocator>
typename builtin_nary_function<Im(const Dom&),Allocator>::iterator builtin_nary_function<Im(const Dom&),Allocator>::begin()
{
    return m_functions.begin();
}
template<set_model Im, set_model Dom, typename Allocator>
typename builtin_nary_function<Im(const Dom&),Allocator>::const_iterator builtin_nary_function<Im(const Dom&),Allocator>::begin() const
{
    return m_functions.begin();
}
template<set_model Im, set_model Dom, typename Allocator>
typename builtin_nary_function<Im(const Dom&),Allocator>::iterator builtin_nary_function<Im(const Dom&),Allocator>::end()
{
    return m_functions.end();
}
template<set_model Im, set_model Dom, typename Allocator>
typename builtin_nary_function<Im(const Dom&),Allocator>::const_iterator builtin_nary_function<Im(const Dom&),Allocator>::end() const
{
    return m_functions.end();
}

}
}
