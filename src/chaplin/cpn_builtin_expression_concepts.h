#pragma once

#include "cpn_builtin_expressions.h"

namespace cpn
{
namespace concepts
{

template<typename T>
struct constant_inspection
{
	template<typename TT>
	static typename ddk::mpl::which_type<TT::__expression_properties::template contains<cnstant>(),std::true_type,std::false_type>::type resolve(const TT&);
	static std::false_type resolve(...);

public:
	static const bool value = decltype(resolve(std::declval<T>()))::value;
};
template<size_t ... Indexs,typename ... Expressions>
struct constant_inspection<add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>>
{
	static const bool value = (constant_inspection<Expressions>::value && ...);
};
template<size_t ... Indexs,typename ... Expressions>
struct constant_inspection<prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>>
{
	static const bool value = (constant_inspection<Expressions>::value && ...);
};
template<typename Expression>
struct constant_inspection<builtin_minus_expression<Expression>>
{
	static const bool value = constant_inspection<Expression>::value;
};

template<typename T>
struct linear_inspection
{
private:
	template<typename TT>
	static typename ddk::mpl::which_type<TT::__expression_properties::template contains<linear>(),std::true_type,std::false_type>::type resolve(const TT&);
	static std::false_type resolve(...);

public:
	static const bool value = decltype(resolve(std::declval<T>()))::value;
};
template<size_t ... Indexs,typename ... Expressions>
struct linear_inspection<add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>>
{
	static const bool value = (linear_inspection<Expressions>::value && ...);
};
template<size_t ... Indexs,typename ... Expressions>
struct linear_inspection<prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>>
{
private:
	static const size_t s_numConstantExpressions = (static_cast<size_t>(constant_inspection<Expressions>::value) + ...);

public:
	static const bool value = (s_numConstantExpressions >= ddk::mpl::num_types<Expressions...>-1) && (linear_inspection<Expressions>::value && ...);
};
template<typename LhsFunction,typename RhsFunction>
struct linear_inspection<builtin_composed_expression<LhsFunction,RhsFunction>>
{
	static const bool value = linear_inspection<LhsFunction>::value && linear_inspection<RhsFunction>::value;
};
template<typename Expression>
struct linear_inspection<builtin_minus_expression<Expression>>
{
	static const bool value = linear_inspection<Expression>::value;
};

template<typename T>
struct polynomic_inspection
{
	static const bool value = linear_inspection<T>::value || constant_inspection<T>::value;
};
template<size_t ... Indexs,typename ... Expressions>
struct polynomic_inspection<add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>>
{
	static const bool value = (polynomic_inspection<Expressions>::value && ...);
};
template<size_t ... Indexs,typename ... Expressions>
struct polynomic_inspection<prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>>
{
	static const bool value = (polynomic_inspection<Expressions>::value && ...);
};
template<typename Expression, typename EExpression>
struct polynomic_inspection<pow_nary_expression<ddk::mpl::sequence<0,1>,Expression,EExpression>>
{
	static const bool value = polynomic_inspection<Expression>::value && constant_inspection<EExpression>::value;
};
template<typename LhsFunction,typename RhsFunction>
struct polynomic_inspection<builtin_composed_expression<LhsFunction,RhsFunction>>
{
	static const bool value = polynomic_inspection<LhsFunction>::value && polynomic_inspection<RhsFunction>::value;
};
template<typename Expression>
struct polynomic_inspection<builtin_minus_expression<Expression>>
{
	static const bool value = polynomic_inspection<Expression>::value;
};

}

template<literal_expression T>
constexpr bool is_constant_expression = concepts::constant_inspection<ddk::mpl::remove_qualifiers<T>>::value;

template<literal_expression T>
constexpr bool is_linear_expression = concepts::linear_inspection<ddk::mpl::remove_qualifiers<T>>::value;

template<literal_expression T>
constexpr bool is_polynomial_expression = concepts::polynomic_inspection<ddk::mpl::remove_qualifiers<T>>::value;

}