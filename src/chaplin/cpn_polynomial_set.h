#pragma once

#include "cpn_algebraic_concepts.h"
#include "cpn_polynomial.h"
#include "ddkFramework/ddk_system_allocator.h"
#include "ddkFramework/ddk_iterable.h"

namespace cpn
{

template<ring_model T, typename Order, typename Allocator>
struct operator_functor<polynomial<T,Order,Allocator>,operator_tags<set_operation>>
{
	PUBLISH_OPERATION_PROPERTIES(operator_functor,set_operation,totally_ordered);

	typedef polynomial<final_object<T>> ascend_t;

	friend inline constexpr bool operator<(const polynomial<T,Order,Allocator>& i_lhs, polynomial<T,Order,Allocator>& i_rhs)
	{
        const Order _order;

        return _order(i_lhs,i_rhs);
	}
	friend inline constexpr bool operator==(const polynomial<T,Order,Allocator>& i_lhs, const polynomial<T,Order,Allocator>& i_rhs)
	{
        if(auto res = [](const monomial<T>& ii_lhs, const monomial<T>& ii_rhs) -> bool
        {
            if(ii_lhs != ii_rhs)
            {
                ddk::iter::yield(false);
            }
        } <<= ddk::fusion(i_lhs,i_rhs))
        {
            return *res;
        }
        else
        {
            return false;
        }
	}
	friend inline constexpr bool operator!=(const polynomial<T,Order,Allocator>& i_lhs, const polynomial<T,Order,Allocator>& i_rhs)
	{
		return !(i_lhs == i_rhs);
	}
};

}