#pragma once

#include "ddkFramework/ddk_iterable.h"

namespace cpn
{

TEMPLATE(callable_type Generator, iterable_deducible_type ... Iterables)
REQUIRES(ddk::is_callable_by<Generator,typename ddk::detail::adaptor_traits<Iterables>::const_reference...>)
inline auto series(Generator&& i_generator, Iterables&& ... i_iterables);

}

#include "cpn_series.inl"