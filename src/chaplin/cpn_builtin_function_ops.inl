
namespace cpn
{

TEMPLATE(set_model Im, set_model Dom, typename Allocator, numeric_type T)
REQUIRED(ddk::is_constructible<Im,T>)
detail::builtin_number_function<Im(const Dom&),Allocator> instance_function(const builtin_literal_expression<T>& i_exp)
{
	return { i_exp.get() };
}

template<set_model Im,set_model Dom, typename Allocator>
detail::builtin_component_function<Im(const Dom&),Allocator> instance_function(const builtin_incognita_expression& i_exp)
{
	return { ddk::mpl::static_number<0>{} };
}

TEMPLATE(set_model Im, intersection_model Dom, typename Allocator, size_t Comp)
REQUIRED(Comp < Dom::rank())
detail::builtin_component_function<Im(const Dom&),Allocator> instance_function(const builtin_component_expression<Comp>& i_exp)
{
	return { ddk::mpl::static_number<Comp>{} };
}

TEMPLATE(set_model Im,set_model Dom, typename Allocator, typename Expression)
REQUIRED(is_function_instantiable_by<Expression,Im,Dom>)
detail::builtin_minus_function<Im(const Dom&),Allocator> instance_function(const builtin_minus_expression<Expression>& i_exp)
{
	return { instance_function<Im,Dom,Allocator>(i_exp.get()) };
}

TEMPLATE(set_model Im,set_model Dom, typename Allocator, typename Expression)
REQUIRED(is_function_instantiable_by<Expression,Im,Dom>)
detail::builtin_inverted_function<Im(const Dom&),Allocator> instance_function(const builtin_inverted_expression<Expression>& i_exp)
{
	return { instance_function<Im,Dom,Allocator>(i_exp.get()) };
}

TEMPLATE(set_model Im,set_model Dom, typename Allocator, size_t ...Indexs, literal_expression ... Expressions)
REQUIRED(is_function_instantiable_by<Expressions,Im,Dom>...)
detail::builtin_add_nary_function<Im(const Dom&),Allocator> instance_function(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_exp)
{
	detail::builtin_add_nary_function<Im(const Dom&),Allocator> res;

	[&res](auto&& ii_exp)
	{
		res.push(instance_function<Im,Dom,Allocator>(ii_exp));
	} <<= i_exp;

	return res;
}

TEMPLATE(set_model Im,set_model Dom, typename Allocator, size_t ...Indexs, literal_expression ... Expressions)
REQUIRED(is_function_instantiable_by<Expressions,Im,Dom>...)
detail::builtin_mult_nary_function<Im(const Dom&),Allocator> instance_function(const prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_exp)
{
	detail::builtin_mult_nary_function<Im(const Dom&),Allocator> res;

	[&res](auto&& ii_exp)
	{
		res.push(instance_function<Im,Dom,Allocator>(ii_exp));
	} <<= i_exp;

	return res;
}

TEMPLATE(set_model Im,set_model Dom, typename Allocator, literal_expression LhsExpression, literal_expression RhsExpression)
REQUIRED(is_function_instantiable_by<LhsExpression,Im,Im>,is_function_instantiable_by<RhsExpression,Im,Dom>)
detail::builtin_composed_function<Im(const Dom&),Allocator> instance_function(const builtin_composed_expression<LhsExpression,RhsExpression>& i_exp)
{
	return { instance_function<Im,Im,Allocator>(i_exp.lhs()),instance_function<Im,Dom,Allocator>(i_exp.rhs()) };
}

template<set_model Im,set_model Dom, typename Allocator, typename Functor>
detail::builtin_functor_function<Im(const Dom&),Allocator,Functor> instance_function(const builtin_functor_expression<Functor>& i_exp)
{
	return { i_exp };
}

}
