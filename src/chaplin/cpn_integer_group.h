#pragma once

#include "cpn_integer_set.h"
#include "cpn_group.h"

namespace cpn
{

struct integer_addition
{
    PUBLISH_OPERATION_PROPERTIES(integer_addition,semi_group_operation,commutative,associative,distributive);

	static constexpr integer_set identity = integer(0);

	template<typename ... Operations>
	friend inline auto operator+(const algebraic_structure<integer,Operations...>& i_lhs,const algebraic_structure<integer,Operations...>& i_rhs)
	{
		return algebraic_structure<integer,Operations...>{ i_lhs.number() + i_rhs.number() };
	}
};

using IntegerSemiGroup = SemiGroup<IntegerSet,integer_addition>;
using integer_semi_group = semi_group<integer_set,integer_addition>;

template<size_t ... Dims>
using IntegerSemiGroup_n = pow_model<IntegerSemiGroup,Dims...>;

typedef IntegerSemiGroup_n<1> IntegerSemiGroup_1;
typedef IntegerSemiGroup_n<2> IntegerSemiGroup_2;
typedef IntegerSemiGroup_n<3> IntegerSemiGroup_3;
typedef IntegerSemiGroup_n<4> IntegerSemiGroup_4;

template<size_t ... Dims>
using integer_semi_group_n = pow_struct<integer_semi_group,Dims...>;

typedef integer_semi_group_n<1> integer_semi_group_1;
typedef integer_semi_group_n<2> integer_semi_group_2;
typedef integer_semi_group_n<3> integer_semi_group_3;
typedef integer_semi_group_n<4> integer_semi_group_4;

struct integer_addition_inverse
{
    PUBLISH_OPERATION_PROPERTIES(integer_addition_inverse,group_operation);

	template<typename ... Operations>
	friend inline auto operator-(const algebraic_structure<integer,Operations...>& i_rhs)
	{
		return algebraic_structure<integer,Operations...>{ -i_rhs.number() };
	}
	template<typename ... Operations>
	friend inline auto operator-(const algebraic_structure<integer,Operations...>& i_lhs, const algebraic_structure<integer,Operations...>& i_rhs)
	{
		return algebraic_structure<integer,Operations...>{ i_lhs.number() + (-i_rhs.number()) };
	}
};

using IntegerGroup = Group<IntegerSemiGroup,integer_addition_inverse>;
using integer_group = group<integer_semi_group,integer_addition_inverse>;

template<size_t ... Dims>
using IntegerGroup_n = pow_model<IntegerGroup,Dims...>;

typedef IntegerGroup_n<1> IntegerGroup_1;
typedef IntegerGroup_n<2> IntegerGroup_2;
typedef IntegerGroup_n<3> IntegerGroup_3;
typedef IntegerGroup_n<4> IntegerGroup_4;

template<size_t ... Dims>
using integer_group_n = pow_struct<integer_group,Dims...>;

typedef integer_group_n<1> integer_group_1;
typedef integer_group_n<2> integer_group_2;
typedef integer_group_n<3> integer_group_3;
typedef integer_group_n<4> integer_group_4;

}