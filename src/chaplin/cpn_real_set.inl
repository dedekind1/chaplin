
namespace cpn
{

template<literal_type T>
constexpr real<T>::real(const T& i_number)
: m_number(i_number)
{
}
template<literal_type T>
TEMPLATE(typename TT)
REQUIRED(ddk::is_constructible<T,TT>)
constexpr real<T>::real(const real_symbolic_literal<TT>& i_number)
: m_number(i_number)
{
}
template<literal_type T>
TEMPLATE(typename TT)
REQUIRED(ddk::is_constructible<real_symbolic_literal<T>,real_symbolic_literal<TT>>)
constexpr real<T>::real(const real<TT>& i_number)
: m_number(i_number.number())
{
}
template<literal_type T>
constexpr real_symbolic_literal<T> real<T>::number() const
{
    return m_number;
}

}