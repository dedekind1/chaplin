#pragma once

#include "cpn_integer_module.h"
#include "cpn_integer_ring.h"
#include "cpn_algebra.h"

namespace cpn
{

struct integer_algebra_operation : integer_multiplication
{
    PUBLISH_OPERATION_PROPERTIES(integer_algebra_operation,algebra_operation,commutative,associative,distributive);

	template<typename ... Operations>
	friend inline auto operator&(const algebraic_structure<integer,Operations...>& i_lhs,const algebraic_structure<integer,Operations...>& i_rhs)
	{
		return algebraic_structure<integer,Operations...>{ i_lhs.number() * i_rhs.number() };
	}
};

template<ring_model R = IntegerRing>
using IntegerAlgebra = Algebra<IntegerModule<R>,integer_algebra_operation>;
using IIntegerAlgebra = IntegerAlgebra<IntegerRing>;
template<ring_model R = IntegerRing>
using integer_algebra = algebra<integer_module<R>,integer_algebra_operation>;

}