
namespace cpn
{

template<typename T,typename ... Operators>
algebraic_structure<T,Operators...>::algebraic_structure(const T& i_value)
: T(i_value)
{
}
template<typename T,typename ... Operators>
TEMPLATE(typename ... Args)
REQUIRED(ddk::is_constructible<T,Args...>)
constexpr algebraic_structure<T,Operators...>::algebraic_structure(Args&& ... i_args)
: T(std::forward<Args>(i_args)...)
{
}
template<typename T,typename ... Operators>
TEMPLATE(typename TT, typename ... OOperators)
REQUIRED(ddk::is_constructible<T,TT>,is_super_structure_of<ddk::mpl::type_pack<OOperators...>,ddk::mpl::type_pack<Operators...>>)
constexpr algebraic_structure<T,Operators...>::algebraic_structure(const algebraic_structure<TT,OOperators...>& other)
: T(static_cast<const TT&>(other))
{
}
template<typename T,typename ... Operators>
TEMPLATE(typename TT)
REQUIRED(ddk::is_constructible<T,TT>)
constexpr algebraic_structure<T,Operators...>::algebraic_structure(const algebraic_structure<TT,Operators...>& other)
: T(static_cast<const TT&>(other))
{
}
template<typename T,typename ... Operators>
TEMPLATE(typename TT, typename ... OOperators)
REQUIRED(ddk::is_assignable<T,TT>,is_super_structure_of<ddk::mpl::type_pack<OOperators...>,ddk::mpl::type_pack<Operators...>>)
constexpr algebraic_structure<T,Operators...>& algebraic_structure<T,Operators...>::operator=(const algebraic_structure<TT,OOperators...>& other)
{
    T::operator=(static_cast<const TT&>(other));

    return *this;
}
template<typename T,typename ... Operators>
TEMPLATE(typename TT)
REQUIRED(ddk::is_assignable<T,TT>)
constexpr algebraic_structure<T,Operators...>& algebraic_structure<T,Operators...>::operator=(const algebraic_structure<TT,Operators...>& other)
{
    T::operator=(static_cast<const TT&>(other));

    return *this;
}

}