#pragma once

#include "cpn_algebraic_operators.h"
#include "cpn_function_concepts.h"

namespace cpn
{
namespace concepts
{

template<typename,typename...>
struct function_operators_mapper;

template<typename Function,typename ... T>
struct function_operators_mapper<Function,ddk::mpl::type_pack<>,T...>
{
    typedef empty_operators type;
};

template<typename Function, typename OpsTag, typename ... OpsTags>
struct function_operators_mapper<Function,ddk::mpl::type_pack<OpsTag,OpsTags...>>
{
    template<typename ... _OpsTags, typename = typename operator_functor<Function,operator_tags<_OpsTags...>>::__operator_tags>
    static operator_functor<Function,operator_tags<_OpsTags...>> resolve(const operator_tags<_OpsTags...>&);
    static no_op resolve(...);

    typedef typename make_algebraic_operators<decltype(resolve(std::declval<OpsTag>()))>::type local_type;
    typedef function_operators_mapper<Function,ddk::mpl::type_pack<OpsTags...>>::type nested_type;

public:
    typedef typename merge_algebraic_operators<local_type,nested_type>::type type;
};

template<typename Function, typename OpsTag, typename ... OpsTags, typename ... OOpsTags>
struct function_operators_mapper<Function,ddk::mpl::type_pack<OpsTag,OpsTags...>,ddk::mpl::type_pack<OOpsTags...>>
{
    template<typename ... _OpsTags, typename ... _OOptsTags, typename = typename operator_functor<Function,operator_tags<_OpsTags...>,operator_tags<_OOptsTags...>>::__operator_tags>
    static operator_functor<Function,operator_tags<_OpsTags...>,operator_tags<_OOptsTags...>> resolve(const operator_tags<_OpsTags...>&, const operator_tags<_OOptsTags...>&);
    static no_op resolve(...);

    typedef typename make_algebraic_operators<decltype(resolve(std::declval<OpsTag>(),std::declval<OOpsTags>()))...>::type local_type;
    typedef function_operators_mapper<Function,ddk::mpl::type_pack<OpsTags...>,ddk::mpl::type_pack<OOpsTags...>>::type nested_type;

public:
    typedef typename merge_algebraic_operators<local_type,nested_type>::type type;
};

template<callable_type Function>
struct holds_operations_impl<Function>
{
private:
    typedef typename ddk::mpl::aqcuire_callable_return_type<Function>::type return_t;
    typedef typename ddk::mpl::aqcuire_callable_arg_type<Function,0>::type args_t;
    template<typename ... OperationTags>
    static typename operator_tags_acc<OperationTags...>::type construct(const operator_tags<OperationTags...>&);

    typedef decltype(construct(std::declval<algebraic_structure_operators_tags<return_t>>())) target_ops;
    typedef decltype(construct(std::declval<algebraic_structure_operators_tags<args_t>>())) source_ops;

public:
    typedef typename merge_algebraic_operators<typename function_operators_mapper<Function,target_ops>::type,typename function_operators_mapper<Function,target_ops,source_ops>::type>::type type;
    static const bool value = (std::is_same<type,empty_operators>::value == false);
};

}

template<callable_type Function>
using function_operators = typename concepts::holds_operations_impl<Function>::type;

template<callable_type Function>
using function_operator_tags = typename function_operators<Function>::_operator_tags;

template<callable_type Function>
constexpr inline size_t num_function_operators = function_operators<Function>::s_numOps;

}