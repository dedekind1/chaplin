#pragma once

#include "cpn_builtin_expression_concepts.h"
#include "ddkFramework/ddk_system_allocator.h"

namespace cpn
{
namespace concepts
{

template<typename T, typename Im, typename Dom>
struct is_function_instantiable_by_resolver
{
private:
template<typename TT, typename = decltype(instance_function<Im,Dom,ddk::system_allocator>(std::declval<TT>()))>
static std::true_type resolve(const TT&);
static std::false_type resolve(...);

public:
	static const bool value = decltype(resolve(std::declval<T>()))::value;
};

template<typename T>
struct is_builtin_function_resolver
{
	template<typename TT, typename = decltype(__builtin_function_id(std::declval<TT>()))>
	static std::true_type resolve(const TT&);
	static std::false_type resolve(...);

public:
	static const bool value = decltype(resolve(std::declval<T>()))::value;
};

}

template<typename T, typename Im, typename Dom>
inline constexpr bool is_function_instantiable_by = concepts::is_function_instantiable_by_resolver<T,Im,Dom>::value;

template<typename T, typename Im, typename Dom>
inline constexpr bool is_linear_function_instantiable_by = is_linear_expression<T> && is_function_instantiable_by<T,Im,Dom>;

template<typename T>
inline constexpr bool is_builtin_function = concepts::is_builtin_function_resolver<T>::value;

template<typename T>
concept builtin_function_type = is_builtin_function<T>;

}
