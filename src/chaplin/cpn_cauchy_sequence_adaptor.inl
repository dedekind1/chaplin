
namespace ddk
{

template<typename T>
iterable_adaptor<const cpn::cauchy_sequence<T>>::iterable_adaptor(const cpn::cauchy_sequence<T>& i_sequence)
: m_sequence(i_sequence)
{
}
template<typename T>
template<typename Adaptor, typename Sink>
auto iterable_adaptor<const cpn::cauchy_sequence<T>>::perform_action(Adaptor&& i_adaptor, const sink_action_tag<Sink>& i_actionTag)
{
	typedef iterable_action_tag_result<detail::adaptor_traits<Adaptor>,sink_action_tag<Sink>> result_t;

    return make_result<result_t>(i_sink(i_adaptor.m_sequence(i_adaptor.m_currIndex)));
}
template<typename T>
template<typename Adaptor>
auto iterable_adaptor<const cpn::cauchy_sequence<T>>::perform_action(Adaptor&& i_adaptor, const begin_action_tag&)
{
	typedef iterable_action_tag_result<detail::adaptor_traits<Adaptor>,begin_action_tag> result_t;

    i_adaptor.m_currIndex = 0;

    return make_result<result_t>(i_adaptor.m_sequence.point(i_adaptor.m_currIndex));
}
template<typename T>
template<typename Adaptor>
auto iterable_adaptor<const cpn::cauchy_sequence<T>>::perform_action(Adaptor&& i_adaptor, const forward_action_tag&)
{
	typedef iterable_action_tag_result<detail::adaptor_traits<Adaptor>,forward_action_tag> result_t;

    return make_result<result_t>(i_adaptor.m_sequence.point(++i_adaptor.m_currIndex));
}

}