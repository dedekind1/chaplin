
namespace cpn
{

TEMPLATE(typename ... Args)
REQUIRED(ddk::is_constructible<rational_symbolic_literal,Args...>)
constexpr rational::rational(Args&& ... i_args)
: m_number(std::forward<Args>(i_args)...)
{
}
constexpr rational_symbolic_literal rational::number() const
{
    return m_number;
}

}