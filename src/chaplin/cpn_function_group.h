#pragma once

#include "cpn_algebraic_concepts.h"
#include "cpn_function_set.h"
#include "cpn_builtin_functions.h"

namespace cpn
{

template<callable_type Function>
struct operator_functor<Function,operator_tags<semi_group_operation>>
{
	PUBLISH_OPERATION_PROPERTIES(operator_functor,semi_group_operation,binary,commutative,associative,distributive);

	template<typename ... Operations>
	friend inline algebraic_structure<Function,Operations...> operator+(const algebraic_structure<Function,Operations...>& i_lhs, const algebraic_structure<Function,Operations...>& i_rhs)
	{
		return static_cast<const Function&>(i_lhs) + static_cast<const Function&>(i_rhs);
	}
};

template<callable_type Function>
struct operator_functor<Function,operator_tags<group_operation>>
{
	PUBLISH_OPERATION_PROPERTIES(operator_functor,group_operation,unary);

	template<typename ... Operations>
	friend inline algebraic_structure<Function,Operations...> operator-(const algebraic_structure<Function,Operations...>& i_rhs)
	{
		return -static_cast<const Function&>(i_rhs);
	}
	template<typename ... Operations>
	friend inline algebraic_structure<Function,Operations...> operator-(const algebraic_structure<Function,Operations...>& i_lhs, const algebraic_structure<Function,Operations...>& i_rhs)
	{
		return static_cast<const Function&>(i_lhs) + static_cast<const Function&>(-i_rhs);
	}
};

}