
namespace cpn
{
namespace detail
{

template<typename SuperClass, group_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_function<SuperClass,Im,Dom,Allocator>,semi_group_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    if(i_depth == 0)
    {
        return i_rhs.binary_operator(get_operator_tag_id<function_operators<function<Im(const Dom&),Allocator>>,semi_group_operation>(),i_lhs,i_depth+1);
    }
    else
    {
        return builtin_add_nary_function<Im(const Dom&),Allocator>(i_lhs.clone(),i_rhs.clone());
    }
}

template<typename SuperClass, group_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_function<SuperClass,Im,Dom,Allocator>,group_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    return builtin_minus_function<Im(const Dom&),Allocator>(i_rhs.clone());
}

template<typename SuperClass, ring_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_function<SuperClass,Im,Dom,Allocator>,ring_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    if(i_depth == 0)
    {
        return i_rhs.binary_operator(get_operator_tag_id<function_operators<function<Im(const Dom&),Allocator>>,ring_operation>(),i_lhs,i_depth+1);
    }
    else
    {
        return builtin_mult_nary_function<Im(const Dom&),Allocator>(i_lhs.clone(),i_rhs.clone());
    }
}

template<typename SuperClass, field_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_function<SuperClass,Im,Dom,Allocator>,field_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    if(i_depth == 0)
    {
        return i_rhs.binary_operator(get_operator_tag_id<function_operators<function<Im(const Dom&),Allocator>>,field_operation>(),i_lhs,i_depth+1);
    }
    else
    {
        return builtin_mult_nary_function<Im(const Dom&),Allocator>(i_lhs.clone(),builtin_inverted_function<Im(const Dom&),Allocator>(i_rhs.clone()));
    }
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_null_function<Im(const Dom&),Allocator>,semi_group_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    return i_rhs.clone();
}

template<group_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_null_function<Im(const Dom&),Allocator>,group_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    return builtin_minus_function<Im(const Dom&),Allocator>(i_rhs.clone());
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_null_function<Im(const Dom&),Allocator>,ring_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    return null_function<Im,Dom,Allocator>;
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_null_function<Im(const Dom&),Allocator>,field_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    return null_function<Im,Dom,Allocator>;
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_unity_function<Im(const Dom&),Allocator>,ring_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    return i_rhs.clone();
}

template<field_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_unity_function<Im(const Dom&),Allocator>,field_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    return builtin_inverted_function<Im(const Dom&),Allocator>(i_rhs.clone());
}

template<group_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_number_function<Im(const Dom&),Allocator>,semi_group_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    typedef builtin_number_function<Im(const Dom&),Allocator> builtin_type;

    if(i_lhs.id() == i_rhs.id())
    {
        const builtin_type& lhsNum = static_cast<const builtin_type&>(i_lhs);
        const builtin_type& rhsNum = static_cast<const builtin_type&>(i_rhs);

        return builtin_type{ lhsNum.number() + rhsNum.number() };
    }
    else
    {
        return builtin_operator<builtin_function<builtin_type,Im,Dom,Allocator>,semi_group_operation>::binary_operator(i_context,i_lhs,i_rhs,i_depth);
    }
}

template<group_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_number_function<Im(const Dom&),Allocator>,group_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    typedef builtin_number_function<Im(const Dom&),Allocator> builtin_type;

    const builtin_type& rhsNum = static_cast<const builtin_type&>(i_rhs);

    return builtin_type{ -rhsNum.number() };
}

template<ring_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_number_function<Im(const Dom&),Allocator>,ring_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    typedef builtin_number_function<Im(const Dom&),Allocator> builtin_type;

    if(i_lhs.id() == i_rhs.id())
    {
        const builtin_type& lhsNum = static_cast<const builtin_type&>(i_lhs);
        const builtin_type& rhsNum = static_cast<const builtin_type&>(i_rhs);

        return builtin_type{ lhsNum.number() * rhsNum.number() };
    }
    else
    {
        return builtin_operator<builtin_function<builtin_type,Im,Dom,Allocator>,ring_operation>::binary_operator(i_context,i_lhs,i_rhs,i_depth);
    }
}

template<field_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_number_function<Im(const Dom&),Allocator>,field_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    typedef builtin_number_function<Im(const Dom&),Allocator> builtin_type;

    if(i_lhs.id() == i_rhs.id())
    {
        const builtin_type& lhsNum = static_cast<const builtin_type&>(i_lhs);
        const builtin_type& rhsNum = static_cast<const builtin_type&>(i_rhs);

        return builtin_type{ lhsNum.number() / rhsNum.number() };
    }
    else
    {
        return builtin_operator<builtin_function<builtin_type,Im,Dom,Allocator>,field_operation>::binary_operator(i_context,i_lhs,i_rhs,i_depth);
    }
}

template<field_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> builtin_operator<builtin_add_nary_function<Im(const Dom&),Allocator>,semi_group_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    typedef builtin_add_nary_function<Im(const Dom&),Allocator> builtin_type;

    builtin_type res(static_cast<const builtin_type&>(i_lhs));

    if(i_lhs.id() == i_rhs.id())
    {
        const builtin_type& superRhs = static_cast<const builtin_type&>(i_rhs);

        for(const auto& func : superRhs)
        {
            res.push(func);
        }
    }
    else
    {
        res.push(i_rhs.clone());
    }

    return res;
}

template<field_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> builtin_operator<builtin_mult_nary_function<Im(const Dom&),Allocator>,ring_operation>::binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth)
{
    typedef builtin_mult_nary_function<Im(const Dom&),Allocator> builtin_type;

    builtin_type res(static_cast<const builtin_type&>(i_lhs));

    if(i_lhs.id() == i_rhs.id())
    {
        const builtin_type& superRhs = static_cast<const builtin_type&>(i_rhs);

        for(const auto& func : superRhs)
        {
            res.push(func);
        }
    }
    else
    {
        res.push(i_rhs.clone());
    }

    return res;
}

}
}