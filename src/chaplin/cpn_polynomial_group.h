#pragma once

#include "cpn_polynomial_set.h"

namespace cpn
{

template<ring_model T, typename Order, typename Allocator>
struct operator_functor<polynomial<T,Order,Allocator>,operator_tags<semi_group_operation>>
{
    PUBLISH_OPERATION_PROPERTIES(operator_functor,semi_group_operation,commutative,associative,distributive);

	static constexpr auto identity = algebraic_operator<T,semi_group_operation>::identity;

	template<typename ... Operations>
	friend inline auto operator+(const algebraic_structure<polynomial<T,Order,Allocator>,Operations...>& i_lhs, const algebraic_structure<polynomial<T,Order,Allocator>,Operations...>& i_rhs)
	{
        polynomial<T,Order,Allocator> res(i_lhs);

        [&res](auto&& i_monomial)
        {
            res.push(i_monomial);
        } <<= i_rhs;

		return algebraic_structure<polynomial<T,Order,Allocator>,Operations...>{ res };
	}
};

template<ring_model T, typename Order, typename Allocator>
struct operator_functor<polynomial<T,Order,Allocator>,operator_tags<group_operation>>
{
    PUBLISH_OPERATION_PROPERTIES(operator_functor,group_operation);

	template<typename ... Operations>
	friend inline auto operator-(const algebraic_structure<polynomial<T,Order,Allocator>,Operations...>& i_rhs)
	{
        polynomial<T,Order,Allocator> res;

        [&](auto&& i_monomial)
        {
            res.push(-i_monomial);
        } <<= i_rhs;

		return algebraic_structure<polynomial<T,Order,Allocator>,Operations...>{ res };
	}
	template<typename ... Operations>
	friend inline auto operator-(const algebraic_structure<polynomial<T,Order,Allocator>,Operations...>& i_lhs, const algebraic_structure<polynomial<T,Order,Allocator>,Operations...>& i_rhs)
	{
        polynomial<T,Order,Allocator> res(i_lhs);

        [&](auto&& i_monomial)
        {
            res.push(-i_monomial);
        } <<= i_rhs;

		return algebraic_structure<polynomial<T,Order,Allocator>,Operations...>{ res };
	}
};

}