#pragma once

#include "cpn_function.h"
#include "cpn_algebraic_structure.h"

namespace cpn
{

template<callable_type Function>
struct operator_functor<Function,operator_tags<set_operation>>
{
	PUBLISH_OPERATION_PROPERTIES(operator_functor,set_operation,unary);

	template<typename ... Operations>
	friend inline bool operator==(const algebraic_structure<Function,Operations...>& i_lhs, const algebraic_structure<Function,Operations...>& i_rhs)
	{
		return static_cast<const Function&>(i_lhs) == static_cast<const Function&>(i_rhs);
	}
	template<typename ... Operations>
	friend inline bool operator!=(const algebraic_structure<Function,Operations...>& i_lhs, const algebraic_structure<Function,Operations...>& i_rhs)
	{
		return static_cast<const Function&>(i_lhs) != static_cast<const Function&>(i_rhs);
	}
};

}