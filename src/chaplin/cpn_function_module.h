#pragma once

#include "cpn_function_group.h"

namespace cpn
{

template<callable_type Function>
struct operator_functor<Function,operator_tags<mod_operation>>
{
	PUBLISH_OPERATION_PROPERTIES(operator_functor,mod_operation,binary,commutative,associative,distributive);

	typedef typename ddk::mpl::aqcuire_callable_return_type<Function>::type return_type;

	template<typename ... Operations>
	friend inline algebraic_structure<Function,Operations...> operator^(const return_type& i_lhs, const algebraic_structure<Function,Operations...>& i_rhs)
	{
		return i_lhs ^ static_cast<const Function&>(i_rhs);
	}
};

}