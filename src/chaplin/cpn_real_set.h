#pragma once

#include "cpn_set.h"
#include "cpn_builtin_symbolic_literals.h"
#include "cpn_algebraic_structure.h"

namespace cpn
{

template<solvable_literal_type T>
class real
{
	friend inline auto resolve(const real& i_value)
	{
		return cpn::resolve(i_value.number());
	}

public:
	constexpr real(const T& i_number);
	TEMPLATE(typename TT)
	REQUIRES(ddk::is_constructible<T,TT>)
	constexpr real(const real_symbolic_literal<TT>& i_number);
	TEMPLATE(typename TT)
	REQUIRES(ddk::is_constructible<real_symbolic_literal<T>,real_symbolic_literal<TT>>)
	constexpr real(const real<TT>& i_number);

	constexpr real_symbolic_literal<T> number() const;

private:
	real_symbolic_literal<T> m_number;
};
template<typename T>
real(const T&) -> real<T>;

const auto real_null = real{ 0 };
const auto real_unit = real{ 1 };

struct real_set_operation
{
	PUBLISH_OPERATION_PROPERTIES(real_set_operation,set_operation,totally_ordered);

	typedef real<float> decay_t;

	template<solvable_literal_type T, solvable_literal_type TT>
	friend inline constexpr bool operator<(const real<T>& i_lhs,const real<TT>& i_rhs)
	{
		return i_lhs.number() < i_rhs.number();
	}
	template<solvable_literal_type T, solvable_literal_type TT, typename ... Operations>
	friend inline constexpr bool operator==(const real<T>& i_lhs, const real<TT>& i_rhs)
	{
		return i_lhs.number() == i_rhs.number();
	}
	template<solvable_literal_type T, solvable_literal_type TT, typename ... Operations>
	friend inline constexpr bool operator!=(const real<T>& i_lhs, const real<TT>& i_rhs)
	{
		return !(i_lhs.number() == i_rhs.number());
	}
};

using RealSet = algebraic_model<real_set_operation>;
template<solvable_literal_type T>
using real_set = algebraic_structure<real<T>,RealSet>;

template<size_t ... Dims>
using RealSet_n = times_model<RealSet,Dims...>;
template<solvable_literal_type ... T>
using real_set_n = prod_struct<real_set<T>...>;

}

#include "cpn_real_set.inl"