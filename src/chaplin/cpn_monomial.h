#pragma once

#include "cpn_algebraic_type_concepts.h"
#include <map>
#include <stddef.h>

namespace cpn
{

typedef std::map<size_t,size_t> monomial_exponents;

struct lexicographic_order
{
    bool operator()(const monomial_exponents& i_lhs, const monomial_exponents& i_rhs) const;
};

template<ring_model,typename,typename>
class polynomial;

template<ring_model T, typename Order, typename Allocator>
using monomial_bases = std::map<size_t,polynomial<T,Order,Allocator>>;

template<ring_type T, typename Allocator = ddk::system_allocator>
struct monomial
{
    typedef monomial_bases<typename T::model,lexicographic_order,Allocator> bases_t; 

public:
    monomial() = default;
    monomial(const T& i_coeff, const monomial_exponents& i_exponents = {});
    monomial(const T& i_coeff, const monomial_exponents& i_exponents, const bases_t& i_bases);

    TEMPLATE(ring_type TT)
    REQUIRES(ddk::is_assignable<T,TT>)
    void set_coeff(TT&& i_coeff) const;
    const T& coeff() const;
    const monomial_exponents& exponents() const;
    const bases_t& bases() const;
    TEMPLATE(ring_model TT, typename Order)
    REQUIRES(implements_model<T,TT>)
    monomial<T,Allocator> rebase(size_t i_component, const polynomial<TT,Order,Allocator>&) const;
    monomial<T,Allocator>& shift(size_t i_component, size_t i_exponent);
    monomial<T,Allocator> shift(size_t i_component, size_t i_exponent) const;
    size_t length() const;
    bool operator==(const monomial& other) const;
    bool operator!=(const monomial& other) const;

private:
    mutable T m_coeff;
    monomial_exponents m_exponents;
    bases_t m_bases;
};
template<typename T>
monomial(const T&, const monomial_exponents&) -> monomial<T>;
template<typename T>
monomial(const T&, const std::initializer_list<std::pair<const size_t,size_t>>&) -> monomial<T>;
template<typename T>
monomial(const monomial<T>&) -> monomial<T>;

}

#include "cpn_monomial.inl"