#pragma once

#include "cpn_type_concepts.h"

namespace cpn
{

template<typename ...>
struct forgetful_functor;

template<typename T, typename ... Operators>
struct forgetful_functor<T,Operators...>
{
private:
    template<typename TT, typename ... OOperators>
    static algebraic_structure<TT,typename algebraic_operators<OOperators...>::template drop<Operators...>> resolve(const algebraic_structure<TT,OOperators...>&);
    template<typename ... OOperators>
    static typename algebraic_operators<OOperators...>::template drop<Operators...> resolve(const algebraic_operators<OOperators...>&);

public:
    typedef decltype(resolve(std::declval<T>())) type;
};

template<typename T>
struct forget_type_impl
{
    template<typename TT, typename ... Operators>
    static typename algebraic_operators<Operators...>::operators resolve(const algebraic_structure<TT,Operators...>&);
    template<typename ... Operators>
    static typename algebraic_operators<Operators...>::operators resolve(const algebraic_operators<Operators...>&);

public:
    typedef decltype(resolve(std::declval<T>())) type;
};

template<typename Structure>
using forget_type = typename forget_type_impl<Structure>::type;
template<typename Structure, typename Op>
using forget_ops = typename forgetful_functor<Structure,Op>::type;
template<typename Structure>
using forget_add = typename forgetful_functor<Structure,typename Structure::semi_group_operation>::type;
template<typename Structure>
using forget_add_inverse = typename forgetful_functor<Structure,typename Structure::group_operation>::type;
template<typename Structure>
using forget_mult = typename forgetful_functor<Structure,typename Structure::ring_operation>::type;
template<typename Structure>
using forget_div = typename forgetful_functor<Structure,typename Structure::field_operation>::type;
template<typename Structure>
using forget_mod = typename forgetful_functor<Structure,typename Structure::mod_operation>::type;
template<typename Structure>
using forget_basis = typename forgetful_functor<Structure,typename Structure::basis_operation>::type;
template<typename Structure>
using forget_vector_prod = typename forgetful_functor<Structure,typename Structure::vector_prod_operation>::type;

template<typename,typename...>
struct operator_functor;

}
