#pragma once

#include "cpn_rational_module.h"
#include "cpn_rational_ring.h"
#include "cpn_algebra.h"

namespace cpn
{

struct rational_algebra_operation : rational_multiplication
{
    PUBLISH_OPERATION_PROPERTIES(rational_algebra_operation,algebra_operation,commutative,associative,distributive);

	template<typename ... Operations>
	friend inline auto operator&(const algebraic_structure<rational,Operations...>& i_lhs,const algebraic_structure<rational,Operations...>& i_rhs)
	{
		return algebraic_structure<rational,Operations...>{ i_lhs.number() * i_rhs.number() };
	}
};

template<ring_model R = RationalRing>
using RationalAlgebra = Algebra<RationalModule<R>,rational_algebra_operation>;
using RRationalAlgebra = RationalAlgebra<RationalRing>;
template<ring_model R = RationalRing>
using rational_algebra = algebra<rational_module<R>,rational_algebra_operation>;

}