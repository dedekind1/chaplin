#pragma once

#include "cpn_builtin_expressions.h"
#include "cpn_builtin_symbolic_literals.h"
#include "cpn_expression_concepts.h"
#include "ddkFramework/ddk_concepts.h"
#include "ddkFramework/ddk_type_concepts.h"

namespace cpn
{

constexpr builtin_literal_expression<integer_symbolic_literal> number(int i_num);
constexpr builtin_literal_expression<rational_symbolic_literal> fraction(int i_num, unsigned int i_den);
constexpr builtin_literal_expression<root_symbolic_literal> root(int i_num, int i_deg, bool i_pos = true);
constexpr builtin_literal_expression<log_symbolic_literal> log(unsigned int i_num, unsigned int i_base, bool i_pos = true);

template<literal_expression Expression>
constexpr auto forward_expression(Expression&& i_exp);

template<literal_type Expression>
constexpr auto forward_expression(Expression&& i_exp);

template<literal_type T>
constexpr  auto operator-(const builtin_literal_expression<T>& i_lhs);
template<literal_expression T>
constexpr auto operator-(const T& i_exp);

template<literal_type T, literal_type TT>
constexpr auto operator+(const builtin_literal_expression<T>& i_lhs,const builtin_literal_expression<TT>& i_rhs);
template<literal_expression T, size_t ... Indexs, literal_expression ... Expressions>
constexpr auto operator+(const T& i_lhs,const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_rhs);
template<size_t ... Indexs, literal_expression ... Expressions, literal_expression T>
constexpr auto operator+(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const T& i_rhs);
template<size_t ... Indexs, literal_expression ... Expressions, size_t ... IIndexs, literal_expression ... EExpressions>
constexpr auto operator+(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const add_nary_expression<ddk::mpl::sequence<IIndexs...>,EExpressions...>& i_rhs);
template<literal_expression T, literal_expression TT>
constexpr auto operator+(const T& i_lhs, const TT& i_rhs);

template<literal_type T, literal_type TT>
constexpr auto operator-(const builtin_literal_expression<T>& i_lhs,const builtin_literal_expression<TT>& i_rhs);
template<literal_expression T, size_t ... Indexs, literal_expression ... Expressions>
constexpr auto operator-(const T& i_lhs,const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_rhs);
template<size_t ... Indexs, literal_expression ... Expressions, literal_expression T>
constexpr auto operator-(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const T& i_rhs);
template<size_t ... Indexs, literal_expression ... Expressions, size_t ... IIndexs, literal_expression ... EExpressions>
constexpr auto operator-(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const add_nary_expression<ddk::mpl::sequence<IIndexs...>,EExpressions...>& i_rhs);
template<literal_expression T, literal_expression TT>
constexpr auto operator-(const T& i_lhs, const TT& i_rhs);

template<literal_type T, literal_type TT>
constexpr auto operator*(const builtin_literal_expression<T>& i_lhs,const builtin_literal_expression<TT>& i_rhs);
template<literal_expression T, size_t ... Indexs, literal_expression ... Expressions>
constexpr auto operator*(const T& i_lhs,const prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_rhs);
template<size_t ... Indexs, literal_expression ... Expressions, literal_expression T>
constexpr auto operator*(const prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const T& i_rhs);
template<size_t ... Indexs, literal_expression ... Expressions, size_t ... IIndexs, literal_expression ... EExpressions>
constexpr auto operator*(const prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const prod_nary_expression<ddk::mpl::sequence<IIndexs...>,EExpressions...>& i_rhs);
template<literal_expression T, literal_expression TT>
constexpr auto operator*(const T& i_lhs, const TT& i_rhs);

template<literal_type T, literal_type TT>
constexpr auto operator/(const builtin_literal_expression<T>& i_lhs,const builtin_literal_expression<TT>& i_rhs);
template<literal_expression T, literal_expression TT>
constexpr auto operator/(const T& i_lhs, const TT& i_rhs);

template<literal_type T>
constexpr auto operator^(const builtin_incognita_expression& i_lhs, const T& i_rhs);
template<size_t Comp, literal_type T>
constexpr auto operator^(const builtin_component_expression<Comp>& i_lhs, const T& i_rhs);
template<literal_expression LhsExpression, literal_expression RhsExpression, literal_type T>
constexpr auto operator^(const builtin_composed_expression<LhsExpression,RhsExpression>& i_lhs, const T& i_rhs);
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, literal_type T>
constexpr auto operator^(const add_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const T& i_rhs);
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, literal_type T>
constexpr auto operator^(const prod_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const T& i_rhs);
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, size_t IIndex, size_t ... IIndexs, literal_type T, literal_expression ... EExpressions>
constexpr auto operator^(const add_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const add_nary_expression<ddk::mpl::sequence<IIndex,IIndexs...>,builtin_literal_expression<T>,EExpressions...>& i_rhs);
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, size_t IIndex, size_t ... IIndexs, literal_type T, literal_expression ... EExpressions>
constexpr auto operator^(const add_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const prod_nary_expression<ddk::mpl::sequence<IIndex,IIndexs...>,builtin_literal_expression<T>,EExpressions...>& i_rhs);
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, size_t IIndex, size_t ... IIndexs, literal_type T, literal_expression ... EExpressions>
constexpr auto operator^(const prod_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const prod_nary_expression<ddk::mpl::sequence<IIndex,IIndexs...>,builtin_literal_expression<T>,EExpressions...>& i_rhs);
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, size_t IIndex, size_t ... IIndexs, literal_type T, literal_expression ... EExpressions>
constexpr auto operator^(const prod_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const add_nary_expression<ddk::mpl::sequence<IIndex,IIndexs...>,builtin_literal_expression<T>,EExpressions...>& i_rhs);
template<literal_expression T, size_t Index, size_t ... Indexs, literal_expression ... Expressions>
constexpr auto operator^(const T& i_lhs, const add_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_rhs);
template<literal_expression T, size_t Index, size_t ... Indexs, literal_expression ... Expressions>
constexpr auto operator^(const T& i_lhs, const prod_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_rhs);

}

#include "cpn_builtin_expression_ops.inl"