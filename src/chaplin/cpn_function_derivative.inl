
#include "cpn_cauchy_sequence.h"
#include "cpn_algebraic_type_concepts.h"
#include "cpn_module.h"
#include "ddkFramework/ddk_iterable.h"

namespace cpn
{
namespace detail
{

template<group_model Im, metric_space_model Dom, typename Allocator,typename Function>
TEMPLATE(typename FFunction)
REQUIRED(ddk::is_constructible<Function,FFunction>)
builtin_derivative_function<Im(const Dom&),Allocator,Function>::builtin_derivative_function(FFunction&& i_callable, const derivative_context<Dom>& i_context, size_t i_order)
: m_callable(i_callable)
, m_context(i_context)
, m_order(i_order)
{
    DDK_ASSERT(m_order>=1, "You cannot have derivatives of order lower than 1");
}
template<group_model Im, metric_space_model Dom, typename Allocator, typename Function>
const Function& builtin_derivative_function<Im(const Dom&),Allocator,Function>::callable() const
{
    return m_callable;
}
template<group_model Im, metric_space_model Dom, typename Allocator, typename Function>
size_t builtin_derivative_function<Im(const Dom&),Allocator,Function>::order() const
{
    return m_order;
}
template<group_model Im, metric_space_model Dom, typename Allocator, typename Function>
final_object<Im> builtin_derivative_function<Im(const Dom&),Allocator,Function>::operator()(final_object<const Dom&> i_args) const
{
    return (*this)(i_args,m_order);
}
template<group_model Im, metric_space_model Dom, typename Allocator, typename Function>
final_object<Im> builtin_derivative_function<Im(const Dom&),Allocator,Function>::operator()(final_object<const Dom&> i_args, size_t i_order) const
{
    typedef final_object<Im> im_t;
    typedef final_object<Dom> dom_t;
    typedef typename algebraic_operator<Dom,metric_operation>::valuation_type valuation_type;

    static const size_t k_numSteps = 10;
    static const valuation_type k_eps = (2.f / k_numSteps);

    if(const auto resDerivative = [&,thisDer=this](const dom_t& i_currentPoint) -> im_t
    {
        const auto diff = distance(i_args,i_currentPoint);

        if(diff < k_eps)
        {
            const im_t targetIm((i_order == 1) ? ddk::eval(m_callable,i_args) : (*thisDer)(i_args,i_order-1));
            const im_t currIm((i_order == 1) ? ddk::eval(m_callable,i_currentPoint) : (*thisDer)(i_currentPoint,i_order-1));

            ddk::iter::yield<im_t>(inv(diff) ^ as_module<im_t,valuation_type>{ currIm - targetIm });
        }
    } <<= exponential_cauchy_sequencer(i_args,k_numSteps))
    {
        return *resDerivative;
    }
    else
    {
        throw;
    }
}

}


TEMPLATE(group_model Im, metric_space_model Dom, typename Allocator, typename Function)
REQUIRED(module_modellable<Im,typename algebraic_operator<Dom,metric_operation>::valuation_type>)
function<Im(const Dom&),Allocator> derivate(const derivative_context<Dom>& i_context, Function&& i_function)
{
    return detail::builtin_derivative_function<Im(const Dom&),Allocator,Function>(i_function,i_context,1);
}

TEMPLATE(group_model Im, metric_space_model Dom, typename Allocator, typename Function)
REQUIRED(module_modellable<Im,typename algebraic_operator<Dom,metric_operation>::valuation_type>)
function<Im(const Dom&),Allocator> derivate(const derivative_context<Dom>& i_context, const detail::builtin_derivative_function<Im(const Dom&),Allocator,Function>& i_function)
{
    return detail::builtin_derivative_function<Im(const Dom&),Allocator,Function>(i_function.callable(),i_context,i_function.order()+1);
}

}