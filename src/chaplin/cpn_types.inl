
namespace cpn
{
namespace detail
{

template<size_t Index, typename T>
constexpr auto _pow_forward(const T& i_value)
{
    return i_value;
}

template<typename T, size_t ... Dims, size_t ... Indexs, typename TT>
constexpr auto pow_forward(const ddk::mpl::sequence<Indexs...>& , const TT& i_value)
{
    return pow_type<T,Dims...>{ _pow_forward<Indexs>(i_value)... };
}

}

template<size_t ... Dims, typename T>
constexpr pow_type<T,Dims...> pow_forward(const T& i_value)
{
    typedef typename ddk::mpl::make_sequence<0,ddk::mpl::prod_ranks<Dims...>>::type seq_t;

    return detail::pow_forward<T,Dims...>(seq_t{},i_value);
}

template<type ... T>
constexpr auto times(const T& ... i_values)
{
    return pow_type<typename std::common_type<T...>::type,ddk::mpl::num_types<T...>>{ i_values ...};
}

template<type ... T>
constexpr auto prod(const T& ... i_values)
{
    return prod_type<T...>{i_values ...};
}

}