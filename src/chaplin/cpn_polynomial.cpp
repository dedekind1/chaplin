#include "cpn_polynomial.h"

namespace cpn
{

bool lexicographic_order::operator()(const monomial_exponents& i_lhs, const monomial_exponents& i_rhs) const
{
    if(auto res = [] (const std::pair<const size_t,size_t>& ii_lhs, const std::pair<const size_t,size_t>& ii_rhs) -> bool
    {
        if(ii_lhs.first == ii_rhs.first)
        {
            if(ii_lhs.second != ii_rhs.second)
            {
                ddk::iter::yield(ii_lhs.second < ii_rhs.second);
            }
        }
        else
        {
            ddk::iter::yield(ii_lhs.first < ii_rhs.first);
        }
    } <<= ddk::fusion(i_lhs,i_rhs))
    {
        return *res;
    }
    else
    {
        return i_lhs.size() < i_rhs.size();
    }
}

}