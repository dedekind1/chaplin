
namespace cpn
{

template<typename T>
template<solvable_literal_type TT, solvable_literal_type TTT>
constexpr complex<T>::complex(const TT& i_real, const TTT& i_im)
: m_real(i_real)
, m_im(i_im)
{
}
template<typename T>
constexpr real<typename complex<T>::real_t> complex<T>::re() const
{
    return m_real;
}
template<typename T>
constexpr real<typename complex<T>::im_t> complex<T>::im() const
{
    return m_im;
}

}