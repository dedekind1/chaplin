#include "cpn_builtin_symbolic_literal_ops.h"

namespace cpn
{

double resolve(const cpn::rational_symbolic_literal& i_number)
{
	mpfr_t res, num, den;

	mpfr_init(res);

	mpfr_init(num);
	mpfr_init(den);


	mpfr_set_si(num,i_number.numerator(),MPFR_RNDD);
	mpfr_set_ui(num,i_number.numerator(),MPFR_RNDD);

	mpfr_div(res,num,den,MPFR_RNDD);

	return mpfr_get_d(res,MPFR_RNDD);
}
double resolve(const cpn::root_symbolic_literal& i_number)
{
	mpfr_t res, base, deg;

	mpfr_init(res);
	mpfr_init(base);
	mpfr_init(deg);

	mpfr_set_ui(base,i_number.number(),MPFR_RNDD);

	{
		mpfr_t den;
		
		mpfr_set_ui(den,i_number.degree(),MPFR_RNDD);
		mpfr_ui_div(deg,1,den,MPFR_RNDD);
	}

	mpfr_pow(res,base,deg,MPFR_RNDD);

	return mpfr_get_d(res,MPFR_RNDD);
}
double resolve(const cpn::log_symbolic_literal& i_number)
{
	mpfr_t res, logNum, logDen, den;

	{
		mpfr_t base;
		
		mpfr_set_ui(base,i_number.number(),MPFR_RNDD);

		mpfr_log(logNum,base,MPFR_RNDD);
	}

	{
		mpfr_t base;
		
		mpfr_set_ui(base,i_number.base(),MPFR_RNDD);

		mpfr_log(logDen,base,MPFR_RNDD);
	}

	mpfr_div(res,logNum,logDen,MPFR_RNDD);

	return mpfr_get_d(res,MPFR_RNDD);
}

}