#pragma once

#include "cpn_module.h"

namespace cpn
{

template<module_model T, size_t Dim>
struct pow_basis_operation
{
    PUBLISH_OPERATION_PROPERTIES(pow_basis_operation,basis_operation);

    static constexpr auto basis(size_t i_index);
};

template<module_model T, size_t Dim>
using PowFreeModule = Module<pow_model<as_module<typename T::r_model>,Dim>,pow_basis_operation<T,Dim>>;

TEMPLATE(module_model T,ring_type R, size_t Dim)
REQUIRES(implements_model<R,typename algebraic_operator<T,mod_operation>::r_model>)
using pow_free_module = algebraic_structure<pow_type<R,Dim>,PowFreeModule<T,Dim>>;

template<module_model ... T>
struct prod_basis_operation
{
    PUBLISH_OPERATION_PROPERTIES(prod_basis_operation,basis_operation);

    static constexpr auto basis(size_t i_index);
};

template<module_model T, size_t Dim>
struct times_basis_operation
{
private:
    template<size_t ... Indexs>
    static prod_basis_operation<ddk::mpl::index_to_type<Indexs,T>...> resolve(const ddk::mpl::sequence<Indexs...>&);

public:
    typedef decltype(resolve(std::declval<typename ddk::mpl::make_sequence<0,Dim>::type>())) type;
};

template<module_model T, size_t Dim>
using TimesFreeModule = Module<times_model<as_module<typename T::r_model>,Dim>,typename times_basis_operation<T,Dim>::type>;

template<module_model ... T>
using ProdFreeModule = Module<prod_model<as_module<typename T::r_model>...>,prod_basis_operation<T...>>;

TEMPLATE(module_model T,ring_type ... R)
REQUIRES(implements_model<R,typename algebraic_operator<T,mod_operation>::r_model>...)
using prod_free_module = algebraic_structure<prod_type<R...>,ProdFreeModule<ddk::mpl::type_to_type<R,T>...>>;

}

#include "cpn_free_module.inl"