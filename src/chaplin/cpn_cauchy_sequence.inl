
namespace cpn
{
namespace detail
{

template<complete_space_type T, size_t Dim>
linear_cauchy_sequencer_impl<T,Dim>::linear_cauchy_sequencer_impl(const T& i_target, size_t i_numSteps)
: linear_cauchy_sequencer_impl(index_sequence{},i_target,i_numSteps,algebraic_operator<T,point_conv_operation>::k_epsilon)
{
}
template<complete_space_type T, size_t Dim>
linear_cauchy_sequencer_impl<T,Dim>::linear_cauchy_sequencer_impl(const ddk::mpl::sequence<0>&, const T& i_target, size_t i_numSteps, const valuation_type& i_epsilon)
: m_target(i_target)
, m_delta(resolve(inv(i_epsilon)) / i_numSteps)
{
}
template<complete_space_type T, size_t Dim>
template<size_t ... Indexs>
linear_cauchy_sequencer_impl<T,Dim>::linear_cauchy_sequencer_impl(const ddk::mpl::sequence<Indexs...>&, const T& i_target, size_t i_numSteps, const std::array<valuation_type,Dim>& i_epsilon)
: m_target(i_target)
, m_delta({ resolve(inv(i_epsilon[Indexs])) / i_numSteps ... })
{
}
template<complete_space_type T, size_t Dim>
auto linear_cauchy_sequencer_impl<T,Dim>::point(size_t i_index) const
{
    typedef typename ddk::mpl::make_sequence<0,Dim>::type seq_t;

    return point(seq_t{},i_index);
}
template<complete_space_type T, size_t Dim>
template<size_t ... Indexs>
auto linear_cauchy_sequencer_impl<T,Dim>::point(const ddk::mpl::sequence<Indexs...>&, size_t i_index) const
{
    return convergence(m_target,{ i_index * m_delta[Indexs] ... });
}

template<complete_space_type T, size_t Dim>
exponential_cauchy_sequencer_impl<T,Dim>::exponential_cauchy_sequencer_impl(const T& i_target, size_t i_numSteps)
: exponential_cauchy_sequencer_impl(index_sequence{},i_target,i_numSteps,algebraic_operator<T,point_conv_operation>::k_epsilon)
{
}
template<complete_space_type T, size_t Dim>
exponential_cauchy_sequencer_impl<T,Dim>::exponential_cauchy_sequencer_impl(const ddk::mpl::sequence<0>&, const T& i_target, size_t i_numSteps, const valuation_type& i_epsilon)
: m_target(i_target)
, m_delta(construct_delta<0>({ resolve(inv(i_epsilon)) },i_numSteps))
{
}
template<complete_space_type T, size_t Dim>
template<size_t ... Indexs>
exponential_cauchy_sequencer_impl<T,Dim>::exponential_cauchy_sequencer_impl(const ddk::mpl::sequence<Indexs...>&, const T& i_target, size_t i_numSteps, const std::array<valuation_type,Dim>& i_epsilon)
: m_target(i_target)
, m_delta(construct_delta<Indexs...>({ resolve(inv(i_epsilon[Indexs])) ... },i_numSteps))
{
}
template<complete_space_type T, size_t Dim>
auto exponential_cauchy_sequencer_impl<T,Dim>::point(size_t i_index) const
{
    typedef typename ddk::mpl::make_sequence<0,Dim>::type seq_t;

    return point(seq_t{},i_index);
}
template<complete_space_type T, size_t Dim>
template<size_t ... Indexs>
auto exponential_cauchy_sequencer_impl<T,Dim>::point(const ddk::mpl::sequence<Indexs...>&, size_t i_index) const
{
    return convergence(m_target,{ m_delta[Indexs][i_index] ... });
}
template<complete_space_type T, size_t Dim>
template<size_t ... Indexs>
std::array<std::vector<size_t>,Dim> exponential_cauchy_sequencer_impl<T,Dim>::construct_delta(const std::array<size_t,Dim>& i_lattice, size_t i_numSteps)
{
    static const float k_scale = 1.105;
    static const int k_accel = 10;
    const float stepLeap = 1.f / static_cast<float>(i_numSteps);
    std::array<std::vector<size_t>,Dim> res;

    ( res[Indexs].reserve(i_numSteps), ... );

    float stepAcc = stepLeap;
    for(size_t step=0;step<i_numSteps;step++,stepAcc+=stepLeap)
    {
        ( res[Indexs].push_back(i_lattice[Indexs] * k_scale * std::exp(-1.f / (k_accel * stepAcc))), ...);
    }

    return res;
}

}

template<typename T>
cauchy_sequence<T>::cauchy_sequence(const T& i_generator)
: m_sequencer(i_generator)
{
}
template<typename T>
auto cauchy_sequence<T>::point(size_t i_index) const
{
    return m_sequencer.point(i_index);
}

template<complete_space_type T>
auto linear_cauchy_sequencer(const T& i_target, size_t i_numSteps)
{
    return cauchy_sequence{ detail::linear_cauchy_sequencer_impl(i_target,i_numSteps) };
}
template<complete_space_type T>
auto exponential_cauchy_sequencer(const T& i_target, size_t i_numSteps)
{
    return cauchy_sequence{ detail::exponential_cauchy_sequencer_impl(i_target,i_numSteps) };
}


}