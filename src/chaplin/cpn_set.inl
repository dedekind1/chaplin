
namespace cpn
{

template<group_type T>
auto as_set(T&& i_obj)
{
    return algebraic_structure<typename T::value_type,typename T::template keep_ops<set_operation>>{ i_obj };
}

template<set_model ... T>
constexpr size_t prod_operation<proj_operation,T...>::rank()
{
    return ddk::mpl::num_types<T...>;
}
template<set_model ... T>
template<typename TT, size_t ... Indexs, typename ... TTT>
TT prod_operation<proj_operation,T...>::access(const ddk::mpl::sequence<Indexs...>&, const prod_type<TTT...>& i_object, size_t i_index)
{
    typedef TT(*funcType)(const prod_type<TTT...>&);

    const funcType funcTable[ddk::mpl::num_ranks<Indexs...>] = { &prod_operation<proj_operation,T...>::template access<TT,Indexs,TTT...> ... };

    return (*funcTable[i_index])(i_object);
}
template<set_model ... T>
template<typename TT, size_t Index, typename ... TTT>
TT prod_operation<proj_operation,T...>::access(const prod_type<TTT...>& i_object)
{
    return { i_object.template get<Index>() };
}

template<set_model T, size_t ... Dims>
constexpr size_t pow_operation<proj_operation,T,Dims...>::rank()
{
    return ddk::mpl::prod_ranks<Dims...>;
}

}