#pragma once

#include "cpn_real_group.h"
#include "cpn_ring.h"

namespace cpn
{

struct real_multiplication
{
    PUBLISH_OPERATION_PROPERTIES(real_multiplication,ring_operation,commutative,associative,distributive);

	static constexpr real_set identity = real(1);
	static constexpr real_set annihilator = real(0);

	template<solvable_literal_type T, solvable_literal_type TT, typename ... Operations>
	friend inline auto operator*(const algebraic_structure<real<T>,Operations...>& i_lhs, const algebraic_structure<real<TT>,Operations...>& i_rhs)
	{
		typedef decltype(std::declval<T>() * std::declval<TT>()) ret_type;

		return algebraic_structure<real<ret_type>,Operations...>{ i_lhs.number() * i_rhs.number() };
	}
};

using RealSemiRing = SemiRing<RealSemiGroup,real_multiplication>;
template<solvable_literal_type T>
using real_semi_ring = semi_ring<real_semi_group<T>,real_multiplication,real<T>>;

template<size_t ... Dims>
using RealSemiRing_n = times_model<RealSemiRing,Dims...>;

using RealSemiRing_1 = RealSemiRing_n<1>;
using RealSemiRing_2 = RealSemiRing_n<2>;
using RealSemiRing_3 = RealSemiRing_n<3>;
using RealSemiRing_4 = RealSemiRing_n<4>;

template<solvable_literal_type ... T>
using real_semi_ring_n = prod_struct<real_semi_ring<T>...>;

template<solvable_literal_type T>
using real_semi_ring_1 = real_semi_ring_n<T>;
template<solvable_literal_type T, solvable_literal_type TT>
using real_semi_ring_2 = real_semi_ring_n<T,TT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT>
using real_semi_ring_3 = real_semi_ring_n<TTT,TTT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, solvable_literal_type TTTT>
using real_semi_ring_4 = real_semi_ring_n<T,TT,TTT,TTTT>;

using RealRing = Ring<RealGroup,real_multiplication>;
template<solvable_literal_type T>
using real_ring = ring<real_group<T>,real_multiplication,real<T>>;

template<size_t ... Dims>
using RealRing_n = times_model<RealRing,Dims...>;

using RealRing_1 = RealRing_n<1>;
using RealRing_2 = RealRing_n<2>;
using RealRing_3 = RealRing_n<3>;
using RealRing_4 = RealRing_n<4>;

template<solvable_literal_type ... T>
using real_ring_n = prod_struct<real_ring<T>...>;

template<solvable_literal_type T>
using real_ring_1 = real_ring_n<T>;
template<solvable_literal_type T, solvable_literal_type TT>
using real_ring_2 = real_ring_n<T,TT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT>
using real_ring_3 = real_ring_n<T,TT,TTT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, solvable_literal_type TTTT>
using real_ring_4 = real_ring_n<T,TT,TTT,TTTT>;

}