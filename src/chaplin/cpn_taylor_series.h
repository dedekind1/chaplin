#pragma once

#include "cpn_functions.h"
#include "cpn_polynomials.h"

namespace cpn
{

TEMPLATE(ring_model Im, metric_space_model Dom, typename Allocator, metric_space_type DDom)
REQUIRES(implements_model<DDom,Dom>)
inline auto taylor_expansion(const function<Im(const Dom&),Allocator>& i_func, const DDom& i_point);

TEMPLATE(ring_model Im, vector_space_model Dom, typename Allocator, vector_space_type DDom)
REQUIRES(implements_model<DDom,Dom>)
inline auto taylor_expansion(const function<Im(const Dom&),Allocator>& i_func, const DDom& i_point);

}

#include "cpn_taylor_series.inl"