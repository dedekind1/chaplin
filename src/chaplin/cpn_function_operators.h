#pragma once

#include "cpn_algebraic_operators.h"
#include "cpn_algebraic_defs.h"
#include "cpn_type_concepts.h"

namespace cpn
{

template<typename>
struct derivative_context;

template<set_model T>
struct derivative_context<T> : public operator_context
{
public:
    derivative_context(const operator_tag_id& i_id);

    template<set_model Im, typename Allocator>
    function<Im(const T&),Allocator> derivative(const function<Im(const T&),Allocator>& i_func) const;
    bool test(size_t i_comp) const;
};

template<vector_space_model T>
struct derivative_context<T> : public operator_context
{
	struct bool_conversor
	{
    public:
		template<module_type TT>
        bool_conversor(const TT& i_value);

        operator bool() const;

    private:
        bool m_value;
	};

public:
    TEMPLATE(vector_space_type TT)
    REQUIRES(implements_model<TT,T>)
    derivative_context(const operator_tag_id& i_id, const TT& i_direction);

    template<set_model Im, typename Allocator>
    constexpr function<Im(const T&),Allocator> derivative(const function<Im(const T&),Allocator>& i_func) const;
    bool test(size_t i_comp) const;

private:
    const final_object<T> m_direction;
};

}

#include "cpn_function_operators.inl"
#include "cpn_algebraic_function_concepts.h"