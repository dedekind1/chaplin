#pragma once

#include "cpn_polynomial_ring.h"
#include "cpn_polynomial_module.h"

namespace cpn
{

template<ring_model T, typename Order, typename Allocator>
struct operator_functor<polynomial<T,Order,Allocator>,operator_tags<mod_operation,ring_operation>>
{
    PUBLISH_OPERATION_PROPERTIES(operator_functor,algebra_operation,commutative,associative,distributive);

	template<typename ... Operations>
	friend inline auto operator&(const algebraic_structure<polynomial<T,Order,Allocator>,Operations...>& i_lhs, const algebraic_structure<polynomial<T,Order,Allocator>,Operations...>& i_rhs)
	{
        return i_lhs * i_rhs;
	}
};




}