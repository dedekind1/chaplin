#pragma once

#include "cpn_algebraic_structure.h"

namespace cpn
{

template<module_model Model, typename Operation>
using Algebra = typename Model::template embed<Operation>;

template<module_type Structure, typename Operation, typename Set = Structure::value_type>
using algebra = algebraic_structure<Set,Algebra<Structure,Operation>>;

template<algebra_model T, size_t ... Dims>
struct pow_operation<algebra_operation,T,Dims...>
{
    PUBLISH_OPERATION_PROPERTIES(pow_operation,algebra_operation,concepts::algebraic_structure_properties<T,algebra_operation>);

	TEMPLATE(algebra_type TT, typename ... Operations)
	REQUIRES(implements_operation<algebra_operation,T,TT>)
	friend inline auto operator&(const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_lhs, const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_rhs)
	{
		algebraic_structure<pow_type<TT,Dims...>,Operations...> res = i_lhs;

		[&,counter=0](auto&& i_value) mutable
		{
			auto& resValue = res.at(counter++);
			resValue = resValue & i_value;
		} <<= i_rhs;

		return res;
	}
};

template<algebra_model ... T>
struct prod_operation<algebra_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(prod_operation,algebra_operation,typename ddk::mpl::intersect_type_packs<concepts::algebraic_structure_properties<T,algebra_operation>...>::type);

	template<size_t ... Indexs, algebra_type ... TT, algebra_type ... TTT>
	static auto prod_tuples(const ddk::mpl::sequence<Indexs...>&, const prod_type<TT...>& i_lhs,const prod_type<TTT...>& i_rhs)
	{
		return 	prod_type{ i_lhs.template get<Indexs>() & i_rhs.template get<Indexs>() ... };
	}
	TEMPLATE(algebra_type ... TT, algebra_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<algebra_operation,prod_type<TT...>,T...>,implements_operation<algebra_operation,prod_type<TTT...>,T...>)
	friend inline auto operator&(const algebraic_structure<prod_type<TT...>,Operations...>& i_lhs,const algebraic_structure<prod_type<TTT...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,ddk::mpl::num_types<T...>>::type seq_t;
		typedef prod_type<decltype(std::declval<TT>() & std::declval<TTT>())...> prod_type_t;

		return algebraic_structure<prod_type_t,Operations...>{ prod_tuples(seq_t{},i_lhs,i_rhs) };
	}
};

template<algebra_model ... T>
struct sum_operation<algebra_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(sum_operation,algebra_operation,typename ddk::mpl::intersect_type_packs<concepts::algebraic_structure_properties<T,algebra_operation>...>::type);

	template<typename Return>
	struct mult_operation_visitor
	{
		template<algebra_type T1,algebra_type T2>
		Return operator()(T1&& i_lhs,T2&& i_rhs) const
		{
			return i_lhs & i_rhs;
		}
	};

	TEMPLATE(algebra_type ... TT, algebra_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<algebra_operation,sum_type<TT...>,T...>,implements_operation<algebra_operation,sum_type<TTT...>,T...>)
	friend inline auto operator*(const algebraic_structure<sum_type<TT...>,Operations...>& i_lhs,const algebraic_structure<sum_type<TTT...>,Operations...>& i_rhs)
	{
		typedef sum_type<decltype(std::declval<TT>() & std::declval<TTT>())...> sum_type_t;

		return algebraic_structure<sum_type_t,Operations...>{ ddk::visit<mult_operation_visitor<sum_type_t>>(i_lhs,i_rhs) };
	}
};

}