#pragma once

namespace cpn
{
namespace detail
{

template<typename SuperClass, group_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_function<SuperClass,Im,Dom,Allocator>,semi_group_operation>
{
    static function<Im(const Dom&),Allocator> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<typename SuperClass, group_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_function<SuperClass,Im,Dom,Allocator>,group_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

template<typename SuperClass, ring_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_function<SuperClass,Im,Dom,Allocator>,ring_operation>
{
    static function<Im(const Dom&),Allocator> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<typename SuperClass, field_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_function<SuperClass,Im,Dom,Allocator>,field_operation>
{
    static function<Im(const Dom&),Allocator> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_null_function<Im(const Dom&),Allocator>,semi_group_operation>
{
    static function<Im(const Dom&),Allocator> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<group_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_null_function<Im(const Dom&),Allocator>,group_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_null_function<Im(const Dom&),Allocator>,ring_operation>
{
    static function<Im(const Dom&),Allocator> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_null_function<Im(const Dom&),Allocator>,field_operation>
{
    static function<Im(const Dom&),Allocator> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_unity_function<Im(const Dom&),Allocator>,ring_operation>
{
    static function<Im(const Dom&),Allocator> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<field_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_unity_function<Im(const Dom&),Allocator>,field_operation>
{
    static function<Im(const Dom&),Allocator> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<group_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_number_function<Im(const Dom&),Allocator>,semi_group_operation>
{
    static function<Im(const Dom&),Allocator> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<group_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_number_function<Im(const Dom&),Allocator>,group_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

template<ring_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_number_function<Im(const Dom&),Allocator>,ring_operation>
{
    static function<Im(const Dom&),Allocator> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<field_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_number_function<Im(const Dom&),Allocator>,field_operation>
{
    static function<Im(const Dom&),Allocator> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<typename Im, typename Dom, typename Allocator>
struct builtin_operator<builtin_add_nary_function<Im(const Dom&),Allocator>,semi_group_operation>
{
    static function<Im(const Dom&)> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

template<typename Im, typename Dom, typename Allocator>
struct builtin_operator<builtin_mult_nary_function<Im(const Dom&),Allocator>,ring_operation>
{
    static function<Im(const Dom&)> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_lhs, const function_base<Im,const Dom&>& i_rhs, size_t i_depth);
};

}
}

#include "cpn_arithmetic_operators.inl"