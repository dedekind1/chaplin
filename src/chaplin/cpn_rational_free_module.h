#pragma once

#include "cpn_rational_ring.h"
#include "cpn_rational_module.h"
#include "cpn_free_module.h"

namespace cpn
{

template<ring_model R, size_t Dim>
using RationalFreeModule_n = PowFreeModule<RationalModule<R>,Dim>;

template<ring_model R = RationalRing>
using RationalFreeModule_1 = RationalFreeModule_n<R,1>;
template<ring_model R = RationalRing>
using RationalFreeModule_2 = RationalFreeModule_n<R,2>;
template<ring_model R = RationalRing>
using RationalFreeModule_3 = RationalFreeModule_n<R,3>;
template<ring_model R = RationalRing>
using RationalFreeModule_4 = RationalFreeModule_n<R,4>;

template<ring_model R, size_t Dim>
using rational_free_module_n = pow_free_module<rational_module<R>,reify_model<rational,R>,Dim>;

template<ring_model R = RationalRing>
using rational_free_module_1 = rational_free_module_n<R,1>;
template<ring_model R = RationalRing>
using rational_free_module_2 = rational_free_module_n<R,2>;
template<ring_model R = RationalRing>
using rational_free_module_3 = rational_free_module_n<R,3>;
template<ring_model R = RationalRing>
using rational_free_module_4 = rational_free_module_n<R,4>;

}