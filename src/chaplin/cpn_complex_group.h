#pragma once

#include "cpn_complex_set.h"
#include "cpn_group.h"

namespace cpn
{

struct complex_addition
{
    PUBLISH_OPERATION_PROPERTIES(complex_addition,semi_group_operation,commutative,associative,distributive);

	static constexpr complex_set identity = complex(0,0);

	template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, typename ... Operations>
	friend inline auto operator+(const algebraic_structure<complex<T>,Operations...>& i_lhs, const algebraic_structure<complex<TT>,Operations...>& i_rhs)
	{
		typedef ddk::mpl::type_pack<decltype(std::declval<typename T::nth_type<0>>() + std::declval<typename TT::nth_type<0>>()),decltype(std::declval<typename T::nth_type<1>>() + std::declval<typename TT::nth_type<1>>())> ret_type;

		return algebraic_structure<complex<ret_type>,Operations...>{ i_lhs.re().number() + i_rhs.re().number(),i_lhs.im().number() + i_rhs.im().number() };
	}
};

using ComplexSemiGroup = SemiGroup<ComplexSet,complex_addition>;
template<solvable_literal_pack<2> T>
using complex_semi_group = semi_group<complex_set<T>,complex_addition,complex<T>>;

template<size_t ... Dims>
using ComplexSemiGroup_n = times_model<ComplexSemiGroup,Dims...>;

using ComplexSemiGroup_1 = ComplexSemiGroup_n<1>;
using ComplexSemiGroup_2 = ComplexSemiGroup_n<2>;
using ComplexSemiGroup_3 = ComplexSemiGroup_n<3>;
using ComplexSemiGroup_4 = ComplexSemiGroup_n<4>;

template<solvable_literal_pack<2> ... T>
using complex_semi_group_n = prod_struct<complex_semi_group<T>...>;

template<solvable_literal_pack<2> T>
using complex_semi_group_1 = complex_semi_group_n<T>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT>
using complex_semi_group_2 = complex_semi_group_n<T,TT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT>
using complex_semi_group_3 = complex_semi_group_n<T,TT,TTT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT, solvable_literal_pack<2> TTTT>
using complex_semi_group_4 = complex_semi_group_n<T,TT,TTT,TTTT>;

struct complex_addition_inverse
{
    PUBLISH_OPERATION_PROPERTIES(complex_addition_inverse,group_operation);

	template<solvable_literal_pack<2> T, typename ... Operations>
	friend inline auto operator-(const algebraic_structure<complex<T>,Operations...>& i_lhs)
	{
		typedef ddk::mpl::type_pack<decltype(-std::declval<typename T::nth_type<0>>()),decltype(-std::declval<typename T::nth_type<1>>())> ret_type;

		return algebraic_structure<complex<ret_type>,Operations...>{ -i_lhs.re().number(),-i_lhs.im().number() };
	}
	template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, typename ... Operations>
	friend inline auto operator-(const algebraic_structure<complex<T>,Operations...>& i_lhs, const algebraic_structure<complex<TT>,Operations...>& i_rhs)
	{
		typedef ddk::mpl::type_pack<decltype(std::declval<typename T::nth_type<0>>() + (-std::declval<typename T::nth_type<0>>())),decltype(std::declval<typename TT::nth_type<0>>() + (-std::declval<typename TT::nth_type<0>>()))> ret_type;

		return algebraic_structure<complex<ret_type>,Operations...>{ i_lhs.re().number() + (-i_rhs.re().number()),i_lhs.im().number() + (-i_rhs.im().number()) };
	}
};

using ComplexGroup = Group<ComplexSemiGroup,complex_addition_inverse>;
template<solvable_literal_pack<2> T>
using complex_group = group<complex_semi_group<T>,complex_addition_inverse,complex<T>>;

template<size_t ... Dims>
using ComplexGroup_n = times_model<ComplexGroup,Dims...>;

using ComplexGroup_1 = ComplexGroup_n<1>;
using ComplexGroup_2 = ComplexGroup_n<2>;
using ComplexGroup_3 = ComplexGroup_n<3>;
using ComplexGroup_4 = ComplexGroup_n<4>;

template<solvable_literal_type ... T>
using complex_group_n = prod_struct<complex_group<T>...>;

template<solvable_literal_type T>
using complex_group_1 = complex_group_n<T>;
template<solvable_literal_type T, solvable_literal_type TT>
using complex_group_2 = complex_group_n<T,TT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT>
using complex_group_3 = complex_group_n<T,TT,TTT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, solvable_literal_type TTTT>
using complex_group_4 = complex_group_n<T,TT,TTT,TTTT>;

}