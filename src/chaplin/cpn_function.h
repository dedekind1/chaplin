#pragma once

#include "cpn_function_impl.h"
#include "cpn_function_concepts.h"
#include "ddkFramework/ddk_system_allocator.h"

namespace cpn
{

template<flat_model Im, set_model Dom, typename Allocator>
class function<Im(const Dom&),Allocator> : public ddk::detail::function_impl<Im(const Dom&),Allocator,detail::function_base>
{
    typedef ddk::detail::function_impl<Im(const Dom&),Allocator,detail::function_base> base_t;

public:
    using base_t::callable_tag;
    using base_t::callable;
    using base_t::operator->;

    template<literal_expression Expression>
    function(Expression&& i_exp);
    function(const Im& i_value);
    template<builtin_function_type Function>
    function(Function&& i_function);

   	TEMPLATE(set_type ... Args)
    REQUIRES(ddk::is_constructible<final_object<Dom>,Args...>)
    inline NO_DISCARD_RETURN auto operator()(Args&& ... args) const;

};

template<intersection_model Im, set_model Dom, typename Allocator>
class function<Im(const Dom&),Allocator> : public ddk::detail::function_impl<Im(const Dom&),Allocator,detail::function_base>
{
    typedef ddk::detail::function_impl<Im(const Dom&),Allocator,detail::function_base> base_t;

public:
    using base_t::callable;
    using base_t::operator->;

    TEMPLATE(literal_expression ... Expression)
    REQUIRES(ddk::is_num_of_args_equal<Im::rank(),Expression...>)
    function(Expression&& ... i_exps);
    function(const Im& i_value);
    template<builtin_function_type Function>
    function(Function&& i_function);

   	TEMPLATE(typename ... Args)
    REQUIRES(ddk::is_constructible<Dom,Args...>)
    inline NO_DISCARD_RETURN auto operator()(Args&& ... args) const;

private:
    template<typename ... Callable>
    static auto make_fusion(Callable&& ... i_callables);

};

}

#include "cpn_function_ops.h"
#include "cpn_builtin_functions.h"
#include "cpn_function.inl"
#include "cpn_linear_function.h"