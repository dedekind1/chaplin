
namespace cpn
{

TEMPLATE(callable_type Generator, iterable_deducible_type ... Iterables)
REQUIRED(ddk::is_callable_by<Generator,typename ddk::detail::adaptor_traits<Iterables>::const_reference...>)
auto series(Generator&& i_generator, Iterables&& ... i_iterables)
{
    return ddk::iter::transform(std::forward<Generator>(i_generator)) <<= ddk::fusion(std::forward<Iterables>(i_iterables)...);
}

}