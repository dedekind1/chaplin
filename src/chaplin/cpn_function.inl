
namespace cpn
{

template<flat_model Im,set_model Dom, typename Allocator>
template<literal_expression Expression>
function<Im(const Dom&),Allocator>::function(Expression&& i_exp)
: base_t(instance_function<Im,Dom,Allocator>(i_exp))
{
}
template<flat_model Im,set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator>::function(const Im& i_value)
: base_t(detail::builtin_number_function<Im(const Dom&),Allocator>(i_value))
{
}
template<flat_model Im,set_model Dom, typename Allocator>
template<builtin_function_type Function>
function<Im(const Dom&),Allocator>::function(Function&& i_function)
: base_t(std::forward<Function>(i_function))
{
}
template<flat_model Im,set_model Dom, typename Allocator>
TEMPLATE(set_type ... Args)
REQUIRED(ddk::is_constructible<final_object<Dom>,Args...>)
auto function<Im(const Dom&),Allocator>::operator()(Args&& ... i_args) const
{
    return base_t::inline_eval(final_object<Dom>{ std::forward<Args>(i_args)... });
}

template<intersection_model Im,set_model Dom, typename Allocator>
TEMPLATE(literal_expression ... Expression)
REQUIRED(ddk::is_num_of_args_equal<Im::rank(),Expression...>)
function<Im(const Dom&),Allocator>::function(Expression&& ... i_exps)
: base_t(make_fusion(instance_function<typename Im::principal_type,Dom,Allocator>(i_exps)...))
{
}
template<intersection_model Im,set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator>::function(const Im& i_value)
: base_t(detail::builtin_number_function<Im(const Dom&),Allocator>(i_value))
{
}
template<intersection_model Im,set_model Dom, typename Allocator>
template<builtin_function_type Function>
function<Im(const Dom&),Allocator>::function(Function&& i_function)
: base_t(std::forward<Function>(i_function))
{
}
template<intersection_model Im,set_model Dom, typename Allocator>
TEMPLATE(typename ... Args)
REQUIRED(ddk::is_constructible<Dom,Args...>)
auto function<Im(const Dom&),Allocator>::operator()(Args&& ... i_args) const
{
    return base_t::operator()(Dom{ std::forward<Args>(i_args)... });
}
template<intersection_model Im,set_model Dom, typename Allocator>
template<typename ... Callable>
auto function<Im(const Dom&),Allocator>::make_fusion(Callable&& ... i_callables)
{
    return detail::builtin_fusioned_function<Im(const Dom&),Allocator>(std::forward<Callable>(i_callables)...);
}

}
