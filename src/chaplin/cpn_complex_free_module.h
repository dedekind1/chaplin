#pragma once

#include "cpn_complex_module.h"
#include "cpn_free_module.h"

namespace cpn
{

template<ring_model R, size_t Dim>
using ComplexFreeModule_n = TimesFreeModule<ComplexModule<R>,Dim>;

template<ring_model R = ComplexRing>
using ComplexFreeModule_1 = ComplexFreeModule_n<R,1>;
template<ring_model R = ComplexRing>
using ComplexFreeModule_2 = ComplexFreeModule_n<R,2>;
template<ring_model R = ComplexRing>
using ComplexFreeModule_3 = ComplexFreeModule_n<R,3>;
template<ring_model R = ComplexRing>
using ComplexFreeModule_4 = ComplexFreeModule_n<R,4>;

template<ring_model R, solvable_literal_pack<2> ... T>
using complex_free_module_n = prod_free_module<ComplexModule<R>,reify_model<complex<T>,R>...>;

template<solvable_literal_pack<2> T, ring_model R = ComplexRing>
using complex_free_module_1 = complex_free_module_n<R,T>;
template<solvable_literal_pack<2> T,solvable_literal_pack<2> TT, ring_model R = ComplexRing>
using complex_free_module_2 = complex_free_module_n<R,T,TT>;
template<solvable_literal_pack<2> T,solvable_literal_pack<2> TT,solvable_literal_pack<2> TTT, ring_model R = ComplexRing>
using complex_free_module_3 = complex_free_module_n<R,T,TT,TTT>;
template<solvable_literal_pack<2> T,solvable_literal_pack<2> TT,solvable_literal_pack<2> TTT,solvable_literal_pack<2> TTTT, ring_model R = ComplexRing>
using complex_free_module_4 = complex_free_module_n<R,T,TT,TTT,TTTT>;

}