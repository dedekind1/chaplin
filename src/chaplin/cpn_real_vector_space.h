#pragma once

#include "cpn_reals.h"
#include "cpn_real_free_module.h"
#include "cpn_vector_space.h"

namespace cpn
{

template<size_t Dim>
using R_n = VectorSpace<RealFreeModule_n<Reals,Dim>,canonical_vector_mult_operation>;

using R1 = R_n<1>;
using R2 = R_n<2>;
using R3 = R_n<3>;
using R4 = R_n<4>;

template<solvable_literal_type ... T>
using r_n = vector_space<real_free_module_n<Reals,T...>,canonical_vector_mult_operation,prod_type<reify_model<real<T>,Reals>...>>;

template<solvable_literal_type T>
using r1 = r_n<T>;
template<solvable_literal_type T, solvable_literal_type TT>
using r2 = r_n<T,TT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT>
using r3 = r_n<T,TT,TTT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, solvable_literal_type TTTT>
using r4 = r_n<T,TT,TTT,TTTT>;

}