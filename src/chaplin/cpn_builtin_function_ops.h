#pragma once

#include "cpn_builtin_expressions.h"
#include "cpn_function_concepts.h"
#include "cpn_type_concepts.h"

namespace cpn
{

TEMPLATE(set_model Im, set_model Dom, typename Allocator, numeric_type T)
REQUIRES(ddk::is_constructible<Im,T>)
inline detail::builtin_number_function<Im(const Dom&),Allocator> instance_function(const builtin_literal_expression<T>& i_exp);
template<set_model Im,set_model Dom, typename Allocator>
inline detail::builtin_component_function<Im(const Dom&),Allocator> instance_function(const builtin_incognita_expression& i_exp);
TEMPLATE(set_model Im, intersection_model Dom, typename Allocator, size_t Comp)
REQUIRES(Comp < Dom::rank())
inline detail::builtin_component_function<Im(const Dom&),Allocator> instance_function(const builtin_component_expression<Comp>& i_exp);
TEMPLATE(set_model Im,set_model Dom, typename Allocator, typename Expression)
REQUIRES(is_function_instantiable_by<Expression,Im,Dom>)
inline detail::builtin_minus_function<Im(const Dom&),Allocator> instance_function(const builtin_minus_expression<Expression>& i_exp);
TEMPLATE(set_model Im,set_model Dom, typename Allocator, typename Expression)
REQUIRES(is_function_instantiable_by<Expression,Im,Dom>)
inline detail::builtin_inverted_function<Im(const Dom&),Allocator> instance_function(const builtin_inverted_expression<Expression>& i_exp);

TEMPLATE(set_model Im,set_model Dom, typename Allocator, size_t ...Indexs, literal_expression ... Expressions)
REQUIRES(is_function_instantiable_by<Expressions,Im,Dom>...)
inline detail::builtin_add_nary_function<Im(const Dom&),Allocator> instance_function(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_exp);
TEMPLATE(set_model Im,set_model Dom, typename Allocator, size_t ...Indexs, literal_expression ... Expressions)
REQUIRES(is_function_instantiable_by<Expressions,Im,Dom>...)
inline detail::builtin_mult_nary_function<Im(const Dom&),Allocator> instance_function(const prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_exp);
TEMPLATE(set_model Im,set_model Dom, typename Allocator, literal_expression LhsExpression, literal_expression RhsExpression)
REQUIRES(is_function_instantiable_by<LhsExpression,Im,Im>,is_function_instantiable_by<RhsExpression,Im,Dom>)
inline detail::builtin_composed_function<Im(const Dom&),Allocator> instance_function(const builtin_composed_expression<LhsExpression,RhsExpression>& i_exp);
template<set_model Im,set_model Dom, typename Allocator, typename Functor>
inline detail::builtin_functor_function<Im(const Dom&),Allocator,Functor> instance_function(const builtin_functor_expression<Functor>& i_exp);

template<set_model Im, set_model Dom, typename Allocator, typename Expression>
using instanced_expression = decltype(instance_function<Im,Dom,Allocator>(std::declval<Expression>()));

}

#include "cpn_builtin_function_ops.inl"