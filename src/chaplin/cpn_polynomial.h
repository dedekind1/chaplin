#pragma once

#include "cpn_monomial.h"
#include "cpn_algebraic_type_concepts.h"
#include "cpn_polynomial_concepts.h"
#include "cpn_expression_concepts.h"
#include <map>
#include <set>

namespace cpn
{

template<ring_model T, typename Order = lexicographic_order, typename Allocator = ddk::system_allocator>
class polynomial
{
    friend inline auto deduce_iterable(const polynomial& i_poly)
    {
        using namespace ddk;

        return deduce_iterable(i_poly.m_monomials);
    }
    friend inline auto deduce_iterable(polynomial& i_poly)
    {
        using namespace ddk;

        return deduce_iterable(i_poly.m_monomials);
    }
    struct _Order
    {
        TEMPLATE(ring_type TT, ring_type TTT)
        REQUIRES(implements_model<TT,T>,implements_model<TTT,T>)
        bool operator()(const monomial<TT,Allocator>& i_lhs, const monomial<TTT,Allocator>& i_rhs) const;
    };

    typedef final_object<T> ring_t;
    typedef std::set<monomial<ring_t>,_Order> monomial_set;
    typedef monomial_set::iterator monomial_iterator;
    typedef monomial_set::const_iterator monomial_const_iterator;

public:
    typedef monomial_set iterable_type;

    polynomial() = default;
    TEMPLATE(ring_type ... TT)
    REQUIRES(implements_model<TT,T>...)
    polynomial(const monomial<TT,Allocator>& ... i_monomials);
    TEMPLATE(literal_expression Expression)
    REQUIRES(is_polynomial_expression<Expression>)
    polynomial(Expression&& i_exp);
    TEMPLATE(iterable_deducible_type Iterable)
    REQUIRES(is_monomial_type<typename ddk::detail::adaptor_traits<Iterable>::value_type>)
    polynomial(const Iterable& i_iterable, size_t i_depth);

    TEMPLATE(ring_type TT, typename AAllocator)
    REQUIRES(implements_model<TT,T>)
    void push(const monomial<TT,AAllocator>& i_monomial);
    void push(const polynomial<T,Order,Allocator>& i_poly);
    polynomial<T,Order,Allocator> rebase(size_t i_component, const polynomial<T,Order,Allocator>&) const;
    polynomial<T,Order,Allocator> shift(size_t i_component, size_t i_exponent) const;

private:
    monomial_set m_monomials;
};
template<typename T>
polynomial(const monomial<T>&) -> polynomial<T>;

}

#include "cpn_polynomial.inl"