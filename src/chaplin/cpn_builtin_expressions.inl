
namespace cpn
{

template<literal_type T>
constexpr builtin_literal_expression<T>::builtin_literal_expression(const T& i_number)
: m_number(i_number)
{
}
template<literal_type T>
constexpr const T& builtin_literal_expression<T>::get() const
{
    return m_number;
}
template<literal_type T>
constexpr builtin_literal_expression<T>::operator const T&() const
{
    return m_number;
}

TEMPLATE(typename T)
REQUIRED(is_instantiable<T>)
constexpr builtin_composed_expression<builtin_incognita_expression,T> builtin_incognita_expression::operator()(const T& other) const
{
    return { *this, other };
}

template<size_t Comp>
TEMPLATE(typename T)
REQUIRED(is_instantiable<T>)
constexpr builtin_composed_expression<builtin_component_expression<Comp>,T> builtin_component_expression<Comp>::operator()(const T& other) const
{
    return { *this, other };
}

template<literal_expression Expression>
constexpr builtin_minus_expression<Expression>::builtin_minus_expression(const Expression& i_exp)
: m_exp(i_exp)
{
}
template<literal_expression Expression>
constexpr const Expression& builtin_minus_expression<Expression>::get() const
{
    return m_exp;
}

template<literal_expression Expression>
constexpr builtin_inverted_expression<Expression>::builtin_inverted_expression(const Expression& i_exp)
: m_exp(i_exp)
{
}
template<literal_expression Expression>
constexpr const Expression& builtin_inverted_expression<Expression>::get() const
{
    return m_exp;
}

template<literal_expression LhsFunction,literal_expression RhsFunction>
constexpr builtin_composed_expression<LhsFunction,RhsFunction>::builtin_composed_expression(const LhsFunction& i_lhs,const RhsFunction& i_rhs)
: m_lhs(i_lhs)
, m_rhs(i_rhs)
{
}
template<literal_expression LhsFunction,literal_expression RhsFunction>
constexpr const LhsFunction& builtin_composed_expression<LhsFunction,RhsFunction>::lhs() const
{
    return m_lhs;
}
template<literal_expression LhsFunction,literal_expression RhsFunction>
constexpr const RhsFunction& builtin_composed_expression<LhsFunction,RhsFunction>::rhs() const
{
    return m_rhs;
}

template<callable_type Functor>
constexpr builtin_functor_expression<Functor>::builtin_functor_expression(const Functor& i_functor)
: m_functor(i_functor)
{
}
template<callable_type Functor>
template<literal_expression T>
constexpr builtin_composed_expression<builtin_functor_expression<Functor>,T> builtin_functor_expression<Functor>::operator()(const T& other) const
{
    return { *this,other };
}
template<callable_type Functor>
constexpr const Functor& builtin_functor_expression<Functor>::functor() const
{
    return m_functor;
}

template<size_t ... Indexs, literal_expression ... Expressions>
constexpr builtin_monoid_expression<ddk::mpl::sequence<Indexs...>,Expressions...>::builtin_monoid_expression(const ddk::tuple<Expressions...>& i_exps)
: m_expressions(i_exps)
{
}
template<size_t ... Indexs, literal_expression ... Expressions>
constexpr builtin_monoid_expression<ddk::mpl::sequence<Indexs...>,Expressions...>::builtin_monoid_expression(ddk::tuple<Expressions...>&& i_exps)
: m_expressions(i_exps)
{
}
template<size_t ... Indexs, literal_expression ... Expressions>
TEMPLATE(typename ... EExpressions)
REQUIRED(ddk::is_constructible<Expressions,EExpressions>...)
constexpr builtin_monoid_expression<ddk::mpl::sequence<Indexs...>,Expressions...>::builtin_monoid_expression(const EExpressions& ... i_exps)
: m_expressions(i_exps...)
{
}
template<size_t ... Indexs, literal_expression ... Expressions>
template<size_t Index>
constexpr auto builtin_monoid_expression<ddk::mpl::sequence<Indexs...>,Expressions...>::get() const
{
    return m_expressions.template get<Index>();
}

constexpr builtin_functor_expression sin = [](auto&& i_arg)
{
    mpfr_t res,num;
    
    mpfr_init(res);
    mpfr_init(num);
    
    mpfr_set_d(num,resolve(i_arg),MPFR_RNDD);
    mpfr_sin(res,num,MPFR_RNDD);
    
    return mpfr_get_d(res,MPFR_RNDD);
};
typedef typename decltype(sin)::functor_t sin_t;
constexpr builtin_functor_expression cos = [](auto&& i_arg)
{
    mpfr_t res,num;
    
    mpfr_init(res);
    mpfr_init(num);
    
    mpfr_set_d(num,resolve(i_arg),MPFR_RNDD);
    
    mpfr_cos(res,num,MPFR_RNDD);
    
    return mpfr_get_d(res,MPFR_RNDD);
};
typedef typename decltype(cos)::functor_t cos_t;
constexpr builtin_functor_expression tan = [](auto&& i_arg)
{
    mpfr_t res,num;
    
    mpfr_init(res);
    mpfr_init(num);
    
    mpfr_set_d(num,resolve(i_arg),MPFR_RNDD);
    
    mpfr_tan(res,num,MPFR_RNDD);
    
    return mpfr_get_d(res,MPFR_RNDD);
};
typedef typename decltype(tan)::functor_t tan_t;
constexpr builtin_functor_expression cotan = [](auto&& i_arg)
{
    mpfr_t res,num;
    
    mpfr_init(res);
    mpfr_init(num);
    
    mpfr_set_d(num,resolve(i_arg),MPFR_RNDD);
    
    mpfr_cot(res,num,MPFR_RNDD);
    
    return mpfr_get_d(res,MPFR_RNDD);
};
typedef typename decltype(cotan)::functor_t cotan_t;
constexpr builtin_functor_expression sec = [](auto&& i_arg)
{
    mpfr_t res,num;
    
    mpfr_init(res);
    mpfr_init(num);
    
    mpfr_set_d(num,resolve(i_arg),MPFR_RNDD);
    
    mpfr_sec(res,num,MPFR_RNDD);
    
    return mpfr_get_d(res,MPFR_RNDD);
};
typedef typename decltype(sec)::functor_t sec_t;
constexpr builtin_functor_expression cosec = [](auto&& i_arg)
{
    mpfr_t res,num;
    
    mpfr_init(res);
    mpfr_init(num);
    
    mpfr_set_d(num,resolve(i_arg),MPFR_RNDD);
    
    mpfr_csc(res,num,MPFR_RNDD);
    
    return mpfr_get_d(res,MPFR_RNDD);
};
typedef typename decltype(cosec)::functor_t cosec_t;
constexpr builtin_functor_expression asin = [](auto&& i_arg)
{
    mpfr_t res,num;
    
    mpfr_init(res);
    mpfr_init(num);
    
    mpfr_set_d(num,resolve(i_arg),MPFR_RNDD);
    
    mpfr_asin(res,num,MPFR_RNDD);
    
    return mpfr_get_d(res,MPFR_RNDD);
};
typedef typename decltype(asin)::functor_t asin_t;
constexpr builtin_functor_expression acos = [](auto&& i_arg)
{
    mpfr_t res,num;
    
    mpfr_init(res);
    mpfr_init(num);
    
    mpfr_set_d(num,resolve(i_arg),MPFR_RNDD);
    
    mpfr_acos(res,num,MPFR_RNDD);
    
    return mpfr_get_d(res,MPFR_RNDD);
};
typedef typename decltype(acos)::functor_t acos_t;
constexpr builtin_functor_expression atan = [](auto&& i_arg)
{
    mpfr_t res,num;
    
    mpfr_init(res);
    mpfr_init(num);
    
    mpfr_set_d(num,resolve(i_arg),MPFR_RNDD);
    
    mpfr_atan(res,num,MPFR_RNDD);
    
    return mpfr_get_d(res,MPFR_RNDD);
};
typedef typename decltype(atan)::functor_t atan_t;
constexpr builtin_functor_expression exp = [](auto&& i_arg)
{
    mpfr_t res,num;
    
    mpfr_init(res);
    mpfr_init(num);
    
    mpfr_set_d(num,resolve(i_arg),MPFR_RNDD);
    
    mpfr_exp(res,num,MPFR_RNDD);
    
    return mpfr_get_d(res,MPFR_RNDD);
};
typedef typename decltype(exp)::functor_t exp_t;
constexpr builtin_functor_expression ln = [](auto&& i_arg)
{
    mpfr_t res,num;
    
    mpfr_init(res);
    mpfr_init(num);
    
    mpfr_set_d(num,resolve(i_arg),MPFR_RNDD);
    
    mpfr_log(res,num,MPFR_RNDD);
    
    return mpfr_get_d(res,MPFR_RNDD);
};
typedef typename decltype(ln)::functor_t ln_t;

}