#pragma once

#include "cpn_free_module.h"
#include "cpn_metric_space.h"

namespace cpn
{

struct canonical_vector_mult_operation
{
private:
    template<size_t ... Indexs, typename T, typename TT>
    static inline auto product(const ddk::mpl::sequence<Indexs...>&, const T& i_lhs, const TT& i_rhs)
    {
        return ( (project<Indexs>(i_lhs) * project<Indexs>(i_rhs)) + ... );
    }    

public:
    template<free_module_type T, free_module_type TT>
    auto operator()(const T& i_lhs, const TT& i_rhs) const
    {
        static_assert(T::rank() == TT::rank(), "You shall not multiply elements of free modules of different rank");

        typedef typename ddk::mpl::make_sequence<0,T::rank()>::type seq_t;

        return product(seq_t{},i_lhs,i_rhs);
    }
};

template<typename ScalarProduct>
struct metric_from_scalar_product
{
    typedef real<float> valued_type;

    template<free_module_type TT, free_module_type TTT>
    inline valued_type operator()(const TT& i_lhs, const TTT& i_rhs) const
    {
        static const ScalarProduct _innerProd;

        return std::sqrt(_innerProd(i_lhs-i_rhs,i_lhs-i_rhs));
    }
};

template<free_module_model T, typename ScalarProduct>
struct scalar_product
{
    PUBLISH_OPERATION_PROPERTIES(scalar_product,vector_operation,linear,conjugate,positive_definite);

    typedef algebraic_operator<typename algebraic_operator<T,mod_operation>::r_model,ring_operation> ring_op;
    static constexpr auto identity = pow_forward<T::rank()>(ring_op::identity);
    static constexpr auto annihilator = pow_forward<T::rank()>(ring_op::annihilator);

    TEMPLATE(free_module_type TT, free_module_type TTT)
    REQUIRES(implements_operation<ring_operation,TT,T>,implements_operation<ring_operation,TTT,T>)
    friend inline auto operator*(const TT& i_lhs, const TTT& i_rhs)
    {
        static const ScalarProduct _innerProd;

        return _innerProd(i_lhs,i_rhs);
    }
};

template<free_module_model Model, typename ScalarProduct>
using VectorSpace = typename Model::template drop<ring_operation>::template embed<scalar_product<Model,ScalarProduct>>;

template<free_module_type Structure, typename ScalarProduct, typename Set = typename Structure::value_type>
using vector_space = algebraic_structure<Set,VectorSpace<typename Structure::operators,ScalarProduct>>;

}

#include "cpn_vector_space.inl"