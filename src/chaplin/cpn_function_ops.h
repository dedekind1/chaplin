#pragma once

#include "cpn_algebraic_type_concepts.h"

namespace cpn
{

template<set_model Im, metric_space_model Dom, typename Allocator>
inline function<Im(const Dom&),Allocator> derivative(const function<Im(const Dom&),Allocator>& i_func);
template<set_model Im, vector_space_model Dom, typename Allocator>
inline std::array<function<Im(const Dom&),Allocator>,Dom::rank()> derivative(const function<Im(const Dom&),Allocator>& i_func);
TEMPLATE(set_model Im, vector_space_model Dom, typename Allocator, vector_space_type DDom)
REQUIRES(implements_model<DDom,Dom>)
inline function<Im(const Dom&),Allocator> derivative(const function<Im(const Dom&),Allocator>& i_func, const DDom& i_direction);

template<semi_group_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&),Allocator> operator+(const function<Im(const Dom&),Allocator>& i_lhs, const function<Im(const Dom&),Allocator>& i_rhs);
template<group_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&),Allocator> operator-(const function<Im(const Dom&),Allocator>& i_rhs);
template<semi_ring_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&),Allocator> operator*(const function<Im(const Dom&),Allocator>& i_lhs, const function<Im(const Dom&),Allocator>& i_rhs);
template<field_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&),Allocator> operator/(const function<Im(const Dom&),Allocator>& i_lhs, const function<Im(const Dom&),Allocator>& i_rhs);

}

#include "cpn_function_ops.inl"