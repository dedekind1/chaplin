#pragma once

#include "cpn_algebraic_operators.h"
#include "cpn_algebraic_concepts.h"
#include "ddkFramework/ddk_type_concepts.h"
#include "ddkFramework/ddk_concepts.h"

namespace cpn
{

template<typename T,typename ... Operators>
class algebraic_structure : public T, public algebraic_operators<Operators...>
{
public:
    typedef T value_type;
    typedef algebraic_operators<Operators...> model;

    template<typename ... Operations>
    using add_ops = typename algebraic_operators<Operators...>::template embed<Operations...>;
    template<typename OperationTag>
    using ops = decltype(resolve_operation(std::declval<algebraic_structure<T,Operators...>>(),std::declval<OperationTag>()));
    template<typename ... OpsTags>
    using keep_ops = typename std::enable_if<algebraic_operators<Operators...>::template contains<ops<OpsTags>...>,algebraic_operators<ops<OpsTags>...>>::type;

    algebraic_structure(const T&);
    TEMPLATE(typename ... Args)
    REQUIRES(ddk::is_constructible<T,Args...>)
    constexpr algebraic_structure(Args&& ... i_args);
    TEMPLATE(typename TT, typename ... OOperators)
    REQUIRES(ddk::is_constructible<T,TT>,is_super_structure_of<ddk::mpl::type_pack<OOperators...>,ddk::mpl::type_pack<Operators...>>)
    constexpr algebraic_structure(const algebraic_structure<TT,OOperators...>& other);
    TEMPLATE(typename TT)
    REQUIRES(ddk::is_constructible<T,TT>)
    constexpr algebraic_structure(const algebraic_structure<TT,Operators...>& other);

    algebraic_structure& operator=(const algebraic_structure&) = default;
    TEMPLATE(typename TT, typename ... OOperators)
    REQUIRES(ddk::is_assignable<T,TT>,is_super_structure_of<ddk::mpl::type_pack<OOperators...>,ddk::mpl::type_pack<Operators...>>)
    constexpr algebraic_structure& operator=(const algebraic_structure<TT,OOperators...>& other);
    TEMPLATE(typename TT)
    REQUIRES(ddk::is_assignable<T,TT>)
    constexpr algebraic_structure& operator=(const algebraic_structure<TT,Operators...>& other);
};
template<typename T, typename ... Operators>
algebraic_structure(const algebraic_structure<T,Operators...>&) -> algebraic_structure<T,Operators...>;

template<typename,typename...>
struct make_algebraic_structure;

template<typename T, typename ... Operators>
struct make_algebraic_structure
{
    typedef algebraic_structure<T,algebraic_operators<Operators...>> type;
};

template<typename T, typename ... Operators>
struct make_algebraic_structure<T,algebraic_operators<Operators...>>
{
    typedef algebraic_structure<T,Operators...> type;
};

}

#include "cpn_algebraic_structure.inl"
#include "cpn_algebraic_model.h"
#include "cpn_algebraic_functor.h"