#pragma once

#include "cpn_rational_group.h"
#include "cpn_ring.h"

namespace cpn
{

struct rational_multiplication
{
    PUBLISH_OPERATION_PROPERTIES(rational_multiplication,ring_operation,commutative,associative,distributive);

	static constexpr rational_set identity = rational(1,1);
	static constexpr rational_set annihilator = rational(0,1);

	template<typename ... Operations>
	friend inline auto operator*(const algebraic_structure<rational,Operations...>& i_lhs,const algebraic_structure<rational,Operations...>& i_rhs)
	{
		return algebraic_structure<rational,Operations...>{ i_lhs.number() * i_rhs.number() };
	}
};

using RationalSemiRing = SemiRing<RationalSemiGroup,rational_multiplication>;
using rational_semi_ring = semi_ring<rational_semi_group,rational_multiplication>;

template<size_t ... Dims>
using RationalSemiRing_n = pow_model<RationalSemiRing,Dims...>;

typedef RationalSemiRing_n<1> RationalSemiRing_1;
typedef RationalSemiRing_n<2> RationalSemiRing_2;
typedef RationalSemiRing_n<3> RationalSemiRing_3;
typedef RationalSemiRing_n<4> RationalSemiRing_4;

template<size_t ... Dims>
using rational_semi_ring_n = pow_struct<rational_semi_ring,Dims...>;

typedef rational_semi_ring_n<1> rational_semi_ring_1;
typedef rational_semi_ring_n<2> rational_semi_ring_2;
typedef rational_semi_ring_n<3> rational_semi_ring_3;
typedef rational_semi_ring_n<4> rational_semi_ring_4;

using RationalRing = Ring<RationalGroup,rational_multiplication>;
using rational_ring = ring<rational_group,rational_multiplication>;

template<size_t ... Dims>
using RationalRing_n = pow_model<RationalRing,Dims...>;

typedef RationalRing_n<1> RationalRing_1;
typedef RationalRing_n<2> RationalRing_2;
typedef RationalRing_n<3> RationalRing_3;
typedef RationalRing_n<4> RationalRing_4;

template<size_t ... Dims>
using rational_ring_n = pow_struct<rational_ring,Dims...>;

typedef rational_ring_n<1> rational_ring_1;
typedef rational_ring_n<2> rational_ring_2;
typedef rational_ring_n<3> rational_ring_3;
typedef rational_ring_n<4> rational_ring_4;

}