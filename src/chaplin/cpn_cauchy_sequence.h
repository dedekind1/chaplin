#pragma once

namespace cpn
{
namespace detail
{

template<complete_space_type T, size_t Dim>
struct linear_cauchy_sequencer_impl
{
    static_assert(concepts::contains_algebraic_structure<typename algebraic_operator<T,mod_operation>::r_model,point_conv_operation>, "Cauchy sequences over modules expect complete spaces over its ring");

    static constexpr auto k_eps = algebraic_operator<typename T::r_model,point_conv_operation>::k_epsilon;

public:
    typedef T value_type;

    linear_cauchy_sequencer_impl(const T& i_target, size_t i_numSteps);

    inline auto point(size_t i_index) const;

private:
    typedef typename ddk::mpl::make_sequence<0,Dim>::type index_sequence;
    typedef typename algebraic_operator<T,point_conv_operation>::valuation_type valuation_type;

    linear_cauchy_sequencer_impl(const ddk::mpl::sequence<0>&, const T& i_target, size_t i_numSteps, const valuation_type& i_epsilon);
    template<size_t ... Indexs>
    linear_cauchy_sequencer_impl(const ddk::mpl::sequence<Indexs...>&, const T& i_target, size_t i_numSteps, const std::array<valuation_type,Dim>& i_epsilon);

    template<size_t ... Indexs>
    inline auto point(const ddk::mpl::sequence<Indexs...>&, size_t i_index) const;

    const T m_target;
    const std::array<size_t,Dim> m_delta;
};
template<flat_type T>
linear_cauchy_sequencer_impl(const T&) -> linear_cauchy_sequencer_impl<T,1>;
template<flat_type T>
linear_cauchy_sequencer_impl(const T&,size_t) -> linear_cauchy_sequencer_impl<T,1>;
template<intersection_type T>
linear_cauchy_sequencer_impl(const T&) -> linear_cauchy_sequencer_impl<T,T::rank()>;
template<intersection_type T>
linear_cauchy_sequencer_impl(const T&,size_t) -> linear_cauchy_sequencer_impl<T,T::rank()>;

template<complete_space_type T, size_t Dim>
struct exponential_cauchy_sequencer_impl
{
    static_assert(concepts::contains_algebraic_structure<typename algebraic_operator<T,mod_operation>::r_model,point_conv_operation>, "Cauchy sequences over modules expect complete spaces over its ring");

    static constexpr auto k_eps = algebraic_operator<typename T::r_model,point_conv_operation>::k_epsilon;

public:
    typedef T value_type;

    exponential_cauchy_sequencer_impl(const T& i_target, size_t i_numSteps);

    inline auto point(size_t i_index) const;

private:
    typedef typename ddk::mpl::make_sequence<0,Dim>::type index_sequence;
    typedef typename algebraic_operator<T,point_conv_operation>::valuation_type valuation_type;

    exponential_cauchy_sequencer_impl(const ddk::mpl::sequence<0>&, const T& i_target, size_t i_numSteps, const valuation_type& i_epsilon);
    template<size_t ... Indexs>
    exponential_cauchy_sequencer_impl(const ddk::mpl::sequence<Indexs...>&, const T& i_target, size_t i_numSteps, const std::array<valuation_type,Dim>& i_epsilon);

    template<size_t ... Indexs>
    inline auto point(const ddk::mpl::sequence<Indexs...>&, size_t i_index) const;
    template<size_t ... Indexs>
    inline static std::array<std::vector<size_t>,Dim> construct_delta(const std::array<size_t,Dim>& i_epsilon, size_t i_numSteps);

    const T m_target;
    const std::array<std::vector<size_t>,Dim> m_delta;
};
template<flat_type T>
exponential_cauchy_sequencer_impl(const T&) -> exponential_cauchy_sequencer_impl<T,1>;
template<flat_type T>
exponential_cauchy_sequencer_impl(const T&,size_t) -> exponential_cauchy_sequencer_impl<T,1>;
template<intersection_type T>
exponential_cauchy_sequencer_impl(const T&) -> exponential_cauchy_sequencer_impl<T,T::rank()>;
template<intersection_type T>
exponential_cauchy_sequencer_impl(const T&,size_t) -> exponential_cauchy_sequencer_impl<T,T::rank()>;

}

template<typename T>
class cauchy_sequence
{
public:
    typedef typename T::value_type value_type;

    cauchy_sequence(const T& i_generator);
    inline auto point(size_t i_index) const;

private:
    const T m_sequencer;
};

template<complete_space_type T>
inline auto linear_cauchy_sequencer(const T& i_target, size_t i_numSteps = 10);
template<complete_space_type T>
inline auto exponential_cauchy_sequencer(const T& i_target, size_t i_numSteps = 10);

}

#include "cpn_cauchy_sequence.inl"
#include "cpn_cauchy_sequence_adaptor.h"