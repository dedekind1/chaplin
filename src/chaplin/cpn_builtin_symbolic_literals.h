#pragma once

#include "cpn_builtin_literal_concepts.h"
#include "ddkFramework/ddk_type_concepts.h"
#include "ddkFramework/ddk_high_order_array.h"
#include "ddkFramework/ddk_variant.h"
#include <array>

#define BUILTIN_LITERAL(_TYPE) \
	friend void symbolic_literal(const _TYPE&);

#define BUILTIN_LITERAL_OPERAND(_OP) \
template<literal_type ... Operands> \
class _OP##_symbolic_literal : public monoid_symbolic_literal<Operands...> \
{ \
public: \
	using monoid_symbolic_literal<Operands...>::monoid_symbolic_literal; \
}; \
template<typename ... Operands> \
_OP##_symbolic_literal(const Operands& ... i_ops) -> _OP##_symbolic_literal<Operands...>;

namespace cpn
{

struct integer_symbolic_literal
{
	BUILTIN_LITERAL(integer_symbolic_literal)

public:
	constexpr integer_symbolic_literal(int i_number);

	constexpr int number() const;

private:
	int m_number = 0;
};

struct rational_symbolic_literal : integer_symbolic_literal
{
	BUILTIN_LITERAL(rational_symbolic_literal)

public:
	using integer_symbolic_literal::integer_symbolic_literal;
	constexpr rational_symbolic_literal(int i_numerator, unsigned int i_denominator);
	constexpr rational_symbolic_literal(int i_numerator, int i_denominator);

	constexpr int numerator() const;
	constexpr unsigned int denominator() const;

private:
	unsigned int m_denominator = 1;
};

struct root_symbolic_literal
{
	BUILTIN_LITERAL(root_symbolic_literal)

public:
	constexpr root_symbolic_literal(int i_number, int i_degree, bool i_positive = true);

	constexpr int number() const;
	constexpr int degree() const;
	constexpr bool positive() const;

private:
	int m_number = 0;
	int m_degree = 0;
	bool m_sign = true;
};

struct log_symbolic_literal
{
	BUILTIN_LITERAL(log_symbolic_literal)

public:
	constexpr log_symbolic_literal(unsigned int i_number, unsigned int i_logBase, bool i_positive = true);

	constexpr unsigned int number() const;
	constexpr unsigned int base() const;
	constexpr bool positive() const;

private:
	unsigned int m_number = 0;
	unsigned int m_base = 0;
	bool m_sign = true;
};

struct irrational_symbolic_literal
{
	typedef ddk::variant<root_symbolic_literal,log_symbolic_literal> number_t;

public:
	TEMPLATE(typename ... Args)
	REQUIRES(ddk::is_constructible<number_t,Args...>)
	constexpr irrational_symbolic_literal(Args&& ... i_args);

private:
	number_t m_number;
};

template<literal_type ... Operands>
struct monoid_symbolic_literal
{
	BUILTIN_LITERAL(monoid_symbolic_literal)

public:
	TEMPLATE(typename ... Args)
	REQUIRES(ddk::is_constructible<Operands,Args>...)
	constexpr monoid_symbolic_literal(Args&& ... i_operands);
	constexpr monoid_symbolic_literal(const monoid_symbolic_literal&) = default;
	constexpr monoid_symbolic_literal(monoid_symbolic_literal&&) = default;

	template<size_t Index>
	constexpr auto get() const;
	TEMPLATE(typename Callable)
	REQUIRES(ddk::is_callable<Callable,Operands>...)
	constexpr void enumerate(Callable&& i_callable) const;

private:
	ddk::tuple<Operands...> m_operands;
};

template<typename>
class real_symbolic_literal;

template<typename T>
real_symbolic_literal(T&&) -> real_symbolic_literal<T>;

template<solvable_literal_type T>
class real_symbolic_literal<T>
{
	BUILTIN_LITERAL(real_symbolic_literal)
public:
	TEMPLATE(typename TT)
	REQUIRES(ddk::is_constructible<T,TT>)
	constexpr real_symbolic_literal(TT&& i_value);

	constexpr  T number() const;

private:
	T m_value;
};

template<numeric_type T>
class real_symbolic_literal<T>
{
	BUILTIN_LITERAL(real_symbolic_literal)

public:
	constexpr real_symbolic_literal(const T& i_value);
	template<solvable_literal_type TT>
	constexpr real_symbolic_literal(const real_symbolic_literal<TT>& i_value);

	constexpr  T number() const;

private:
	T m_value;
};

template<literal_type T, size_t ... Dims>
class times_symbolic_literal_type : public ddk::high_order_array<T,Dims...>
{
	BUILTIN_LITERAL(times_symbolic_literal_type);
public:
	using ddk::high_order_array<T,Dims...>::high_order_array;
};

template<literal_type ... T>
class union_symbolic_literal_type : public ddk::variant<T...>
{
	BUILTIN_LITERAL(union_symbolic_literal_type);
public:
	using ddk::variant<T...>::variant;
};

BUILTIN_LITERAL_OPERAND(sum);
BUILTIN_LITERAL_OPERAND(prod);
BUILTIN_LITERAL_OPERAND(div);

}

#include "cpn_builtin_symbolic_literal_ops.h"
#include "cpn_builtin_symbolic_literals.inl"