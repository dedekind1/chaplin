#pragma once

namespace cpn
{

monomial_exponents operator+(const monomial_exponents& i_lhs, const monomial_exponents& i_rhs);
monomial_exponents operator-(const monomial_exponents& i_lhs, const monomial_exponents& i_rhs);

template<ring_model T, typename Order, typename Allocator>
monomial_bases<T,Order,Allocator> operator+(const monomial_bases<T,Order,Allocator>& i_lhs, const monomial_bases<T,Order,Allocator>& i_rhs);

template<group_type T, typename Allocator>
inline auto operator-(const monomial<T,Allocator>& i_rhs);
TEMPLATE(group_type T, typename Allocator, group_type TT)
REQUIRES(additive_type<T,TT>)
inline auto operator+(const monomial<T,Allocator>& i_lhs, const TT& i_rhs);
TEMPLATE(group_type T, group_type TT, typename AAllocator)
REQUIRES(additive_type<T,TT>)
inline auto operator+(const T& i_lhs, const monomial<TT,AAllocator>& i_rhs);
TEMPLATE(group_type T, typename Allocator, group_type TT, typename AAllocator)
REQUIRES(additive_type<T,TT>,compatible_models<T,TT>)
inline auto operator+(const monomial<T,Allocator>& i_lhs, const monomial<TT,AAllocator>& i_rhs);
TEMPLATE(group_type T, typename Allocator, group_type TT)
REQUIRES(substractive_type<T,TT>)
inline auto operator-(const monomial<T,Allocator>& i_lhs, const TT& i_rhs);
TEMPLATE(group_type T, group_type TT, typename AAllocator)
REQUIRES(substractive_type<T,TT>)
inline auto operator-(const T& i_lhs, const monomial<TT,AAllocator>& i_rhs);
TEMPLATE(group_type T, typename Allocator, group_type TT, typename AAllocator)
REQUIRES(substractive_type<T,TT>,compatible_models<T,TT>)
inline auto operator-(const monomial<T,Allocator>& i_lhs, const monomial<TT,AAllocator>& i_rhs);
TEMPLATE(ring_type T, typename Allocator, ring_type TT)
REQUIRES(multiplicative_type<T,TT>)
inline auto operator*(const monomial<T,Allocator>& i_lhs, const TT& i_rhs);
TEMPLATE(ring_type T, ring_type TT, typename AAllocator)
REQUIRES(multiplicative_type<T,TT>)
inline auto operator*(const T& i_lhs, const monomial<TT,AAllocator>& i_rhs);
TEMPLATE(ring_type T, typename Allocator, ring_type TT, typename AAllocator)
REQUIRES(multiplicative_type<T,TT>,compatible_models<T,TT>)
inline auto operator*(const monomial<T,Allocator>& i_lhs, const monomial<TT,AAllocator>& i_rhs);
template<module_type T, module_type TT, typename AAllocator>
inline auto operator^(const T& i_lhs, const monomial<TT,AAllocator>& i_rhs);
template<module_type T, typename Allocator, module_type TT>
inline auto operator^(const monomial<T,Allocator>& i_lhs, const TT& i_rhs);
TEMPLATE(field_type T, field_type TT, typename AAllocator)
REQUIRES(divisible_type<T,TT>)
inline auto operator/(const T& i_lhs, const monomial<TT,AAllocator>& i_rhs);
TEMPLATE(field_type T, typename Allocator, field_type TT)
REQUIRES(divisible_type<T,TT>)
inline auto operator/(const monomial<T,Allocator>& i_lhs, const TT& i_rhs);
TEMPLATE(field_type T, typename Allocator, field_type TT, typename AAllocator)
REQUIRES(divisible_type<T,TT>,compatible_models<T,TT>)
inline auto operator/(const monomial<T,Allocator>& i_lhs, const monomial<TT,AAllocator>& i_rhs);

template<ring_model T, typename Order, typename Allocator, numeric_type TT>
inline auto instance_polynomial(const builtin_literal_expression<TT>& i_exp);
template<ring_model T, typename Order, typename Allocator>
inline auto instance_polynomial(const builtin_incognita_expression& i_exp);
template<ring_model T, typename Order, typename Allocator, size_t Comp>
inline auto instance_polynomial(const builtin_component_expression<Comp>& i_exp);
TEMPLATE(ring_model T, typename Order, typename Allocator, typename Expression)
REQUIRES(is_polynomial_instantiable_by<Expression,T,Order,Allocator>)
inline auto instance_polynomial(const builtin_minus_expression<Expression>& i_exp);
TEMPLATE(ring_model T, typename Order, typename Allocator, size_t ...Indexs, literal_expression ... Expressions)
REQUIRES(is_polynomial_instantiable_by<Expressions,T,Order,Allocator>...)
inline auto instance_polynomial(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_exp);
TEMPLATE(ring_model T, typename Order, typename Allocator, size_t ...Indexs, literal_expression ... Expressions)
REQUIRES(is_polynomial_instantiable_by<Expressions,T,Order,Allocator>...)
inline auto instance_polynomial(const prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_exp);
template<ring_model T, typename Order, typename Allocator, numeric_type TT>
inline auto instance_polynomial(const pow_nary_expression<ddk::mpl::sequence<0,1>,builtin_incognita_expression,builtin_literal_expression<TT>>& i_exp);
template<ring_model T, typename Order, typename Allocator, size_t Comp, numeric_type TT>
inline auto instance_polynomial(const pow_nary_expression<ddk::mpl::sequence<0,1>,builtin_component_expression<Comp>,builtin_literal_expression<TT>>& i_exp);
TEMPLATE(ring_model T, typename Order, typename Allocator, literal_expression RhsExpression, numeric_type TT)
REQUIRES(is_polynomial_instantiable_by<RhsExpression,T,Order,Allocator>)
inline auto instance_polynomial(const pow_nary_expression<ddk::mpl::sequence<0,1>,builtin_composed_expression<builtin_incognita_expression,RhsExpression>,builtin_literal_expression<TT>>& i_exp);
TEMPLATE(ring_model T, typename Order, typename Allocator, size_t Comp, literal_expression RhsExpression, numeric_type TT)
REQUIRES(is_polynomial_instantiable_by<RhsExpression,T,Order,Allocator>)
inline auto instance_polynomial(const pow_nary_expression<ddk::mpl::sequence<0,1>,builtin_composed_expression<builtin_component_expression<Comp>,RhsExpression>,builtin_literal_expression<TT>>& i_exp);
TEMPLATE(ring_model T, typename Order, typename Allocator, literal_expression RhsExpression)
REQUIRES(is_polynomial_instantiable_by<RhsExpression,T,Order,Allocator>)
inline auto instance_polynomial(const builtin_composed_expression<builtin_incognita_expression,RhsExpression>& i_exp);
TEMPLATE(ring_model T, typename Order, typename Allocator, size_t Comp, literal_expression RhsExpression)
REQUIRES(is_polynomial_instantiable_by<RhsExpression,T,Order,Allocator>)
inline auto instance_polynomial(const builtin_composed_expression<builtin_component_expression<Comp>,RhsExpression>& i_exp);

}

#include "cpn_polynomial_ops.inl"