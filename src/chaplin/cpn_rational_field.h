#pragma once

#include "cpn_rational_ring.h"
#include "cpn_field.h"

namespace cpn
{

struct rational_division
{
    PUBLISH_OPERATION_PROPERTIES(rational_division,field_operation,commutative,associative,distributive);

	template<typename ... Operations>
	friend inline auto operator/(const algebraic_structure<rational,Operations...>& i_lhs,const algebraic_structure<rational,Operations...>& i_rhs)
	{
		return algebraic_structure<rational,Operations...>{ i_lhs.number() / i_rhs.number() };
	}
	template<typename ... Operations>
	friend inline auto inv(const algebraic_structure<rational,Operations...>& i_rhs)
	{
		return algebraic_structure<rational,Operations...>{ rational(1) } / i_rhs;
	}
};

using RationalField = Field<RationalRing,rational_division>;
using rational_field = field<rational_ring,rational_division>;

template<size_t ... Dims>
using RationalField_n = pow_model<RationalField,Dims...>;

typedef RationalField_n<1> RationalField_1;
typedef RationalField_n<2> RationalField_2;
typedef RationalField_n<3> RationalField_3;
typedef RationalField_n<4> RationalField_4;

template<size_t ... Dims>
using rational_field_n = pow_struct<rational_field,Dims...>;

typedef rational_field_n<1> rational_field_1;
typedef rational_field_n<2> rational_field_2;
typedef rational_field_n<3> rational_field_3;
typedef rational_field_n<4> rational_field_4;

}