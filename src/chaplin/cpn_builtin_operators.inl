
namespace cpn
{
namespace detail
{

template<typename Class>
template<typename T, typename>
auto builtin_operators_init<Class>::resolve_binary(const T&)
{
    return &builtin_operator<Class,T>::binary_operator;
}
template<typename Class>
auto builtin_operators_init<Class>::resolve_binary(...)
{
    typedef typename Class::binary_operator_type binary_operator_type;

    return binary_operator_type{ nullptr };
}
template<typename Class>
template<typename T, typename>
auto builtin_operators_init<Class>::resolve_unary(const T&)
{
    return &builtin_operator<Class,T>::unary_operator;
}
template<typename Class>
auto builtin_operators_init<Class>::resolve_unary(...)
{
    typedef typename Class::unary_operator_type unary_operator_type;

    return unary_operator_type{ nullptr };
}
template<typename Class>
template<typename ... OperatorTags>
auto builtin_operators_init<Class>::binary(const operator_tags<OperatorTags...>&)
{
    return std::array{ resolve_binary(OperatorTags{}) ...};
}
template<typename Class>
template<typename ... OperatorTags>
auto builtin_operators_init<Class>::unary(const operator_tags<OperatorTags...>&)
{
    return std::array{ resolve_unary(OperatorTags{}) ...};
}

}
}