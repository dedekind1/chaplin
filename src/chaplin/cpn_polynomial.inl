
namespace cpn
{

template<ring_model T, typename Order, typename Allocator>
TEMPLATE(ring_type TT, ring_type TTT)
REQUIRED(implements_model<TT,T>,implements_model<TTT,T>)
bool polynomial<T,Order,Allocator>::_Order::operator()(const monomial<TT,Allocator>& i_lhs, const monomial<TTT,Allocator>& i_rhs) const
{
    static const Order _order;

    return _order(i_lhs.exponents(),i_rhs.exponents());
}

template<ring_model T, typename Order, typename Allocator>
TEMPLATE(ring_type ... TT)
REQUIRED(implements_model<TT,T>...)
polynomial<T,Order,Allocator>::polynomial(const monomial<TT,Allocator>& ... i_monomials)
{
    ( push(i_monomials), ... );
}
template<ring_model T, typename Order, typename Allocator>
TEMPLATE(literal_expression Expression)
REQUIRED(is_polynomial_expression<Expression>)
polynomial<T,Order,Allocator>::polynomial(Expression&& i_exp)
: polynomial(instance_polynomial<T,Order,Allocator>(std::forward<Expression>(i_exp)))
{
}
template<ring_model T, typename Order, typename Allocator>
TEMPLATE(iterable_deducible_type Iterable)
REQUIRED(is_monomial_type<typename ddk::detail::adaptor_traits<Iterable>::value_type>)
polynomial<T,Order,Allocator>::polynomial(const Iterable& i_iterable, size_t i_depth)
{
    [this](auto&& ii_monomial)
    {
        push(ii_monomial);
    } <<= ddk::view::take_n(i_iterable,i_depth);
}
template<ring_model T, typename Order, typename Allocator>
TEMPLATE(ring_type TT, typename AAllocator)
REQUIRED(implements_model<TT,T>)
void polynomial<T,Order,Allocator>::push(const monomial<TT,AAllocator>& i_monomial)
{
    if(i_monomial.coeff() != algebraic_operator<T,ring_operation>::annihilator)
    {
        std::pair<monomial_iterator,bool> res = m_monomials.insert(i_monomial);

        if(!res.second)
        {
            res.first->set_coeff(res.first->coeff() + i_monomial.coeff());
        }
    }
}
template<ring_model T, typename Order, typename Allocator>
void polynomial<T,Order,Allocator>::push(const polynomial<T,Order,Allocator>& i_poly)
{
    [this](auto&& ii_monomial)
    {
        push(ii_monomial);
    } <<= i_poly;
}
template<ring_model T, typename Order, typename Allocator>
polynomial<T,Order,Allocator> polynomial<T,Order,Allocator>::rebase(size_t i_component, const polynomial<T,Order,Allocator>& i_poly) const
{
    polynomial<T,Order,Allocator> res;

    [&](auto&& ii_monomial)
    {
        res.push(ii_monomial.rebase(i_poly));
    } <<= m_monomials;

    return res;
}
template<ring_model T, typename Order, typename Allocator>
polynomial<T,Order,Allocator> polynomial<T,Order,Allocator>::shift(size_t i_component, size_t i_exponent) const
{
    polynomial<T,Order,Allocator> res;

    [&](auto&& ii_monomial)
    {
        res.push(ii_monomial.shift(i_component,i_exponent));
    } <<= m_monomials;

    return res;
}

}