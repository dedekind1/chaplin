
#include "cpn_series.h"

namespace cpn
{
namespace
{

template<size_t ... Indexs>
auto numeric_indexs(const ddk::mpl::sequence<Indexs...>&)
{
    return ddk::fusion(ddk::integers(Indexs - Indexs)...);
}

}

TEMPLATE(ring_model Im, metric_space_model Dom, typename Allocator, metric_space_type DDom)
REQUIRES(implements_model<DDom,Dom>)
auto taylor_expansion(const function<Im(const Dom&),Allocator>& i_func, const DDom& i_point)
{
    typedef final_object<Im> im_t;

    auto taylorExp = [_partialDer=i_func,_point=i_point,indexFactorial=1](int ii_index) mutable
    {
        if(ii_index == 0)
        {
            return monomial<im_t,Allocator>{ _partialDer(_point) };
        }
        else
        {
            _partialDer = derivative(_partialDer);

            monomial<im_t,Allocator> res = __monomial_component<im_t,Allocator>(0).shift(0,ii_index).rebase(0,__monomial_component<im_t,Allocator>(0) - im_t{ _point });

            indexFactorial *= ii_index;

            return res * monomial<im_t,Allocator>{ _partialDer(_point) / im_t{ indexFactorial} };
        }
    };

    return ddk::view::filter([](const monomial<im_t,Allocator>& i_monomial){ return i_monomial.coeff() != algebraic_operator<Im,ring_operation>::annihilator; }) <<= series(taylorExp,ddk::integers());
}

TEMPLATE(ring_model Im, vector_space_model Dom, typename Allocator, vector_space_type DDom)
REQUIRES(implements_model<DDom,Dom>)
auto taylor_expansion(const function<Im(const Dom&),Allocator>& i_func, const DDom& i_point)
{
    typedef typename ddk::mpl::make_sequence<0,Dom::rank()>::type seq_t;
    typedef final_object<Im> im_t;
    typedef std::array<size_t,Dom::rank()> index_array;

    static constexpr index_array s_zero_indexes = { 0 };

    auto taylorExp = [_partialDer=i_func,_point=i_point,_cachedIndexes=s_zero_indexes,indexFactorial=1](auto&& ... ii_indexes) mutable
    {
        monomial<im_t,Allocator> res;
        const index_array _currIndexes{ii_indexes...};

        if(_currIndexes == s_zero_indexes)
        {
            return monomial<im_t,Allocator>{ _partialDer(_point) };
        }
        else
        {
            if(_currIndexes < _cachedIndexes)
            {
                _cachedIndexes = { 0 };
                indexFactorial = 1;
            }

            for(size_t compIndex=0;compIndex<Dom::rank();compIndex++)
            {
                const size_t powIndex = _currIndexes[compIndex] - _cachedIndexes[compIndex];

                for(size_t derIndex=0;derIndex<powIndex;derIndex++)
                {
                    _partialDer = derivative(_partialDer,Dom::basis(compIndex));
                }

                res = res * __monomial_component<im_t,Allocator>(compIndex).shift(compIndex,powIndex).rebase(compIndex,__monomial_component<im_t,Allocator>(compIndex) - project_onto<im_t>(_point,compIndex));

                if(powIndex > 0)
                {
                    indexFactorial *= powIndex;
                }
            }

            _cachedIndexes = _currIndexes;

            return res * monomial<im_t,Allocator>{ _partialDer(_point) / im_t{ indexFactorial } };
        }
    };

    return ddk::view::filter([](auto&& i_monomial){ return i_monomial.coeff() == algebraic_operator<Im,ring_operation>::annihilator; }) <<= series(taylorExp,numeric_indexs(seq_t{}));
}


}