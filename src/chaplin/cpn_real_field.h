#pragma once

#include "cpn_real_ring.h"
#include "cpn_field.h"

namespace cpn
{

struct real_division
{
    PUBLISH_OPERATION_PROPERTIES(real_division,field_operation,commutative,associative,distributive);

	template<solvable_literal_type T, solvable_literal_type TT, typename ... Operations>
	friend inline auto operator/(const algebraic_structure<real<T>,Operations...>& i_lhs, const algebraic_structure<real<TT>,Operations...>& i_rhs)
	{
		typedef decltype(std::declval<T>() / std::declval<TT>()) ret_type;

		return algebraic_structure<real<ret_type>,Operations...>{ i_lhs.number() / i_rhs.number() };
	}
	template<solvable_literal_type T, typename ... Operations>
	friend inline auto inv(const algebraic_structure<real<T>,Operations...>& i_rhs)
	{
		return algebraic_structure<real<int>,Operations...>{ real_unit } / i_rhs;
	}
};

using RealField = Field<RealRing,real_division>;
template<solvable_literal_type T>
using real_field = field<real_ring<T>,real_division,real<T>>;

template<size_t ... Dims>
using RealField_n = times_model<RealField,Dims...>;

using RealField_1 = RealField_n<1>;
using RealField_2 = RealField_n<2>;
using RealField_3 = RealField_n<3>;
using RealField_4 = RealField_n<4>;

template<solvable_literal_type ... T>
using real_field_n = prod_struct<real_field<T>...>;

template<solvable_literal_type T>
using real_field_1 = real_field_n<T>;
template<solvable_literal_type T, solvable_literal_type TT>
using real_field_2 = real_field_n<T,TT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT>
using real_field_3 = real_field_n<T,TT,TTT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, solvable_literal_type TTTT>
using real_field_4 = real_field_n<T,TT,TTT,TTTT>;

}