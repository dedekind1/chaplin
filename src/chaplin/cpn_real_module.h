#pragma once

#include "cpn_real_ring.h"
#include "cpn_module.h"

namespace cpn
{

template<ring_model R>
struct real_module_operation
{
    PUBLISH_OPERATION_PROPERTIES(real_module_operation,mod_operation,commutative,associative,distributive);

	typedef R r_model;
	static constexpr auto identity = algebraic_operator<R,ring_operation>::identity;

	TEMPLATE(ring_type RR, solvable_literal_type T, typename ... Operations)
	REQUIRES(implements_operation<ring_operation,RR,r_model>)
	friend inline auto operator^(const RR& i_lhs, const algebraic_structure<real<T>,Operations...>& i_rhs)
	{
		typedef decltype(std::declval<RR>().number() * std::declval<real<T>>().number()) prod_t;

		return algebraic_structure<real<prod_t>,Operations...>{ i_lhs.number() * i_rhs.number() };
	}
};

template<ring_model R = RealRing>
using RealModule = Module<RealGroup,real_module_operation<R>>;
template<solvable_literal_type T, ring_model R = RealRing>
using real_module = module<real_group<T>,real_module_operation<R>,real<T>>;

using RRealModule = RealModule<RealRing>;

template<ring_model R, size_t ... Dims>
using RealModule_n = times_model<RealModule<R>,Dims...>;

template<ring_model R = RealRing>
using RealModule_1 = RealModule_n<R,1>;
template<ring_model R = RealRing>
using RealModule_2 = RealModule_n<R,2>;
template<ring_model R = RealRing>
using RealModule_3 = RealModule_n<R,3>;
template<ring_model R = RealRing>
using RealModule_4 = RealModule_n<R,4>;

using RRealModule_1 = RealModule_n<RealRing,1>;
using RRealModule_2 = RealModule_n<RealRing,2>;
using RRealModule_3 = RealModule_n<RealRing,3>;
using RRealModule_4 = RealModule_n<RealRing,4>;

template<ring_model R, solvable_literal_type ... T>
using real_module_n = prod_struct<real_module<T,R>...>;

template<solvable_literal_type T, ring_model R = RealRing>
using real_module_1 = real_module_n<R,T>;
template<solvable_literal_type T, solvable_literal_type TT, ring_model R = RealRing>
using real_module_2 = real_module_n<R,T,TT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, ring_model R = RealRing>
using real_module_3 = real_module_n<R,T,TT,TTT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, solvable_literal_type TTTT, ring_model R = RealRing>
using real_module_4 = real_module_n<R,T,TT,TTT,TTTT>;

}