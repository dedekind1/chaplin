#pragma once

#include "cpn_polynomial_group.h"

namespace cpn
{

template<ring_model T, typename Order, typename Allocator>
struct operator_functor<polynomial<T,Order,Allocator>,operator_tags<mod_operation>>
{
    PUBLISH_OPERATION_PROPERTIES(operator_functor,mod_operation,commutative,associative,distributive);

	typedef T r_model;
	static constexpr auto identity = algebraic_operator<T,ring_operation>::identity;

	TEMPLATE(ring_type TT, typename ... Operations)
	REQUIRES(implements_model<TT,T>)
	friend inline auto operator^(const TT& i_lhs, const algebraic_structure<polynomial<T,Order,Allocator>,Operations...>& i_rhs)
	{
        polynomial<T,Order,Allocator> res;

        [&res,&i_lhs](auto&& ii_monomial)
        {
            res.push(i_lhs ^ ii_monomial);
        } <<= i_rhs;

        return res;
	}
	TEMPLATE(ring_type TT, typename ... Operations)
	REQUIRES(implements_model<TT,T>)
	friend inline auto operator^(const algebraic_structure<polynomial<T,Order,Allocator>,Operations...>& i_lhs, const TT& i_rhs)
	{
        polynomial<T,Order,Allocator> res;

        [&res,&i_rhs](auto&& ii_monomial)
        {
            res.push(ii_monomial ^ i_rhs);
        } <<= i_lhs;

        return res;
	}
};

}