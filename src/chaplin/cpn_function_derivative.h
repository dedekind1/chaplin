#pragma once

#include "cpn_function.h"
#include "cpn_function_operators.h"

namespace cpn
{
namespace detail
{

template<typename,typename,typename>
struct builtin_derivative_function;
template<group_model Im, metric_space_model Dom, typename Allocator, typename Function>
struct builtin_derivative_function<Im(const Dom&),Allocator,Function> : public builtin_function<builtin_derivative_function<Im(const Dom&),Allocator,Function>,Im,Dom,Allocator>
{
public:
    TEMPLATE(typename FFunction)
    REQUIRES(ddk::is_constructible<Function,FFunction>)
    builtin_derivative_function(FFunction&& i_callable, const derivative_context<Dom>& i_context, size_t i_order);

    const Function& callable() const;
    size_t order() const;

private:
    final_object<Im> operator()(final_object<const Dom&> i_args) const final override;
    final_object<Im> operator()(final_object<const Dom&> i_args, size_t i_order) const;
    template<typename Callable>
    static auto order_derivative(Callable&& i_callable, size_t i_order);

    const Function m_callable;
    const derivative_context<Dom> m_context;
    size_t m_order;
};

}

TEMPLATE(group_model Im, metric_space_model Dom, typename Allocator, typename Function)
REQUIRES(module_modellable<Im,typename algebraic_operator<Dom,metric_operation>::valuation_type>)
inline function<Im(const Dom&),Allocator> derivate(const derivative_context<Dom>& i_context, Function&& i_function);

TEMPLATE(group_model Im, metric_space_model Dom, typename Allocator, typename Function)
REQUIRES(module_modellable<Im,typename algebraic_operator<Dom,metric_operation>::valuation_type>)
inline function<Im(const Dom&),Allocator> derivate(const derivative_context<Dom>& i_context, const detail::builtin_derivative_function<Im(const Dom&),Allocator,Function>& i_function);

}

#include "cpn_function_derivative.inl"
#include "cpn_builtin_function_derivatives.h"