
namespace cpn
{
namespace detail
{

template<typename Function>
bool function_compare_by_id<Function>::operator()(const Function& i_rhs, const Function& i_lhs) const
{
    return i_lhs->id() < i_rhs->id();
}

}
}