#pragma once

#include "cpn_algebraic_defs.h"
#include "cpn_type_concepts.h"
#include "ddkFramework/ddk_function_concepts.h"

namespace cpn
{
namespace concepts
{

template<typename T, typename OperationTag>
struct _contains_algebraic_structure
{
private:
    template<typename TT, typename = decltype(resolve_operation(std::declval<T>(),std::declval<TT>()))>
    static std::true_type resolve(const TT&);
    static std::false_type resolve(...);

public:
    static const bool value = decltype(resolve(std::declval<OperationTag>()))::value;
};

template<typename T, typename ... TT>
constexpr inline bool contains_algebraic_structure = (_contains_algebraic_structure<T,TT>::value && ...);

template<typename T, typename TT>
using algebraic_structure_properties = decltype(operation_properties(std::declval<T>(),std::declval<TT>()));

template<typename...>
struct operator_tags_acc;

template<>
struct operator_tags_acc<>
{
public:
    typedef ddk::mpl::empty_type_pack descend_type;
    typedef ddk::mpl::empty_type_pack type;
    static const size_t size = 0;
};

template<typename FreeOperatorTag, typename ... FreeOperatorTags>
struct operator_tags_acc<FreeOperatorTag,FreeOperatorTags...>
{
private:
    template<typename FixedOpsTags>
    struct constructor
    {
        template<typename OperatorTag>
        using merged_ops_tags = typename FixedOpsTags::template add<OperatorTag>::type;

        template<typename ... OperatorTags>
        static ddk::mpl::type_pack<merged_ops_tags<OperatorTags>...> local_construct(const operator_tags<OperatorTags...>&);

        template<typename OperatorTag,typename ... OperatorTags>
        static typename constructor<merged_ops_tags<OperatorTag>>::template type<OperatorTags...> nested_construct(const operator_tags<OperatorTag,OperatorTags...>&);
        static empty_operator_tags nested_construct(const empty_operator_tags&);

        template<typename ... OperatorTags>
        using local_type = decltype(local_construct(std::declval<operator_tags<OperatorTags...>>()));
        template<typename ... OperatorTags>
        using nested_type = decltype(nested_construct(std::declval<operator_tags<OperatorTags...>>()));
        template<typename ... OperatorTags>
        using type = typename ddk::mpl::merge_type_packs<local_type<OperatorTags...>,nested_type<OperatorTags...>>::type;
    };

    typedef typename constructor<empty_operator_tags>::template local_type<FreeOperatorTag,FreeOperatorTags...> local_type;
    typedef typename constructor<empty_operator_tags>::template nested_type<FreeOperatorTag,FreeOperatorTags...> nested_type;

public:
    typedef typename ddk::mpl::merge_type_packs<nested_type,typename operator_tags_acc<FreeOperatorTags...>::descend_type>::type descend_type;
    typedef typename ddk::mpl::merge_type_packs<local_type,descend_type>::type type;
    static const size_t size = type::size();
};

template<typename>
struct holds_operations_impl;

template<typename T>
struct holds_operations_impl
{
private:
    template<typename ... Operators>
    static algebraic_operators<Operators...> resolve(const algebraic_operators<Operators...>&);
    template<typename ... Operators>
    static algebraic_operators<Operators...> resolve(const algebraic_operators<algebraic_operators<Operators...>>&);
    static no_op resolve(...);

public:
	typedef decltype(resolve(std::declval<T>())) type;
    static const bool value = (std::is_same<type,no_op>::value == false);
};

template<typename>
struct holds_structure_impl;

template<typename T>
struct holds_structure_impl
{
private:
    template<typename TT, typename ... Operators>
    static algebraic_structure<TT,Operators...> resolve(const algebraic_structure<TT,Operators...>&);
    static no_op resolve(...);

public:
	typedef decltype(resolve(std::declval<T>())) type;
    static const bool value = (std::is_same<type,no_op>::value == false);
};

//remove any type-like data from operation itself or from any of its dependant types
template<typename T>
struct normalize_operation
{
private:
    template<typename TT>
    static TT normalize(const TT&);
    template<typename ... TT>
    static typename algebraic_operators<decltype(normalize(std::declval<TT>()))...>::operators normalize(const algebraic_operators<TT...>&);
    template<typename TT, typename ... TTT>
    static decltype(normalize(std::declval<typename algebraic_operators<TTT...>::operators>())) normalize(const algebraic_structure<TT,TTT...>&);
    template<typename TT, typename ... TTT>
    static prod_operation<TT,decltype(normalize(std::declval<TTT>()))...> normalize(const prod_operation<TT,TTT...>&);
    template<typename TT, typename TTT, size_t ... Dims>
    static pow_operation<TT,decltype(normalize(std::declval<TTT>())),Dims...> normalize(const pow_operation<TT,TTT,Dims...>&);
    template<typename TT, typename ... TTT>
    static sum_operation<TT,decltype(normalize(std::declval<TTT>()))...> normalize(const sum_operation<TT,TTT...>&);
    template<template<typename ...> typename TT, typename ... TTT>
    static TT<decltype(normalize(std::declval<TTT>()))...> normalize(const TT<TTT...>&);

public:
    typedef decltype(normalize(std::declval<T>())) type;
};

}

template<typename Op>
using normalized_operation = typename concepts::normalize_operation<Op>::type;
template<typename T, typename TT>
using algebraic_operator = decltype(resolve_operation(std::declval<T>(),std::declval<TT>()));
template<typename T>
using algebraic_structure_operators = typename concepts::holds_operations_impl<T>::type;
template<typename T>
using algebraic_structure_operators_tags = typename algebraic_structure_operators<T>::_operator_tags;

template<typename T>
concept model = concepts::holds_operations_impl<T>::value;
template<typename T>
concept type = concepts::holds_structure_impl<T>::value;

template<typename T>
concept set_model = model<T> && concepts::contains_algebraic_structure<T,set_operation>;
template<typename T>
concept set_type = set_model<T> && type<T> && equally_comparable_type<T>;

template<typename T>
concept flat_model = set_model<T> && (concepts::contains_algebraic_structure<T,proj_operation> == false);
template<typename T>
concept flat_type = set_type<T> && flat_model<T>;

template<typename T>
concept intersection_model = set_model<T> && concepts::contains_algebraic_structure<T,proj_operation>;
template<typename T>
concept intersection_type = intersection_model<T> && set_type<T>;

template<typename T>
concept complete_space_model = set_model<T> && concepts::contains_algebraic_structure<T,point_conv_operation>;
template<typename T>
concept complete_space_type = set_type<T> && complete_space_model<T>;

template<typename T>
concept metric_space_model = set_model<T> && concepts::contains_algebraic_structure<T,metric_operation>;
template<typename T>
concept metric_space_type = set_type<T> && metric_space_model<T>;

template<typename T>
concept semi_group_model = set_model<T> && concepts::contains_algebraic_structure<T,semi_group_operation>;
template<typename T>
concept semi_group_type = set_type<T> && closed_additive_type<T> && semi_group_model<T>;

template<typename T>
concept group_model = semi_group_model<T> && concepts::contains_algebraic_structure<T,group_operation>;
template<typename T>
concept group_type = semi_group_type<T> && closed_inverse_additive_type<T> && group_model<T>;

template<typename T>
concept semi_ring_model = semi_group_model<T> && concepts::contains_algebraic_structure<T,ring_operation>;
template<typename T>
concept semi_ring_type = semi_group_type<T> && closed_multiplicative_type<T> && semi_ring_model<T>;

template<typename T>
concept ring_model = group_model<T> && semi_ring_model<T>;
template<typename T>
concept ring_type = group_type<T> && semi_ring_type<T>;

template<typename T>
concept field_model = ring_model<T> && concepts::contains_algebraic_structure<T,field_operation>;
template<typename T>
concept field_type = ring_type<T> && closed_divisible_type<T> && field_model<T>;

template<typename T>
concept module_model = group_model<T> && concepts::contains_algebraic_structure<T,mod_operation>;
template<typename T>
concept module_type = group_type<T> && module_model<T>;

template<typename T>
concept free_module_model = module_model<T> && concepts::contains_algebraic_structure<T,basis_operation>;
template<typename T>
concept free_module_type = free_module_model<T> && set_type<T>;

template<typename T>
concept vector_space_model = free_module_model<T> && concepts::contains_algebraic_structure<T,vector_operation>;
template<typename T>
concept vector_space_type = free_module_type<T> && vector_space_model<T>;

template<typename T>
concept algebra_model = module_model<T> && concepts::contains_algebraic_structure<T,vector_operation>;
template<typename T>
concept algebra_type = module_type<T> && algebra_model<T>;

template<typename T>
concept function_type = ddk::is_callable<T> && module_type<T>;

template<typename T, typename TT>
inline constexpr bool is_super_structure_of = T::template contains<TT>();

template<typename T, typename TT>
inline constexpr bool is_not_super_structure_of = T::template contains<TT>();

template<typename T, typename TT>
inline constexpr bool is_sub_structure_of = TT::template contains<T>();

template<typename T, typename TT>
inline constexpr bool is_not_sub_structure_of = TT::template contains<T>();

template<typename T, typename TT, typename TTT>
constexpr inline bool holds_operation_property = concepts::algebraic_structure_properties<T,TT>::template contains<TTT>();

}
