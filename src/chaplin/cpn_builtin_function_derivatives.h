#pragma once

namespace cpn
{

template<set_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const sin_t& i_function);

template<set_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const cos_t& i_function);

template<set_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const tan_t& i_function);

template<set_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const cotan_t& i_function);

template<set_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const sec_t& i_function);

template<set_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const cosec_t& i_function);

template<set_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const asin_t& i_function);

template<set_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const acos_t& i_function);

template<set_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const atan_t& i_function);

template<set_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const exp_t& i_function);

template<set_model Im, set_model Dom, typename Allocator>
inline function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const ln_t& i_function);

}

#include "cpn_builtin_function_derivatives.inl"