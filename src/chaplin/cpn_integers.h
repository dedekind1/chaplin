#pragma once

#include "cpn_integer_ring.h"
#include "cpn_integer_algebra.h"

namespace cpn
{

using integers = integer_ring;

template<size_t ... Dims>
using integers_n = pow_struct<integers,Dims...>;

using integers_1 = integers_n<1>;
using integers_2 = integers_n<2>;
using integers_3 = integers_n<3>;
using integers_4 = integers_n<4>;

}

#include "cpn_integer_vector_space.h"