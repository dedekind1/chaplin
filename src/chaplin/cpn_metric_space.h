#pragma once

#include "cpn_type_concepts.h"
#include "cpn_algebraic_concepts.h"
#include "cpn_complete_space.h"
#include "cpn_real_field.h"

namespace cpn
{

struct real_valued_metric
{
	typedef real_field<float> valuation_type;

	TEMPLATE(flat_type T, flat_type TT)
	REQUIRES(group_type<T>,group_type<TT>)
	inline valuation_type operator()(const T& i_lhs,const TT& i_rhs) const
	{
		const valuation_type res = i_lhs - i_rhs;

		return (resolve(res) > 0.f) ? res.number() : -res.number() ;
	}
	template<group_type T>
	inline auto operator()(const T& i_lhs,const valuation_type& i_delta) const
	{
		return i_lhs + T{ i_delta };
	}
};

struct euclidean_metric : real_valued_metric
{
	using real_valued_metric::operator();
	TEMPLATE(intersection_type T, intersection_type TT)
	REQUIRES(T::rank() == TT::rank())
	inline valuation_type operator()(const T& i_lhs,const TT& i_rhs) const
	{
		typedef typename ddk::mpl::make_sequence<0,T::rank()>::type seq_t;

		return metric(seq_t{},i_lhs,i_rhs);
	}
	template<intersection_type T>
	inline auto operator()(const T& i_lhs,const valuation_type& i_rhs) const
	{
		typedef typename ddk::mpl::make_sequence<0,T::rank()>::type seq_t;

		return transport(seq_t{},i_lhs,i_rhs);
	}

private:
	template<size_t ... Indexs, vector_space_type T, vector_space_type TT>
	static inline valuation_type metric(const ddk::mpl::sequence<Indexs...>&, const T& i_lhs,const TT& i_rhs)
	{
		const auto diff = i_lhs - i_rhs;

		return std::sqrt(resolve(((project<Indexs>(diff) * project<Indexs>(diff)) + ...)));
	}
	template<size_t ... Indexs, vector_space_type T>
	static inline auto transport(const ddk::mpl::sequence<Indexs...>&, const T& i_lhs,const valuation_type& i_rhs)
	{
		return std::sqrt(resolve(((project<Indexs>(i_lhs) + i_rhs) + ...)));
	}
};

template<set_model T, typename Metric>
struct metric_space_operation : point_convergence_operation<T,Metric>
{
	PUBLISH_OPERATION_PROPERTIES(metric_space_operation,metric_operation);

	typedef Metric metric_t;
	typedef typename Metric::valuation_type valuation_type;

	static_assert(solvable_type<valuation_type>,"You shall provide through Metric of a solvable type");

	TEMPLATE(set_type TT, set_type TTT)
	REQUIRES(implements_model<TT,T>,implements_model<TTT,T>,ddk::is_callable_by<Metric,TT,TTT>)
	friend inline valuation_type distance(const TT& i_lhs,const TTT& i_rhs)
	{
		static const Metric s_metric;

		return s_metric(i_lhs,i_rhs);
	}
};

template<metric_space_model T, size_t ... Dims>
struct pow_operation<metric_operation,T,Dims...>
{
    PUBLISH_OPERATION_PROPERTIES(pow_operation,metric_operation);

	typedef typename algebraic_operator<T,metric_operation>::metric_t metric_t;
	typedef typename algebraic_operator<T,metric_operation>::valuation_type valuation_type;

	static_assert(solvable_type<valuation_type>,"You shall provide through Metric of a solvable type");

	TEMPLATE(metric_space_type TT, metric_space_type TTT, typename ... Operations)
	REQUIRES(implements_operation<metric_operation,TT,T>,implements_operation<metric_operation,TTT,T>)
	friend inline valuation_type distance(const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_lhs,const algebraic_structure<pow_type<TTT,Dims...>,Operations...>& i_rhs)
	{
		static const metric_t s_metric;

		return s_metric(i_lhs,i_rhs);
	}
};

template<metric_space_model ... T>
struct prod_operation<metric_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(prod_operation,metric_operation);

	typedef ddk::mpl::homogeneous_type<typename algebraic_operator<T,metric_operation>::metric_t ...> metric_t;
	typedef ddk::mpl::homogeneous_type<typename algebraic_operator<T,metric_operation>::valuation_type ...> valuation_type;

	static_assert(solvable_type<valuation_type>,"You shall provide through Metric of a solvable type");

	TEMPLATE(metric_space_type ... TT, metric_space_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<metric_operation,prod_type<TT...>,T...>,implements_operation<metric_operation,prod_type<TTT...>,T...>)
	friend inline valuation_type distance(const algebraic_structure<prod_type<TT...>,Operations...>& i_lhs,const algebraic_structure<prod_type<TTT...>,Operations...>& i_rhs)
	{
		static const metric_t s_metric;

		return s_metric(i_lhs,i_rhs);
	}
};

template<set_model Model, typename Metric>
using MetricSpace = typename Model::template embed<metric_space_operation<Model,Metric>>;
template<set_type Structure, typename Metric, typename Set = typename Structure::value_type>
using metric_space = algebraic_structure<Set,MetricSpace<Structure,Metric>>;

}