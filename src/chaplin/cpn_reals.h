#pragma once

#include "cpn_real_field.h"
#include "cpn_real_algebra.h"
#include "cpn_metric_space.h"

namespace cpn
{

using Reals = MetricSpace<RealField,euclidean_metric>;

template<solvable_literal_type T>
using reals = algebraic_structure<real<T>,Reals>;

template<size_t ... Dims>
using Reals_n = pow_model<Reals,Dims...>;

using Reals1 = Reals_n<1>;
using Reals2 = Reals_n<2>;
using Reals3 = Reals_n<3>;
using Reals4 = Reals_n<4>;

template<solvable_literal_type ... T>
using reals_n = prod_struct<reals<T>...>;

template<solvable_literal_type T>
using reals1 = reals_n<T>;
template<solvable_literal_type T, solvable_literal_type TT>
using reals2 = reals_n<T,TT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT>
using reals3 = reals_n<T,TT,TTT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, solvable_literal_type TTTT>
using reals4 = reals_n<T,TT,TTT,TTTT>;

using R = Module<Reals,real_module_operation<Reals>>;
template<solvable_literal_type T>
using r = module<reals<T>,real_module_operation<Reals>,real<T>>;

}

#include "cpn_real_vector_space.h"