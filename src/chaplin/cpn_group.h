#pragma once

#include "cpn_set.h"
#include "cpn_algebraic_structure.h"
#include "ddkFramework/ddk_iterable.h"

namespace cpn
{

template<set_model Model, typename AddOperation>
using SemiGroup = typename Model::template embed<AddOperation>;

template<set_type Structure,typename AddOperation, typename Set = typename Structure::value_type>
using semi_group = algebraic_structure<Set,SemiGroup<Structure,AddOperation>>;

template<semi_group_model Model, typename AddInvOperation>
using Group = typename Model::template embed<AddInvOperation>;

template<semi_group_type Structure, typename AddInvOperation, typename Set = typename Structure::value_type>
using group = algebraic_structure<Set,Group<Structure,AddInvOperation>>;

template<semi_group_model T, size_t ... Dims>
struct pow_operation<semi_group_operation,T,Dims...>
{
    PUBLISH_OPERATION_PROPERTIES(pow_operation,semi_group_operation);

	static constexpr auto identity = pow_forward<Dims...>(algebraic_operator<T,semi_group_operation>::identity);

	template<size_t ... Indexs, semi_group_type TT, semi_group_type TTT>
	static auto sum_array(const ddk::mpl::sequence<Indexs...>&, const pow_type<TT,Dims...>& i_lhs,const pow_type<TTT,Dims...>& i_rhs)
	{
		typedef pow_type<decltype(std::declval<TT>() + std::declval<TTT>()),Dims...> pow_t;

		return 	pow_t{ i_lhs.at(Indexs) + i_rhs.at(Indexs) ... };
	}
	TEMPLATE(semi_group_type TT, semi_group_type TTT, typename ... Operations)
	REQUIRES(implements_operation<semi_group_operation,TT,T>,implements_operation<semi_group_operation,TTT,T>)
	friend inline auto operator+(const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_lhs, const algebraic_structure<pow_type<TTT,Dims...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,ddk::mpl::prod_ranks<Dims...>>::type seq_t;
		typedef decltype(sum_array(seq_t{},i_lhs,i_rhs)) pow_t;

		return algebraic_structure<pow_t,Operations...>{ sum_array(seq_t{},i_lhs,i_rhs) };
	}
};

template<group_model T, size_t ... Dims>
struct pow_operation<group_operation,T,Dims...>
{
	PUBLISH_OPERATION_PROPERTIES(pow_operation,group_operation,concepts::algebraic_structure_properties<T,group_operation>);

	template<size_t ... Indexs, semi_group_type TT>
	static auto inv_array(const ddk::mpl::sequence<Indexs...>&, const pow_type<TT,Dims...>& i_rhs)
	{
		typedef pow_type<decltype(-std::declval<TT>()),Dims...> pow_t;

		return 	pow_t{ -i_rhs.at(Indexs) ... };
	}
	template<size_t ... Indexs, semi_group_type TT, semi_group_type TTT>
	static auto subs_array(const ddk::mpl::sequence<Indexs...>&, const pow_type<TT,Dims...>& i_lhs,const pow_type<TTT,Dims...>& i_rhs)
	{
		typedef pow_type<decltype(std::declval<TT>() - std::declval<TTT>()),Dims...> pow_t;

		return 	pow_t{ i_lhs.at(Indexs) - i_rhs.at(Indexs) ... };
	}
	TEMPLATE(group_type TT, typename ... Operations)
	REQUIRES(implements_operation<group_operation,TT,T>)
	friend inline auto operator-(const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,ddk::mpl::prod_ranks<Dims...>>::type seq_t;
		typedef decltype(inv_array(seq_t{},i_rhs)) pow_t;

		return algebraic_structure<pow_t,Operations...>{ inv_array(seq_t{},i_rhs) };
	}
	TEMPLATE(group_type TT, group_type TTT, typename ... Operations)
	REQUIRES(implements_operation<group_operation,TT,T>,implements_operation<group_operation,TTT,T>)
	friend inline auto operator-(const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_lhs,const algebraic_structure<pow_type<TTT,Dims...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,ddk::mpl::prod_ranks<Dims...>>::type seq_t;
		typedef decltype(subs_array(seq_t{},i_lhs,i_rhs)) pow_t;

		return algebraic_structure<pow_t,Operations...>{ subs_array(seq_t{},i_lhs,i_rhs) };
	}
};

template<semi_group_model ... T>
struct prod_operation<semi_group_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(prod_operation,semi_group_operation);

	static const size_t s_num_types = ddk::mpl::num_types<T...>;
	static constexpr auto identity = prod_type{ algebraic_operator<T,semi_group_operation>::identity... };

	template<size_t ... Indexs, semi_group_type ... TT, semi_group_type ... TTT>
	static auto sum_tuples(const ddk::mpl::sequence<Indexs...>&, const prod_type<TT...>& i_lhs,const prod_type<TTT...>& i_rhs)
	{
		return 	prod_type{ i_lhs.template get<Indexs>() + i_rhs.template get<Indexs>() ... };
	}
	TEMPLATE(semi_group_type ... TT, semi_group_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<semi_group_operation,prod_type<TT...>,T...>,implements_operation<semi_group_operation,prod_type<TTT...>,T...>)
	friend inline auto operator+(const algebraic_structure<prod_type<TT...>,Operations...>& i_lhs,const algebraic_structure<prod_type<TTT...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,s_num_types>::type seq_t;
		typedef decltype(sum_tuples(seq_t{},i_lhs,i_rhs)) prod_t;

		return algebraic_structure<prod_t,Operations...>{ sum_tuples(seq_t{},i_lhs,i_rhs) };
	}
};

template<group_model ... T>
struct prod_operation<group_operation,T...>
{
	PUBLISH_OPERATION_PROPERTIES(prod_operation,group_operation);

	static const size_t s_num_types = ddk::mpl::num_types<T...>;

	template<size_t ... Indexs, group_type ... TT, group_type ... TTT>
	static auto subs_tuples(const ddk::mpl::sequence<Indexs...>&, const prod_type<TT...>& i_lhs,const prod_type<TTT...>& i_rhs)
	{
		return 	prod_type{ i_lhs.template get<Indexs>() - i_rhs.template get<Indexs>() ...};
	}
	template<size_t ... Indexs, group_type ... TT>
	static auto inv_tuple(const ddk::mpl::sequence<Indexs...>&, const prod_type<TT...>& i_rhs)
	{
		return 	prod_type{ -i_rhs.template get<Indexs>() ...};
	}

	TEMPLATE(group_type ... TT, typename ... Operations)
	REQUIRES(implements_operation<group_operation,prod_type<TT...>,T...>)
	friend inline auto operator-(const algebraic_structure<prod_type<TT...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,s_num_types>::type seq_t;
		typedef decltype(inv_tuple(seq_t{},i_rhs)) prod_t;

		return algebraic_structure<prod_t,Operations...>{ inv_tuple(seq_t{},i_rhs) };
	}
	TEMPLATE(group_type ... TT, group_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<group_operation,prod_type<TT...>,T...>,implements_operation<group_operation,prod_type<TTT...>,T...>)
	friend inline auto operator-(const algebraic_structure<prod_type<TT...>,Operations...>& i_lhs, const algebraic_structure<prod_type<TTT...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,s_num_types>::type seq_t;
		typedef decltype(subs_tuples(seq_t{},i_lhs,i_rhs)) prod_t;

		return algebraic_structure<prod_t,Operations...>{ subs_tuples(seq_t{},i_lhs,i_rhs) };
	}
};

template<semi_group_model ... T>
struct sum_operation<semi_group_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(sum_operation,semi_group_operation);

	template<typename Return>
	struct add_operation_visitor
	{
		template<semi_group_type T1, semi_group_type T2>
		Return operator()(T1&& i_lhs, T2&& i_rhs) const
		{
			return i_lhs + i_rhs;
		}
	};

	TEMPLATE(semi_group_type ... TT, semi_group_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<semi_group_operation,prod_type<TT...>,T...>,implements_operation<semi_group_operation,prod_type<TTT...>,T...>)
	friend inline auto operator+(const algebraic_structure<sum_type<TT...>,Operations...>& i_lhs,const algebraic_structure<sum_type<TTT...>,Operations...>& i_rhs)
	{
		typedef sum_type<decltype(std::declval<TT>() + std::declval<TTT>())...> sum_type_t;

		return algebraic_structure<sum_type_t,Operations...>{ ddk::visit<add_operation_visitor<sum_type_t>>(i_lhs,i_rhs) };
	}
};

template<group_model ... T>
struct sum_operation<group_operation,T...>
{
	PUBLISH_OPERATION_PROPERTIES(sum_operation,group_operation);

	template<typename Return>
	struct inverse_operation_visitor
	{
		template<typename T1,typename T2>
		Return operator()(T1&& i_lhs,T2&& i_rhs) const
		{
			return -i_lhs;
		}
	};
	template<typename Return>
	struct subs_operation_visitor
	{
		template<typename T1,typename T2>
		Return operator()(T1&& i_lhs,T2&& i_rhs) const
		{
			return i_lhs - i_rhs;
		}
	};

	TEMPLATE(group_type ... TT, typename ... Operations)
	REQUIRES(implements_operation<group_operation,prod_type<TT...>,T...>)
	friend inline auto operator-(const algebraic_structure<sum_type<TT...>,Operations...>& i_rhs)
	{
		typedef sum_type<decltype(-std::declval<TT>())...> sum_type_t;

		return algebraic_structure<sum_type_t,Operations...>{ ddk::visit<inverse_operation_visitor<sum_type_t>>(i_rhs) };
	}
	TEMPLATE(group_type ... TT, group_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<group_operation,prod_type<TT...>,T...>,implements_operation<group_operation,prod_type<TTT...>,T...>)
	friend inline auto operator-(const algebraic_structure<sum_type<TT...>,Operations...>& i_lhs,const algebraic_structure<sum_type<TTT...>,Operations...>& i_rhs)
	{
		typedef sum_type<decltype(std::declval<TT>() - std::declval<TTT>())...> sum_type_t;

		return algebraic_structure<sum_type_t,Operations...>{ ddk::visit<subs_operation_visitor<sum_type_t>>(i_lhs,i_rhs) };
	}
};

template<model T>
using as_group = infer_model<T,group_operation>;

}

#include "cpn_group.inl"