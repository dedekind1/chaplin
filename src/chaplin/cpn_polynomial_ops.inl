
namespace cpn
{

template<ring_model T, typename Order, typename Allocator>
monomial_bases<T,Order,Allocator> operator+(const monomial_bases<T,Order,Allocator>& i_lhs, const monomial_bases<T,Order,Allocator>& i_rhs)
{
    monomial_bases<T,Order,Allocator> res(i_lhs);

    res.insert(i_rhs.begin(),i_rhs.end());

    return res;
}

template<group_type T>
auto operator-(const monomial<T>& i_rhs)
{
    return monomial{-i_rhs.coeff(),i_rhs.exponents()};
}
TEMPLATE(group_type T, typename Allocator, group_type TT)
REQUIRED(additive_type<T,TT>)
auto operator+(const monomial<T,Allocator>& i_lhs, const TT& i_rhs)
{
    return i_lhs + monomial{i_rhs};
}
TEMPLATE(group_type T, group_type TT, typename AAllocator)
REQUIRED(additive_type<T,TT>)
auto operator+(const T& i_lhs, const monomial<TT,AAllocator>& i_rhs)
{
    return monomial{i_lhs} + i_rhs;
}
TEMPLATE(group_type T, typename Allocator, group_type TT, typename AAllocator)
REQUIRED(additive_type<T,TT>,compatible_models<T,TT>)
auto operator+(const monomial<T,Allocator>& i_lhs, const monomial<TT,AAllocator>& i_rhs)
{
    return polynomial<forget_type<T>>{i_lhs,i_rhs};
}
TEMPLATE(group_type T, typename Allocator, group_type TT)
REQUIRED(substractive_type<T,TT>)
auto operator-(const monomial<T,Allocator>& i_lhs, const TT& i_rhs)
{
    return i_lhs - monomial{i_rhs};
}
template<group_type T, group_type TT, typename AAllocator>
REQUIRED(substractive_type<T,TT>)
auto operator-(const T& i_lhs, const monomial<TT,AAllocator>& i_rhs)
{
    return monomial{i_lhs} - i_rhs;
}
TEMPLATE(group_type T, typename Allocator, group_type TT, typename AAllocator)
REQUIRED(substractive_type<T,TT>,compatible_models<T,TT>)
auto operator-(const monomial<T,Allocator>& i_lhs, const monomial<TT,AAllocator>& i_rhs)
{
    return polynomial<forget_type<T>>{i_lhs,-i_rhs};
}
template<module_type T, module_type TT>
auto operator^(const T& i_lhs, const monomial<TT>& i_rhs)
{
    return monomial{i_lhs ^ i_rhs.coeff(),i_rhs.exponents(),i_rhs.bases()};
}
template<module_type T, module_type TT>
auto operator^(const monomial<T>& i_lhs, const TT& i_rhs)
{
    return monomial{i_lhs.coeff() ^ i_rhs,i_rhs.exponents(),i_rhs.bases()};
}
TEMPLATE(ring_type T, typename Allocator, ring_type TT)
REQUIRED(multiplicative_type<T,TT>)
auto operator*(const monomial<T,Allocator>& i_lhs, const TT& i_rhs)
{
    return i_lhs * monomial{i_rhs};
}
TEMPLATE(ring_type T, ring_type TT, typename AAllocator)
REQUIRED(multiplicative_type<T,TT>)
auto operator*(const T& i_lhs, const monomial<TT,AAllocator>& i_rhs)
{
    return monomial{i_lhs} * i_rhs;
}
TEMPLATE(ring_type T, typename Allocator, ring_type TT, typename AAllocator)
REQUIRED(multiplicative_type<T,TT>,compatible_models<T,TT>)
auto operator*(const monomial<T,Allocator>& i_lhs, const monomial<TT,AAllocator>& i_rhs)
{
    return monomial{i_lhs.coeff() * i_rhs.coeff(),i_lhs.exponents() + i_rhs.exponents(),i_lhs.bases() + i_rhs.bases()};
}
TEMPLATE(field_type T, typename Allocator, field_type TT)
REQUIRED(divisible_type<T,TT>)
auto operator/(const monomial<T,Allocator>& i_lhs, const TT& i_rhs)
{
    return i_lhs / monomial{i_rhs};
}
TEMPLATE(field_type T, field_type TT, typename AAllocator)
REQUIRED(divisible_type<T,TT>)
auto operator/(const T& i_lhs, const monomial<TT,AAllocator>& i_rhs)
{
    return monomial{i_lhs} * i_rhs;
}
TEMPLATE(field_type T, typename Allocator, field_type TT, typename AAllocator)
REQUIRED(divisible_type<T,TT>,compatible_models<T,TT>)
auto operator/(const monomial<T,Allocator>& i_lhs, const monomial<TT,AAllocator>& i_rhs)
{
    return monomial{i_lhs.coeff() / i_rhs.coeff(),i_lhs.exponents() - i_rhs.exponents(),i_lhs.bases() + i_rhs.bases()};
}

template<ring_model T, typename Order, typename Allocator, numeric_type TT>
auto instance_polynomial(const builtin_literal_expression<TT>& i_exp)
{
    typedef final_object<T> ring_t;

    polynomial<T,Order,Allocator> res;

    res.push(monomial{ring_t{ i_exp.get() },{}});

    return res;
}
template<ring_model T, typename Order, typename Allocator>
auto instance_polynomial(const builtin_incognita_expression& i_exp)
{
    typedef final_object<T> ring_t;

    polynomial<T,Order,Allocator> res;

    res.push(monomial{ring_t{ algebraic_operator<T,ring_operation>::identity },{{0,1}}});

    return res;
}
template<ring_model T, typename Order, typename Allocator, size_t Comp>
auto instance_polynomial(const builtin_component_expression<Comp>& i_exp)
{
    typedef final_object<T> ring_t;

    polynomial<T,Order,Allocator> res;

    res.push(monomial{ring_t{ algebraic_operator<T,ring_operation>::identity },{{Comp,1}}});

    return res;
}
TEMPLATE(ring_model T, typename Order, typename Allocator, typename Expression)
REQUIRED(is_polynomial_instantiable_by<Expression,T,Order,Allocator>)
auto instance_polynomial(const builtin_minus_expression<Expression>& i_exp)
{
    polynomial<T,Order,Allocator> res{ instance_polynomial<T,Order,Allocator>(i_exp.get()) };

    [](auto&& ii_mono) noexcept
    {
        ii_mono.set_coeff(-ii_mono.coeff());
    } <<= res;

    return res;
}
TEMPLATE(ring_model T, typename Order, typename Allocator, size_t ...Indexs, literal_expression ... Expressions)
REQUIRED(is_polynomial_instantiable_by<Expressions,T,Order,Allocator>...)
auto instance_polynomial(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_exp)
{
    polynomial<T,Order,Allocator> res;

    [&](auto&& ii_exp) noexcept
    {
        res.push(instance_polynomial<T,Order,Allocator>(ii_exp));
    } <<= i_exp;

    return res;
}
TEMPLATE(ring_model T, typename Order, typename Allocator, size_t ...Indexs, literal_expression ... Expressions)
REQUIRED(is_polynomial_instantiable_by<Expressions,T,Order,Allocator>...)
auto instance_polynomial(const prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_exp)
{
    return (Polynomial<T>{ instance_polynomial<T,Order,Allocator>(i_exp.template get<Indexs>()) } * ...);
}
template<ring_model T, typename Order, typename Allocator, numeric_type TT>
auto instance_polynomial(const pow_nary_expression<ddk::mpl::sequence<0,1>,builtin_incognita_expression,builtin_literal_expression<TT>>& i_exp)
{
    typedef final_object<T> ring_t;

    polynomial<T,Order,Allocator> res;

    const auto& exponent = i_exp.template get<1>();

    res.push(monomial{ring_t{ algebraic_operator<T,ring_operation>::identity },{{0,exponent.get()}}});

    return res;
}
template<ring_model T, typename Order, typename Allocator, size_t Comp, numeric_type TT>
auto instance_polynomial(const pow_nary_expression<ddk::mpl::sequence<0,1>,builtin_component_expression<Comp>,builtin_literal_expression<TT>>& i_exp)
{
    typedef final_object<T> ring_t;

    polynomial<T,Order,Allocator> res;

    const auto& exponent = i_exp.template get<1>();

    res.push(monomial{ring_t{ algebraic_operator<T,ring_operation>::identity },{{Comp,exponent.get()}}});

    return res;
}
TEMPLATE(ring_model T, typename Order, typename Allocator, literal_expression RhsExpression, numeric_type TT)
REQUIRES(is_polynomial_instantiable_by<RhsExpression,T,Order,Allocator>)
auto instance_polynomial(const pow_nary_expression<ddk::mpl::sequence<0,1>,builtin_composed_expression<builtin_incognita_expression,RhsExpression>,builtin_literal_expression<TT>>& i_exp)
{
    const auto& base = i_exp.template get<0>();
    const auto& exponent = i_exp.template get<1>();

    return instance_polynomial<T,Order,Allocator>(base).shift(0,exponent.get());
}
TEMPLATE(ring_model T, typename Order, typename Allocator, size_t Comp, literal_expression RhsExpression, numeric_type TT)
REQUIRES(is_polynomial_instantiable_by<RhsExpression,T,Order,Allocator>)
auto instance_polynomial(const pow_nary_expression<ddk::mpl::sequence<0,1>,builtin_composed_expression<builtin_component_expression<Comp>,RhsExpression>,builtin_literal_expression<TT>>& i_exp)
{
    const auto& base = i_exp.template get<0>();
    const auto& exponent = i_exp.template get<1>();

    return instance_polynomial<T,Order,Allocator>(base).shift(Comp,exponent.get());
}
TEMPLATE(ring_model T, typename Order, typename Allocator, literal_expression RhsExpression)
REQUIRED(is_polynomial_instantiable_by<RhsExpression,T,Order,Allocator>)
auto instance_polynomial(const builtin_composed_expression<builtin_incognita_expression,RhsExpression>& i_exp)
{
    typedef final_object<T> ring_t;

    return polynomial<T,Order,Allocator>{ __monomial_component<ring_t,Allocator>(0).rebase(0,instance_polynomial<T,Order,Allocator>(i_exp.rhs())) };

}
TEMPLATE(ring_model T, typename Order, typename Allocator, size_t Comp, literal_expression RhsExpression)
REQUIRED(is_polynomial_instantiable_by<RhsExpression,T,Order,Allocator>)
auto instance_polynomial(const builtin_composed_expression<builtin_component_expression<Comp>,RhsExpression>& i_exp)
{
    typedef final_object<T> ring_t;

    return polynomial<T,Order,Allocator>{ __monomial_component<ring_t,Allocator>(Comp).rebase(Comp,instance_polynomial<T,Order,Allocator>(i_exp.rhs())) };
}

}