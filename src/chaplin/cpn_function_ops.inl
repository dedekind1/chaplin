
#include "cpn_builtin_functions.h"
#include "cpn_algebraic_type_concepts.h"

namespace cpn
{
namespace detail
{

template<set_model Im, vector_space_model Dom, typename Allocator, size_t ... Indexs>
std::array<function<Im(const Dom&),Allocator>,Dom::rank()> derivative(const function<Im(const Dom&),Allocator>& i_func, const ddk::mpl::sequence<Indexs...>&)
{
	std::array<const derivative_context<Dom>,ddk::mpl::num_ranks<Indexs...>> _context = { derivative_context<Dom>(get_operator_tag_id<function_operators<function<Im(const Dom&),Allocator>>,derivative_operation>(),Dom::basis(Indexs))... };

	return { _context[Indexs].derivative(i_func)... };
}

}

template<set_model Im, metric_space_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> derivative(const function<Im(const Dom&),Allocator>& i_func)
{
	const derivative_context<Dom> _context{ get_operator_tag_id<function_operators<function<Im(const Dom&),Allocator>>,derivative_operation>() };

	return _context.derivative(i_func);
}
template<set_model Im, vector_space_model Dom, typename Allocator>
std::array<function<Im(const Dom&),Allocator>,Dom::rank()> derivative(const function<Im(const Dom&),Allocator>& i_func)
{
	typedef typename ddk::mpl::make_sequence<0,Dom::rank()>::type seq_t;

	return detail::derivative(i_func,seq_t{});
}
TEMPLATE(set_model Im, vector_space_model Dom, typename Allocator, vector_space_type DDom)
REQUIRED(implements_model<DDom,Dom>)
function<Im(const Dom&),Allocator> derivative(const function<Im(const Dom&),Allocator>& i_func, const DDom& i_direction)
{
	const derivative_context<Dom> _context{ get_operator_tag_id<function_operators<function<Im(const Dom&),Allocator>>,derivative_operation>(),i_direction };

	return _context.derivative(i_func);
}

template<semi_group_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> operator+(const function<Im(const Dom&),Allocator>& i_lhs, const function<Im(const Dom&),Allocator>& i_rhs)
{
	return i_lhs->binary_operator(get_operator_tag_id<function_operators<function<Im(const Dom&),Allocator>>,semi_group_operation>(),*i_rhs);
}
template<group_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> operator-(const function<Im(const Dom&),Allocator>& i_rhs)
{
	return i_rhs->unary_operator(get_operator_tag_id<function_operators<function<Im(const Dom&),Allocator>>,group_operation>());
}
template<semi_ring_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> operator*(const function<Im(const Dom&),Allocator>& i_lhs, const function<Im(const Dom&),Allocator>& i_rhs)
{
	return i_lhs->binary_operator(get_operator_tag_id<function_operators<function<Im(const Dom&),Allocator>>,ring_operation>(),*i_rhs);
}
template<field_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> operator/(const function<Im(const Dom&),Allocator>& i_lhs, const function<Im(const Dom&),Allocator>& i_rhs)
{
	return i_lhs->binary_operator(get_operator_tag_id<function_operators<function<Im(const Dom&),Allocator>>,field_operation>(),*i_rhs);
}

}
