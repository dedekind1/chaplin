#pragma once

#include "ddkFramework/ddk_type_id.h"
#include <type_traits>

#define PUBLISH_OPERATION_PROPERTIES(_Operation,_Tag,...) \
    typedef operator_tags<_Tag> local_operator_tag; \
    friend local_operator_tag _resolve_operators(const _Operation&, ...); \
    template<typename _OOperation> \
    using nested_operator_tag = decltype(_resolve_operators(std::declval<_OOperation>(),0)); \
    friend typename ddk::mpl::merge_pair_type_packs<nested_operator_tag<_Operation>,local_operator_tag>::type _resolve_operators(const _Operation&, int); \
    typedef typename ddk::mpl::merge_pair_type_packs<nested_operator_tag<_Operation>,local_operator_tag>::type __operator_tags; \
    friend _Operation resolve_operation(const _Operation&, const _Tag&); \
    friend typename ddk::mpl::type_pack<__VA_ARGS__> operation_properties(const _Operation&, const _Tag&);

namespace cpn
{

typedef ddk::Id<size_t,struct _operator_tag_id> operator_tag_id;

struct no_op;
struct proj_operation{};
struct set_operation{};
struct semi_group_operation{};
struct group_operation{};
struct ring_operation{};
struct field_operation{};
struct mod_operation{};
struct algebra_operation{};
struct basis_operation{};
struct vector_operation{};
struct conjugate_operation{};
struct point_conv_operation{};
struct metric_operation{};
struct coordinate_transform_operation{};
struct derivative_operation{};

struct unary;
struct binary;
struct associative;
struct distributive;
struct commutative;
struct cnstant;
struct linear;
struct conjugate;
struct positive_definite;
struct incognita;
struct partially_ordered;
struct totally_ordered;

template<typename ... OpTags>
using operator_tags = ddk::mpl::type_pack<OpTags...>;
typedef operator_tags<> empty_operator_tags;

template<typename T>
std::false_type resolve_operation_property(T&&, ...);

template<typename ...>
struct algebraic_operators;

template<typename,typename ...>
struct algebraic_structure;

template<typename>
struct initial_object_impl;

template<typename T>
using initial_object = ddk::mpl::apply_qualifiers<T,typename initial_object_impl<ddk::mpl::remove_qualifiers<T>>::type>;

template<typename>
struct final_object_impl;

template<typename T>
using final_object = ddk::mpl::apply_qualifiers<T,typename final_object_impl<ddk::mpl::remove_qualifiers<T>>::type>;

template<typename,typename,size_t...>
struct pow_operation
{
	PUBLISH_OPERATION_PROPERTIES(pow_operation,no_op);
};

template<typename,typename...>
struct prod_operation
{
	PUBLISH_OPERATION_PROPERTIES(prod_operation,no_op);
};

template<typename,typename...>
struct sum_operation
{    
	PUBLISH_OPERATION_PROPERTIES(sum_operation,no_op);
};

template<typename,typename,typename...>
struct infer_operation;

}