#pragma once

#include "cpn_rational_ring.h"
#include "cpn_module.h"

namespace cpn
{

template<ring_model R>
struct rational_module_operation
{
    PUBLISH_OPERATION_PROPERTIES(rational_module_operation,mod_operation,commutative,associative,distributive);

	typedef R r_model;
	static constexpr auto identity = algebraic_operator<R,ring_operation>::identity;

	TEMPLATE(ring_type RR, typename ... Operations)
	REQUIRES(implements_model<RR,r_model>)
	friend inline auto operator^(const RR& i_lhs,const algebraic_structure<rational,Operations...>& i_rhs)
	{
		return algebraic_structure<rational,Operations...>{ i_lhs.number() * i_rhs.number() };
	}
};

template<ring_model R = RationalRing>
using RationalModule = Module<RationalGroup,rational_module_operation<R>>;
template<ring_model R = RationalRing>
using rational_module = module<rational_group,rational_module_operation<R>>;

template<ring_model R, size_t ... Dims>
using RationalModule_n = pow_model<RationalModule<R>,Dims...>;

template<ring_model R = RationalRing>
using RationalModule_1 = RationalModule_n<R,1>;
template<ring_model R = RationalRing>
using RationalModule_2 = RationalModule_n<R,2>;
template<ring_model R = RationalRing>
using RationalModule_3 = RationalModule_n<R,3>;
template<ring_model R = RationalRing>
using RationalModule_4 = RationalModule_n<R,4>;

template<ring_model R, size_t ... Dims>
using rational_module_n = pow_struct<rational_module<R>,Dims...>;

template<ring_model R = RationalRing>
using rational_module_1 = rational_module_n<R,1>;
template<ring_model R = RationalRing>
using rational_module_2 = rational_module_n<R,2>;
template<ring_model R = RationalRing>
using rational_module_3 = rational_module_n<R,3>;
template<ring_model R = RationalRing>
using rational_module_4 = rational_module_n<R,4>;

}