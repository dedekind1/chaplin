#pragma once

#include "cpn_builtin_expressions.h"
#include "cpn_algebraic_function_concepts.h"
#include <vector>

#define BUILTIN_FUNCTION(_TYPE) \
struct callable_tag; \
friend inline builtin_function_id __builtin_function_id(const _TYPE&) \
{  \
    static const builtin_function_id _id = get_builtin_function_id<_TYPE>(); \
    \
    return _id; \
}

#define DEFINE_ARITHMETIC_NARY_OPERATION(_NAME,_OP,_CONCEPT) \
template<typename,typename> \
struct builtin_##_NAME##_nary_function; \
template<cpn::_CONCEPT Im, set_model Dom, typename Allocator> \
struct builtin_##_NAME##_nary_function<Im(const Dom&),Allocator> : public cpn::detail::builtin_nary_function<Im(const Dom&),Allocator>, \
                                                                  public builtin_function<builtin_##_NAME##_nary_function<Im(const Dom&),Allocator>,Im,Dom,Allocator> \
{ \
    using typename cpn::detail::builtin_nary_function<Im(const Dom&),Allocator>::const_iterator; \
    using typename cpn::detail::builtin_nary_function<Im(const Dom&),Allocator>::function_t; \
    \
public: \
    using cpn::detail::builtin_nary_function<Im(const Dom&),Allocator>::builtin_nary_function; \
    builtin_##_NAME##_nary_function(const builtin_##_NAME##_nary_function&) = default; \
    \
    void push(const function_t& i_func) \
    { \
        const std::pair<const_iterator,bool> pushRes = this->m_functions.emplace(i_func); \
        \
        if(!pushRes.second) \
        { \
            const_iterator itFunc = pushRes.first; \
            \
            const function_t opFunc = *itFunc _OP i_func; \
            \
            this->m_functions.erase(itFunc); \
            \
            this->m_functions.emplace(opFunc); \
        } \
    } \
    final_object<Im> operator()(final_object<const Dom&> i_args) const final override \
    { \
        if(this->m_functions.empty()) \
        { \
            throw ddk::call_function_exception{"Trying to call empty function"}; \
        } \
        \
        const_iterator itFunc = this->begin(); \
        final_object<Im> res = ddk::eval(*itFunc++,i_args); \
        \
        for(;itFunc!=this->end();++itFunc) \
        { \
            res = res _OP ddk::eval(*itFunc,i_args); \
        } \
        \
        return res; \
    } \
};

#define DEFINE_BUILTIN_FUNCTION(_NAME,_FUNC) \
template<typename,typename> \
struct _NAME##__builtin_function; \
template<typename Im, typename Dom, typename Allocator> \
struct _NAME##__builtin_function<Im(const Dom&),Allocator> : public builtin_function<_NAME##__builtin_function<Im(const Dom&),Allocator>,Im,Dom,Allocator> \
{ \
    struct __builtin_function_tag; \
    \
    constexpr _NAME##__builtin_function() = default; \
    final_object<Im> operator()(final_object<const Dom&> i_args) const final override \
    { \
        return _FUNC(i_args); \
    } \
}; \
template<typename,typename> \
struct _NAME##_builtin_function; \
template<typename Im, typename Dom, typename Allocator> \
struct _NAME##_builtin_function<Im(Dom&),Allocator> : _NAME##__builtin_function<Im(const Dom&),Allocator> \
{ \
    using _NAME##__builtin_function<Im(const Dom&),Allocator>::operator(); \
}; \
template<typename Im, typename Dom, typename Allocator> \
struct builtin_expression_function<::cpn::_NAME##_builtin_expression,Im(const Dom&),Allocator> : _NAME##_builtin_function<Im(const Dom&),Allocator> \
{ \
	struct callable_tag; \
    using _NAME##_builtin_function<Im(const Dom&),Allocator>::operator(); \
};

namespace cpn
{
namespace detail
{

builtin_function_id get_next_builtin_function_id();

template<builtin_function_type Function>
builtin_function_id get_builtin_function_id();

template<typename SuperClass, set_model Im, set_model Dom, typename Allocator>
struct builtin_function : public ddk::detail::inherited_functor_impl<function_base<Im,const Dom&>>
{
    BUILTIN_FUNCTION(SuperClass)

    typedef builtin_function<SuperClass,Im,Dom,Allocator> base_t;
    typedef function<Im(const Dom&)>(*binary_operator_type)(const operator_context&, const function_base<Im,const Dom&>&, const function_base<Im,const Dom&>&, size_t);
    typedef function<Im(const Dom&)>(*unary_operator_type)(const operator_context&, const function_base<Im,const Dom&>&);

protected:
    static const size_t s_numOps = num_function_operators<function<Im(const Dom&),Allocator>>;
    builtin_function() = default;

    function<Im(const Dom&)> clone() const override;
    builtin_function_id id() const override;
    function<Im(const Dom&)> binary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& other, size_t i_depth) const override;
    function<Im(const Dom&)> unary_operator(const operator_context& i_context) const override;

    static const std::array<binary_operator_type,s_numOps> s_binary_operators;
    static const std::array<unary_operator_type,s_numOps> s_unary_operators;
};

template<typename,typename>
struct builtin_null_function;
template<set_model Im,set_model Dom, typename Allocator>
struct builtin_null_function<Im(const Dom&),Allocator> : public builtin_function<builtin_null_function<Im(const Dom&),Allocator>,Im,Dom,Allocator>
{
public:
    typedef ddk::detail::function_impl<Im(const Dom&),Allocator,detail::function_base> function_t;
    typedef builtin_function<builtin_null_function<Im(const Dom&),Allocator>,Im,Dom,Allocator> base_t;

    builtin_null_function() = default;

    final_object<Im> operator()(final_object<const Dom&> i_args) const final override;
};
template<set_model Im,set_model Dom, typename Allocator>
const builtin_null_function<Im(const Dom&),Allocator> null_function = builtin_null_function<Im(const Dom&),Allocator>();

template<typename,typename>
struct builtin_unity_function;
template<set_model Im,set_model Dom, typename Allocator>
struct builtin_unity_function<Im(const Dom&),Allocator> : public builtin_function<builtin_unity_function<Im(const Dom&),Allocator>,Im,Dom,Allocator>
{
public:
    typedef ddk::detail::function_impl<Im(const Dom&),Allocator,detail::function_base> function_t;
    typedef builtin_function<builtin_unity_function<Im(const Dom&),Allocator>,Im,Dom,Allocator> base_t;

    builtin_unity_function() = default;

    final_object<Im> operator()(final_object<const Dom&> i_args) const final override;
};
template<set_model Im,set_model Dom, typename Allocator>
const builtin_unity_function<Im(const Dom&),Allocator> unity_function = builtin_unity_function<Im(const Dom&),Allocator>();

template<typename,typename>
struct builtin_fusioned_function;
template<intersection_model Im,set_model Dom, typename Allocator>
struct builtin_fusioned_function<Im(const Dom&),Allocator> : public builtin_function<builtin_fusioned_function<Im(const Dom&),Allocator>,Im,Dom,Allocator>
{
public:
    typedef ddk::detail::function_impl<typename Im::principal_type(const Dom&),Allocator,detail::function_base> function_t;

    TEMPLATE(typename ... CCallables)
    REQUIRES(ddk::is_num_of_args_equal<Im::rank(),CCallables...>,ddk::is_constructible<function_t,CCallables>...)
    builtin_fusioned_function(CCallables&& ... i_callables);

    inline const function_t& operator[](size_t i_index) const;

    final_object<Im> operator()(final_object<const Dom&> i_args) const final override;

private:
    template<size_t ... Indexs>
    inline Im execute(const ddk::mpl::sequence<Indexs...>&, const Dom& i_args) const;

    std::array<function_t,Im::rank()> m_callables;
};

template<typename,typename>
struct builtin_composed_function;
template<set_model Im,set_model Dom, typename Allocator>
struct builtin_composed_function<Im(const Dom&),Allocator> : public builtin_function<builtin_composed_function<Im(const Dom&),Allocator>,Im,Dom,Allocator>
{
    typedef function<Im(const Im&),Allocator> function_lhs_t;
    typedef function<Im(const Dom&),Allocator> function_rhs_t;

public:
    TEMPLATE(typename Function, typename FFunction)
    REQUIRES(ddk::is_constructible<function_lhs_t,Function>,ddk::is_constructible<function_rhs_t,FFunction>)
    builtin_composed_function(Function&& i_lhs, FFunction&& i_rhs);

    const function_lhs_t& lhs_function() const;
    const function_rhs_t& rhs_function() const;

    final_object<Im> operator()(final_object<const Dom&> i_args) const final override;

private:
    const function_lhs_t m_lhs;
    const function_rhs_t m_rhs;
};

template<typename,typename>
struct builtin_component_function;
template<set_model Im,set_model Dom, typename Allocator>
struct builtin_component_function<Im(const Dom&),Allocator> : public builtin_function<builtin_component_function<Im(const Dom&),Allocator>,Im,Dom,Allocator>
{
public:
    template<size_t Comp>
    builtin_component_function(const ddk::mpl::static_number<Comp>&);

    final_object<Im> operator()(final_object<const Dom&> i_arg) const final override;
    size_t component() const;

private:
    template<intersection_type DDom>
    final_object<Im> forward_arg(const DDom& i_arg) const;
    template<flat_type DDom>
    final_object<Im> forward_arg(const DDom& i_arg) const;

    const size_t m_component;

};

template<typename,typename>
struct builtin_number_function;
template<set_model Im,set_model Dom, typename Allocator>
struct builtin_number_function<Im(const Dom&),Allocator> : public builtin_function<builtin_number_function<Im(const Dom&),Allocator>,Im,Dom,Allocator>
{
public:
    typedef ddk::detail::function_impl<Im(const Dom&),Allocator,detail::function_base> function_t;
    typedef builtin_function<builtin_number_function<Im(const Dom&),Allocator>,Im,Dom,Allocator> base_t;

    builtin_number_function(const final_object<Im>& i_number);
    template<solvable_literal_type IIm>
    builtin_number_function(const IIm& i_number);

    const final_object<Im>& number() const;

    final_object<Im> operator()(final_object<const Dom&> i_args) const final override;

private:
    const final_object<Im> m_number;
};

template<typename,typename>
struct builtin_minus_function;
template<group_model Im, set_model Dom, typename Allocator>
struct builtin_minus_function<Im(const Dom&),Allocator> : public builtin_function<builtin_minus_function<Im(const Dom&),Allocator>,Im,Dom,Allocator>
{
public:
    typedef function<Im(const Dom&),Allocator> function_t;

    TEMPLATE(typename Function)
    REQUIRES(ddk::is_constructible<function_t,Function>)
    builtin_minus_function(Function&& i_function);

    const function_t& func() const;

    final_object<Im> operator()(final_object<const Dom&> i_args) const final override;

private:
    const function_t m_function;
};

template<typename,typename>
struct builtin_inverted_function;
template<field_model Im, set_model Dom, typename Allocator>
struct builtin_inverted_function<Im(const Dom&),Allocator> : public builtin_function<builtin_inverted_function<Im(const Dom&),Allocator>,Im,Dom,Allocator>
{
public:
    typedef function<Im(const Dom&),Allocator> function_t;

    TEMPLATE(typename Function)
    REQUIRES(ddk::is_constructible<function_t,Function>)
    builtin_inverted_function(Function&& i_expression);

    const function_t& func() const;

    final_object<Im> operator()(final_object<const Dom&> i_args) const final override;

private:
    const function_t m_function;
};

template<typename,typename>
struct builtin_pow_function;
template<ring_model Im, typename Dom, typename Allocator>
struct builtin_pow_function<Im(const Dom&),Allocator> : public builtin_function<builtin_pow_function<Im(const Dom&),Allocator>,Im,Dom,Allocator>
{
public:
    typedef function<Im(const Dom&),Allocator> function_t;

    TEMPLATE(typename Function, typename FFunction)
    REQUIRES(ddk::is_constructible<function_t,Function>,ddk::is_constructible<function_t,FFunction>)
    builtin_pow_function(Function&& i_base, FFunction&& i_exp);

    const function_t& base() const;
    const function_t& exp() const;

    final_object<Im> operator()(final_object<const Dom&> i_args) const final override;

private:
    const function_t m_base;
    const function_t m_exp;
};

template<typename,typename,typename>
struct builtin_functor_function;
template<set_model Im, set_model Dom, typename Allocator, typename Functor>
struct builtin_functor_function<Im(const Dom&),Allocator,Functor> : public builtin_function<builtin_functor_function<Im(const Dom&),Allocator,Functor>,Im,Dom,Allocator>
{
public:
    builtin_functor_function(const builtin_functor_expression<Functor>& i_functor);

    const Functor& functor() const;

    final_object<Im> operator()(final_object<const Dom&> i_args) const final override;

private:
    const Functor m_functor;
};

template<typename,typename>
struct builtin_nary_function;
template<set_model Im, set_model Dom, typename Allocator>
struct builtin_nary_function<Im(const Dom&),Allocator>
{
    typedef std::set<function<Im(const Dom&),Allocator>,detail::function_compare_by_id<function<Im(const Dom&),Allocator>>> container_func;

public:
    typedef function<Im(const Dom&),Allocator> function_t;
    typedef container_func::iterator iterator;
    typedef container_func::const_iterator const_iterator;

    // TEMPLATE(typename ... Functions)
    // REQUIRES(ddk::is_constructible<function_t,Functions>...)
    template<typename ... Functions>
    explicit builtin_nary_function(const Functions& ... i_functions);
    builtin_nary_function(const container_func& i_functions);
    builtin_nary_function(container_func&& i_functions);
    builtin_nary_function(const builtin_nary_function&) = default;

    builtin_nary_function& operator=(const builtin_nary_function&) = default;
    bool empty() const;
    size_t size() const;
    template<typename Callable>
    void enumerate(Callable&& i_callable) const;
    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;

protected:
    container_func m_functions;
};

template<typename,typename>
struct builtin_operator;

//arithmetic operations
DEFINE_ARITHMETIC_NARY_OPERATION(add,+,group_model);
DEFINE_ARITHMETIC_NARY_OPERATION(mult,*,ring_model);

}
}

#include "cpn_builtin_functions.inl"
#include "cpn_builtin_function_ops.h"