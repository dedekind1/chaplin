#include "cpn_polynomial.h"

namespace cpn
{

monomial_exponents operator+(const monomial_exponents& i_lhs, const monomial_exponents& i_rhs)
{
    monomial_exponents res(i_lhs);

    [&](const std::pair<const size_t,size_t>& ii_exponent)
    {
        res[ii_exponent.first] += ii_exponent.second;
    } <<= i_rhs;

    return res;
}

monomial_exponents operator-(const monomial_exponents& i_lhs, const monomial_exponents& i_rhs)
{
    monomial_exponents res(i_lhs);

    [&](const std::pair<const size_t,size_t>& ii_exponent)
    {
        res[ii_exponent.first] -= ii_exponent.second;
    } <<= i_rhs;

    return res;
}

}