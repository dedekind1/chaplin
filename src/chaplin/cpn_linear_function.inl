
#include "cpn_builtin_functions.h"

namespace cpn
{

template<flat_type Im, typename Dom, typename Allocator>
TEMPLATE(literal_expression Expression)
REQUIRED(is_linear_expression<Expression>)
linear_function<Im(const Dom&),Allocator>::linear_function(Expression&& i_exp)
: base_t(instance_function<Im,Dom,Allocator>(std::forward<Expression>(i_exp)))
{
}
template<flat_type Im, typename Dom, typename Allocator>
linear_function<Im(const Dom&),Allocator>::linear_function(const Im& i_value)
: base_t(detail::builtin_number_function<Im(const Dom&),Allocator>(i_value))
{
}
template<flat_type Im, typename Dom, typename Allocator>
TEMPLATE(typename ... Args)
REQUIRED(ddk::is_constructible<Dom,Args...>)
auto linear_function<Im(const Dom&),Allocator>::operator()(Args&& ... i_args) const
{
    return base_t::operator()(Dom{ std::forward<Args>(i_args)... });
}

template<intersection_type Im,typename Dom, typename Allocator>
template<typename ... Callable>
linear_function<Im(const Dom&),Allocator>::fusion_callable<Callable...>::fusion_callable(const Callable& ... i_callables)
: m_callables(i_callables...)
{
}
template<intersection_type Im,typename Dom, typename Allocator>
template<typename ... Callable>
Im linear_function<Im(const Dom&),Allocator>::fusion_callable<Callable...>::operator()(const Dom& i_args) const
{
    typedef typename ddk::mpl::make_sequence<0,Im::num_places>::type seq_t;

    return execute(seq_t{},i_args);
}
template<intersection_type Im,typename Dom, typename Allocator>
template<typename ... Callable>
template<size_t ... Indexs>
Im linear_function<Im(const Dom&),Allocator>::fusion_callable<Callable...>::execute(const ddk::mpl::sequence<Indexs...>&, const Dom& i_args) const
{
    return Im{ ddk::eval(m_callables.template get<Indexs>(),i_args) ... };
}

template<intersection_type Im,typename Dom, typename Allocator>
TEMPLATE(literal_expression ... Expression)
REQUIRED(ddk::is_num_of_args_equal<Im::num_places,Expression...>,is_linear_expression<Expression>...)
linear_function<Im(const Dom&),Allocator>::linear_function(Expression&& ... i_exps)
: base_t(make_fusion(instance_function<typename Im::place_type,Dom,Allocator>(i_exps)...))
{
}
template<intersection_type Im,typename Dom, typename Allocator>
linear_function<Im(const Dom&),Allocator>::linear_function(const Im& i_value)
: base_t(detail::builtin_number_function<Im(const Dom&),Allocator>(i_value))
{
}
template<intersection_type Im,typename Dom, typename Allocator>
TEMPLATE(typename ... Args)
REQUIRED(ddk::is_constructible<Dom,Args...>)
auto linear_function<Im(const Dom&),Allocator>::operator()(Args&& ... i_args) const
{
    return base_t::operator()(Dom{ std::forward<Args>(i_args)... });
}
template<intersection_type Im,typename Dom, typename Allocator>
TEMPLATE(typename ... Callable)
REQUIRED(ddk::is_num_of_args_equal<Im::num_places,Callable...>)
auto linear_function<Im(const Dom&),Allocator>::make_fusion(Callable&& ... i_callables)
{
    return fusion_callable<ddk::mpl::remove_qualifiers<Callable>...>(std::forward<Callable>(i_callables)...);
}

}
