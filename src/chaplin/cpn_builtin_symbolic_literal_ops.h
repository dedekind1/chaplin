#pragma once

#include "cpn_builtin_symbolic_literals.h"
#include "ddkFramework/ddk_type_concepts.h"
#include "ddkFramework/ddk_concepts.h"

namespace cpn
{

//resolve
template<numeric_type T>
constexpr T resolve(const T& i_number);
constexpr int resolve(const integer_symbolic_literal& i_number);
double resolve(const rational_symbolic_literal& i_number);
double resolve(const root_symbolic_literal& i_number);
double resolve(const log_symbolic_literal& i_number);
template<literal_type ... Operands>
auto resolve(const sum_symbolic_literal<Operands...>& i_number);
template<literal_type ... Operands>
auto resolve(const prod_symbolic_literal<Operands...>& i_number);
template<literal_type ... Operands>
auto resolve(const div_symbolic_literal<Operands...>& i_number);
template<literal_type T>
auto resolve(const real_symbolic_literal<T>& i_number);
template<typename T, size_t ... Dims>
auto resolve(const times_symbolic_literal_type<T,Dims...>& i_number);
template<literal_type ... T>
auto resolve(const union_symbolic_literal_type<T...>& i_number);

//comparisons
constexpr bool operator<(const integer_symbolic_literal& i_lhs, const integer_symbolic_literal& i_rhs);
constexpr bool operator<(const rational_symbolic_literal& i_lhs, const rational_symbolic_literal& i_rhs);
template<literal_type T, literal_type TT>
constexpr inline bool operator<(const T& i_lhs, const TT& i_rhs);
constexpr bool operator==(const integer_symbolic_literal& i_lhs, const integer_symbolic_literal& i_rhs);
constexpr bool operator==(const rational_symbolic_literal& i_lhs, const rational_symbolic_literal& i_rhs);
template<literal_type T, literal_type TT>
constexpr bool operator==(const T& i_lhs, const TT& i_rhs);

// - operator
constexpr integer_symbolic_literal operator-(const integer_symbolic_literal& i_value);
constexpr rational_symbolic_literal operator-(const rational_symbolic_literal& i_value);
constexpr root_symbolic_literal operator-(const root_symbolic_literal& i_value);
constexpr log_symbolic_literal operator-(const log_symbolic_literal& i_value);
template<literal_type ... Operands>
constexpr auto operator-(const sum_symbolic_literal<Operands...>& i_value);
template<literal_type ... Operands>
constexpr auto operator-(const prod_symbolic_literal<Operands...>& i_value);
template<literal_type ... Operands>
constexpr auto operator-(const div_symbolic_literal<Operands...>& i_value);
template<literal_type T>
constexpr auto operator-(const real_symbolic_literal<T>& i_lhs);

// operator +
constexpr integer_symbolic_literal operator+(const integer_symbolic_literal& i_lhs, const integer_symbolic_literal& i_rhs);
constexpr rational_symbolic_literal operator+(const rational_symbolic_literal& i_lhs, const rational_symbolic_literal& i_rhs);
template<literal_type T,literal_type TT>
constexpr auto operator+(const real_symbolic_literal<T>& i_lhs, const real_symbolic_literal<TT>& i_rhs);
template<literal_type T, literal_type TT>
constexpr auto operator+(const T& i_lhs, const TT& i_rhs);

// operator -
constexpr integer_symbolic_literal operator-(const integer_symbolic_literal& i_lhs, const integer_symbolic_literal& i_rhs);
constexpr rational_symbolic_literal operator-(const rational_symbolic_literal& i_lhs, const rational_symbolic_literal& i_rhs);
template<literal_type T,literal_type TT>
constexpr auto operator-(const real_symbolic_literal<T>& i_lhs, const real_symbolic_literal<TT>& i_rhs);
template<literal_type T, literal_type TT>
constexpr auto operator-(const T& i_lhs, const TT& i_rhs);

// operator *
constexpr integer_symbolic_literal operator*(const integer_symbolic_literal& i_lhs,const integer_symbolic_literal& i_rhs);
constexpr rational_symbolic_literal operator*(const rational_symbolic_literal& i_lhs,const rational_symbolic_literal& i_rhs);
template<literal_type T,literal_type TT>
constexpr auto operator*(const real_symbolic_literal<T>& i_lhs, const real_symbolic_literal<TT>& i_rhs);
template<literal_type T, literal_type TT>
constexpr auto operator*(const T& i_lhs, const TT& i_rhs);

// operator /
constexpr rational_symbolic_literal operator/(const integer_symbolic_literal& i_lhs, const integer_symbolic_literal& i_rhs);
constexpr rational_symbolic_literal operator/(const rational_symbolic_literal& i_lhs, const rational_symbolic_literal& i_rhs);
template<literal_type T,literal_type TT>
constexpr auto operator/(const real_symbolic_literal<T>& i_lhs, const real_symbolic_literal<TT>& i_rhs);
template<literal_type T, literal_type TT>
constexpr auto operator/(const T& i_lhs, const TT& i_rhs);

}

#include "cpn_builtin_symbolic_literal_ops.inl"