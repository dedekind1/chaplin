#pragma once

#include "cpn_algebraic_structure.h"
#include "ddkFramework/ddk_high_order_array.h"
#include "ddkFramework/ddk_tuple.h"
#include "ddkFramework/ddk_variant.h"

namespace cpn
{

template<typename ... T>
using prod_type = ddk::tuple<T...>;

template<typename T, size_t ... Dims>
using pow_type = ddk::high_order_array<T,Dims...>;

template<typename ... T>
using sum_type = ddk::variant<T...>;

template<type T, size_t ... Dims, typename ... Args>
constexpr pow_type<T,Dims...> pow_forward(Args&& ... i_args);

template<type ... T>
constexpr auto times(const T& ... i_values);

template<type ... T>
constexpr auto prod(const T& ... i_values);

struct pow_impl
{
private:
    template<typename T, size_t ... Dims, typename ... OperatorTags>
    static algebraic_operators<pow_operation<OperatorTags,T,Dims...>...> resolve(const ddk::mpl::type_pack<OperatorTags...>&);

public:
    template<typename T, size_t ... Dims>
    using model = decltype(resolve<T,Dims...>(std::declval<typename T::_operator_tags>()));
};

template<model T, size_t ... Dims>
using pow_model = typename pow_impl::model<T,Dims...>;
template<type T, size_t ... Dims>
using pow_struct = algebraic_structure<pow_type<T,Dims...>,pow_model<T,Dims...>>;

struct times_impl
{
private:
    template<size_t ... Dims>
    using sequence = typename ddk::mpl::make_sequence<0,ddk::mpl::prod_ranks<Dims...>>::type;
    template<typename OperatorTag, typename T, size_t ... Indexs>
    using prod_op = prod_operation<OperatorTag,ddk::mpl::index_to_type<Indexs,T>...>;
    template<typename T, size_t ... Indexs>
    using prod_t = prod_type<ddk::mpl::index_to_type<Indexs,T>...>;
    template<typename T, size_t ... Indexs, typename ... OperatorTags>
    static algebraic_operators<prod_op<OperatorTags,T,Indexs...>...> resolve_model(const ddk::mpl::sequence<Indexs...>&, const ddk::mpl::type_pack<OperatorTags...>&);
    template<typename T, size_t ... Indexs, typename ... OperatorTags>
    static algebraic_structure<prod_t<T,Indexs...>,prod_op<OperatorTags,T,Indexs...>...> resolve_type(const ddk::mpl::sequence<Indexs...>&, const ddk::mpl::type_pack<OperatorTags...>&);

public:
    template<typename T, size_t ... Dims>
    using model = decltype(resolve_model<T>(std::declval<sequence<Dims...>>(),std::declval<typename T::_operator_tags>()));
    template<typename T, size_t ... Dims>
    using type = decltype(resolve_type<T>(std::declval<sequence<Dims...>>(),std::declval<typename T::_operator_tags>()));
};

template<model T, size_t ... Dims>
using times_model = typename times_impl::model<T,Dims...>;
template<model T, size_t ... Dims>
using times_struct = typename times_impl::type<T,Dims...>;

struct prod_impl
{
private:
    template<typename ... T, typename ... OperatorTags>
    static algebraic_operators<prod_operation<OperatorTags,T...>...> resolve(const ddk::mpl::type_pack<OperatorTags...>&);

public:
    template<typename ... T>
    using model = decltype(resolve<T...>(std::declval<typename ddk::mpl::intersect_type_packs<typename T::_operator_tags...>::type>()));
};

template<model ... T>
using prod_model = typename prod_impl::model<T...>;
template<type ... T>
using prod_struct = algebraic_structure<prod_type<T...>,prod_model<T...>>;

struct sum_impl
{
private:
    template<typename ... T, typename ... OperatorTags>
    static algebraic_operators<sum_operation<OperatorTags,T...>...> resolve(const ddk::mpl::type_pack<OperatorTags...>&);

public:
    template<typename ... T>
    using model = decltype(resolve<T...>(std::declval<typename ddk::mpl::intersect_type_packs<typename T::_operator_tags...>::type>()));
};

template<model ... T>
using sum_model = typename sum_impl::model<T...>;
template<type ... T>
using sum_struct = algebraic_structure<sum_type<T...>,sum_model<T...>>;

}

#include "cpn_types.inl"