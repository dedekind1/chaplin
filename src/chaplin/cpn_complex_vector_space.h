#pragma once

#include "cpn_complex.h"
#include "cpn_complex_free_module.h"
#include "cpn_vector_space.h"

namespace cpn
{

template<size_t Dim>
using C_n = VectorSpace<ComplexFreeModule_n<Complexs,Dim>,canonical_vector_mult_operation>;

using C1 = C_n<1>;
using C2 = C_n<2>;
using C3 = C_n<3>;
using C4 = C_n<4>;

template<solvable_literal_pack<2> ... T>
using c_n = vector_space<complex_free_module_n<Complexs,T...>,canonical_vector_mult_operation,prod_type<reify_model<complex<T>,Complexs>...>>;

template<solvable_literal_pack<2> T>
using c1 = c_n<T>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT>
using c2 = c_n<T,TT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT>
using c3 = c_n<T,TT,TTT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT, solvable_literal_pack<2> TTTT>
using c4 = c_n<T,TT,TTT,TTTT>;

}