#pragma once

#include "cpn_real_module.h"
#include "cpn_free_module.h"

namespace cpn
{

template<ring_model R, size_t Dim>
using RealFreeModule_n = TimesFreeModule<RealModule<R>,Dim>;

template<ring_model R = RealRing>
using RealFreeModule_1 = RealFreeModule_n<R,1>;
template<ring_model R = RealRing>
using RealFreeModule_2 = RealFreeModule_n<R,2>;
template<ring_model R = RealRing>
using RealFreeModule_3 = RealFreeModule_n<R,3>;
template<ring_model R = RealRing>
using RealFreeModule_4 = RealFreeModule_n<R,4>;

template<ring_model R, solvable_literal_type ... T>
using real_free_module_n = prod_free_module<RealModule<R>,reify_model<real<T>,R>...>;

template<solvable_literal_type T, ring_model R = RealRing>
using real_free_module_1 = real_free_module_n<R,T>;
template<solvable_literal_type T,solvable_literal_type TT, ring_model R = RealRing>
using real_free_module_2 = real_free_module_n<R,T,TT>;
template<solvable_literal_type T,solvable_literal_type TT,solvable_literal_type TTT, ring_model R = RealRing>
using real_free_module_3 = real_free_module_n<R,T,TT,TTT>;
template<solvable_literal_type T,solvable_literal_type TT,solvable_literal_type TTT,solvable_literal_type TTTT, ring_model R = RealRing>
using real_free_module_4 = real_free_module_n<R,T,TT,TTT,TTTT>;

}