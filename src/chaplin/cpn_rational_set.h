#pragma once

#include "cpn_set.h"
#include "cpn_builtin_symbolic_literals.h"
#include "cpn_algebraic_structure.h"

namespace cpn
{

class rational
{
	friend inline auto resolve(const rational& i_value)
	{
		return cpn::resolve(i_value.number());
	}

public:
	TEMPLATE(typename ... Args)
	REQUIRES(ddk::is_constructible<rational_symbolic_literal,Args...>)
	constexpr rational(Args&& ... i_args);

	constexpr rational_symbolic_literal number() const;

private:
	rational_symbolic_literal m_number;
};

const auto rational_null = rational{ 0 };
const auto rational_unit = rational{ 1 };

struct rational_set_operation
{
	PUBLISH_OPERATION_PROPERTIES(rational_set_operation,set_operation,totally_ordered);

	typedef rational decay_t;

	friend inline constexpr bool operator<(const rational& i_lhs,const rational& i_rhs)
	{
		return i_lhs.number() < i_rhs.number();
	}
	friend inline constexpr bool operator==(const rational& i_lhs,const rational& i_rhs)
	{
		return i_lhs.number() == i_rhs.number();
	}
	friend inline constexpr bool operator!=(const rational& i_lhs,const rational& i_rhs)
	{
		return !(i_lhs.number() == i_rhs.number());
	}
};

using RationalSet = algebraic_model<rational_set_operation>;
using rational_set = algebraic_structure<rational,RationalSet>;

template<size_t ... Dims>
using RationalSet_n = pow_model<RationalSet,Dims...>;
template<size_t ... Dims>
using rational_set_n = pow_struct<rational_set,Dims...>;

}

#include "cpn_rational_set.inl"