#pragma once

#include "cpn_algebraic_concepts.h"
#include "cpn_types.h"

namespace cpn
{
namespace concepts
{

template<typename,typename...>
struct implements_model_impl;

template<model T, model ... TT>
struct implements_model_impl<T,TT...>
{
public:
    static const bool value = (T::_operators::contains(typename TT::_operators{}) && ...);
};

template<type T, size_t ... Dims, model ... TT>
struct implements_model_impl<pow_type<T,Dims...>,TT...>
{
    static const bool value = (implements_model_impl<T,TT>::value && ...);
};

template<type ... T, model ... TT>
struct implements_model_impl<prod_type<T...>,TT...>
{
    static const bool value = (implements_model_impl<T,TT>::value && ...);
};

template<type ... T, model ... TT>
struct implements_model_impl<sum_type<T...>,TT...>
{
    static const bool value = (implements_model_impl<T,TT>::value && ...);
};

template<typename,typename...>
struct implements_operation_impl;

template<type T, typename ... Op>
struct implements_operation_impl<T,Op...>
{
public:
    static const bool value = (T::_operators::template contains<Op>() && ...);
};

template<type T, size_t ... Dims, typename ... Op>
struct implements_operation_impl<pow_type<T,Dims...>,Op...>
{
    static const bool value = (implements_operation_impl<T,Op>::value && ...);
};

template<type ... T, typename ... Op>
struct implements_operation_impl<prod_type<T...>,Op...>
{
    static const bool value = (implements_operation_impl<T,Op>::value && ...);
};

template<type ... T, typename ... Op>
struct implements_operation_impl<sum_type<T...>,Op...>
{
    static const bool value = (implements_operation_impl<T,Op>::value && ...);
};

template<typename T, typename Tag, typename ... Args>
struct is_modellable_impl
{
private:
    template<typename TT, typename = decltype(resolve_operation(std::declval<infer_operation<TT,Tag,Args...>>(),std::declval<Tag>()))>
    static std::true_type resolve(const TT&);
    static typename ddk::mpl::which_type<T::_operator_tags::template contains<Tag>(),std::true_type,std::false_type>::type resolve(...);

public:
    static const bool value = decltype(resolve(std::declval<T>()))::value;
};

template<typename,typename,bool,typename...>
struct infer_model_impl;

template<typename T, typename Tag,typename ... Args>
struct infer_model_impl<T,Tag,true,Args...>
{
public:
    typedef T type;
};
template<typename T, typename Tag,typename ... Args>
struct infer_model_impl<T,Tag,false,Args...>
{
private:
    template<typename TT, typename ... Operations>
    static algebraic_structure<TT,Operations...,infer_operation<algebraic_operators<Operations...>,Tag,Args...>> resolve(const algebraic_structure<TT,Operations...>&);
    template<typename ... Operations>
    static algebraic_operators<Operations...,infer_operation<algebraic_operators<Operations...>,Tag,Args...>> resolve(const algebraic_operators<Operations...>&);

public:
    typedef decltype(resolve(std::declval<T>())) type;
};

}

template<typename T, model ... TT>
inline constexpr bool implements_model = concepts::implements_model_impl<T,TT...>::value;

template<typename Tag, typename T, model ... TT>
inline constexpr bool implements_operation = concepts::implements_operation_impl<T,algebraic_operator<TT,Tag>...>::value;

template<type T, type TT>
inline constexpr bool compatible_models = ddk::is_same_class<typename T::model,typename TT::model>;

template<typename T, model TT>
using reify_model = algebraic_structure<T,TT>;

template<model T, typename Tag, typename ... Args>
inline constexpr bool is_modellable = concepts::is_modellable_impl<T,Tag,Args...>::value;

template<model T, typename Tag, typename ... Args>
using infer_model = typename concepts::infer_model_impl<T,Tag,T::_operator_tags::template contains<Tag>(),Args...>::type;

template<typename T>
concept set_modellable = model<T> && is_modellable<T,set_operation>;

template<typename T>
concept complete_space_modellable = set_model<T> && is_modellable<T,point_conv_operation>;

template<typename T>
concept metric_space_modellable = set_model<T> && is_modellable<T,metric_operation>;

template<typename T>
concept semi_group_modellable = set_model<T> && is_modellable<T,semi_group_operation>;

template<typename T>
concept group_modellable = semi_group_modellable<T> && is_modellable<T,group_operation>;

template<typename T>
concept semi_ring_modellable = set_model<T> && is_modellable<T,ring_operation>;

template<typename T>
concept ring_modellable = group_modellable<T> && semi_ring_modellable<T>;

template<typename T>
concept field_modellable = ring_modellable<T> && is_modellable<T,field_operation>;

template<typename T, typename R>
concept module_modellable = group_modellable<T> && is_modellable<T,mod_operation,R>;

template<typename T, typename R>
concept free_module_modellable = module_modellable<T,R> && is_modellable<T,basis_operation>;

template<typename T, typename R>
concept vector_space_modellable = free_module_modellable<T,R> && is_modellable<T,vector_operation>;

}