#pragma once

#include "cpn_algebraic_concepts.h"
#include "cpn_polynomial.h"

namespace cpn
{
namespace concepts
{

template<typename,typename>
struct polynomial_operators_mapper;

template<typename Poly>
struct polynomial_operators_mapper<Poly,ddk::mpl::type_pack<>>
{
    typedef empty_operators type;
};

template<typename Poly, typename OpsTag, typename ... OpsTags>
struct polynomial_operators_mapper<Poly,ddk::mpl::type_pack<OpsTag,OpsTags...>>
{
    template<typename ... _OpsTags, typename = typename operator_functor<Poly,operator_tags<_OpsTags...>>::__operator_tags>
    static operator_functor<Poly,operator_tags<_OpsTags...>> resolve(const operator_tags<_OpsTags...>&);
    static no_op resolve(...);

    typedef typename make_algebraic_operators<decltype(resolve(std::declval<OpsTag>()))>::type local_type;
    typedef polynomial_operators_mapper<Poly,ddk::mpl::type_pack<OpsTags...>>::type nested_type;

public:
    typedef typename merge_algebraic_operators<local_type,nested_type>::type type;
};

template<ring_model T, typename Order, typename Allocator>
struct holds_operations_impl<polynomial<T,Order,Allocator>>
{
private:
    template<typename ... OperationTags>
    static typename operator_tags_acc<OperationTags...>::type construct(const operator_tags<OperationTags...>&);

public:
    typedef typename polynomial_operators_mapper<polynomial<T,Order,Allocator>,decltype(construct(std::declval<algebraic_structure_operators_tags<T>>()))>::type type;
    static const bool value = (std::is_same<type,empty_operators>::value == false);
};

}
}