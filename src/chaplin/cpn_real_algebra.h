#pragma once

#include "cpn_real_module.h"
#include "cpn_real_ring.h"
#include "cpn_algebra.h"

namespace cpn
{

struct real_algebra_operation : real_multiplication
{
    PUBLISH_OPERATION_PROPERTIES(real_algebra_operation,algebra_operation,commutative,associative,distributive);

	template<solvable_literal_type T, solvable_literal_type TT, typename ... Operations>
	friend inline auto operator&(const algebraic_structure<real<T>,Operations...>& i_lhs, const algebraic_structure<real<TT>,Operations...>& i_rhs)
	{
		typedef decltype(std::declval<T>() * std::declval<TT>()) ret_type;

		return algebraic_structure<real<ret_type>,Operations...>{ i_lhs.number() * i_rhs.number() };
	}
};

template<ring_model R = RealRing>
using RealAlgebra = Algebra<RealModule<R>,real_algebra_operation>;
using RRealAlgebra = RealAlgebra<RealRing>;
template<solvable_literal_type T, ring_model R = RealRing>
using real_algebra = algebra<real_module<T,R>,real_algebra_operation,real<T>>;

}