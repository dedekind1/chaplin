
#include "mpfr.h"

namespace cpn
{
namespace detail
{

template<size_t ... Indexs, literal_type ... Operands>
constexpr auto inv(const ddk::mpl::sequence<Indexs...>&, const sum_symbolic_literal<Operands...>& i_value)
{
	return sum_symbolic_literal{ -i_value.template get<Indexs>() ... };
}
template<size_t Index, size_t ... Indexs, literal_type ... Operands>
constexpr auto inv(const ddk::mpl::sequence<Index,Indexs...>&, const prod_symbolic_literal<Operands...>& i_value)
{
	return sum_symbolic_literal{ -i_value.template get<Index>(),i_value.template get<Indexs>() ... };
}
template<size_t Index, size_t ... Indexs, literal_type ... Operands>
constexpr auto inv(const ddk::mpl::sequence<Index,Indexs...>&, const div_symbolic_literal<Operands...>& i_value)
{
	return sum_symbolic_literal{ -i_value.template get<Index>(),i_value.template get<Indexs>() ... };
}

}

template<numeric_type T>
constexpr T resolve(const T& i_number)
{
	return i_number;
}
constexpr int resolve(const integer_symbolic_literal& i_number)
{
	return i_number.number();
}
template<literal_type ... Operands>
auto resolve(const sum_symbolic_literal<Operands...>& i_number)
{
	mpfr_t res;

	mpfr_init(res);

	i_number.enumerate([&](auto&& i_value) mutable
	{
		mpfr_t summand;

		mpfr_init(summand);

		mpfr_set_d(summand,resolve(i_value),MPFR_RNDD);

		mpfr_add(res,res,summand,MPFR_RNDD);
	});

	return mpfr_get_d(res,MPFR_RNDD);
}
template<literal_type ... Operands>
auto resolve(const prod_symbolic_literal<Operands...>& i_number)
{
	mpfr_t res;

	mpfr_init(res);

	mpfr_set_ui(res,1,MPFR_RNDD);

	i_number.enumerate([&](auto&& i_value) mutable
	{
		mpfr_t prod;

		mpfr_init(prod);

		mpfr_set_d(prod,resolve(i_value),MPFR_RNDD);

		mpfr_mul(res,res,prod,MPFR_RNDD);
	});

	return mpfr_get_d(res,MPFR_RNDD);
}
template<literal_type ... Operands>
auto resolve(const div_symbolic_literal<Operands...>& i_number)
{
	bool first = true;
	mpfr_t res;

	mpfr_init(res);

	i_number.enumerate([&](auto&& i_value) mutable
	{
		if(first)
		{
			mpfr_set_d(res,resolve(i_value),MPFR_RNDD);
		}
		else
		{
			mpfr_t div;

			mpfr_init(div);

			mpfr_set_d(div,resolve(i_value),MPFR_RNDD);

			mpfr_div(res,res,div,MPFR_RNDD);
		}
	});

	return mpfr_get_d(res,MPFR_RNDD);
}
template<solvable_literal_type T>
auto resolve(const real_symbolic_literal<T>& i_number)
{
	return resolve(i_number.number());
}
template<typename T, size_t ... Dims>
auto resolve(const times_symbolic_literal_type<T,Dims...>& i_number)
{
	// typedef decltype(resolve(std::declval<T>())) resolved_type;

	times_symbolic_literal_type<float,Dims...> res;

	// [&,counter=0](auto&& i_value) mutable
	// {
	// 	res.at(counter++) = i_value; 
	// } <<= i_number;

	return res;
}
template<literal_type ... T>
auto resolve(const union_symbolic_literal_type<T...>& i_number)
{
	typedef typename std::common_type<decltype(resolve(std::declval<T>()))...>::type resolved_type;

	return i_number.visit([](auto&& i_value) -> resolved_type
	{
		return resolve(i_value);
	});
}

constexpr bool operator<(const integer_symbolic_literal& i_lhs, const integer_symbolic_literal& i_rhs)
{
	return i_lhs.number() < i_rhs.number();
}
constexpr bool operator<(const rational_symbolic_literal& i_lhs, const rational_symbolic_literal& i_rhs)
{
	return i_lhs.numerator() * i_rhs.denominator() < i_rhs.numerator() * i_lhs.denominator();
}
template<literal_type T, literal_type TT>
constexpr inline bool operator<(const T& i_lhs, const TT& i_rhs)
{
    return resolve(i_lhs) < resolve(i_rhs);    
}
constexpr bool operator==(const integer_symbolic_literal& i_lhs, const integer_symbolic_literal& i_rhs)
{
	return i_lhs.number() == i_rhs.number();
}
constexpr bool operator==(const rational_symbolic_literal& i_lhs, const rational_symbolic_literal& i_rhs)
{
	return i_lhs.numerator() * i_rhs.denominator() == i_rhs.numerator() * i_lhs.denominator();
}
template<literal_type T, literal_type TT>
constexpr bool operator==(const T& i_lhs, const TT& i_rhs)
{
    return resolve(i_lhs) == resolve(i_rhs);
}

constexpr integer_symbolic_literal operator-(const integer_symbolic_literal& i_value)
{
	return { -i_value.number() };
}
constexpr rational_symbolic_literal operator-(const rational_symbolic_literal& i_value)
{
	return { -i_value.numerator(), i_value.denominator() };
}
constexpr root_symbolic_literal operator-(const root_symbolic_literal& i_value)
{
	return { i_value.number(),i_value.degree(),!i_value.positive() };
}
constexpr log_symbolic_literal operator-(const log_symbolic_literal& i_value)
{
	return { i_value.number(),i_value.base(),!i_value.positive() };
}
template<literal_type ... Operands>
constexpr auto operator-(const sum_symbolic_literal<Operands...>& i_value)
{
	typedef typename ddk::mpl::make_sequence<0,ddk::mpl::num_types<Operands...>>::type seq_t;

	return detail::inv(seq_t{},i_value);
}
template<literal_type ... Operands>
constexpr auto operator-(const prod_symbolic_literal<Operands...>& i_value)
{
	typedef typename ddk::mpl::make_sequence<0,ddk::mpl::num_types<Operands...>>::type seq_t;

	return detail::inv(seq_t{},i_value);
}
template<literal_type ... Operands>
constexpr auto operator-(const div_symbolic_literal<Operands...>& i_value)
{
	typedef typename ddk::mpl::make_sequence<0,ddk::mpl::num_types<Operands...>>::type seq_t;

	return detail::inv(seq_t{},i_value);
}
template<literal_type T>
constexpr auto operator-(const real_symbolic_literal<T>& i_lhs)
{
	return real_symbolic_literal{ -i_lhs.number() };
}

constexpr integer_symbolic_literal operator+(const integer_symbolic_literal& i_lhs, const integer_symbolic_literal& i_rhs)
{
	return { i_lhs.number() + i_rhs.number() };
}
constexpr rational_symbolic_literal operator+(const rational_symbolic_literal& i_lhs, const rational_symbolic_literal& i_rhs)
{
	return { i_lhs.numerator() * static_cast<int>(i_rhs.denominator()) + i_rhs.numerator() * static_cast<int>(i_lhs.denominator()), i_lhs.denominator() * i_rhs.denominator() };
}
template<literal_type T,literal_type TT>
constexpr auto operator+(const real_symbolic_literal<T>& i_lhs, const real_symbolic_literal<TT>& i_rhs)
{
	return real_symbolic_literal{ i_lhs.number() + i_rhs.number() };
}
template<literal_type T, literal_type TT>
constexpr auto operator+(const T& i_lhs, const TT& i_rhs)
{
	return sum_symbolic_literal{ i_lhs,i_rhs };
}

constexpr integer_symbolic_literal operator-(const integer_symbolic_literal& i_lhs, const integer_symbolic_literal& i_rhs)
{
	return i_lhs + (-i_rhs);
}
constexpr rational_symbolic_literal operator-(const rational_symbolic_literal& i_lhs, const rational_symbolic_literal& i_rhs)
{
	return i_lhs + (-i_rhs);
}
template<literal_type T,literal_type TT>
constexpr auto operator-(const real_symbolic_literal<T>& i_lhs, const real_symbolic_literal<TT>& i_rhs)
{
	return i_lhs + (-i_rhs);
}
template<literal_type T, literal_type TT>
constexpr auto operator-(const T& i_lhs, const TT& i_rhs)
{
	return i_lhs + (-i_rhs);
}

constexpr integer_symbolic_literal operator*(const integer_symbolic_literal& i_lhs,const integer_symbolic_literal& i_rhs)
{
	return { i_lhs.number() * i_rhs.number() };
}
constexpr rational_symbolic_literal operator*(const rational_symbolic_literal& i_lhs,const rational_symbolic_literal& i_rhs)
{
	return { i_lhs.numerator() * i_rhs.numerator(), i_lhs.denominator() * i_rhs.denominator() };
}
template<literal_type T,literal_type TT>
constexpr auto operator*(const real_symbolic_literal<T>& i_lhs, const real_symbolic_literal<TT>& i_rhs)
{
	return real_symbolic_literal{ i_lhs.number() * i_rhs.number()};
}
template<literal_type T, literal_type TT>
constexpr auto operator*(const T& i_lhs, const TT& i_rhs)
{
	return prod_symbolic_literal{ i_lhs,i_rhs };
}

constexpr rational_symbolic_literal operator/(const integer_symbolic_literal& i_lhs, const integer_symbolic_literal& i_rhs)
{
	return { i_lhs.number(),i_rhs.number() };
}
constexpr rational_symbolic_literal operator/(const rational_symbolic_literal& i_lhs, const rational_symbolic_literal& i_rhs)
{
	return { i_lhs.numerator() * static_cast<int>(i_rhs.denominator()), i_lhs.denominator() * i_rhs.numerator() };
}
template<literal_type T,literal_type TT>
constexpr auto operator/(const real_symbolic_literal<T>& i_lhs, const real_symbolic_literal<TT>& i_rhs)
{
	return real_symbolic_literal{ i_lhs.number() / i_rhs.number()};
}
template<literal_type T, literal_type TT>
constexpr auto operator/(const T& i_lhs, const TT& i_rhs)
{
	return div_symbolic_literal{ i_lhs,i_rhs };
}

}