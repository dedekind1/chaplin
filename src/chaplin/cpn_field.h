#pragma once

namespace cpn
{

template<ring_model Model, typename DivOperation>
using Field = typename Model::template embed<DivOperation>;

template<ring_type Structure, typename DivOperation, typename Set = typename Structure::value_type>
using field = algebraic_structure<Set,Field<Structure,DivOperation>>;

template<field_model T, size_t ... Dims>
struct pow_operation<field_operation,T,Dims...>
{
    PUBLISH_OPERATION_PROPERTIES(pow_operation,field_operation,concepts::algebraic_structure_properties<T,field_operation>);

	template<size_t ... Indexs, field_type TT, field_type TTT>
	static auto div_array(const ddk::mpl::sequence<Indexs...>&, const pow_type<TT,Dims...>& i_lhs,const pow_type<TTT,Dims...>& i_rhs)
	{
		typedef pow_type<decltype(std::declval<TT>() / std::declval<TTT>()),Dims...> pow_t;

		return 	pow_t{ i_lhs.at(Indexs) / i_rhs.at(Indexs) ... };
	}
	template<size_t ... Indexs, field_type TT, field_type TTT>
	static auto inv_array(const ddk::mpl::sequence<Indexs...>&, const pow_type<TT,Dims...>& i_rhs)
	{
		typedef pow_type<decltype(inv(std::declval<TT>())),Dims...> pow_t;

		return 	pow_t{ inv(i_rhs.at(Indexs)) ... };
	}
	TEMPLATE(field_type TT, typename ... Operations)
	REQUIRES(implements_operation<field_operation,TT,T>)
	friend inline auto operator/(const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_lhs,const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,ddk::mpl::prod_ranks<Dims...>>::type seq_t;
		typedef decltype(div_array(seq_t{},i_lhs,i_rhs)) pow_t;

		return algebraic_structure<pow_t,Operations...>{ div_array(seq_t{},i_lhs,i_rhs) };
	}
	TEMPLATE(field_type TT, typename ... Operations)
	REQUIRES(implements_operation<field_operation,TT,T>)
	friend inline auto inv(const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,ddk::mpl::prod_ranks<Dims...>>::type seq_t;
		typedef decltype(inv_array(seq_t{},i_rhs)) pow_t;

		return algebraic_structure<pow_t,Operations...>{ inv_array(seq_t{},i_rhs) };
	}
};

template<field_model ... T>
struct prod_operation<field_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(prod_operation,field_operation,typename ddk::mpl::intersect_type_packs<concepts::algebraic_structure_properties<T,field_operation>...>::type);

	static const size_t s_num_types = ddk::mpl::num_types<T...>;

	template<size_t ... Indexs, field_type ... TT, field_type ... TTT>
	static auto div_tuples(const ddk::mpl::sequence<Indexs...>&, const prod_type<TT...>& i_lhs,const prod_type<TTT...>& i_rhs)
	{
		return 	prod_type{ i_lhs.template get<Indexs>() / i_rhs.template get<Indexs>() ... };
	}
	template<size_t ... Indexs, field_type ... TT, field_type ... TTT>
	static auto inv_tuples(const ddk::mpl::sequence<Indexs...>&, const prod_type<TTT...>& i_rhs)
	{
		return 	prod_type{ inv(i_rhs.template get<Indexs>()) ... };
	}
	TEMPLATE(field_type ... TT, field_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<field_operation,prod_type<TT...>,T...>,implements_operation<field_operation,prod_type<TTT...>,T...>)
	friend inline auto operator/(const algebraic_structure<prod_type<TT...>,Operations...>& i_lhs,const algebraic_structure<prod_type<TTT...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,s_num_types>::type seq_t;
		typedef decltype(div_tuples(seq_t{},i_lhs,i_rhs)) prod_t;

		return algebraic_structure<prod_t,Operations...>{ div_tuples(seq_t{},i_lhs,i_rhs) };
	}
	TEMPLATE(field_type ... TT, typename ... Operations)
	REQUIRES(implements_operation<field_operation,prod_type<TT...>,T...>)
	friend inline auto inv(const algebraic_structure<prod_type<TT...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,s_num_types>::type seq_t;
		typedef decltype(inv_tuples(seq_t{},i_rhs)) prod_t;

		return algebraic_structure<prod_t,Operations...>{ inv_tuples(seq_t{},i_rhs) };
	}
};

template<field_model ... T>
struct sum_operation<field_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(sum_operation,field_operation,typename ddk::mpl::intersect_type_packs<concepts::algebraic_structure_properties<T,field_operation>...>::type);

	template<typename Return>
	struct div_field_operation
	{
		template<typename T1,typename T2>
		Return operator()(T1&& i_lhs,T2&& i_rhs) const
		{
			return i_lhs / i_rhs;
		}
	};
	template<typename Return>
	struct inv_field_operation
	{
		template<typename TT>
		Return operator()(TT&& i_lhs) const
		{
			return inv(std::forward<TT>(i_lhs));
		}
	};

	TEMPLATE(field_type ... TT, field_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<field_operation,sum_type<TT...>,T...>,implements_operation<field_operation,sum_type<TTT...>,T...>)
	friend inline auto operator/(const algebraic_structure<sum_type<TT...>,Operations...>& i_lhs,const algebraic_structure<sum_type<TTT...>,Operations...>& i_rhs)
	{
		typedef sum_type<decltype(std::declval<TT>() / std::declval<TTT>())...> sum_type_t;

		return algebraic_structure<sum_type_t,Operations...>{ ddk::visit<div_field_operation<sum_type_t>>(i_lhs,i_rhs) };
	}
	TEMPLATE(field_type ... TT, typename ... Operations)
	REQUIRES(implements_operation<field_operation,sum_type<TT...>,T...>)
	friend inline auto inv(const algebraic_structure<sum_type<TT...>,Operations...>& i_lhs)
	{
		typedef sum_type<decltype(inv(std::declval<TT>()))...> sum_type_t;

		return algebraic_structure<sum_type_t,Operations...>{ ddk::visit<inv_field_operation<sum_type_t>>(i_lhs) };
	}
};

template<model T>
using as_field = infer_model<T,field_operation>;

}
