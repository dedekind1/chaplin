
namespace cpn
{

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const sin_t& i_function)
{
    return detail::builtin_functor_function<Im(const Dom&),Allocator,cos_t>{ cos };
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const cos_t& i_function)
{
    return detail::builtin_minus_function<Im(const Dom&),Allocator>{ detail::builtin_functor_function<Im(const Dom&),Allocator,sin_t>{ sin } };
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const tan_t& i_function)
{
    return detail::builtin_pow_function<Im(const Dom&),Allocator>{ detail::builtin_functor_function<Im(const Dom&),Allocator,sec_t>{ sec },detail::builtin_number_function<Im(const Dom&),Allocator>{2} };
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const cotan_t& i_function)
{
    return detail::builtin_minus_function<Im(const Dom&),Allocator>{ detail::builtin_pow_function<Im(const Dom&),Allocator>{ detail::builtin_functor_function<Im(const Dom&),Allocator,cosec_t>{ cosec },detail::builtin_number_function<Im(const Dom&),Allocator>{2} } };
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const sec_t& i_function)
{
    return detail::builtin_mult_nary_function<Im(const Dom&),Allocator>{ detail::builtin_functor_function<Im(const Dom&),Allocator,sec_t>{ sec },detail::builtin_functor_function<Im(const Dom&),Allocator,tan_t>{ tan } };
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const cosec_t& i_function)
{
    return detail::builtin_minus_function<Im(const Dom&),Allocator>{ detail::builtin_mult_nary_function<Im(const Dom&),Allocator>{ detail::builtin_functor_function<Im(const Dom&),Allocator,cosec_t>{ cosec },detail::builtin_functor_function<Im(const Dom&),Allocator,cotan_t>{ cotan } } };
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const asin_t& i_function)
{
    return detail::builtin_inverted_function<Im(const Dom&),Allocator>{ detail::builtin_pow_function<Im(const Dom&),Allocator>{ detail::builtin_add_nary_function<Im(const Dom&),Allocator>{ detail::builtin_number_function<Im(const Dom&),Allocator>{1},detail::builtin_minus_function<Im(const Dom&),Allocator>{ detail::builtin_pow_function<Im(const Dom&),Allocator>{ detail::builtin_component_function<Im(const Dom&),Allocator>{0},detail::builtin_number_function<Im(const Dom&),Allocator>(2)}} },detail::builtin_number_function<Im(const Dom&),Allocator>{ 0.5f } } };
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const acos_t& i_function)
{
    return detail::builtin_minus_function<Im(const Dom&),Allocator>{ detail::builtin_inverted_function<Im(const Dom&),Allocator>{ detail::builtin_pow_function<Im(const Dom&),Allocator>{ detail::builtin_add_nary_function<Im(const Dom&),Allocator>{ detail::builtin_number_function<Im(const Dom&),Allocator>{1},detail::builtin_minus_function<Im(const Dom&),Allocator>{ detail::builtin_pow_function<Im(const Dom&),Allocator>{ detail::builtin_component_function<Im(const Dom&),Allocator>{0},detail::builtin_number_function<Im(const Dom&),Allocator>(2)}} },detail::builtin_number_function<Im(const Dom&),Allocator>{ 0.5f } } } };
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const atan_t& i_function)
{
    return detail::builtin_inverted_function<Im(const Dom&),Allocator>{ detail::builtin_add_nary_function<Im(const Dom&),Allocator>{ detail::builtin_number_function<Im(const Dom&),Allocator>{1},detail::builtin_pow_function<Im(const Dom&),Allocator>{ detail::builtin_component_function<Im(const Dom&),Allocator>{0},detail::builtin_number_function<Im(const Dom&),Allocator>(2) } } };
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const exp_t& i_function)
{
    return detail::builtin_minus_function<Im(const Dom&),Allocator>{ detail::builtin_functor_function<Im(const Dom&),Allocator,exp_t>{ exp } };
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&)> derivate(const derivative_context<Dom>& i_context, const ln_t& i_function)
{
    return detail::builtin_inverted_function<Im(const Dom&),Allocator>{ detail::builtin_functor_function<Im(const Dom&),Allocator,ln_t>{ ln }};
}

}