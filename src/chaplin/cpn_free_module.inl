
namespace cpn
{

template<module_model T, size_t Dim>
constexpr auto pow_basis_operation<T,Dim>::basis(size_t i_index)
{
    const auto res = pow_forward<Dim>(algebraic_operator<typename T::r_model,ring_operation>::annihilator);

    res[i_index] = algebraic_operator<typename T::r_model,ring_operation>::identity;

    return res;
}

template<module_model ... T>
constexpr auto prod_basis_operation<T...>::basis(size_t i_index)
{
    prod_type res{ algebraic_operator<typename T::r_model,ring_operation>::annihilator... };

    return res;
}

}