#pragma once

#include "cpn_rational_field.h"
#include "cpn_rational_algebra.h"

namespace cpn
{

using rationals = rational_field;

template<size_t ... Dims>
using rationals_n = pow_struct<rationals,Dims...>;

using rationals_1 = rationals_n<1>;
using rationals_2 = rationals_n<2>;
using rationals_3 = rationals_n<3>;
using rationals_4 = rationals_n<4>; 

}

#include "cpn_rational_vector_space.h"