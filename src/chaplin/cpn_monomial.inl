
namespace cpn
{

template<ring_type T, typename Allocator = ddk::system_allocator>
monomial<T,Allocator> __monomial_component(size_t i_comp)
{
    return monomial<T,Allocator>(T{ algebraic_operator<T,ring_operation>::identity },{{i_comp,1}});
}

template<ring_type T, typename Allocator>
monomial<T,Allocator>::monomial(const T& i_coeff, const monomial_exponents& i_exponents)
: m_coeff(i_coeff)
, m_exponents(i_exponents)
{
}
template<ring_type T, typename Allocator>
monomial<T,Allocator>::monomial(const T& i_coeff, const monomial_exponents& i_exponents, const bases_t& i_bases)
: m_coeff(i_coeff)
, m_exponents(i_exponents)
, m_bases(i_bases)
{
}
template<ring_type T, typename Allocator>
TEMPLATE(ring_type TT)
REQUIRES(ddk::is_assignable<T,TT>)
void monomial<T,Allocator>::set_coeff(TT&& i_coeff) const
{
    m_coeff = std::forward<TT>(i_coeff);
}
template<ring_type T, typename Allocator>
const T& monomial<T,Allocator>::coeff() const
{
    return m_coeff;
}
template<ring_type T, typename Allocator>
const monomial_exponents& monomial<T,Allocator>::exponents() const
{
    return m_exponents;
}
template<ring_type T, typename Allocator>
const typename monomial<T,Allocator>::bases_t& monomial<T,Allocator>::bases() const
{
    return m_bases;
}
template<ring_type T, typename Allocator>
TEMPLATE(ring_model TT, typename Order)
REQUIRED(implements_model<T,TT>)
monomial<T,Allocator> monomial<T,Allocator>::rebase(size_t i_component, const polynomial<TT,Order,Allocator>& i_poly) const
{
    bases_t otherBases = m_bases;

    otherBases.insert_or_assign(i_component,i_poly);

    return { m_coeff,m_exponents,otherBases };
}
template<ring_type T, typename Allocator>
monomial<T,Allocator>& monomial<T,Allocator>::shift(size_t i_component, size_t i_exponent)
{
    m_exponents[i_component] = i_exponent;

    return *this;
}
template<ring_type T, typename Allocator>
monomial<T,Allocator> monomial<T,Allocator>::shift(size_t i_component, size_t i_exponent) const
{
    monomial_exponents otherExponents = m_exponents;

    otherExponents[i_component] = i_exponent;

    return {m_coeff,otherExponents,m_bases};
}
template<ring_type T, typename Allocator>
size_t monomial<T,Allocator>::length() const
{
    return m_exponents.size();
}
template<ring_type T, typename Allocator>
bool monomial<T,Allocator>::operator==(const monomial& other) const
{
    return m_coeff == other.m_coeff && m_exponents == other.m_exponents;
}
template<ring_type T, typename Allocator>
bool monomial<T,Allocator>::operator!=(const monomial& other) const
{
    return m_coeff != other.m_coeff || m_exponents != other.m_exponents;
}

}