#pragma once

#include "cpn_builtin_expression_concepts.h"
#include "cpn_monomial.h"

namespace cpn
{
namespace concepts
{

template<typename Expression, typename T, typename Order, typename Allocator>
struct is_polynomial_instantiable_by_resolver
{
private:
	template<typename EExpression, typename = decltype(instance_polynomial<T,Order,Allocator>(std::declval<EExpression>()))>
	static std::true_type resolve(const EExpression&);
	static std::false_type resolve(...);

public:
	static const bool value = decltype(resolve(std::declval<Expression>()))::value;
};

template<typename T>
struct is_monomial_type_impl
{
private:
	template<typename R, typename Allocator>
	static std::true_type resolve(const monomial<R,Allocator>&);
	static std::false_type resolve(...);

public:
	static const bool value = decltype(resolve(std::declval<T>()))::value;
};

}

template<literal_expression Expression, ring_model T, typename Order, typename Allocator>
inline constexpr bool is_polynomial_instantiable_by = is_polynomial_expression<Expression> && concepts::is_polynomial_instantiable_by_resolver<Expression,T,Order,Allocator>::value;

template<typename T>
inline constexpr bool is_monomial_type = concepts::is_monomial_type_impl<T>::value;

}