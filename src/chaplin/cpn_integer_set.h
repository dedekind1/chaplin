#pragma once

#include "cpn_set.h"
#include "cpn_builtin_symbolic_literals.h"
#include "cpn_algebraic_structure.h"

namespace cpn
{

class integer
{
	friend inline auto resolve(const integer& i_value)
	{
		return cpn::resolve(i_value.number());
	}

public:
	constexpr integer(const integer_symbolic_literal& i_number);

	constexpr integer_symbolic_literal number() const;

private:
	integer_symbolic_literal m_number;
};

const auto integer_null = integer{ 0 };
const auto integer_unit = integer{ 1 };

struct integer_set_operation
{
	PUBLISH_OPERATION_PROPERTIES(integer_set_operation,set_operation,totally_ordered);

	typedef integer decay_t;

	friend inline constexpr bool operator<(const integer& i_lhs,const integer& i_rhs)
	{
		return i_lhs.number() < i_rhs.number();
	}
	friend inline constexpr bool operator==(const integer& i_lhs,const integer& i_rhs)
	{
		return i_lhs.number() == i_rhs.number();
	}
	friend inline constexpr bool operator!=(const integer& i_lhs,const integer& i_rhs)
	{
		return !(i_lhs.number() == i_rhs.number());
	}
};

using IntegerSet = algebraic_model<integer_set_operation>;
using integer_set = algebraic_structure<integer,integer_set_operation>;

template<size_t ... Dims>
using IntegerSet_n = pow_model<IntegerSet,Dims...>;
template<size_t ... Dims>
using integer_set_n = pow_struct<integer_set,Dims...>;

}

#include "cpn_integer_set.inl"