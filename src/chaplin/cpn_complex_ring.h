#pragma once

#include "cpn_complex_group.h"
#include "cpn_ring.h"

namespace cpn
{

struct complex_multiplication
{
    PUBLISH_OPERATION_PROPERTIES(complex_multiplication,ring_operation,commutative,associative,distributive);

	static constexpr complex_set identity = complex(1,0);
	static constexpr complex_set annihilator = complex(0,0);

	template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, typename ... Operations>
	friend inline auto operator*(const algebraic_structure<complex<T>,Operations...>& i_lhs, const algebraic_structure<complex<TT>,Operations...>& i_rhs)
	{
        typedef decltype(mult(i_lhs,i_rhs)) ret_type;

		return algebraic_structure<ret_type,Operations...>{ i_lhs.re().number() * i_rhs.re().number() - i_lhs.im().number() * i_rhs.im().number(),i_lhs.re().number() * i_rhs.im().number() + i_lhs.im().number() * i_rhs.re().number() };
	}

private:
	template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT>
	static inline auto mult(const complex<T>& i_lhs, const complex<TT>& i_rhs)
    {
        return complex{ i_lhs.re().number() * i_rhs.re().number() - i_lhs.im().number() * i_rhs.im().number(),i_lhs.re().number() * i_rhs.im().number() + i_lhs.im().number() * i_rhs.re().number() };
    }
};

using ComplexSemiRing = SemiRing<ComplexSemiGroup,complex_multiplication>;
template<solvable_literal_pack<2> T>
using complex_semi_ring = semi_ring<complex_semi_group<T>,complex_multiplication,complex<T>>;

template<size_t ... Dims>
using ComplexSemiRing_n = times_model<ComplexSemiRing,Dims...>;

using ComplexSemiRing_1 = ComplexSemiRing_n<1>;
using ComplexSemiRing_2 = ComplexSemiRing_n<2>;
using ComplexSemiRing_3 = ComplexSemiRing_n<3>;
using ComplexSemiRing_4 = ComplexSemiRing_n<4>;

template<solvable_literal_pack<2> ... T>
using complex_semi_ring_n = prod_struct<complex_semi_ring<T>...>;

template<solvable_literal_pack<2> T>
using complex_semi_ring_1 = complex_semi_ring_n<T>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT>
using complex_semi_ring_2 = complex_semi_ring_n<T,TT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT>
using complex_semi_ring_3 = complex_semi_ring_n<TTT,TTT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT, solvable_literal_pack<2> TTTT>
using complex_semi_ring_4 = complex_semi_ring_n<T,TT,TTT,TTTT>;

using ComplexRing = Ring<ComplexGroup,complex_multiplication>;
template<solvable_literal_pack<2> T>
using complex_ring = ring<complex_group<T>,complex_multiplication,complex<T>>;

template<size_t ... Dims>
using ComplexRing_n = times_model<ComplexRing,Dims...>;

using ComplexRing_1 = ComplexRing_n<1>;
using ComplexRing_2 = ComplexRing_n<2>;
using ComplexRing_3 = ComplexRing_n<3>;
using ComplexRing_4 = ComplexRing_n<4>;

template<solvable_literal_pack<2> ... T>
using complex_ring_n = prod_struct<complex_ring<T>...>;

template<solvable_literal_pack<2> T>
using complex_ring_1 = complex_ring_n<T>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT>
using complex_ring_2 = complex_ring_n<T,TT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT>
using complex_ring_3 = complex_ring_n<T,TT,TTT>;
template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT, solvable_literal_pack<2> TTT, solvable_literal_pack<2> TTTT>
using complex_ring_4 = complex_ring_n<T,TT,TTT,TTTT>;

}