#pragma once

#include "cpn_integer_group.h"
#include "cpn_ring.h"

namespace cpn
{

struct integer_multiplication
{
    PUBLISH_OPERATION_PROPERTIES(integer_multiplication,ring_operation,commutative,associative,distributive);

	static constexpr integer_set identity = integer(1);
	static constexpr integer_set annihilator = integer(0);

	template<typename ... Operations>
	friend inline auto operator*(const algebraic_structure<integer,Operations...>& i_lhs,const algebraic_structure<integer,Operations...>& i_rhs)
	{
		return algebraic_structure<integer,Operations...>{ i_lhs.number() * i_rhs.number() };
	}
};

using IntegerSemiRing = SemiRing<IntegerSemiGroup,integer_multiplication>;
using integer_semi_ring = semi_ring<integer_semi_group,integer_multiplication>;

template<size_t ... Dims>
using IntegerSemiRing_n = pow_model<IntegerSemiRing,Dims...>;

typedef IntegerSemiRing_n<1> IntegerSemiRing_1;
typedef IntegerSemiRing_n<2> IntegerSemiRing_2;
typedef IntegerSemiRing_n<3> IntegerSemiRing_3;
typedef IntegerSemiRing_n<4> IntegerSemiRing_4;

template<size_t ... Dims>
using integer_semi_ring_n = pow_struct<integer_semi_ring,Dims...>;

typedef integer_semi_ring_n<1> integer_semi_ring_1;
typedef integer_semi_ring_n<2> integer_semi_ring_2;
typedef integer_semi_ring_n<3> integer_semi_ring_3;
typedef integer_semi_ring_n<4> integer_semi_ring_4;

using IntegerRing = Ring<IntegerGroup,integer_multiplication>;
using integer_ring = ring<integer_group,integer_multiplication>;

template<size_t ... Dims>
using IntegerRing_n = pow_model<IntegerRing,Dims...>;

typedef IntegerRing_n<1> IntegerRing_1;
typedef IntegerRing_n<2> IntegerRing_2;
typedef IntegerRing_n<3> IntegerRing_3;
typedef IntegerRing_n<4> IntegerRing_4;

template<size_t ... Dims>
using integer_ring_n = pow_struct<integer_ring,Dims...>;

typedef integer_ring_n<1> integer_ring_1;
typedef integer_ring_n<2> integer_ring_2;
typedef integer_ring_n<3> integer_ring_3;
typedef integer_ring_n<4> integer_ring_4;

}