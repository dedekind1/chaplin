#pragma once

#include "cpn_integer_ring.h"
#include "cpn_integer_free_module.h"
#include "cpn_vector_space.h"

namespace cpn
{

template<size_t Dim, ring_model R = IntegerRing>
using I_n = VectorSpace<IntegerFreeModule_n<R,Dim>,canonical_vector_mult_operation>;

using I1 = I_n<1>;
using I2 = I_n<2>;
using I3 = I_n<3>;
using I4 = I_n<4>;

template<size_t Dim, ring_model R = IntegerRing>
using i_n = vector_space<integer_free_module_n<R,Dim>,canonical_vector_mult_operation,pow_type<integer,Dim>>;

using i1 = i_n<1>;
using i2 = i_n<2>;
using i3 = i_n<3>;
using i4 = i_n<4>;

}