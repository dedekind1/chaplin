
namespace cpn
{

template<set_model T>
derivative_context<T>::derivative_context(const operator_tag_id& i_id)
: operator_context(i_id)
{
}
template<set_model T>
template<set_model Im, typename Allocator>
function<Im(const T&),Allocator> derivative_context<T>::derivative(const function<Im(const T&),Allocator>& i_func) const
{
	return i_func->unary_operator({ get_operator_tag_id<function_operators<function<Im(const T&),Allocator>>,derivative_operation>() });
}
template<set_model T>
bool derivative_context<T>::test(size_t i_comp) const
{
    return i_comp == 0;
}

template<vector_space_model T>
template<module_type TT>
derivative_context<T>::bool_conversor::bool_conversor(const TT& i_value)
: m_value(i_value != TT{ algebraic_operator<TT,mod_operation>::identity })
{
}
template<vector_space_model T>
derivative_context<T>::bool_conversor::operator bool() const
{
	return m_value;	
}

template<vector_space_model T>
TEMPLATE(vector_space_type TT)
REQUIRED(implements_model<TT,T>)
derivative_context<T>::derivative_context(const operator_tag_id& i_id, const TT& i_direction)
: operator_context(i_id)
, m_direction(i_direction)
{ 
}
template<vector_space_model T>
template<set_model Im, typename Allocator>
constexpr function<Im(const T&),Allocator> derivative_context<T>::derivative(const function<Im(const T&),Allocator>& i_func) const
{
	detail::builtin_add_nary_function<Im(const T&),Allocator> res;

	for(size_t index=0;index<T::rank();index++)
	{
		res.push(detail::builtin_mult_nary_function<Im(const T&),Allocator>{ i_func->unary_operator(*this), detail::builtin_number_function<Im(const T&),Allocator>{ project_onto<final_object<Im>>(m_direction,index) } });
	}

	return res;
}
template<vector_space_model T>
bool derivative_context<T>::test(size_t i_comp) const
{
	return project_onto<bool_conversor>(m_direction,i_comp);
}

}