#pragma once

#include "cpn_algebraic_defs.h"
#include "cpn_function_allocator.h"
#include "cpn_algebraic_operators.h"
#include "ddkFramework/ddk_function.h"
#include "ddkFramework/ddk_type_id.h"

namespace cpn
{

typedef ddk::system_allocator function_allocator;
typedef ddk::Id<size_t,struct ___builtin_function_id> builtin_function_id;

template<typename,typename = function_allocator>
class function;

namespace detail
{

template<model Im, model Dom>
class function_base : public ddk::detail::function_base<final_object<Im>,final_object<Dom>>
{
public:
    virtual function<Im(const Dom&)> clone() const = 0;
    virtual builtin_function_id id() const = 0;
    virtual function<Im(const Dom&)> binary_operator(const operator_context& i_context, const function_base<Im,Dom>& other, size_t i_depth = 0) const = 0;
    virtual function<Im(const Dom&)> unary_operator(const operator_context& i_context) const = 0;
};

template<typename Function>
struct function_compare_by_id
{
    inline bool operator()(const Function& i_rhs, const Function& i_lhs) const;
};

}
}

#include "cpn_function_impl.inl"