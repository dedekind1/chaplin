#pragma once

#include "cpn_rational_ring.h"
#include "cpn_rational_free_module.h"
#include "cpn_vector_space.h"

namespace cpn
{

template<size_t Dim, ring_model R = RationalRing>
using Q_n = VectorSpace<RationalFreeModule_n<R,Dim>,canonical_vector_mult_operation>;

using Q1 = Q_n<1>;
using Q2 = Q_n<2>;
using Q3 = Q_n<3>;
using Q4 = Q_n<4>;

template<size_t Dim, ring_model R = RationalRing>
using q_n = vector_space<rational_free_module_n<R,Dim>,canonical_vector_mult_operation,pow_type<rational,Dim>>;

using q1 = q_n<1>;
using q2 = q_n<2>;
using q3 = q_n<3>;
using q4 = q_n<4>;

}