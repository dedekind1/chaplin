
#include "ddkFramework/ddk_iterable.h"

namespace cpn
{

constexpr integer_symbolic_literal::integer_symbolic_literal(int i_number)
: m_number(i_number)
{
}
constexpr int integer_symbolic_literal::number() const
{
	return m_number;
}

constexpr rational_symbolic_literal::rational_symbolic_literal(int i_numerator, unsigned int i_denominator)
: integer_symbolic_literal(i_numerator)
, m_denominator(i_denominator)
{
}
constexpr rational_symbolic_literal::rational_symbolic_literal(int i_numerator, int i_denominator)
: integer_symbolic_literal((i_denominator > 0) ? i_numerator : -i_numerator)
, m_denominator(static_cast<unsigned int>(i_denominator))
{
}
constexpr int rational_symbolic_literal::numerator() const
{
	return number();
}
constexpr unsigned int rational_symbolic_literal::denominator() const
{
	return m_denominator;
}

constexpr root_symbolic_literal::root_symbolic_literal(int i_number,int i_degree, bool i_positive)
: m_number(i_number)
, m_degree(i_degree)
, m_sign(i_positive)
{
}
constexpr int root_symbolic_literal::number() const
{
	return m_number;
}
constexpr int root_symbolic_literal::degree() const
{
	return m_degree;
}
constexpr bool root_symbolic_literal::positive() const
{
	return m_sign;
}

constexpr log_symbolic_literal::log_symbolic_literal(unsigned int i_number, unsigned int i_logBase, bool i_positive)
: m_number(i_number)
, m_base(i_logBase)
, m_sign(i_positive)
{
}
constexpr unsigned int log_symbolic_literal::number() const
{
	return m_number;
}
constexpr unsigned int log_symbolic_literal::base() const
{
	return m_base;
}
constexpr bool log_symbolic_literal::positive() const
{
	return m_sign;
}

template<literal_type ... Operands>
TEMPLATE(typename ... Args)
REQUIRED(ddk::is_constructible<Operands,Args>...)
constexpr monoid_symbolic_literal<Operands...>::monoid_symbolic_literal(Args&& ... i_args)
: m_operands(std::forward<Args>(i_args)...)
{
}
template<literal_type ... Operands>
template<size_t Index>
constexpr auto monoid_symbolic_literal<Operands...>::get() const
{
	return m_operands.template get<Index>();
}
template<literal_type ... Operands>
TEMPLATE(typename Callable)
REQUIRED(ddk::is_callable<Callable,Operands>...)
constexpr void monoid_symbolic_literal<Operands...>::enumerate(Callable&& i_callable) const
{
	[&](auto&& i_operand)
	{
		ddk::eval(i_callable,i_operand);
	} <<= m_operands;
}

template<solvable_literal_type T>
TEMPLATE(typename TT)
REQUIRED(ddk::is_constructible<T,TT>)
constexpr real_symbolic_literal<T>::real_symbolic_literal(TT&& i_value)
: m_value(std::forward<TT>(i_value))
{
}
template<solvable_literal_type T>
constexpr  T real_symbolic_literal<T>::number() const
{
	return m_value;
}

template<numeric_type T>
constexpr real_symbolic_literal<T>::real_symbolic_literal(const T& i_value)
: m_value(i_value)
{
}
template<numeric_type T>
template<solvable_literal_type TT>
constexpr real_symbolic_literal<T>::real_symbolic_literal(const real_symbolic_literal<TT>& i_value)
: m_value(resolve(i_value.number()))
{
}
template<numeric_type T>
constexpr  T real_symbolic_literal<T>::number() const
{
	return m_value;
}

}