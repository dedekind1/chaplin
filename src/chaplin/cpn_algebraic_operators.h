#pragma once

#include "cpn_algebraic_concepts.h"

namespace cpn
{

struct operator_context
{
public:
    operator_context(const operator_tag_id& i_id);

    operator_tag_id id() const;

private:
    const operator_tag_id m_id;
};

typedef algebraic_operators<> empty_operators;

template<typename ...>
struct make_algebraic_operators;

template<typename T, typename ... TT>
struct make_algebraic_operators<T,TT...>
{
    typedef make_algebraic_operators<TT...>::type::template embed<T> type;
};

template<typename ... TT>
struct make_algebraic_operators<no_op,TT...>
{
    typedef make_algebraic_operators<TT...>::type type;
};

template<typename ... T>
struct make_algebraic_operators<operator_tags<T...>>
{
    typedef algebraic_operators<T...> type;
};

template<typename T>
struct make_algebraic_operators<T>
{
    typedef algebraic_operators<T> type;
};

template<>
struct make_algebraic_operators<no_op>
{
    typedef algebraic_operators<> type;
};

template<typename,typename ...>
struct merge_algebraic_operators;

template<typename T, typename TT>
struct merge_algebraic_operators<T,TT>
{
private:
    template<typename ... Ops>
    static algebraic_operators<Ops...> resolve(const operator_tags<Ops...>&);

public:
    typedef decltype(resolve(std::declval<typename ddk::mpl::merge_pair_type_packs<typename T::_operators,typename TT::_operators>::type>())) type;
};
template<typename T, typename TT, typename ... TTT>
struct merge_algebraic_operators<T,TT,TTT...>
{
    typedef merge_algebraic_operators<T,TT>::type partial_type;

public:
    typedef merge_algebraic_operators<partial_type,TTT...>::type type;
};

template<typename,template<typename>typename>
struct filter_algebraic_operators;

template<typename ... Operators, template<typename> typename Predicate>
struct filter_algebraic_operators<algebraic_operators<Operators...>,Predicate>
{
    typedef typename merge_algebraic_operators<typename ddk::mpl::which_type<Predicate<Operators>::value,algebraic_operators<Operators>,empty_operators>::type...>::type type;
};

template<typename ... Operators>
struct algebraic_operators<algebraic_operators<Operators...>> : public Operators ...
{
    typedef typename ddk::mpl::merge_type_packs<typename Operators::__operator_tags...>::type _operator_tags;
    
    typedef ddk::mpl::type_pack<normalized_operation<Operators>...> _operators;
    template<typename ... OOperators>
    inline static constexpr bool contains = (ddk::mpl::is_among_types<OOperators,Operators...> && ...); 
    template<typename OperatorTag>
    inline static constexpr size_t pos_of_tag = _operator_tags::template pos_in_type_pack<OperatorTag>();
    template<typename ... OOperators>
    using embed = algebraic_operators<Operators...,OOperators...>;
    typedef algebraic_operators<Operators...> operators;
    template<typename ... OperatorTags>
    using drop = typename make_algebraic_operators<typename _operators::template drop<decltype(resolve_operation(std::declval<operators>(),std::declval<OperatorTags>()))...>::type>::type;
    static const size_t s_numOps = _operator_tags::size();
};

template<typename ... Operators>
struct algebraic_operators : public Operators...
{
    typedef typename ddk::mpl::merge_type_packs<typename Operators::__operator_tags...>::type _operator_tags;

    typedef ddk::mpl::type_pack<normalized_operation<Operators>...> _operators;
    template<typename ... OOperators>
    inline static constexpr bool contains = (ddk::mpl::is_among_types<OOperators,Operators...> && ...); 
    template<typename OperatorTag>
    inline static constexpr size_t pos_of_tag = _operator_tags::template pos_in_type_pack<OperatorTag>();
    template<typename ... OOperators>
    using embed = algebraic_operators<Operators...,OOperators...>;
    typedef algebraic_operators<Operators...> operators;
    template<typename ... OperatorTags>
    using drop = typename make_algebraic_operators<typename _operators::template drop<decltype(resolve_operation(std::declval<operators>(),std::declval<OperatorTags>()))...>::type>::type;
    static const size_t s_numOps = _operator_tags::size();
};

template<typename Space, typename T>
constexpr operator_tag_id get_operator_tag_id();

}

#include "cpn_algebraic_operators.inl"