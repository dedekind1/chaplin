#pragma once

#include "cpn_real_set.h"

namespace cpn
{

template<solvable_literal_pack<2> T>
class complex
{
	friend inline auto resolve(const complex& i_value)
	{
		return complex{ resolve(i_value.real()),resolve(i_value.imaginary()) };
	}

public:
    typedef typename T::nth_type<0> real_t;
    typedef typename T::nth_type<1> im_t;

    template<solvable_literal_type TT, solvable_literal_type TTT>
	constexpr complex(const TT& i_real, const TTT& i_im);

	constexpr real<real_t> re() const;
	constexpr real<im_t> im() const;

private:
    real<real_t> m_real;
    real<im_t> m_im;
};
template<solvable_literal_type T, solvable_literal_type TT>
complex(const T&,const TT&) -> complex<ddk::mpl::type_pack<T,TT>>;
template<solvable_literal_type T, solvable_literal_type TT>
complex(const real_symbolic_literal<T>&, const real_symbolic_literal<TT>&) -> complex<ddk::mpl::type_pack<T,TT>>;
template<solvable_literal_type T, solvable_literal_type TT>
complex(const real<T>&,const real<TT>&) -> complex<ddk::mpl::type_pack<T,TT>>;

const auto complex_null = complex{ 0,0 };
const auto complex_unit = complex{ 1,0 };

struct complex_conjugate_operation
{
	PUBLISH_OPERATION_PROPERTIES(complex_conjugate_operation,conjugate_operation);

    template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT>
	friend inline constexpr bool conjugate(const complex<T>& i_lhs)
	{
		return complex{ i_lhs.re().number(),-i_lhs.im().number() };
	}
};

struct complex_set_operation : complex_conjugate_operation
{
	PUBLISH_OPERATION_PROPERTIES(complex_set_operation,set_operation);

	typedef complex<ddk::mpl::type_pack<float,float>> decay_t;

    template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT>
	friend inline constexpr bool operator==(const complex<T>& i_lhs, const complex<TT>& i_rhs)
	{
		return i_lhs.re().number() == i_rhs.re().number() && i_lhs.im().number() == i_rhs.im().number();
	}
    template<solvable_literal_pack<2> T, solvable_literal_pack<2> TT>
	friend inline constexpr bool operator!=(const complex<T>& i_lhs, const complex<TT>& i_rhs)
	{
		return !(i_lhs.re().number() == i_rhs.re().number() && i_lhs.im().number() == i_rhs.im().number());
	}
};

using ComplexSet = algebraic_model<complex_set_operation>;
template<typename T>
using complex_set = algebraic_structure<complex<T>,complex_set_operation>;

template<size_t ... Dims>
using ComplexSet_n = pow_model<ComplexSet,Dims...>;
template<typename ... T>
using complex_set_n = prod_struct<complex_set<T>...>;

}

#include "cpn_complex_set.inl"