#include "cpn_function.h"

namespace cpn
{
namespace detail
{

builtin_function_id get_next_builtin_function_id()
{
    static size_t __id = 0;

    return builtin_function_id{ __id++ };
}

}
}