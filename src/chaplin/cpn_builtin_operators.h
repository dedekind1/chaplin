#pragma once

#include "cpn_function_impl.h"
#include "cpn_algebraic_defs.h"

namespace cpn
{
namespace detail
{

template<typename SuperClass, typename Im, typename Dom, typename Allocator, typename OperationTag>
struct builtin_operator<builtin_function<SuperClass,Im,Dom,Allocator>,OperationTag>
{
};

template<typename T, typename OperatorTag>
struct builtin_operator : public builtin_operator<typename T::base_t,OperatorTag>
{
};

template<typename Class>
struct builtin_operators_init
{
private:
    typedef typename Class::base_t base_t;

    template<typename T, typename = decltype(&builtin_operator<Class,T>::binary_operator)>
    static auto resolve_binary(const T&);
    static auto resolve_binary(...);

    template<typename T, typename = decltype(&builtin_operator<Class,T>::unary_operator)>
    static auto resolve_unary(const T&);
    static auto resolve_unary(...);

public:
    template<typename ... OperatorTags>
    static auto binary(const operator_tags<OperatorTags...>&);
    template<typename ... OperatorTags>
    static auto unary(const operator_tags<OperatorTags...>&);
};

}
}

#include "cpn_builtin_operators.inl"
#include "cpn_arithmetic_operators.h"
#include "cpn_derivative_operators.h"