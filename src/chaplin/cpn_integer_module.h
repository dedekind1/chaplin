#pragma once

#include "cpn_integer_ring.h"
#include "cpn_module.h"

namespace cpn
{

template<ring_model R>
struct integer_module_operation
{
    PUBLISH_OPERATION_PROPERTIES(integer_module_operation,mod_operation,commutative,associative,distributive);

	typedef R r_model;
	static constexpr auto identity = algebraic_operator<R,ring_operation>::identity;

	TEMPLATE(ring_type RR, typename ... Operations)
	REQUIRES(implements_model<RR,r_model>)
	friend inline auto operator^(const RR& i_lhs, const algebraic_structure<integer,Operations...>& i_rhs)
	{
		return algebraic_structure<integer,Operations...>{ i_lhs.number() * i_rhs.number() };
	}
};

template<ring_model R = IntegerRing>
using IntegerModule = Module<IntegerGroup,integer_module_operation<R>>;
template<ring_model R = IntegerRing>
using integer_module = module<integer_group,integer_module_operation<R>>;

template<ring_model R, size_t ... Dims>
using IntegerModule_n = pow_model<IntegerModule<R>,Dims...>;

template<ring_model R = IntegerRing>
using IntegerModule_1 = IntegerModule_n<R,1>;
template<ring_model R = IntegerRing>
using IntegerModule_2 = IntegerModule_n<R,2>;
template<ring_model R = IntegerRing>
using IntegerModule_3 = IntegerModule_n<R,3>;
template<ring_model R = IntegerRing>
using IntegerModule_4 = IntegerModule_n<R,4>;

template<ring_model R, size_t ... Dims>
using integer_module_n = pow_struct<integer_module<R>,Dims...>;

template<ring_model R = IntegerRing>
using integer_module_1 = integer_module_n<R,1>;
template<ring_model R = IntegerRing>
using integer_module_2 = integer_module_n<R,2>;
template<ring_model R = IntegerRing>
using integer_module_3 = integer_module_n<R,3>;
template<ring_model R = IntegerRing>
using integer_module_4 = integer_module_n<R,4>;

}