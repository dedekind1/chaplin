#pragma once

#include "cpn_real_set.h"
#include "cpn_group.h"

namespace cpn
{

struct real_addition
{
    PUBLISH_OPERATION_PROPERTIES(real_addition,semi_group_operation,commutative,associative,distributive);

	static constexpr real_set identity = real(0);

	template<solvable_literal_type T, solvable_literal_type TT, typename ... Operations>
	friend inline auto operator+(const algebraic_structure<real<T>,Operations...>& i_lhs, const algebraic_structure<real<TT>,Operations...>& i_rhs)
	{
		typedef decltype(std::declval<T>() + std::declval<TT>()) ret_type;

		return algebraic_structure<real<ret_type>,Operations...>{ i_lhs.number() + i_rhs.number() };
	}
};

using RealSemiGroup = SemiGroup<RealSet,real_addition>;
template<solvable_literal_type T>
using real_semi_group = semi_group<real_set<T>,real_addition,real<T>>;

template<size_t ... Dims>
using RealSemiGroup_n = times_model<RealSemiGroup,Dims...>;

using RealSemiGroup_1 = RealSemiGroup_n<1>;
using RealSemiGroup_2 = RealSemiGroup_n<2>;
using RealSemiGroup_3 = RealSemiGroup_n<3>;
using RealSemiGroup_4 = RealSemiGroup_n<4>;

template<solvable_literal_type ... T>
using real_semi_group_n = prod_struct<real_semi_group<T>...>;

template<solvable_literal_type T>
using real_semi_group_1 = real_semi_group_n<T>;
template<solvable_literal_type T, solvable_literal_type TT>
using real_semi_group_2 = real_semi_group_n<T,TT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT>
using real_semi_group_3 = real_semi_group_n<T,TT,TTT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, solvable_literal_type TTTT>
using real_semi_group_4 = real_semi_group_n<T,TT,TTT,TTTT>;

struct real_addition_inverse
{
    PUBLISH_OPERATION_PROPERTIES(real_addition_inverse,group_operation);

	template<solvable_literal_type T, typename ... Operations>
	friend inline auto operator-(const algebraic_structure<real<T>,Operations...>& i_lhs)
	{
		typedef decltype(-std::declval<T>()) ret_type;

		return algebraic_structure<real<ret_type>,Operations...>{ -i_lhs.number() };
	}
	template<solvable_literal_type T, solvable_literal_type TT, typename ... Operations>
	friend inline auto operator-(const algebraic_structure<real<T>,Operations...>& i_lhs, const algebraic_structure<real<TT>,Operations...>& i_rhs)
	{
		typedef decltype(std::declval<T>() + (-std::declval<TT>())) ret_type;

		return algebraic_structure<real<ret_type>,Operations...>{ i_lhs.number() + (-i_rhs.number()) };
	}
};

using RealGroup = Group<RealSemiGroup,real_addition_inverse>;
template<solvable_literal_type T>
using real_group = group<real_semi_group<T>,real_addition_inverse,real<T>>;

template<size_t ... Dims>
using RealGroup_n = times_model<RealGroup,Dims...>;

using RealGroup_1 = RealGroup_n<1>;
using RealGroup_2 = RealGroup_n<2>;
using RealGroup_3 = RealGroup_n<3>;
using RealGroup_4 = RealGroup_n<4>;

template<solvable_literal_type ... T>
using real_group_n = prod_struct<real_group<T>...>;

template<solvable_literal_type T>
using real_group_1 = real_group_n<T>;
template<solvable_literal_type T, solvable_literal_type TT>
using real_group_2 = real_group_n<T,TT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT>
using real_group_3 = real_group_n<T,TT,TTT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, solvable_literal_type TTTT>
using real_group_4 = real_group_n<T,TT,TTT,TTTT>;

}