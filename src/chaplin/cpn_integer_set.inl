
namespace cpn
{

constexpr integer::integer(const integer_symbolic_literal& i_number)
: m_number(i_number)
{
}
constexpr integer_symbolic_literal integer::number() const
{
    return m_number;
}

}