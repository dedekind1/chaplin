#pragma once

#include "cpn_function.h"

namespace cpn
{

template<typename,typename = ddk::system_allocator>
class linear_function;

template<flat_type Im, typename Dom, typename Allocator>
class linear_function<Im(const Dom&),Allocator> : ddk::detail::function_impl<Im(const Dom&),Allocator,detail::function_base>
{
    typedef ddk::detail::function_impl<Im(const Dom&),Allocator,detail::function_base> base_t;

public:
    TEMPLATE(literal_expression Expression)
    REQUIRES(is_linear_expression<Expression>)
    linear_function(Expression&& i_exp);
    linear_function(const Im& i_value);

   	TEMPLATE(typename ... Args)
    REQUIRES(ddk::is_constructible<Dom,Args...>)
    inline NO_DISCARD_RETURN auto operator()(Args&& ... args) const;
};

template<intersection_type Im, typename Dom, typename Allocator>
class linear_function<Im(const Dom&),Allocator> : public ddk::detail::function_impl<Im(const Dom&),Allocator,detail::function_base>
{
    typedef ddk::detail::function_impl<Im(const Dom&),Allocator,detail::function_base> base_t;

    template<typename ... Callable>
    class fusion_callable : public ddk::detail::inherited_functor_impl<detail::function_base<Im,const Dom&>>
    {
    public:
        fusion_callable(const Callable& ... i_callables);
        Im operator()(const Dom& i_args) const override final;

    private:
        template<size_t ... Indexs>
        Im execute(const ddk::mpl::sequence<Indexs...>&, const Dom& i_args) const;

        ddk::tuple<Callable...> m_callables;
    };

public:
    TEMPLATE(literal_expression ... Expression)
    REQUIRES(ddk::is_num_of_args_equal<Im::num_places,Expression...>,is_linear_expression<Expression>...)
    linear_function(Expression&& ... i_exps);
    linear_function(const Im& i_value);

   	TEMPLATE(typename ... Args)
    REQUIRES(ddk::is_constructible<Dom,Args...>)
    inline NO_DISCARD_RETURN auto operator()(Args&& ... args) const;

private:
    TEMPLATE(typename ... Callable)
    REQUIRES(ddk::is_num_of_args_equal<Im::num_places,Callable...>)
    static auto make_fusion(Callable&& ... i_callables);

};

}

#include "cpn_linear_function.inl"