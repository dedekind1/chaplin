#pragma once

#include "cpn_algebraic_structure.h"
#include "cpn_set.h"
#include "cpn_group.h"
#include "cpn_ring.h"

namespace cpn
{

template<group_model Model, typename ModOperation>
using Module = typename Model::template embed<ModOperation>;

template<group_type Structure, typename ModOperation, typename Set = typename Structure::value_type>
using module = algebraic_structure<Set,Module<Structure,ModOperation>>;

TEMPLATE(ring_model T, ring_model R)
REQUIRES(implements_model<T,R>)
struct infer_operation<T,mod_operation,R>
{
	PUBLISH_OPERATION_PROPERTIES(infer_operation,mod_operation);

	typedef R r_model;
	static constexpr auto identity = algebraic_operator<R,ring_operation>::identity;

	TEMPLATE(ring_type TT, ring_type TTT)
	REQUIRES(implements_model<TT,R>,implements_model<TTT,T>)
	friend inline auto operator^(const TT& i_lhs, const TTT& i_rhs)
	{
		return TTT{ i_lhs } * i_rhs;
	}
};

template<module_model T,size_t ... Dims>
struct pow_operation<mod_operation,T,Dims...>
{
    PUBLISH_OPERATION_PROPERTIES(pow_operation,mod_operation);

	typedef typename algebraic_operator<T,mod_operation>::r_model r_model;
	static constexpr auto identity = algebraic_operator<T,mod_operation>::identity;

	template<size_t ... Indexs, ring_type TT, group_type TTT>
	static auto mod_array(const ddk::mpl::sequence<Indexs...>&, const TT& i_lhs,const pow_type<TTT,Dims...>& i_rhs)
	{
		typedef pow_type<decltype(std::declval<TT>() ^ std::declval<TTT>()),Dims...> pow_t;

		return 	pow_t{ i_lhs ^ i_rhs.at(Indexs) ... };
	}
	template<size_t ... Indexs, group_type TT, ring_type TTT>
	static auto mod_array(const ddk::mpl::sequence<Indexs...>&, const pow_type<TT,Dims...>& i_lhs, const TTT& i_rhs)
	{
		typedef pow_type<decltype(std::declval<TT>() ^ std::declval<TTT>()),Dims...> pow_t;

		return 	pow_t{ i_lhs.at(Indexs) ^ i_rhs ... };
	}
	TEMPLATE(ring_type TT, module_type TTT, typename ... Operations)
	REQUIRES(implements_model<TT,r_model>,implements_model<TTT,T>)
	friend inline auto operator^(const TT& i_lhs, const algebraic_structure<pow_type<TTT,Dims...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,ddk::mpl::prod_ranks<Dims...>>::type seq_t;
		typedef decltype(mod_array(seq_t{},i_lhs,i_rhs)) pow_t;

		return algebraic_structure<pow_t,Operations...>{ mod_array(seq_t{},i_lhs,i_rhs) };
	}
	TEMPLATE(module_type TT, typename ... Operations, ring_type TTT)
	REQUIRES(implements_model<TT,T>,implements_model<TTT,r_model>)
	friend inline auto operator^(const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_lhs, const TTT& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,ddk::mpl::prod_ranks<Dims...>>::type seq_t;
		typedef decltype(mod_array(seq_t{},i_lhs,i_rhs)) pow_t;

		return algebraic_structure<pow_t,Operations...>{ mod_array(seq_t{},i_lhs,i_rhs) };
	}
};

template<module_model ... T>
struct prod_operation<mod_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(prod_operation,mod_operation,typename ddk::mpl::intersect_type_packs<concepts::algebraic_structure_properties<T,mod_operation>...>::type);

	static const size_t s_num_types = ddk::mpl::num_types<T...>;
	typedef ddk::mpl::homogeneous_type<algebraic_operator<T,mod_operation>...> mod_operator;
	typedef typename mod_operator::r_model r_model;
	static constexpr auto identity = mod_operator::identity;

	template<size_t ... Indexs, ring_type TT, group_type ... TTT>
	static auto mod_tuples(const ddk::mpl::sequence<Indexs...>&, const TT& i_lhs,const prod_type<TTT...>& i_rhs)
	{
		return 	prod_type{ i_lhs ^ i_rhs.template get<Indexs>() ... };
	}
	template<size_t ... Indexs, group_type ... TT, ring_type TTT>
	static auto mod_tuples(const ddk::mpl::sequence<Indexs...>&, const prod_type<TT...>& i_lhs, const TTT& i_rhs)
	{
		return 	prod_type{ i_lhs.template get<Indexs>() ^ i_rhs ... };
	}

	TEMPLATE(ring_type TT, module_type ... TTT, typename ... Operations)
	REQUIRES(implements_model<TT,r_model>,implements_model<prod_type<TTT...>,T...>)
	friend inline auto operator^(const TT& i_lhs, const algebraic_structure<prod_type<TTT...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,s_num_types>::type seq_t;
		typedef decltype(mod_tuples(seq_t{},i_lhs,i_rhs)) prod_t;

		return algebraic_structure<prod_t,Operations...>{ mod_tuples(seq_t{},i_lhs,i_rhs) };
	}
	TEMPLATE(module_type ... TT, typename ... Operations, ring_type TTT)
	REQUIRES(implements_model<prod_type<TT...>,T...>,implements_model<TTT,r_model>)
	friend inline auto operator^(const algebraic_structure<prod_type<TT...>,Operations...>& i_lhs, const TTT& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,s_num_types>::type seq_t;
		typedef decltype(mod_tuples(seq_t{},i_lhs,i_rhs)) prod_t;

		return algebraic_structure<prod_t,Operations...>{ mod_tuples(seq_t{},i_lhs,i_rhs) };
	}
};

template<module_model ... T>
struct sum_operation<mod_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(sum_operation,mod_operation,typename ddk::mpl::intersect_type_packs<concepts::algebraic_structure_properties<T,mod_operation>...>::type);

	template<typename Return>
	struct module_prod_operation
	{
    public:
	    module_prod_operation() = default;

		template<typename T1, typename T2>
		inline Return operator()(T1&& i_lhs, T2&& i_rhs) const
		{
			    return i_lhs ^ i_rhs;
		}
	};

	static const sum_type<T...> identity;
	TEMPLATE(ring_type TT, module_type ... TTT, typename ... Operations)
	REQUIRES(implements_model<sum_type<TTT...>,T...>)
	friend inline auto operator^(const TT& i_lhs,const algebraic_structure<sum_type<TTT...>,Operations...>& i_rhs)
	{
		typedef sum_type<decltype(std::declval<TT>() ^ std::declval<TTT>())...> sum_type_t;

		return algebraic_structure<sum_type_t,Operations...>{ ddk::visit<module_prod_operation<sum_type_t>>(i_lhs,i_rhs) };
	}
	TEMPLATE(module_type ... TT, typename ... Operations, ring_type TTT)
	REQUIRES(implements_model<sum_type<TT...>,T...>)
	friend inline auto operator^(const algebraic_structure<sum_type<TT...>,Operations...>& i_lhs, const TTT& i_rhs)
	{
		typedef sum_type<decltype(std::declval<TT>() ^ std::declval<TTT>())...> sum_type_t;

		return algebraic_structure<sum_type_t,Operations...>{ ddk::visit<module_prod_operation<sum_type_t>>(i_lhs,i_rhs) };
	}
};

template<model T, ring_model R = T>
using as_module = infer_model<T,mod_operation,R>;

}


#include "cpn_module.inl"
