
#include "cpn_function_derivative.h"

namespace cpn
{
namespace detail
{

template<typename SuperClass, set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_function<SuperClass,Im,Dom,Allocator>,derivative_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    const derivative_context<Dom>& _context = static_cast<const derivative_context<Dom>&>(i_context);

    return derivate<Im,Dom,Allocator>(_context,static_cast<const SuperClass&>(i_rhs));
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_number_function<Im(const Dom&),Allocator>,derivative_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    return null_function<Im,Dom,Allocator>;
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_component_function<Im(const Dom&),Allocator>,derivative_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    const derivative_context<Dom>& _context = static_cast<const derivative_context<Dom>&>(i_context);
    const builtin_component_function<Im(const Dom&),Allocator>& rhsCompFunc = static_cast<const builtin_component_function<Im(const Dom&),Allocator>&>(i_rhs);

    if(_context.test(rhsCompFunc.component()))
    {
        return unity_function<Im,Dom,Allocator>;
    }
    else
    {
        return null_function<Im,Dom,Allocator>;
    }
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_composed_function<Im(const Dom&),Allocator>,derivative_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    const builtin_composed_function<Im(const Dom&),Allocator>& rhsCompFunc = static_cast<const builtin_composed_function<Im(const Dom&),Allocator>&>(i_rhs);

    return derivate(i_context,rhsCompFunc.lhs_function(),rhsCompFunc.rhs_function());
}
template<set_model Im, set_model Dom, typename Allocator>
template<flat_model IIm>
function<Im(const Dom&),Allocator> builtin_operator<builtin_composed_function<Im(const Dom&),Allocator>,derivative_operation>::derivate(const operator_context& i_context, const function<Im(const IIm&),Allocator>& i_lhs, const function<IIm(const Dom&),Allocator>& i_rhs)
{
    const derivative_context<Dom>& _context = static_cast<const derivative_context<Dom>&>(i_context);
    
    return builtin_mult_nary_function<Im(const Dom&),Allocator>{ builtin_composed_function<Im(const Dom&),Allocator>{ cpn::derivative(i_lhs),i_rhs },
                                                                 _context.derivative(i_rhs) };
}
template<set_model Im, set_model Dom, typename Allocator>
template<free_module_model IIm>
function<Im(const Dom&),Allocator> builtin_operator<builtin_composed_function<Im(const Dom&),Allocator>,derivative_operation>::derivate(const operator_context& i_context, const function<Im(const IIm&),Allocator>& i_lhs, const function<IIm(const Dom&),Allocator>& i_rhs)
{
    builtin_add_nary_function<Im(const Dom&),Allocator> res;

    const derivative_context<Dom>& _context = static_cast<const derivative_context<Dom>&>(i_context);
    const builtin_fusioned_function<Im(const Dom&),Allocator>& rhsFusionedFunc = static_cast<const builtin_fusioned_function<Im(const Dom&),Allocator>&>(i_rhs);

    for(size_t index=0;index<IIm::rank();index++)
    {
        res.push(builtin_mult_nary_function<Im(const Dom&),Allocator>{ builtin_composed_function<Im(const Dom&),Allocator>{ cpn::derivative(i_lhs,IIm::basis(index)),rhsFusionedFunc[index] },
                                                                       _context.derivative(rhsFusionedFunc[index]) });
    }    

    return res;
}

template<intersection_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_fusioned_function<Im(const Dom&),Allocator>,derivative_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    typedef typename ddk::mpl::make_sequence<0,Im::num_places>::type seq_t;

    return derivate(seq_t{},i_context,i_rhs);
}
template<intersection_model Im, set_model Dom, typename Allocator>
template<size_t ... Indexs>
function<Im(const Dom&),Allocator> builtin_operator<builtin_fusioned_function<Im(const Dom&),Allocator>,derivative_operation>::derivate(const ddk::mpl::sequence<Indexs...>&, const operator_context& i_context, const function<Im(const Dom&),Allocator>& i_rhs)
{
    const derivative_context<Dom>& _context = static_cast<const derivative_context<Dom>&>(i_context);
    const builtin_fusioned_function<Im(const Dom&),Allocator>& rhsFusionedFunc = static_cast<const builtin_fusioned_function<Im(const Dom&),Allocator>&>(i_rhs);

    return builtin_fusioned_function<Im(const Dom&),Allocator>{ _context.derivative(rhsFusionedFunc[Indexs])... };
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_minus_function<Im(const Dom&),Allocator>,derivative_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    const derivative_context<Dom>& _context = static_cast<const derivative_context<Dom>&>(i_context);
    const builtin_minus_function<Im(const Dom&),Allocator>& rhsMinusFunc = static_cast<const builtin_minus_function<Im(const Dom&),Allocator>&>(i_rhs);

    return -_context.derivative(rhsMinusFunc.func());
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_inverted_function<Im(const Dom&),Allocator>,derivative_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    const derivative_context<Dom>& _context = static_cast<const derivative_context<Dom>&>(i_context);
    const builtin_inverted_function<Im(const Dom&),Allocator>& rhsInvFunc = static_cast<const builtin_inverted_function<Im(const Dom&),Allocator>&>(i_rhs);
    const function<Im(const Dom&),Allocator>& invFunc = rhsInvFunc.func();

    return builtin_mult_nary_function<Im(const Dom&),Allocator>{ _context.derivative(invFunc),builtin_inverted_function<Im(const Dom&),Allocator>(builtin_mult_nary_function<Im(const Dom&),Allocator>{ invFunc,invFunc })};
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_pow_function<Im(const Dom&),Allocator>,derivative_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    const builtin_pow_function<Im(const Dom&),Allocator>& rhsPowFunc = static_cast<const builtin_pow_function<Im(const Dom&),Allocator>&>(i_rhs);

    if(rhsPowFunc.exp().id() == get_builtin_function_id<builtin_number_function<Im(const Dom&),Allocator>>())
    {
        const derivative_context<Dom>& _context = static_cast<const derivative_context<Dom>&>(i_context);
        const builtin_number_function<Im(const Dom&),Allocator>& expFunc = static_cast<const builtin_number_function<Im(const Dom&),Allocator>&>(rhsPowFunc.exp());

        return builtin_mult_nary_function<Im(const Dom&),Allocator>{ expFunc.number(),builtin_pow_function<Im(const Dom&),Allocator>{ rhsPowFunc.exp(),builtin_number_function<Im(const Dom&),Allocator>{ expFunc.number() - algebraic_operator<Im,semi_group_operation>::identity } },_context.derivative(expFunc.base()) };
    }
    else
    {
        return builtin_operator<builtin_function<builtin_pow_function<Im(const Dom&),Allocator>,Im,Dom,Allocator>,derivative_operation>::unary_operator(i_context,i_rhs);
    }
}

template<set_model Im, set_model Dom, typename Allocator, typename Functor>
function<Im(const Dom&),Allocator> builtin_operator<builtin_functor_function<Im(const Dom&),Allocator,Functor>,derivative_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    const derivative_context<Dom>& _context = static_cast<const derivative_context<Dom>&>(i_context);
    const builtin_functor_function<Im(const Dom&),Allocator,Functor>& rhsFunctorFunc = static_cast<const builtin_functor_function<Im(const Dom&),Allocator,Functor>&>(i_rhs);

    return derivate<Im,Dom,Allocator>(_context,rhsFunctorFunc.functor());
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_add_nary_function<Im(const Dom&),Allocator>,derivative_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    typedef builtin_add_nary_function<Im(const Dom&),Allocator> builtin_type;

    builtin_type res;

    const derivative_context<Dom>& _context = static_cast<const derivative_context<Dom>&>(i_context);
    const builtin_type& rhsNary = static_cast<const builtin_type&>(i_rhs);

    rhsNary.enumerate([&res,&_context](const function<Im(const Dom&),Allocator>& i_summand)
    {
        res.push(_context.derivative(i_summand));
    });

    return res;
}

template<set_model Im, set_model Dom, typename Allocator>
function<Im(const Dom&),Allocator> builtin_operator<builtin_mult_nary_function<Im(const Dom&),Allocator>,derivative_operation>::unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs)
{
    typedef builtin_add_nary_function<Im(const Dom&),Allocator> builtin_type;

    builtin_type res;

    const derivative_context<Dom>& _context = static_cast<const derivative_context<Dom>&>(i_context);
    const builtin_type& rhsNary = static_cast<const builtin_type&>(i_rhs);

    for(size_t index=0;index<rhsNary.size();++index)
    {
        builtin_mult_nary_function<Im(const Dom&),Allocator> summand;

        rhsNary.enumerate([&summand,&index,&_context,counter=0](const function<Im(const Dom&),Allocator>& i_prod) mutable
        {
            summand.push((counter++ == index) ? _context.derivative(i_prod) : i_prod);
        });

        res.push(summand);
    }

    return res;
}

}
}