#pragma once

#include "cpn_complex_ring.h"
#include "cpn_module.h"

namespace cpn
{

template<ring_model R>
struct complex_module_operation
{
    PUBLISH_OPERATION_PROPERTIES(complex_module_operation,mod_operation,commutative,associative,distributive);

	typedef R r_model;
	static constexpr auto identity = algebraic_operator<R,ring_operation>::identity;

	TEMPLATE(ring_type RR, solvable_literal_pack<2> T, typename ... Operations)
	REQUIRES(implements_model<RR,r_model>)
	friend inline auto operator^(const RR& i_lhs, const algebraic_structure<complex<T>,Operations...>& i_rhs)
	{
		typedef decltype(mult(i_lhs,i_rhs)) prod_t;

		return algebraic_structure<complex<prod_t>,Operations...>{ mult(i_lhs,i_rhs) };
	}

private:
	template<ring_type RR, solvable_literal_pack<2> T>
    static inline auto mult(const RR& i_lhs, const complex<T>& i_rhs)
    {
        return i_lhs * i_rhs;
    }
};

template<ring_model R = ComplexRing>
using ComplexModule = Module<ComplexGroup,complex_module_operation<R>>;
template<solvable_literal_type T, ring_model R = ComplexRing>
using complex_module = module<complex_group<T>,complex_module_operation<R>,real<T>>;

template<ring_model R, size_t ... Dims>
using ComplexModule_n = times_model<ComplexModule<R>,Dims...>;

template<ring_model R = ComplexRing>
using ComplexModule_1 = ComplexModule_n<R,1>;
template<ring_model R = ComplexRing>
using ComplexModule_2 = ComplexModule_n<R,2>;
template<ring_model R = ComplexRing>
using ComplexModule_3 = ComplexModule_n<R,3>;
template<ring_model R = ComplexRing>
using ComplexModule_4 = ComplexModule_n<R,4>;

template<ring_model R, solvable_literal_type ... T>
using complex_module_n = prod_struct<complex_module<T,R>...>;

template<solvable_literal_type T, ring_model R = ComplexRing>
using complex_module_1 = complex_module_n<R,T>;
template<solvable_literal_type T, solvable_literal_type TT, ring_model R = ComplexRing>
using complex_module_2 = complex_module_n<R,T,TT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, ring_model R = ComplexRing>
using complex_module_3 = complex_module_n<R,T,TT,TTT>;
template<solvable_literal_type T, solvable_literal_type TT, solvable_literal_type TTT, solvable_literal_type TTTT, ring_model R = ComplexRing>
using complex_module_4 = complex_module_n<R,T,TT,TTT,TTTT>;

}