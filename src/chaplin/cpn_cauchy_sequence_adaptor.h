#pragma once

#include "cpn_algebraic_concepts.h"
#include "ddkFramework/ddk_iterable.h"
#include "ddkFramework/ddk_optional.h"

namespace ddk
{

template<typename T>
class iterable_adaptor<const cpn::cauchy_sequence<T>>
{
    typedef typename cpn::cauchy_sequence<T>::value_type underlying_type;

public:
    typedef detail::iterable_by_type_adaptor<const underlying_type,
                                            mpl::empty_type_pack,
                                            mpl::type_pack<agnostic_sink_action_tag<const underlying_type&>,begin_action_tag,forward_action_tag>> traits;
    typedef detail::const_iterable_traits<traits> const_traits;
	typedef typename traits::tags_t tags_t;
	typedef typename traits::const_tags_t const_tags_t;

    iterable_adaptor(const cpn::cauchy_sequence<T>& i_sequence);
    template<typename Adaptor, typename Sink>
    static inline auto perform_action(Adaptor&& i_adaptor, const sink_action_tag<Sink>& i_sink);
    template<typename Adaptor>
    static inline auto perform_action(Adaptor&& i_adaptor, const begin_action_tag&);
    template<typename Adaptor>
    static inline auto perform_action(Adaptor&& i_adaptor, const forward_action_tag&);

private:
    mutable size_t m_currIndex = 0;
    const cpn::cauchy_sequence<T> m_sequence;
};

template<typename T>
class iterable_adaptor<cpn::cauchy_sequence<T>> : public iterable_adaptor<const cpn::cauchy_sequence<T>>
{
public:
    using iterable_adaptor<const cpn::cauchy_sequence<T>>::traits;
    using iterable_adaptor<const cpn::cauchy_sequence<T>>::const_traits;
    using iterable_adaptor<const cpn::cauchy_sequence<T>>::tags_t;
    using iterable_adaptor<const cpn::cauchy_sequence<T>>::const_tags_t;

    using iterable_adaptor<const cpn::cauchy_sequence<T>>::iterable_adaptor;
    using iterable_adaptor<const cpn::cauchy_sequence<T>>::perform_action;
};

}

#include "cpn_cauchy_sequence_adaptor.inl"