#pragma once

#include "cpn_function_module.h"
#include "cpn_function_ring.h"
#include "cpn_real_vector_space.h"

namespace cpn
{

template<typename Function>
struct operator_functor<Function,operator_tags<point_conv_operation>,operator_tags<point_conv_operation>>
{
	PUBLISH_OPERATION_PROPERTIES(operator_functor,derivative_operation,unary);
};

template<typename FunctionType, typename Allocator = function_allocator>
using Function = typename make_algebraic_structure<function<FunctionType,Allocator>,function_operators<function<FunctionType,Allocator>>>::type;

template<typename FunctionType, typename Allocator = function_allocator>
using LinearFunction = typename make_algebraic_structure<linear_function<FunctionType,Allocator>,function_operators<linear_function<FunctionType,Allocator>>>::type;

template<set_model T, typename Allocator = function_allocator>
using EndoFunction = Function<T(const T&),Allocator>;

template<size_t DimIm, size_t DimDom = DimIm>
using Mn = LinearFunction<R_n<DimIm>(const R_n<DimDom>&)>;

}

#include "cpn_functions.inl"