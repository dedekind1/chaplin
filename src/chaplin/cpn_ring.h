#pragma once

#include "cpn_algebraic_structure.h"
#include "cpn_set.h"
#include "cpn_group.h"
#include "ddkFramework/ddk_iterable_algorithm.h"

namespace cpn
{

template<semi_group_model Model, typename MultOperation>
using SemiRing = typename Model::template embed<MultOperation>;

template<semi_group_type Structure, typename MultOperation, typename Set = typename Structure::value_type>
using semi_ring = algebraic_structure<Set,SemiRing<Structure,MultOperation>>;

template<group_model Model, typename MultOperation>
using Ring = typename Model::template embed<MultOperation>;

template<group_type Structure, typename MultOperation, typename Set = typename Structure::value_type>
using ring = algebraic_structure<Set,Ring<Structure,MultOperation>>;

template<semi_ring_model T, size_t ... Dims>
struct pow_operation<ring_operation,T,Dims...>
{
    PUBLISH_OPERATION_PROPERTIES(pow_operation,ring_operation,concepts::algebraic_structure_properties<T,ring_operation>);

	static constexpr auto identity = pow_forward<Dims...>(algebraic_operator<T,ring_operation>::identity);
	static constexpr auto annihilator = pow_forward<Dims...>(algebraic_operator<T,ring_operation>::annihilator);

	template<size_t ... Indexs, semi_ring_type TT, semi_ring_type TTT>
	static auto prod_array(const ddk::mpl::sequence<Indexs...>&, const pow_type<TT,Dims...>& i_lhs,const pow_type<TTT,Dims...>& i_rhs)
	{
		typedef pow_type<decltype(std::declval<TT>() * std::declval<TTT>()),Dims...> pow_t;

		return 	pow_t{ i_lhs.at(Indexs) * i_rhs.at(Indexs) ... };
	}
	TEMPLATE(semi_ring_type TT, semi_ring_type TTT, typename ... Operations)
	REQUIRES(implements_operation<ring_operation,T,TT>,implements_operation<ring_operation,T,TTT>)
	friend inline auto operator*(const algebraic_structure<pow_type<TT,Dims...>,Operations...>& i_lhs, const algebraic_structure<pow_type<TTT,Dims...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,ddk::mpl::prod_ranks<Dims...>>::type seq_t;
		typedef decltype(prod_array(seq_t{},i_lhs,i_rhs)) pow_t;

		return algebraic_structure<pow_t,Operations...>{ prod_array(seq_t{},i_lhs,i_rhs) };
	}
};

template<semi_ring_model ... T>
struct prod_operation<ring_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(prod_operation,ring_operation,typename ddk::mpl::intersect_type_packs<concepts::algebraic_structure_properties<T,ring_operation>...>::type);

	static const size_t s_num_types = ddk::mpl::num_types<T...>;
	static constexpr auto identity = prod_type{ algebraic_operator<T,ring_operation>::identity ... };
	static constexpr auto annihilator = prod_type{ algebraic_operator<T,ring_operation>::annihilator ... };

	template<size_t ... Indexs, semi_ring_type ... TT, semi_ring_type ... TTT>
	static auto prod_tuples(const ddk::mpl::sequence<Indexs...>&, const prod_type<TT...>& i_lhs,const prod_type<TTT...>& i_rhs)
	{
		return 	prod_type{ i_lhs.template get<Indexs>() * i_rhs.template get<Indexs>() ... };
	}
	TEMPLATE(semi_ring_type ... TT, semi_ring_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<ring_operation,prod_type<TT...>,T...>,implements_operation<ring_operation,prod_type<TTT...>,T...>)
	friend inline auto operator*(const algebraic_structure<prod_type<TT...>,Operations...>& i_lhs,const algebraic_structure<prod_type<TTT...>,Operations...>& i_rhs)
	{
		typedef typename ddk::mpl::make_sequence<0,s_num_types>::type seq_t;
		typedef decltype(prod_tuples(seq_t{},i_lhs,i_rhs)) prod_t;

		return algebraic_structure<prod_t,Operations...>{ prod_tuples(seq_t{},i_lhs,i_rhs) };
	}
};

template<semi_ring_model ... T>
struct sum_operation<ring_operation,T...>
{
    PUBLISH_OPERATION_PROPERTIES(sum_operation,ring_operation,typename ddk::mpl::intersect_type_packs<concepts::algebraic_structure_properties<T,ring_operation>...>::type);

	template<typename Return>
	struct mult_operation_visitor
	{
		template<semi_ring_type T1,semi_ring_type T2>
		Return operator()(T1&& i_lhs,T2&& i_rhs) const
		{
			return i_lhs * i_rhs;
		}
	};

	static const sum_type<T...> identity;
	static const sum_type<T...> annihilator;

	TEMPLATE(semi_ring_type ... TT, semi_ring_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<ring_operation,sum_type<TT...>,T...>,implements_operation<ring_operation,sum_type<TTT...>,T...>)
	friend inline auto operator*(const algebraic_structure<sum_type<TT...>,Operations...>& i_lhs,const algebraic_structure<sum_type<TTT...>,Operations...>& i_rhs)
	{
		typedef sum_type<decltype(std::declval<TT>() * std::declval<TTT>())...> sum_type_t;

		return algebraic_structure<sum_type_t,Operations...>{ ddk::visit<mult_operation_visitor<sum_type_t>>(i_lhs,i_rhs) };
	}
};

}
