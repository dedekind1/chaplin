
#include "cpn_algebraic_concepts.h"

namespace cpn
{

template<typename Space, typename T>
constexpr operator_tag_id get_operator_tag_id()
{
    typedef algebraic_structure_operators<Space> space_operators;

    constexpr size_t tagPos = space_operators::template pos_of_tag<T>;

    static_assert(tagPos < space_operators::s_numOps, "Operation tag not found in Space");

    return operator_tag_id{ tagPos };
}

}