#pragma once

#include "cpn_algebraic_type_concepts.h"
#include "cpn_builtin_symbolic_literals.h"

namespace cpn
{

template<set_type T>
auto as_set(T&& i_obj);

template<typename CompareOperation>
using Set = algebraic_model<CompareOperation>;

template<typename T, typename CompareOperation>
using set = algebraic_structure<T,Set<CompareOperation>>;

template<set_model T, size_t ... Dims>
struct pow_operation<proj_operation,T,Dims...>
{
	PUBLISH_OPERATION_PROPERTIES(pow_operation,proj_operation);

	TEMPLATE(typename TT, set_type TTT)
	REQUIRES(implements_operation<set_operation,TTT,T>)
	friend inline TT project_onto(const pow_type<TTT,Dims...>& i_object, size_t i_index)
	{
        return { i_object.at(i_index) };
	}
	TEMPLATE(size_t Index, set_type TT)
	REQUIRES(Index < ddk::mpl::prod_ranks<Dims...>,implements_operation<set_operation,TT,T>)
	friend inline TT project(const pow_type<TT,Dims...>& i_object)
	{
        return { i_object.at(Index) };
	}

	typedef T principal_type;

	static constexpr size_t rank();
};

template<set_model T, size_t ... Dims>
struct pow_operation<set_operation,T,Dims...> : pow_operation<proj_operation,T,Dims...>
{
	PUBLISH_OPERATION_PROPERTIES(pow_operation,set_operation,concepts::algebraic_structure_properties<T,set_operation>);

	typedef pow_type<final_object<T>,Dims...> decay_t;

	TEMPLATE(set_type TT, set_type TTT)
	REQUIRES(implements_operation<set_operation,TT,T>,implements_operation<set_operation,TTT,T>)
	friend inline bool operator==(const pow_type<TT,Dims...>& i_lhs,const pow_type<TTT,Dims...>& i_rhs)
	{
		return i_lhs == i_rhs;
	}
	TEMPLATE(set_type TT, set_type TTT)
	REQUIRES(implements_operation<set_operation,TT,T>,implements_operation<set_operation,TTT,T>)
	friend inline bool operator!=(const pow_type<TT,Dims...>& i_lhs,const pow_type<TTT,Dims...>& i_rhs)
	{
		return (i_lhs == i_rhs) == false;
	}
};

template<set_model ... T>
struct prod_operation<proj_operation,T...>
{
	PUBLISH_OPERATION_PROPERTIES(prod_operation,proj_operation);

	TEMPLATE(typename TT, set_type ... TTT)
	REQUIRES(implements_operation<set_operation,prod_type<TTT...>,T...>)
	friend inline TT project_onto(const prod_type<TTT...>& i_object, size_t i_index)
	{
        typedef typename ddk::mpl::make_sequence<0,ddk::mpl::num_types<T...>>::type seq_t;

        return access<TT>(seq_t{},i_object,i_index);
	}
	TEMPLATE(size_t Index, set_type ... TT)
	REQUIRES(Index < ddk::mpl::num_types<T...>,implements_operation<set_operation,prod_type<TT...>,T...>)
	friend inline auto project(const prod_type<TT...>& i_object)
	{
		return i_object.template get<Index>();
	}

	typedef ddk::mpl::conductor_type<T...> principal_type;

	static constexpr size_t rank();

private:
    template<typename TT, size_t ... Indexs, typename ... TTT>
    static TT access(const ddk::mpl::sequence<Indexs...>&, const prod_type<TTT...>& i_object, size_t i_index);
    template<typename TT, size_t Index, typename ... TTT>
    static TT access(const prod_type<TTT...>& i_object);
};

template<set_model ... T>
struct prod_operation<set_operation,T...> : prod_operation<proj_operation,T...>
{
	PUBLISH_OPERATION_PROPERTIES(prod_operation,set_operation);

	typedef prod_type<final_object<T>...> decay_t;

	TEMPLATE(set_type ... TT, set_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<set_operation,prod_type<TT...>,T...>,implements_operation<set_operation,prod_type<TTT...>,T...>)
	friend inline bool operator==(const prod_type<TT...>& i_lhs,const prod_type<TTT...>& i_rhs)
	{
		bool res = true;

		[&res](auto&& ii_lhs, auto&& ii_rhs)
		{
			res &= (ii_lhs == ii_rhs);
		} <<= ddk::fusion(i_lhs,i_rhs);

		return res;
	}
	TEMPLATE(set_type ... TT, set_type ... TTT, typename ... Operations)
	REQUIRES(implements_operation<set_operation,prod_type<TT...>,T...>,implements_operation<set_operation,prod_type<TTT...>,T...>)
	friend inline bool operator!=(const prod_type<T...>& i_lhs,const prod_type<TT...>& i_rhs)
	{
		bool res = true;

		[&res](auto&& ii_lhs, auto&& ii_rhs)
		{
			res &= (ii_lhs == ii_rhs);
		} <<= ddk::fusion(i_lhs,i_rhs);

		return !res;
	}
};

template<set_model ... T>
struct sum_operation<set_operation,T...>
{
	PUBLISH_OPERATION_PROPERTIES(sum_operation,set_operation);

	typedef sum_type<typename algebraic_operator<T,set_operation>::decay_t...> decay_t;

	friend inline bool operator==(const ddk::variant<T...>& i_lhs,const ddk::variant<T...>& i_rhs)
	{
		return i_lhs == i_rhs;
	}
	friend inline bool operator!=(const ddk::variant<T...>& i_lhs,const ddk::variant<T...>& i_rhs)
	{
		return (i_lhs == i_rhs) == false;
	}
};

template<set_model ... T>
using SumSet = algebraic_model<sum_operation<T...>>;

template<set_type ... T>
using sum_set = algebraic_structure<sum_type<T...>,SumSet<T...>>;

}

#include "cpn_set.inl"