#pragma once

#include "cpn_polynomial_group.h"

namespace cpn
{

template<ring_model T, typename Order, typename Allocator>
struct operator_functor<polynomial<T,Order,Allocator>,operator_tags<ring_operation>>
{
    PUBLISH_OPERATION_PROPERTIES(operator_functor,ring_operation,commutative,associative,distributive);

	static constexpr auto annihilator = algebraic_operator<T,ring_operation>::annihilator;
	static constexpr auto identity = algebraic_operator<T,ring_operation>::identity;

	template<typename ... Operations>
	friend inline auto operator*(const algebraic_structure<polynomial<T,Order,Allocator>,Operations...>& i_lhs, const algebraic_structure<polynomial<T,Order,Allocator>,Operations...>& i_rhs)
	{
        polynomial<T,Order,Allocator> res;

        [&res](auto&& ii_lhs, auto&& ii_rhs)
        {
            res.push(ii_lhs * ii_rhs);
        } <<= ddk::view::order(ddk::cumulative_order) <<= ddk::fusion(i_lhs,i_rhs);

		return algebraic_structure<polynomial<T,Order,Allocator>,Operations...>{ res };
	}
};

}