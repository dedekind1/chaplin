
namespace cpn
{

constexpr builtin_literal_expression<integer_symbolic_literal> number(int i_num)
{
	return integer_symbolic_literal{ i_num };
}
constexpr builtin_literal_expression<rational_symbolic_literal> fraction(int i_num, unsigned int i_den)
{
	return rational_symbolic_literal{ i_num,i_den };
}
constexpr builtin_literal_expression<root_symbolic_literal> root(int i_num, int i_deg, bool i_pos)
{
	return root_symbolic_literal{ i_num,i_deg,i_pos};
}
constexpr builtin_literal_expression<log_symbolic_literal> log(unsigned int i_num, unsigned int i_base, bool i_pos)
{
	return log_symbolic_literal{ i_num,i_base,i_pos };
}

template<literal_expression Expression>
constexpr auto forward_expression(Expression&& i_exp)
{
	return i_exp;
}
template<literal_type Expression>
constexpr auto forward_expression(Expression&& i_exp)
{
	return builtin_literal_expression{ std::forward<Expression>(i_exp) };
}

template<literal_type T>
constexpr  auto operator-(const builtin_literal_expression<T>& i_lhs)
{
	return builtin_literal_expression{ -i_lhs.get() };
}
template<literal_expression T>
constexpr auto operator-(const T& i_exp)
{
	return builtin_minus_expression{ i_exp };
}

template<literal_type T, literal_type TT>
constexpr auto operator+(const builtin_literal_expression<T>& i_lhs,const builtin_literal_expression<TT>& i_rhs)
{
	return builtin_literal_expression{ i_lhs.get() + i_rhs.get() };
}
template<literal_expression T, size_t ... Indexs, literal_expression ... Expressions>
constexpr auto operator+(const T& i_lhs,const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_rhs)
{
	return add_nary_expression{ forward_expression(i_lhs),i_rhs.template get<Indexs>()... };
}
template<size_t ... Indexs, literal_expression ... Expressions, literal_expression T>
constexpr auto operator+(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const T& i_rhs)
{
	return add_nary_expression{ i_lhs.template get<Indexs>()...,forward_expression(i_rhs) };
}
template<size_t ... Indexs, literal_expression ... Expressions, size_t ... IIndexs, literal_expression ... EExpressions>
constexpr auto operator+(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const add_nary_expression<ddk::mpl::sequence<IIndexs...>,EExpressions...>& i_rhs)
{
	return add_nary_expression{ i_lhs.template get<Indexs>()...,i_rhs.template get<IIndexs>()... };
}
template<literal_expression T, literal_expression TT>
constexpr auto operator+(const T& i_lhs, const TT& i_rhs)
{
	return add_nary_expression{ forward_expression(i_lhs),forward_expression(i_rhs) };
}

template<literal_type T, literal_type TT>
constexpr auto operator-(const builtin_literal_expression<T>& i_lhs,const builtin_literal_expression<TT>& i_rhs)
{
	return builtin_literal_expression{ i_lhs.get() - i_rhs.get() };
}
template<literal_expression T, size_t ... Indexs, literal_expression ... Expressions>
constexpr auto operator-(const T& i_lhs,const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_rhs)
{
	return add_nary_expression{ forward_expression(i_lhs),builtin_minus_expression{ i_rhs.template get<Indexs>()} ... };
}
template<size_t ... Indexs, literal_expression ... Expressions, literal_expression T>
constexpr auto operator-(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const T& i_rhs)
{
	return add_nary_expression{ i_lhs.template get<Indexs>()...,builtin_minus_expression{ forward_expression(i_rhs) } };
}
template<size_t ... Indexs, literal_expression ... Expressions, size_t ... IIndexs, literal_expression ... EExpressions>
constexpr auto operator-(const add_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const add_nary_expression<ddk::mpl::sequence<IIndexs...>,EExpressions...>& i_rhs)
{
	return add_nary_expression{ i_lhs.template get<Indexs>()...,builtin_minus_expression{ i_rhs.template get<IIndexs>() } ... };
}
template<literal_expression T, literal_expression TT>
constexpr auto operator-(const T& i_lhs, const TT& i_rhs)
{
	return add_nary_expression{ forward_expression(i_lhs),builtin_minus_expression{ forward_expression(i_rhs) } };
}

template<literal_type T, literal_type TT>
constexpr auto operator*(const builtin_literal_expression<T>& i_lhs,const builtin_literal_expression<TT>& i_rhs)
{
	return builtin_literal_expression{ i_lhs.get() * i_rhs.get() };
}
template<literal_expression T, size_t ... Indexs, literal_expression ... Expressions>
constexpr auto operator*(const T& i_lhs,const prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_rhs)
{
	return prod_nary_expression{ forward_expression(i_lhs),i_rhs.template get<Indexs>()... };
}
template<size_t ... Indexs, literal_expression ... Expressions, literal_expression T>
constexpr auto operator*(const prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const T& i_rhs)
{
	return prod_nary_expression{ i_lhs.template get<Indexs>()...,forward_expression(i_rhs) };
}
template<size_t ... Indexs, literal_expression ... Expressions, size_t ... IIndexs, literal_expression ... EExpressions>
constexpr auto operator-(const prod_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...>& i_lhs, const prod_nary_expression<ddk::mpl::sequence<IIndexs...>,EExpressions...>& i_rhs)
{
	return prod_nary_expression{ i_lhs.template get<Indexs>()...,i_rhs.template get<IIndexs>()... };
}
template<literal_expression T, literal_expression TT>
constexpr auto operator*(const T& i_lhs, const TT& i_rhs)
{
	return prod_nary_expression{ forward_expression(i_lhs),forward_expression(i_rhs) };
}

template<literal_type T, literal_type TT>
constexpr auto operator/(const builtin_literal_expression<T>& i_lhs,const builtin_literal_expression<TT>& i_rhs)
{
	return builtin_literal_expression{ i_lhs.get() / i_rhs.get() };
}
template<literal_expression T, literal_expression TT>
constexpr auto operator/(const T& i_lhs, const TT& i_rhs)
{
	return prod_nary_expression{ forward_expression(i_lhs),builtin_inverted_expression{ forward_expression(i_rhs) }};
}

template<literal_type T>
constexpr auto operator^(const builtin_incognita_expression& i_lhs, const T& i_rhs)
{
    return pow_nary_expression{ i_lhs,builtin_literal_expression{ i_rhs } };
}
template<size_t Comp, literal_type T>
constexpr auto operator^(const builtin_component_expression<Comp>& i_lhs, const T& i_rhs)
{
    return pow_nary_expression{ i_lhs,builtin_literal_expression{ i_rhs } };
}
template<literal_expression LhsExpression, literal_expression RhsExpression, literal_type T>
constexpr auto operator^(const builtin_composed_expression<LhsExpression,RhsExpression>& i_lhs, const T& i_rhs)
{
    return pow_nary_expression{i_lhs, builtin_literal_expression{ i_rhs } };
}
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, literal_type T>
constexpr auto operator^(const add_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const T& i_rhs)
{
	//fix operator precedence ¬¬
    return add_nary_expression{i_lhs.template get<Indexs-1>()..., pow_nary_expression{ i_lhs.template get<ddk::mpl::num_ranks<Indexs...>>(),builtin_literal_expression{ i_rhs } } };
}
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, size_t IIndex, size_t ... IIndexs, literal_type T, literal_expression ... EExpressions>
constexpr auto operator^(const add_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const add_nary_expression<ddk::mpl::sequence<IIndex,IIndexs...>,builtin_literal_expression<T>,EExpressions...>& i_rhs)
{
	//fix operator precedence ¬¬
    return add_nary_expression{i_lhs.template get<Indexs-1>()..., pow_nary_expression{ i_lhs.template get<ddk::mpl::num_ranks<Indexs...>>(),builtin_literal_expression{ i_rhs.template get<IIndex>() } },i_rhs.template get<IIndexs>()... };
}
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, size_t IIndex, size_t ... IIndexs, literal_type T, literal_expression ... EExpressions>
constexpr auto operator^(const add_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const prod_nary_expression<ddk::mpl::sequence<IIndex,IIndexs...>,builtin_literal_expression<T>,EExpressions...>& i_rhs)
{
	//fix operator precedence ¬¬
    return add_nary_expression{i_lhs.template get<Indexs-1>()..., pow_nary_expression{ i_lhs.template get<ddk::mpl::num_ranks<Indexs...>>(),builtin_literal_expression{ i_rhs.template get<IIndex>() } },prod_nary_expression{ i_rhs.template get<IIndexs>()... } };
}
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, size_t IIndex, size_t ... IIndexs, literal_type T, literal_expression ... EExpressions>
constexpr auto operator^(const prod_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const prod_nary_expression<ddk::mpl::sequence<IIndex,IIndexs...>,builtin_literal_expression<T>,EExpressions...>& i_rhs)
{
	//fix operator precedence ¬¬
    return prod_nary_expression{i_lhs.template get<Indexs-1>()..., pow_nary_expression{ i_lhs.template get<ddk::mpl::num_ranks<Indexs...>>(),builtin_literal_expression{ i_rhs.template get<IIndex>() } },i_rhs.template get<IIndexs>()... };
}
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, size_t IIndex, size_t ... IIndexs, literal_type T, literal_expression ... EExpressions>
constexpr auto operator^(const prod_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const add_nary_expression<ddk::mpl::sequence<IIndex,IIndexs...>,builtin_literal_expression<T>,EExpressions...>& i_rhs)
{
	//fix operator precedence ¬¬
    return add_nary_expression{prod_nary_expression{ i_lhs.template get<Indexs-1>()... , pow_nary_expression{ i_lhs.template get<ddk::mpl::num_ranks<Indexs...>>(),builtin_literal_expression{ i_rhs.template get<IIndex>() } } },i_rhs.template get<IIndexs>()... };
}
template<size_t Index, size_t ... Indexs, literal_expression ... Expressions, literal_type T>
constexpr auto operator^(const prod_nary_expression<ddk::mpl::sequence<Index,Indexs...>,Expressions...>& i_lhs, const T& i_rhs)
{
	//fix operator precedence ¬¬
    return prod_nary_expression{i_lhs.template get<Indexs-1>()...,pow_nary_expression{ i_lhs.template get<ddk::mpl::num_ranks<Indexs...>>(),builtin_literal_expression{ i_rhs } } };
}
template<literal_expression T, size_t Index, size_t ... Indexs, literal_type TT, literal_expression ... Expressions>
constexpr auto operator^(const T& i_lhs, const add_nary_expression<ddk::mpl::sequence<Index,Indexs...>,builtin_literal_expression<TT>,Expressions...>& i_rhs)
{
	//fix operator precedence ¬¬
    return add_nary_expression{ i_lhs ^ i_rhs.template get<Index>(), i_rhs.template get<Indexs>()... };
}
template<literal_expression T, size_t Index, size_t ... Indexs, literal_type TT, literal_expression ... Expressions>
constexpr auto operator^(const T& i_lhs, const prod_nary_expression<ddk::mpl::sequence<Index,Indexs...>,builtin_literal_expression<TT>,Expressions...>& i_rhs)
{
	//fix operator precedence ¬¬
    return prod_nary_expression{ i_lhs ^ i_rhs.template get<Index>(), i_rhs.template get<Indexs>()... };
}

}