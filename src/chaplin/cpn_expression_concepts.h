#pragma once

#include "cpn_builtin_literal_concepts.h"
#include "ddkFramework/ddk_function_concepts.h"

namespace cpn
{
namespace concepts
{

std::false_type is_instantiable_resolver(...);
template<typename T, typename = typename T::__instantiable_tag>
std::true_type is_instantiable_resolver(const T&);

}

template<typename T>
inline constexpr bool is_instantiable = decltype(concepts::is_instantiable_resolver(std::declval<T>()))::value;

template<typename T>
concept literal_expression = literal_type<T> || is_instantiable<T>;

}