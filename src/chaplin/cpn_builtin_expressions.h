#pragma once

#include "cpn_builtin_symbolic_literals.h"
#include "cpn_type_concepts.h"
#include "cpn_algebraic_defs.h"
#include "cpn_builtin_literal_concepts.h"
#include "cpn_expression_concepts.h"
#include <cmath>

#define BUILTIN_EXPRESSION(...) typedef void __instantiable_tag; \
    typedef ddk::mpl::type_pack<__VA_ARGS__> __expression_properties;

#define DEFINE_ARITHMETIC_NARY_EXPRESSION(_NAME) \
template<typename, literal_expression ...> \
struct _NAME##_nary_expression; \
template<size_t ... Indexs, literal_expression ... Expressions> \
struct _NAME##_nary_expression<ddk::mpl::sequence<Indexs...>,Expressions...> : public cpn::builtin_monoid_expression<ddk::mpl::sequence<Indexs...>,Expressions...> \
{ \
public: \
    using cpn::builtin_monoid_expression<ddk::mpl::sequence<Indexs...>,Expressions...>::builtin_monoid_expression; \
}; \
template<typename ... T> \
_NAME##_nary_expression(const T& ... i_args) -> _NAME##_nary_expression<typename ddk::mpl::make_sequence<0,ddk::mpl::num_types<T...>>::type,T...>; \
template<typename ... T> \
_NAME##_nary_expression(const ddk::tuple<T...>&) -> _NAME##_nary_expression<typename ddk::mpl::make_sequence<0,ddk::mpl::num_types<T...>>::type,T...>;

namespace cpn
{

template<literal_type T>
struct builtin_literal_expression
{
    BUILTIN_EXPRESSION(linear,cnstant);

    builtin_literal_expression() = default;
    constexpr builtin_literal_expression(const T& i_number);
    constexpr const T& get() const;
    constexpr operator const T&() const;

private:
    const T m_number;
};
template<typename T>
builtin_literal_expression(const T& i_number) -> builtin_literal_expression<T>;

template<literal_expression Expression>
struct builtin_minus_expression
{
    BUILTIN_EXPRESSION();

    builtin_minus_expression() = default;
    constexpr builtin_minus_expression(const Expression& i_exp);

    constexpr const Expression& get() const;

private:
    const Expression m_exp;
};
template<typename Expression>
builtin_minus_expression(const Expression&) -> builtin_minus_expression<Expression>;

template<literal_expression Expression>
struct builtin_inverted_expression
{
    BUILTIN_EXPRESSION();

    builtin_inverted_expression() = default;
    constexpr builtin_inverted_expression(const Expression& i_exp);

    constexpr const Expression& get() const;

private:
    const Expression m_exp;
};
template<typename Expression>
builtin_inverted_expression(const Expression&) -> builtin_inverted_expression<Expression>;

template<literal_expression LhsExpression,literal_expression RhsExpression>
struct builtin_composed_expression
{
    BUILTIN_EXPRESSION();

    builtin_composed_expression() = default;
    constexpr builtin_composed_expression(const LhsExpression& i_lhs,const RhsExpression& i_rhs);

    constexpr const LhsExpression& lhs() const;
    constexpr const RhsExpression& rhs() const;

private:
    const LhsExpression m_lhs;
    const RhsExpression m_rhs;
};

struct builtin_incognita_expression
{
    BUILTIN_EXPRESSION(incognita,linear);

    builtin_incognita_expression() = default;
    TEMPLATE(typename T)
    REQUIRES(is_instantiable<T>)
    constexpr builtin_composed_expression<builtin_incognita_expression,T> operator()(const T & other) const;
};

template<size_t Comp>
struct builtin_component_expression
{
    BUILTIN_EXPRESSION(incognita,linear);

    builtin_component_expression() = default;
    TEMPLATE(typename T)
    REQUIRES(is_instantiable<T>)
    constexpr builtin_composed_expression<builtin_component_expression<Comp>,T> operator()(const T & other) const;
};

template<typename Functor>
struct builtin_functor_expression
{
    BUILTIN_EXPRESSION()

    typedef Functor functor_t;

    builtin_functor_expression() = default;
    constexpr builtin_functor_expression(const Functor& i_functor);

    template<literal_expression T>
    constexpr builtin_composed_expression<builtin_functor_expression<Functor>,T> operator()(const T& other) const;
    constexpr const Functor& functor() const;

private:
    const Functor m_functor;
};
template<typename Functor>
builtin_functor_expression(const Functor&) -> builtin_functor_expression<Functor>;

template<typename, literal_expression ...>
struct builtin_monoid_expression;
template<size_t ... Indexs, literal_expression ... Expressions>
struct builtin_monoid_expression<ddk::mpl::sequence<Indexs...>,Expressions...>
{
    BUILTIN_EXPRESSION()

    template<typename Callable>
    friend inline constexpr void operator<<=(Callable&& i_callable, const builtin_monoid_expression& i_expression)
    {
        std::forward<Callable>(i_callable) <<= i_expression.m_expressions;
    }
    template<typename,literal_expression ...>
    friend struct builtin_monoid_expression;

    constexpr builtin_monoid_expression() = default;
    constexpr builtin_monoid_expression(const builtin_monoid_expression&) = default;
    constexpr builtin_monoid_expression(builtin_monoid_expression&&) = default;
    constexpr builtin_monoid_expression(const ddk::tuple<Expressions...>& i_exps);
    constexpr builtin_monoid_expression(ddk::tuple<Expressions...>&& i_exps);
    TEMPLATE(typename ... EExpressions)
    REQUIRES(ddk::is_constructible<Expressions,EExpressions>...)
    constexpr builtin_monoid_expression(const EExpressions& ... i_exps);
    template<size_t Index>
    constexpr auto get() const;

private:
    const ddk::tuple<Expressions...> m_expressions;
};

//predfined nary arithmetic operators
DEFINE_ARITHMETIC_NARY_EXPRESSION(add);
DEFINE_ARITHMETIC_NARY_EXPRESSION(subs);
DEFINE_ARITHMETIC_NARY_EXPRESSION(prod);
DEFINE_ARITHMETIC_NARY_EXPRESSION(div);
DEFINE_ARITHMETIC_NARY_EXPRESSION(pow);

constexpr builtin_incognita_expression x = builtin_incognita_expression();
constexpr builtin_component_expression<0> x_0 = builtin_component_expression<0>();
constexpr builtin_component_expression<1> x_1 = builtin_component_expression<1>();
constexpr builtin_component_expression<2> x_2 = builtin_component_expression<2>();
constexpr builtin_component_expression<3> x_3 = builtin_component_expression<3>();
constexpr builtin_component_expression<4> x_4 = builtin_component_expression<4>();
constexpr builtin_component_expression<5> x_5 = builtin_component_expression<5>();
constexpr builtin_component_expression<6> x_6 = builtin_component_expression<6>();
constexpr builtin_component_expression<7> x_7 = builtin_component_expression<7>();
constexpr builtin_component_expression<8> x_8 = builtin_component_expression<8>();
constexpr builtin_component_expression<9> x_9 = builtin_component_expression<9>();

}

#include "cpn_builtin_expressions.inl"
#include "cpn_builtin_expression_ops.h"