#pragma once

#include "cpn_rational_set.h"
#include "cpn_group.h"

namespace cpn
{

struct rational_addition
{
    PUBLISH_OPERATION_PROPERTIES(rational_addition,semi_group_operation,commutative,associative,distributive);

	static constexpr rational_set identity = rational(0,1);

	template<typename ... Operations>
	friend inline auto operator+(const algebraic_structure<rational,Operations...>& i_lhs,const algebraic_structure<rational,Operations...>& i_rhs)
	{
		return algebraic_structure<rational,Operations...>{ i_lhs.number() + i_rhs.number() };
	}
};

using RationalSemiGroup = SemiGroup<RationalSet,rational_addition>;
using rational_semi_group = semi_group<rational_set,rational_addition>;

template<size_t ... Dims>
using RationalSemiGroup_n = pow_model<RationalSemiGroup,Dims...>;

typedef RationalSemiGroup_n<1> RationalSemiGroup_1;
typedef RationalSemiGroup_n<2> RationalSemiGroup_2;
typedef RationalSemiGroup_n<3> RationalSemiGroup_3;
typedef RationalSemiGroup_n<4> RationalSemiGroup_4;

template<size_t ... Dims>
using rational_semi_group_n = pow_struct<rational_semi_group,Dims...>;

typedef rational_semi_group_n<1> rational_semi_group_1;
typedef rational_semi_group_n<2> rational_semi_group_2;
typedef rational_semi_group_n<3> rational_semi_group_3;
typedef rational_semi_group_n<4> rational_semi_group_4;

struct rational_addition_inverse
{
    PUBLISH_OPERATION_PROPERTIES(rational_addition_inverse,group_operation);

	template<typename ... Operations>
	friend inline auto operator-(const algebraic_structure<rational,Operations...>& i_rhs)
	{
		return algebraic_structure<rational,Operations...>{ -i_rhs.number() };
	}
	template<typename ... Operations>
	friend inline auto operator-(const algebraic_structure<rational,Operations...>& i_lhs,const algebraic_structure<rational,Operations...>& i_rhs)
	{
		return algebraic_structure<rational,Operations...>{ i_lhs.number() + (-i_rhs.number()) };
	}
};

using RationalGroup = Group<RationalSemiGroup,rational_addition_inverse>;
using rational_group = group<rational_semi_group,rational_addition_inverse>;

template<size_t ... Dims>
using RationalGroup_n = pow_model<RationalGroup,Dims...>;

typedef RationalGroup_n<1> RationalGroup_1;
typedef RationalGroup_n<2> RationalGroup_2;
typedef RationalGroup_n<3> RationalGroup_3;
typedef RationalGroup_n<4> RationalGroup_4;

template<size_t ... Dims>
using rational_group_n = pow_struct<rational_group,Dims...>;

typedef rational_group_n<1> rational_group_1;
typedef rational_group_n<2> rational_group_2;
typedef rational_group_n<3> rational_group_3;
typedef rational_group_n<4> rational_group_4;

}