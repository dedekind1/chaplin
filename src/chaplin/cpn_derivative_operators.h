#pragma once

#include "cpn_algebraic_type_concepts.h"

namespace cpn
{
namespace detail
{

template<typename SuperClass, set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_function<SuperClass,Im,Dom,Allocator>,derivative_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_number_function<Im(const Dom&),Allocator>,derivative_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_minus_function<Im(const Dom&),Allocator>,derivative_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_inverted_function<Im(const Dom&),Allocator>,derivative_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_pow_function<Im(const Dom&),Allocator>,derivative_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_component_function<Im(const Dom&),Allocator>,derivative_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_composed_function<Im(const Dom&),Allocator>,derivative_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);

private:
    template<flat_model IIm>
    static function<Im(const Dom&),Allocator> derivate(const operator_context& i_context, const function<Im(const IIm&),Allocator>& i_lhs, const function<IIm(const Dom&),Allocator>& i_rhs);
    template<free_module_model IIm>
    static function<Im(const Dom&),Allocator> derivate(const operator_context& i_context, const function<Im(const IIm&),Allocator>& i_lhs, const function<IIm(const Dom&),Allocator>& i_rhs);
};

template<intersection_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_fusioned_function<Im(const Dom&),Allocator>,derivative_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);

private:
    template<size_t ... Indexs>
    static function<Im(const Dom&),Allocator> derivate(const ddk::mpl::sequence<Indexs...>&, const operator_context& i_context, const function<Im(const Dom&),Allocator>& i_rhs);
};

template<set_model Im, set_model Dom, typename Allocator, typename Functor>
struct builtin_operator<builtin_functor_function<Im(const Dom&),Allocator,Functor>,derivative_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_add_nary_function<Im(const Dom&),Allocator>,derivative_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

template<set_model Im, set_model Dom, typename Allocator>
struct builtin_operator<builtin_mult_nary_function<Im(const Dom&),Allocator>,derivative_operation>
{
    static function<Im(const Dom&),Allocator> unary_operator(const operator_context& i_context, const function_base<Im,const Dom&>& i_rhs);
};

}
}

#include "cpn_derivative_operators.inl"