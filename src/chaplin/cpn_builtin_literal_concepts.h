#pragma once

#include "cpn_type_concepts.h"

#define IS_SYMBOLIC_LITERAL_COND(_TYPE) \
    (cpn::concepts::is_symbolic_literal<_TYPE>)

#define IS_SYMBOLIC_LITERAL(_TYPE) \
    typename std::enable_if<IS_SYMBOLIC_LITERAL_COND(_TYPE)>::type

namespace cpn
{
namespace concepts
{

template<typename T>
struct is_symbolic_literal_impl
{
private:
    template<typename TT, typename = decltype(symbolic_literal(std::declval<TT>()))>
    static std::true_type resolve(const TT&);
    template<typename ... TT>
    static typename ddk::mpl::which_type<(resolve(std::declval<TT>()) && ...),std::true_type,std::false_type>::type resolve(const ddk::mpl::type_pack<TT...>&);
    static std::false_type resolve(...);

public:
    static const bool value = decltype(resolve(std::declval<T>()))::value; 
};

template<typename T>
inline constexpr bool is_symbolic_literal = is_symbolic_literal_impl<T>::value;

}

template<typename T>
concept solvable_type = numeric_type<T> || requires (T i_value) { resolve(i_value); };

template<typename T>
concept literal_type = numeric_type<T> || concepts::is_symbolic_literal<T>;

template<typename T>
concept solvable_literal_type = solvable_type<T> && literal_type<T>;

namespace concepts
{

template<size_t,typename>
struct solvable_pack_impl;

template<size_t Size, typename ... T>
struct solvable_pack_impl<Size,ddk::mpl::type_pack<T...>>
{
    static const bool value = (ddk::mpl::num_types<T...> == Size) && (solvable_type<T> && ...);
};

template<size_t,typename>
struct literal_pack_impl;

template<size_t Size, typename ... T>
struct literal_pack_impl<Size,ddk::mpl::type_pack<T...>>
{
    static const bool value = (ddk::mpl::num_types<T...> == Size) && (literal_type<T> && ...);
};

}

template<typename T, size_t Size>
concept solvable_pack = concepts::solvable_pack_impl<Size,T>::value;

template<typename T, size_t Size>
concept literal_pack = concepts::literal_pack_impl<Size,T>::value;

template<typename T, size_t Size>
concept solvable_literal_pack = solvable_pack<T,Size> && literal_pack<T,Size>;

}