
#if defined(WIN32)

#define _ENABLE_EXTENDED_ALIGNED_STORAGE

#endif

#include <gtest/gtest.h>
#include "cpn_integers.h"
#include "cpn_rationals.h"
#include "cpn_reals.h"
#include "cpn_complex.h"
#include "cpn_polynomials.h"
#include "cpn_linear_function.h"
#include "cpn_scalar_function.h"
#include "cpn_functions.h"
#include <utility>

#include <string>
#include <cstddef>

using namespace testing;
using namespace cpn;

template<typename T = float>
struct my_type
{
};

template<template<typename...>typename Template, typename ... T>
struct type_instancer_impl
{
	typedef Template<T...> type;
};
template<template<typename...>typename Template>
using instanced_type = typename type_instancer_impl<Template>::type;

template<typename>
struct my_instance;

template<typename A,typename B>
struct my_instance<A(B)>
{
};


class CPNSymbolicNumberTest : public Test
{
};

template<cpn::set_type T>
void foo(const T& value)
{
}

TEST(CPNSymbolicNumberTest,defaultRationalSetConstruction)
{
	rational_group fooReal1(10,7);
	rational_group fooReal2(7,10);

	rational_group fooReal3 = fooReal1 + fooReal2;
}

TEST(CPNSymbolicNumberTest, defaultRationalGroupConstruction)
{
	rational_group fooReal1(10,7);
	rational_group fooReal2(7,10);

	rational_group fooReal3 = fooReal1 + fooReal2;
}

TEST(CPNSymbolicNumberTest, defaultGroupConstruction)
{
	real fooReal0(10);
	real_set fooReal(fooReal0);

	complex _fooReal0(10,10);
	complex_field _fooReal1(_fooReal0);
	c2 _fooReal2(prod(complexs(_fooReal0),complexs(_fooReal0)));

	c2 _fooReal3 = _fooReal2 + _fooReal2;
	c2 _fooReal4 = _fooReal2 * _fooReal2;

	// cpn::algebraic_structure(fooRational0.number(),cpn::real_comparison_operation{});
	real_group fooReal1(real(20));
	real_group fooReal2(fooReal1);

	real_group fooReal3 = fooReal1 + fooReal2 + fooReal2;
}

TEST(CPNSymbolicNumberTest, defaultRingConstruction)
{
	real_ring fooReal1(real(10));
	real_ring fooReal2(real(20));

	real_ring fooReal3(fooReal1 + fooReal2);
	real_ring fooReal4(fooReal1 * fooReal2);

	real_ring kk2(real(0));
	real_group kk1 = kk2;
}

TEST(CPNSymbolicNumberTest, defaultModuleConstruction)
{
	real_ring fooReal0(real(10));
	real_module fooReal1(real(10));
	real_module fooReal2(real(20));

	real_module fooReal3(fooReal1 + fooReal2);
	real_module fooReal4(fooReal0 ^ fooReal2);
}

TEST(CPNSymbolicNumberTest, defaultFieldConstruction)
{
	real_field fooReal0(real(10));
	real_field fooReal1(real(10));

	real_field fooReal3 = fooReal0 + fooReal1;
	real_field fooReal4 = fooReal0 * fooReal1;
	real_field fooReal5 = fooReal0 / fooReal1;

	Polynomial<RRealAlgebra> myPoly = 1 + x;

	Polynomial<RRealAlgebra> myOtherPoly = myPoly & myPoly;

	[](auto&& i_mono) noexcept
	{


	} <<= myPoly;
}

TEST(CPNSymbolicNumberTest, defaultPowGroupConstruction)
{
	real_group fooReal0(real(20));

	real_group_3 fooReal1(prod(fooReal0,fooReal0,fooReal0));//(cpn::symbolic_number(cpn::integer(10)),cpn::symbolic_number(cpn::integer(10)),cpn::symbolic_number(cpn::integer(10)));
	real_group_3 fooReal2(prod(fooReal0,fooReal0,fooReal0));//(cpn::symbolic_number(cpn::integer(10)),cpn::symbolic_number(cpn::integer(10)),cpn::symbolic_number(cpn::integer(10)));
	real_group_3 fooReal3(prod(fooReal0,fooReal0,fooReal0));//(cpn::symbolic_number(cpn::integer(20)),cpn::symbolic_number(cpn::integer(20)),cpn::symbolic_number(cpn::integer(20)));

//	cpn::real_ring_3 fooReal4 = fooReal2;
	real_group_3 kk = fooReal3;
	real_group_3 fooReal4 = fooReal1 + fooReal2 + fooReal3;
}

TEST(CPNSymbolicNumberTest, defaultPowRingConstruction)
{
	real_ring fooReal0(real(20));

	real_ring_3 fooReal1(prod(fooReal0,fooReal0,fooReal0));//(cpn::symbolic_number(cpn::integer(10)),cpn::symbolic_number(cpn::integer(10)),cpn::symbolic_number(cpn::integer(10)));
	real_ring_3 fooReal2(prod(fooReal0,fooReal0,fooReal0));//(cpn::symbolic_number(cpn::integer(20)),cpn::symbolic_number(cpn::integer(20)),cpn::symbolic_number(cpn::integer(20)));

	real_ring_3 fooReal3 = fooReal1 + fooReal2;
	real_ring_3 fooReal4 = fooReal1 * fooReal2;
	//real_group_3 fooReal5 = as_group(fooReal4);
}

template<typename...>
struct my_class;
template<typename ImSet, typename ... Dom>
struct my_class<ImSet,ddk::mpl::type_pack<Dom...>> : public ddk::detail::inherited_functor_impl<ImSet,Dom...>
{
};

TEST(CPNSymbolicNumberTest, defaultPowModuleConstruction)
{
	real_ring fooReal0(real(10.5));
	real_module fooReal1(real(0));//(cpn::symbolic_number(cpn::integer(10)),cpn::symbolic_number(cpn::integer(10)),cpn::symbolic_number(cpn::integer(10)));
	real_free_module_3 fooReal2(prod(fooReal0,fooReal0,fooReal0));//(cpn::symbolic_number(cpn::integer(20)),cpn::symbolic_number(cpn::integer(20)),cpn::symbolic_number(cpn::integer(20)));

	fooReal2 + fooReal2;

	auto my_var = x_0 + 2 * x_1 + 4;// + cpn::sin(cpn::cos(cpn::x_0) + cpn::x_2 * 5);
	try
	{
		real<cpn::integer_symbolic_literal> caca(10);
		real<float> kiki(caca);

		Function<integer_ring(const integer_ring_2&)> prova_0(my_var);
		Function<integer_ring_2(const integer_ring_2&)> prova_1(my_var,my_var);

		//Function<reals,const reals&> = 1 + x;

		Function<R(const R2&)> prova_3(x_0);

		reals _direction(real(10));

		const cpn::r2 direction(prod(_direction,_direction));
		static_assert(implements_model<decltype(direction),cpn::R2>,"wtf");

		const auto _der = derivative(prova_3,direction);

		_der(direction);

		Function<R(const R&)> prova_4 = cpn::sin;

		[](auto&& i_monomial)
		{

		} <<= taylor_expansion(prova_4,cpn::r(real(0)));

		Polynomial<R> myPoly = { taylor_expansion(prova_4,cpn::r(real(0))),10 };

		// Function<R2,R3> prova_1 = prova_0 + prova_0;

		// prova_0 == prova_1;

		// Function<R1,R3> prova1 = my_var;

		// prova1 = prova1 + prova1;

		//cpn::derivative(prova_0);
	}
	catch(...)
	{
	}



	// cpn::real_module_3 fooReal3 = fooReal1 ^ fooReal2;
    // cpn::real_group_3 fooReal4 = fooReal2 + fooReal2;

    // cpn::real_module_3 fooReal5 = fooReal2;

}

TEST(CPNSymbolicNumberTest, defaultPowFieldConstruction)
{
	real_ring fooReal0(real(20));

	cpn::real_field_3 fooReal1(prod(fooReal0,fooReal0,fooReal0));// = cpn::symbolic_number(cpn::integer(10));
	cpn::real_field_3 fooReal2(prod(fooReal0,fooReal0,fooReal0));// = cpn::symbolic_number(cpn::integer(10));

	cpn::real_field_3 fooReal3 = fooReal1 + fooReal2;
	cpn::real_field_3 fooReal4 = fooReal1 * fooReal2;
	cpn::real_field_3 fooReal5 = fooReal1 / fooReal2;
}

TEST(CPNSymbolicNumberTest, defaultVectorSpaceConstruction)
{
	// cpn::R3 fooRes1(0,0,0);
	// cpn::R3 fooRes2(0,0,0);
	// cpn::real_ring res = fooRes1 * fooRes2;
}
