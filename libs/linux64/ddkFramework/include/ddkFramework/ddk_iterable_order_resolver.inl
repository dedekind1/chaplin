
namespace ddk
{
namespace detail
{

TEMPLATE(typename Adaptor)
REQUIRED(action_tags_supported<Adaptor,end_action_tag,backward_action_tag>)
constexpr auto backward_order_resolver::perform_action(Adaptor&& i_adaptor, const begin_action_tag& i_action) const
{
	typedef mpl::remove_qualifiers<Adaptor> adaptor_t;

	if constexpr (action_tags_supported<Adaptor,backward_action_tag>)
	{
		if (adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor),end_action_tag{}))
		{
			if (auto actionRes = adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor),backward_action_tag{}))
			{
				return make_result<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,begin_action_tag>>(actionRes);
			}
		}

		return make_error<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,begin_action_tag>>();
	}
	else
	{
		if (adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor),end_action_tag{}))
		{
			return make_result<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,begin_action_tag>>(success);
		}
		else
		{
			return make_error<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,begin_action_tag>>();
		}
	}
}
TEMPLATE(typename Adaptor)
REQUIRED(action_tags_supported<Adaptor,begin_action_tag>)
constexpr auto backward_order_resolver::perform_action(Adaptor&& i_adaptor,const end_action_tag& i_action) const
{
	typedef mpl::remove_qualifiers<Adaptor> adaptor_t;

	if (const auto actionRes = adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor),begin_action_tag{}))
	{
		return make_result<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,end_action_tag>>(success);
	}
	else
	{
		return make_error<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,end_action_tag>>();
	}
}
TEMPLATE(typename Adaptor)
REQUIRED(action_tags_supported<Adaptor,backward_action_tag>)
constexpr auto backward_order_resolver::perform_action(Adaptor&& i_adaptor, const forward_action_tag& i_action) const
{
	typedef mpl::remove_qualifiers<Adaptor> adaptor_t;

	if (const auto actionRes = adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor),backward_action_tag{}))
	{
		return make_result<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,forward_action_tag>>(actionRes);
	}
	else
	{
		return make_error<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,forward_action_tag>>();
	}
}
TEMPLATE(typename Adaptor)
REQUIRED(action_tags_supported<Adaptor,forward_action_tag>)
constexpr auto backward_order_resolver::perform_action(Adaptor&& i_adaptor, const backward_action_tag& i_action) const
{
	typedef mpl::remove_qualifiers<Adaptor> adaptor_t;

	if (const auto actionRes = adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor),forward_action_tag{}))
	{
		return make_result<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,backward_action_tag>>(actionRes);
	}
	else
	{
		return make_error<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,backward_action_tag>>();
	}
}
TEMPLATE(typename Adaptor)
REQUIRED(action_tags_supported<Adaptor,displace_action_tag>)
constexpr auto backward_order_resolver::perform_action(Adaptor&& i_adaptor, const displace_action_tag& i_action) const
{
	typedef mpl::remove_qualifiers<Adaptor> adaptor_t;

	if (const auto actionRes = adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor),displace_action_tag{ -i_action.displacement() }))
	{
		return make_result<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,displace_action_tag>>(actionRes);
	}
	else
	{
		return make_error<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,displace_action_tag>>(-actionRes.error().recovery().displacement());
	}
}

template<typename ActionTag>
interleaved_order_resolver::interleaved_action_tag<ActionTag>::interleaved_action_tag(const ActionTag& i_action)
: m_action(i_action)
{
}
template<typename ActionTag>
interleaved_order_resolver::interleaved_action_tag<ActionTag>::interleaved_action_tag(const ActionTag& i_action, ssize_t i_currIndex)
: m_action(i_action)
, m_currIndex(i_currIndex)
{
}
template<typename ActionTag>
template<typename Traits, size_t ... Indexs, typename ... Adaptors>

constexpr auto interleaved_order_resolver::interleaved_action_tag<ActionTag>::apply_action(const mpl::sequence<Indexs...>&, size_t i_currIndex, Adaptors&& ... i_adaptors) const
{
	typedef iterable_action_tag_result<Traits,ActionTag>(*funcType)(const ActionTag& i_action,Adaptors&&...);

	const funcType funcTable[mpl::num_ranks<Indexs...>] = { &interleaved_order_resolver::interleaved_action_tag<ActionTag>::template _apply_action<Traits,Indexs> ... };

	return (*funcTable[i_currIndex])(m_action,std::forward<Adaptors>(i_adaptors)...);
}
template<typename ActionTag>
template<typename Traits, size_t Index, typename ... Adaptors>
constexpr auto interleaved_order_resolver::interleaved_action_tag<ActionTag>::_apply_action(const ActionTag& i_action, Adaptors&& ... i_adaptors)
{
	typedef mpl::remove_qualifiers<mpl::nth_type_of_t<Index,Adaptors...>> indexed_adaptor_t;
	typedef iterable_action_tag_result<Traits,ActionTag> intersection_result;

	if(const auto actionRes = indexed_adaptor_t::perform_action(mpl::nth_value_of<Index>(std::forward<Adaptors>(i_adaptors)...),i_action))
	{
		typedef typename Traits::reference reference;

		const auto create_reference = [](auto&& ... i_result) -> optional<reference>
		{
			if ((static_cast<bool>(i_result) && ...))
			{
				return reference{ i_result.get() ... };
			}
			else
			{
				return none;
			}
		};

		if(auto sinkRes = create_reference(mpl::remove_qualifiers<Adaptors>::perform_action(std::forward<Adaptors>(i_adaptors),k_iterableEmptySink)...))
		{
			return make_result<intersection_result>(*sinkRes);
		}
		else
		{
			return make_error<intersection_result>(std::move(i_action));
		}
	}
	else
	{
		return make_error<intersection_result>(i_action);
	}
}

TEMPLATE(typename ... Iterables)
REQUIRED(action_tags_supported<deduced_adaptor<Iterables>,forward_action_tag>...)
constexpr auto interleaved_order_resolver::perform_action(const iterable_adaptor<detail::intersection_iterable_impl<Iterables...>>& i_adaptor, const forward_action_tag& i_action) const
{
	typedef const intersection_iterable_adaptor<Iterables...> adaptor_t;

	if(auto actionRes = adaptor_t::perform_action(i_adaptor,interleaved_action_tag<forward_action_tag>{i_action,m_index++}))
	{
		return ddk::make_result<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,forward_action_tag>>(actionRes);
	}
	else
	{
		return ddk::make_error<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,forward_action_tag>>(i_action);
	}
}
TEMPLATE(typename ... Iterables)
REQUIRED(action_tags_supported<deduced_adaptor<Iterables>,backward_action_tag>...)
constexpr auto interleaved_order_resolver::perform_action(const iterable_adaptor<detail::intersection_iterable_impl<Iterables...>>& i_adaptor, const backward_action_tag& i_action) const
{
	typedef const intersection_iterable_adaptor<Iterables...> adaptor_t;

	if(m_index-- == 0)
	{
		return adaptor_t::perform_action(i_adaptor,i_action);
	}
	else
	{
		if(auto actionRes = adaptor_t::perform_action(i_adaptor,interleaved_action_tag<backward_action_tag>{i_action,m_index+1}))
		{
			return ddk::make_result<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,backward_action_tag>>(actionRes);
		}
		else
		{
			return ddk::make_error<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,backward_action_tag>>(i_action);
		}
	}
}
TEMPLATE(typename ... Iterables)
REQUIRED(action_tags_supported<deduced_adaptor<Iterables>,forward_action_tag>...)
constexpr auto interleaved_order_resolver::perform_action(iterable_adaptor<detail::intersection_iterable_impl<Iterables...>>& i_adaptor, const forward_action_tag& i_action) const
{
	typedef intersection_iterable_adaptor<Iterables...> adaptor_t;

	if(auto actionRes = adaptor_t::perform_action(i_adaptor,interleaved_action_tag<forward_action_tag>{i_action,m_index++}))
	{
		return ddk::make_result<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,forward_action_tag>>(actionRes);
	}
	else
	{
		return ddk::make_error<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,forward_action_tag>>(i_action);
	}
}
TEMPLATE(typename ... Iterables)
REQUIRED(action_tags_supported<deduced_adaptor<Iterables>,backward_action_tag>...)
constexpr auto interleaved_order_resolver::perform_action(iterable_adaptor<detail::intersection_iterable_impl<Iterables...>>& i_adaptor, const backward_action_tag& i_action) const
{
	typedef intersection_iterable_adaptor<Iterables...> adaptor_t;

	if(m_index-- == 0)
	{
		return adaptor_t::perform_action(i_adaptor,i_action);
	}
	else
	{
		if(auto actionRes = adaptor_t::perform_action(i_adaptor,interleaved_action_tag<backward_action_tag>{i_action,m_index+1}))
		{
			return ddk::make_result<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,backward_action_tag>>(actionRes);
		}
		else
		{
			return ddk::make_error<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,backward_action_tag>>(i_action);
		}
	}
}
TEMPLATE(typename ... Iterables)
REQUIRED(action_tags_supported<deduced_adaptor<Iterables>,begin_action_tag,forward_action_tag>...)
constexpr auto interleaved_order_resolver::perform_action(const iterable_adaptor<detail::union_iterable_impl<Iterables...>>& i_adaptor, const forward_action_tag& i_action) const
{
	typedef const union_iterable_adaptor<Iterables...> adaptor_t;

	size_t nextIndex = i_adaptor.get_current_iterable_index() + 1;

	if(nextIndex == mpl::num_types<Iterables...>)
	{
		nextIndex = 0;
		m_index++;
	}

	i_adaptor.set_current_iterable_index(nextIndex);

	if(m_index == 0)
	{
		if(auto actionRes = adaptor_t::perform_action(i_adaptor,begin_action_tag{}))
		{
			return ddk::make_result<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,forward_action_tag>>(actionRes);
		}
		else
		{
			return ddk::make_error<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,forward_action_tag>>(i_action);
		}
	}
	else
	{
		return adaptor_t::perform_action(i_adaptor,i_action);
	}
}
TEMPLATE(typename ... Iterables)
REQUIRED(action_tags_supported<deduced_adaptor<Iterables>,backward_action_tag,end_action_tag>...)
constexpr auto interleaved_order_resolver::perform_action(const iterable_adaptor<detail::union_iterable_impl<Iterables...>>& i_adaptor, const backward_action_tag& i_action) const
{
	typedef const union_iterable_adaptor<Iterables...> adaptor_t;

	if(auto actionRes = adaptor_t::perform_action(i_adaptor,i_action))
	{
		size_t nextIndex = i_adaptor.get_current_iterable_index();

		if(nextIndex == 0)
		{
			nextIndex = mpl::num_types<Iterables...>;
			m_index++;
		}

		i_adaptor.set_current_iterable_index(nextIndex - 1);
		
		if(m_index == 0)
		{
			if_not(auto actionRes = adaptor_t::perform_action(i_adaptor,end_action_tag{}))
			{
				return ddk::make_error<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,backward_action_tag>>(i_action);
			}
		}

		return ddk::make_result<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,backward_action_tag>>(actionRes);
	}
	else
	{
		return ddk::make_error<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,backward_action_tag>>(i_action);
	}
}
TEMPLATE(typename ... Iterables)
REQUIRED(action_tags_supported<deduced_adaptor<Iterables>,begin_action_tag,forward_action_tag>...)
constexpr auto interleaved_order_resolver::perform_action(iterable_adaptor<detail::union_iterable_impl<Iterables...>>& i_adaptor, const forward_action_tag& i_action) const
{
	typedef union_iterable_adaptor<Iterables...> adaptor_t;

	size_t nextIndex = i_adaptor.get_current_iterable_index() + 1;

	if(nextIndex == mpl::num_types<Iterables...>)
	{
		nextIndex = 0;
		m_index++;
	}

	i_adaptor.set_current_iterable_index(nextIndex);

	if(m_index == 0)
	{
		if(auto actionRes = adaptor_t::perform_action(i_adaptor,begin_action_tag{}))
		{
			return ddk::make_result<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,forward_action_tag>>(actionRes);
		}
		else
		{
			return ddk::make_error<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,forward_action_tag>>(i_action);
		}
	}
	else
	{
		return adaptor_t::perform_action(i_adaptor,i_action);
	}
}
TEMPLATE(typename ... Iterables)
REQUIRED(action_tags_supported<deduced_adaptor<Iterables>,backward_action_tag,end_action_tag>...)
constexpr auto interleaved_order_resolver::perform_action(iterable_adaptor<detail::union_iterable_impl<Iterables...>>& i_adaptor, const backward_action_tag& i_action) const
{
	typedef union_iterable_adaptor<Iterables...> adaptor_t;

	if(auto actionRes = adaptor_t::perform_action(i_adaptor,i_action))
	{
		size_t nextIndex = i_adaptor.get_current_iterable_index();

		if(nextIndex == 0)
		{
			nextIndex = mpl::num_types<Iterables...>;
			m_index++;
		}

		i_adaptor.set_current_iterable_index(nextIndex - 1);
		
		if(m_index == 0)
		{
			if_not(auto actionRes = adaptor_t::perform_action(i_adaptor,end_action_tag{}))
			{
				return ddk::make_error<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,backward_action_tag>>(i_action);
			}
		}

		return ddk::make_result<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,backward_action_tag>>(actionRes);
	}
	else
	{
		return ddk::make_error<iterable_action_tag_result<detail::adaptor_traits<adaptor_t>,backward_action_tag>>(i_action);
	}
}

template<typename Traits, size_t Index, typename ... Adaptors>
constexpr auto cumulative_order_resolver::cumulative_forward_action_tag::apply_action(Adaptors&& ... i_adaptors) const
{
	typedef mpl::remove_qualifiers<mpl::nth_type_of_t<Index,Adaptors...>> indexed_adaptor_t;
	typedef iterable_action_tag_result<Traits,forward_action_tag> intersection_result;

	if(auto actionRes = indexed_adaptor_t::perform_action(mpl::nth_value_of<Index>(std::forward<Adaptors>(i_adaptors)...),m_action))
	{
		typedef typename Traits::reference reference;

		const auto create_reference = [](auto&& ... i_result) -> optional<reference>
		{
			if ((static_cast<bool>(i_result) && ...))
			{
				return reference{ i_result.get() ... };
			}
			else
			{
				return none;
			}
		};

		if(auto sinkRes = create_reference(mpl::remove_qualifiers<Adaptors>::perform_action(std::forward<Adaptors>(i_adaptors),k_iterableEmptySink)...))
		{
			return make_result<intersection_result>(*sinkRes);
		}
		else
		{
			return make_error<intersection_result>(m_action);
		}
	}

	if constexpr (Index < mpl::num_types<Adaptors...> - 1)
	{
		if(auto aactionRes = indexed_adaptor_t::perform_action(mpl::nth_value_of<Index>(std::forward<Adaptors>(i_adaptors)...),begin_action_tag{}))
		{
			return apply_action<Traits,Index+1>(std::forward<Adaptors>(i_adaptors)...);
		}
		else
		{
			return make_error<intersection_result>(m_action);
		}
	}
	else
	{
		return make_error<intersection_result>(m_action);
	}
}

TEMPLATE(typename Adaptor)
REQUIRED(action_tags_supported<Adaptor,forward_action_tag,begin_action_tag>)
constexpr auto cumulative_order_resolver::perform_action(Adaptor&& i_adaptor, const forward_action_tag& i_action) const
{
	typedef mpl::remove_qualifiers<Adaptor> adaptor_t;

	if(auto actionRes = adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor),cumulative_forward_action_tag{i_action}))
	{
		return ddk::make_result<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,forward_action_tag>>(actionRes);
	}
	else
	{
		return ddk::make_error<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,forward_action_tag>>(i_action);
	}
}

TEMPLATE(typename Adaptor)
REQUIRED(action_tags_supported<Adaptor,forward_action_tag>,is_dimensionable_iterable_adaptor<Adaptor>)
constexpr auto transpose_multi_dimensional_order_resolver::perform_action(Adaptor&& i_adaptor, const forward_action_tag& i_action) const
{
	typedef typename adaptor_traits<Adaptor>::difference_type difference_type;

	if (const auto actionRes = _perform_action(std::forward<Adaptor>(i_adaptor),difference_type{ 1 }))
	{
		return make_result<iterable_action_tag_result<adaptor_traits<Adaptor>,forward_action_tag>>(actionRes);
	}
	else
	{
		return make_error<iterable_action_tag_result<adaptor_traits<Adaptor>,forward_action_tag>>();
	}
}
TEMPLATE(typename Adaptor)
REQUIRED(action_tags_supported<Adaptor,backward_action_tag>,is_dimensionable_iterable_adaptor<Adaptor>)
constexpr auto transpose_multi_dimensional_order_resolver::perform_action(Adaptor&& i_adaptor, const backward_action_tag& i_action) const
{
	typedef typename mpl::remove_qualifiers<Adaptor>::traits adaptor_traits;
	typedef typename adaptor_traits::difference_type difference_type;

	if (const auto actionRes = _perform_action(std::forward<Adaptor>(i_adaptor),difference_type{ -1 }))
	{
		return make_result<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,backward_action_tag>>(actionRes);
	}
	else
	{
		return make_error<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,backward_action_tag>>();
	}
}
TEMPLATE(typename Adaptor)
REQUIRED(action_tags_supported<Adaptor,displace_action_tag>,is_dimensionable_iterable_adaptor<Adaptor>)
constexpr auto transpose_multi_dimensional_order_resolver::perform_action(Adaptor&& i_adaptor, const displace_action_tag& i_action) const
{
	return _perform_action(std::forward<Adaptor>(i_adaptor),i_action.displacement());
}
template<typename Adaptor>
constexpr auto transpose_multi_dimensional_order_resolver::_perform_action(Adaptor&& i_adaptor,typename mpl::remove_qualifiers<Adaptor>::traits::difference_type i_displacement)
{
	typedef typename mpl::remove_qualifiers<Adaptor> adaptor_t;
	typedef typename adaptor_t::dimension_t adaptor_dimension;
	typedef typename adaptor_t::traits adaptor_traits;
	typedef typename adaptor_traits::difference_type difference_type;
	typedef typename adaptor_dimension::template drop<0> adaptor_sub_dimension;

	difference_type shift = i_displacement * adaptor_sub_dimension::prod;

	while(true)
	{
		if (const auto actionRes = i_adaptor.perform_action(std::forward<Adaptor>(i_adaptor),displace_action_tag{ static_cast<difference_type>(shift) }))
		{
			return make_result<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,displace_action_tag>>(actionRes);
		}
		else
		{
			const displace_action_tag pendingAction = actionRes.error().recovery();

			if (pendingAction.displacement() < shift)
			{
				shift = -static_cast<difference_type>(adaptor_dimension::prod) + 2;

				continue;
			}
			else
			{
				return make_error<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,displace_action_tag>>(pendingAction.displacement());
			}
		}
	}
}

}
}
