//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable_action_tags.h"
#include "ddk_iterable_action_result.h"
#include "ddk_iterable_action_tag_result.h"
#include "ddk_iterable_action_tag_concepts.h"
#include "ddk_iterable_action_defs.h"
#include "ddk_iterable_action.inl"