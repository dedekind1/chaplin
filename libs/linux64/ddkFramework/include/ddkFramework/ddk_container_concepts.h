//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_type_concepts.h"

namespace ddk
{
namespace concepts
{

template<typename Container>
struct is_container_impl
{
private:
	template<typename T, typename = decltype(std::declval<T>()[0])>
	static std::true_type random_access_type(const T&);
	static std::false_type random_access_type(...);
	template<typename T, typename = decltype(std::declval<T>().front())>
	static std::true_type relative_access_type(const T&);
	static std::false_type relative_access_type(...);
	template<typename T, typename = decltype(std::declval<T>().begin()), typename = decltype(std::declval<T>().end())>
	static std::true_type iterable_access_type(const T&);
	static std::false_type iterable_access_type(...);

public:
	static const bool value = (decltype(random_access_type(std::declval<Container>()))::value ||
							   decltype(relative_access_type(std::declval<Container>()))::value ||
							   decltype(iterable_access_type(std::declval<Container>()))::value) &&
							  std::is_default_constructible<Container>::value;
};

template<typename Container>
struct is_container_dynamic_size_impl
{
private:
	template<typename T>
	static std::true_type _check(const T&,decltype(std::declval<T>().insert(std::declval<typename T::iterator>(),std::declval<typename T::value_type>()))*);
	template<typename T>
	static std::false_type _check(const T&, ...);

public:
	static const bool value = is_container_impl<Container>::value && decltype(_check(std::declval<Container>(),nullptr))::value;
};

template<typename T>
std::false_type is_indexed_container_impl(const T&,...);
template<typename T,typename = typename T::place_type,size_t = T::num_places>
std::true_type is_indexed_container_impl(T&);

template<typename T>
struct is_accessible_by
{
private:
	template<typename TT, typename = decltype(std::declval<T>().at(0))>
	static std::true_type index_access_type(const TT&);
	static std::false_type index_access_type(...);
	template<typename TT, typename = decltype(std::declval<TT>().template get<0>())>
	static std::true_type type_access_type(const T&);
	static std::false_type type_access_type(...);

public:
	static const bool index = decltype(index_access_type(std::declval<T>()))::value;
	static const bool type = decltype(random_access_type(std::declval<T>()))::value;
};

template<typename T, typename Value>
struct is_assignable_by
{
private:
	template<typename TT, typename = decltype(std::declval<TT>() = std::declval<Value>())>
	static std::true_type resolve(const TT&);
	static std::false_type resolve(...);

public:
	static const bool index = decltype(resolve(std::declval<typename is_accessible_by<T>::index>()))::value;
	static const bool type = decltype(resolve(std::declval<typename is_accessible_by<T>::type>()))::value;
};

}

template<typename T>
inline constexpr bool is_container = concepts::is_container_impl<typename std::remove_reference<T>::type>::value;

template<typename T>
inline constexpr bool is_container_fixed_size = is_container<T> && (concepts::is_container_dynamic_size_impl<T>::value == false);

template<typename T>
inline constexpr bool is_container_dynamic_size = is_container<T> && (concepts::is_container_dynamic_size_impl<T>::value == false);

template<typename T>
inline constexpr bool is_sized_container = is_container<T> && is_sized<T>;

template<typename T>
inline constexpr bool accessible_by_index_access = concepts::is_accessible_by<typename std::remove_reference<T>::type>::index;

template<typename T>
inline constexpr bool accessible_by_type_access = concepts::is_accessible_by<typename std::remove_reference<T>::type>::type;

template<typename T, typename TT>
inline constexpr bool assignable_by_index_access = concepts::is_assignable_by<typename std::remove_reference<T>::type,TT>::index;

template<typename T, typename TT>
inline constexpr bool assignable_by_type_access = concepts::is_assignable_by<typename std::remove_reference<T>::type,TT>::type;

template<typename T>
inline constexpr bool is_indexed_container = decltype(concepts::is_indexed_container_impl(std::declval<T&>()))::value;
	
}
