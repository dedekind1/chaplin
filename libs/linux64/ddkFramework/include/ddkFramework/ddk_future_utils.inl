
namespace ddk
{

TEMPLATE(typename ... Futures)
REQUIRED(is_num_of_args_greater<0,Futures...>,is_future<Futures>...)
auto compose(Futures&& ... i_components)
{
	if constexpr (mpl::are_same_type<typename Futures::value_type ...>())
	{
		return composed_future<std::array<mpl::nth_type_of_t<0, typename Futures::value_type...>,mpl::num_types<Futures...>>>(std::forward<Futures>(i_components)...);
	}
	else
	{
		return composed_future<std::tuple<typename Futures::value_type...>>(std::forward<Futures>(i_components) ...);
	}
}

}
