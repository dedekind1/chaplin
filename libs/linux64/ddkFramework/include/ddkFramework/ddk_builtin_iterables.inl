
#include <limits>

namespace ddk
{
namespace detail
{

template<typename Stepper>
TEMPLATE(typename T)
REQUIRED(is_constructible<typename __numbers_iterable<Stepper>::value_type,const T&>)
__numbers_iterable<Stepper>::__numbers_iterable(const Stepper& i_step, const T& i_initValue)
: m_step(i_step)
, m_initValue(i_initValue)
{
}
template<typename Stepper>
const typename __numbers_iterable<Stepper>::value_type& __numbers_iterable<Stepper>::init_value() const
{
    return m_initValue;
}
template<typename Stepper>
auto __numbers_iterable<Stepper>::step() const
{
    return ddk::eval(m_step);
}
template<typename Stepper>
template<typename SStepper>
__numbers_iterable<SStepper> __numbers_iterable<Stepper>::operator()(const SStepper& i_step) const
{
    return { i_step,m_initValue };
}

}

template<typename Stepper>
iterable_adaptor<const detail::__numbers_iterable<Stepper>>::iterable_adaptor(const detail::__numbers_iterable<Stepper>& i_numbers)
: m_numbers(i_numbers)
, m_currValue(i_numbers.init_value())
{
}
template<typename Stepper>
template<typename Adaptor, typename Sink>
iterable_action_tag_result<typename iterable_adaptor<const detail::__numbers_iterable<Stepper>>::traits,sink_action_tag<Sink>> iterable_adaptor<const detail::__numbers_iterable<Stepper>>::perform_action(Adaptor&& i_adaptor, const sink_action_tag<Sink>& i_sink)
{
    return i_sink(i_adaptor.m_currValue);
}
template<typename Stepper>
template<typename Adaptor>
iterable_action_tag_result<typename iterable_adaptor<const detail::__numbers_iterable<Stepper>>::traits,begin_action_tag> iterable_adaptor<const detail::__numbers_iterable<Stepper>>::perform_action(Adaptor&& i_adaptor, const begin_action_tag&)
{
    i_adaptor.m_currValue = i_adaptor.m_numbers.init_value();

    return make_result<iterable_action_tag_result<traits,begin_action_tag>>(i_adaptor.m_currValue);
}
template<typename Stepper>
template<typename Adaptor>
iterable_action_tag_result<typename iterable_adaptor<const detail::__numbers_iterable<Stepper>>::traits,end_action_tag> iterable_adaptor<const detail::__numbers_iterable<Stepper>>::perform_action(Adaptor&& i_adaptor, const end_action_tag&)
{
    i_adaptor.m_currValue = std::numeric_limits<underlying_type>::max();

    return make_result<iterable_action_tag_result<traits,end_action_tag>>(success);
}
template<typename Stepper>
template<typename Adaptor>
iterable_action_tag_result<typename iterable_adaptor<const detail::__numbers_iterable<Stepper>>::traits,forward_action_tag> iterable_adaptor<const detail::__numbers_iterable<Stepper>>::perform_action(Adaptor&& i_adaptor,const forward_action_tag&)
{
    const underlying_type k_newValue = i_adaptor.m_currValue + i_adaptor.m_numbers.step();

    if(k_newValue < std::numeric_limits<underlying_type>::max())
    {
        i_adaptor.m_currValue = k_newValue;

        return make_result<iterable_action_tag_result<traits,forward_action_tag>>(i_adaptor.m_currValue);
    }
    else
    {
        return make_error<iterable_action_tag_result<traits,forward_action_tag>>();
    }
}
template<typename Stepper>
template<typename Adaptor>
iterable_action_tag_result<typename iterable_adaptor<const detail::__numbers_iterable<Stepper>>::traits,backward_action_tag> iterable_adaptor<const detail::__numbers_iterable<Stepper>>::perform_action(Adaptor&& i_adaptor,const backward_action_tag&)
{
    const underlying_type k_newValue = i_adaptor.m_currValue - i_adaptor.m_numbers.step();

    if(k_newValue > std::numeric_limits<underlying_type>::min())
    {
        i_adaptor.m_currValue = k_newValue;

        return make_result<iterable_action_tag_result<traits,backward_action_tag>>(i_adaptor.m_currValue);
    }
    else
    {
        return make_error<iterable_action_tag_result<traits,backward_action_tag>>();
    }
}

}