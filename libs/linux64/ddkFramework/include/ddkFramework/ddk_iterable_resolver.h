//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable_adaptor_resolver.h"
#include "ddk_iterable_traits.h"

namespace ddk
{
namespace detail
{

template<typename,typename>
class iterable_impl;

template<typename>
struct iterable_traits_correspondence_resolver;

template<typename Adaptor>
struct iterable_traits_correspondence_resolver
{
	typedef detail::iterable_traits<typename Adaptor::traits> type;
};

template<typename>
struct iterable_correspondence_impl;

template<typename Iterable>
struct iterable_correspondence_impl<iterable<Iterable>>
{
	typedef iterable<Iterable> type;
};
template<typename Iterable>
struct iterable_correspondence_impl<const iterable<Iterable>>
{
	typedef const iterable<Iterable> type;
};
template<typename Iterable>
struct iterable_correspondence_impl
{
private:
	template<typename IIterable, typename = typename IIterable::iterable_type>
	static typename iterable_correspondence_impl<typename IIterable::iterable_type>::type resolve(IIterable&,int);
	template<typename IIterable>
	static iterable<iterable_impl<typename iterable_traits_correspondence_resolver<iterable_adaptor<IIterable>>::type,IIterable>> resolve(IIterable&,float);

public:
	typedef decltype(resolve(std::declval<Iterable&>(),0)) type;
};

template<typename Iterable>
using iterable_correspondence = typename iterable_correspondence_impl<Iterable>::type;

}
}

#include "ddk_iterable_traits_resolver.h"