
#include "ddk_async_shared_state_base.h"
#include "ddk_system_allocator.h"

namespace ddk
{

template<typename T, typename TT>
void async_allocator::deallocate(detail::embedded_private_async_state<T,TT>* i_ptr) const
{
    //get chained async (if any)
    detail::private_async_state_base_shared_ptr chainedExecutor = i_ptr->get_executor().get_chained_executor();

    //regular deallocation
    i_ptr->~embedded_private_async_state();

    if(const allocator_interface* _allocator = extract_raw_ptr(m_allocator))
    {
        _allocator->deallocate(static_cast<void*>(i_ptr));
    }
    else
    {
        g_system_allocator.deallocate(static_cast<void*>(i_ptr));
    }
}

}