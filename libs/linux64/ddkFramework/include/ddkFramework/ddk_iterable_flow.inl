
#include "ddk_fiber_utils.h"
#include "ddk_iterable_result.h"

namespace ddk
{
namespace iter
{

template<typename T>
void yield(T&& i_value)
{
    ddk::yield(make_result<iterable_result<mpl::remove_qualifiers<T>>>(std::forward<T>(i_value)));
}
TEMPLATE(typename T, typename ... Args)
REQUIRED(is_constructible<T,Args...>)
void yield(Args&& ... i_args)
{
    ddk::yield(make_result<iterable_result<T>>(std::forward<Args>(i_args)...));
}

template<typename T>
void stop()
{
    ddk::yield(make_result<iterable_result<T>>(success));
}

TEMPLATE(typename T, typename ... Args)
REQUIRED(is_constructible<iterable_error,Args...>)
void suspend(Args&& ... i_args)
{
    ddk::yield(make_error<iterable_result<T>>(std::forward<Args>(i_args)...));
}

}
}