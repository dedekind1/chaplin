//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable_action.h"
#include "ddk_function.h"

namespace ddk
{
namespace detail
{

template<typename Stepper>
struct __numbers_iterable
{
public:
    typedef typename mpl::aqcuire_callable_return_type<Stepper>::type value_type;

    static_assert(is_numeric<value_type>, "You shall provide a numeric type.");

    TEMPLATE(typename T)
    REQUIRES(is_constructible<value_type,const T&>)
    __numbers_iterable(const Stepper& i_step, const T& i_initValue);

    const value_type& init_value() const;
    auto step() const;
    template<typename SStepper>
    inline __numbers_iterable<SStepper> operator()(const SStepper& i_step) const;

private:
    const Stepper m_step;
    const value_type m_initValue;
};
template<typename Stepper, typename T>
__numbers_iterable(const Stepper&, const T&) -> __numbers_iterable<Stepper>;

const float k_eps = 0.0001f;

}

template<typename Stepper>
class iterable_adaptor<const detail::__numbers_iterable<Stepper>>
{
    typedef typename detail::__numbers_iterable<Stepper>::value_type underlying_type;

public:
    typedef mpl::empty_type_pack tags_t;
    typedef mpl::type_pack<agnostic_sink_action_tag<const underlying_type&>,begin_action_tag,forward_action_tag,backward_action_tag,end_action_tag> const_tags_t;
    typedef detail::iterable_by_type_adaptor<const underlying_type,tags_t,const_tags_t> traits;
    typedef detail::const_iterable_traits<traits> const_traits;

    iterable_adaptor(const detail::__numbers_iterable<Stepper>& i_numbers);
    template<typename Adaptor, typename Sink>
    static inline iterable_action_tag_result<traits,sink_action_tag<Sink>> perform_action(Adaptor&& i_adaptor, const sink_action_tag<Sink>& i_sink);
    template<typename Adaptor>
    static inline iterable_action_tag_result<traits,begin_action_tag> perform_action(Adaptor&& i_adaptor, const begin_action_tag&);
    template<typename Adaptor>
    static inline iterable_action_tag_result<traits,end_action_tag> perform_action(Adaptor&& i_adaptor, const end_action_tag&);
    template<typename Adaptor>
    static inline iterable_action_tag_result<traits,forward_action_tag> perform_action(Adaptor&& i_adaptor, const forward_action_tag&);
    template<typename Adaptor>
    static inline iterable_action_tag_result<traits,backward_action_tag> perform_action(Adaptor&& i_adaptor, const backward_action_tag&);

private:
    mutable underlying_type m_currValue;
    const detail::__numbers_iterable<Stepper> m_numbers;
};

template<typename Stepper>
class iterable_adaptor<detail::__numbers_iterable<Stepper>> : public iterable_adaptor<const detail::__numbers_iterable<Stepper>>
{
public:
    using iterable_adaptor<const detail::__numbers_iterable<Stepper>>::traits;
    using iterable_adaptor<const detail::__numbers_iterable<Stepper>>::const_traits;
    using iterable_adaptor<const detail::__numbers_iterable<Stepper>>::tags_t;
    using iterable_adaptor<const detail::__numbers_iterable<Stepper>>::const_tags_t;

    using iterable_adaptor<const detail::__numbers_iterable<Stepper>>::iterable_adaptor;
};

const_bidirectional_iterable<const int> integers(int i_initialValue = 0);
const_bidirectional_iterable<const int> odd_integers(int i_initialValue = 1);
const_bidirectional_iterable<const int> even_integers(int i_initialValue = 0);

const_bidirectional_iterable<const int> naturals(unsigned int i_initialValue = 0);
const_bidirectional_iterable<const int> odd_naturals(unsigned int i_initialValue = 1);
const_bidirectional_iterable<const int> even_naturals(unsigned int i_initialValue = 0);

const_bidirectional_iterable<const int> finite_naturals(unsigned int i_max, unsigned int i_initialValue = 0);
const_bidirectional_iterable<const int> finite_odd_naturals(unsigned int i_max, unsigned int i_initialValue = 1);
const_bidirectional_iterable<const int> finite_even_naturals(unsigned int i_max, unsigned int i_initialValue = 0);

const_bidirectional_iterable<const float> reals(float i_prec = detail::k_eps, float i_initialValue = 0.f);

}
