//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_template_helper.h"
#include "ddk_scoped_enum.h"

namespace ddk
{
namespace concepts
{

template<typename T>
struct is_sized_type_impl
{
private:
	template<typename TT>
	static std::true_type sized_type(TT&,const decltype(std::declval<T>().size())* = nullptr);
	template<typename TT>
	static std::false_type sized_type(const TT&,...);

public:
	static const bool value = decltype(sized_type(std::declval<T&>(),nullptr))::value;
};

template<typename T>
struct is_dimensioned_type_impl
{
private:
	template<typename TT>
	static std::true_type resolve(T&,decltype(std::declval<TT>().dimension())* = nullptr);
	template<template<typename,size_t...> typename TT,typename R,size_t ... Dims>
	static std::true_type resolve(const TT<R,Dims...>&);
	template<typename TT>
	static std::false_type resolve(const TT&,...);

public:
	static const bool value = decltype(resolve(std::declval<T&>()))::value;
};

template<typename T>
inline constexpr bool is_dimensioned_type = is_dimensioned_type_impl<T>::value;

template<typename T>
struct is_comparable
{
private:
	template<typename TT, typename = decltype(std::declval<TT>() == std::declval<TT>())>
	static std::true_type resolve_equal(const TT&);
	static std::false_type resolve_equal(...);
	template<typename TT, typename = decltype(std::declval<TT>() == std::declval<TT>())>
	static std::true_type resolve_less(const TT&);
	static std::false_type resolve_less(...);

public:
	static const bool equally = decltype(resolve_equal(std::declval<T>()))::value;
	static const bool less_than = decltype(resolve_less(std::declval<T>()))::value;
};

}

template<size_t Num, typename ... Args>
inline constexpr bool is_num_of_args_equal = mpl::num_types<Args...> == Num;

template<size_t Num, typename ... Args>
inline constexpr bool is_num_of_args_greater = mpl::num_types<Args...> > Num;

template<size_t Num, typename ... Args>
inline constexpr bool is_num_of_args_greater_or_equal = mpl::num_types<Args...> >= Num;

template<size_t Num, typename ... Args>
inline constexpr bool is_num_of_args_lesser = mpl::num_types<Args...> < Num;

template<size_t Num, typename ... Args>
inline constexpr bool is_num_of_args_lesser_or_equal = mpl::num_types<Args...> <= Num;

template<typename T, typename ... TT>
inline constexpr bool is_among_types = mpl::is_among_types<T,TT...>;

template<typename T, typename ... TT>
inline constexpr bool is_not_among_types = mpl::is_among_types<T,TT...> == false;

template<typename T, typename ... TT>
inline constexpr bool is_among_constructible_types = mpl::is_among_constructible_types<T,TT...>;

template<typename T, typename ... TT>
inline constexpr bool is_not_among_constructible_types = mpl::is_among_constructible_types<T,TT...> == false;

template<typename T>
inline constexpr bool is_reference = std::is_reference<T>::value;

template<typename T>
inline constexpr bool is_not_reference = std::is_reference<T>::value == false;

template<typename T, typename ... TT>
inline constexpr bool is_base_of = (std::is_base_of<T,TT>::value && ...);

template<typename T, typename ... TT>
inline constexpr bool is_not_base_of = ((std::is_base_of<T,TT>::value == false) || ...);

template<typename T, typename ... TT>
inline constexpr bool is_derived_from = (std::is_base_of<TT,T>::value && ...);

template<typename T, typename ... TT>
inline constexpr bool is_not_derived_from = ((std::is_base_of<TT,T>::value == false) || ...);

template<typename T, typename TT>
inline constexpr bool is_bindable_by = std::is_base_of<TT,T>::value;//concepts::is_bindable_by_impl<T,TT>::value;

template<typename T, typename TT>
inline constexpr bool is_not_bindable_by = is_base_of<TT,T> == false;

template<typename T, typename TT>
inline constexpr bool is_returnable = (std::is_reference<T>::value && std::is_reference<TT>::value && std::is_base_of<TT,T>::value) || ((std::is_reference<T>::value == false) && (std::is_reference<TT>::value == false) && std::is_convertible<T,TT>::value);

template<typename T, typename TT>
inline constexpr bool is_not_returnable = is_returnable<T,TT> == false;

template<typename T, typename TT>
inline constexpr bool is_same_class = std::is_same<T,TT>::value;

template<typename T, typename TT>
inline constexpr bool is_not_same_class = std::is_same<T,TT>::value == false;

template<typename T>
inline constexpr bool is_class = std::is_class<mpl::remove_qualifiers<T>>::value;

template<typename T>
inline constexpr bool is_not_class = std::is_class<mpl::remove_qualifiers<T>>::value == false;

template<typename T>
inline constexpr bool is_default_constructible = std::is_default_constructible<T>::value;

template<typename T, typename ... Args>
inline constexpr bool is_constructible = std::is_constructible<T,Args...>::value;

template<typename T>
inline constexpr bool is_copy_constructible = std::is_copy_constructible<T>::value;

template<typename T>
inline constexpr bool is_move_constructible = std::is_copy_constructible<T>::value;

template<typename T, typename TT>
inline constexpr bool is_convertible = std::is_convertible<T,TT>::value;

template<typename T, typename TT>
inline constexpr bool is_assignable = std::is_assignable<T,TT>::value;

template<typename T>
inline constexpr bool is_copy_assignable = std::is_copy_assignable<T>::value;

template<typename T>
inline constexpr bool is_move_assignable = std::is_move_assignable<T>::value;

template<typename T>
inline constexpr bool is_equally_comparable = concepts::is_comparable<T>::equally;

template<typename T>
inline constexpr bool is_comparable = concepts::is_comparable<T>::less_than;

template<typename T>
inline constexpr bool is_integral = std::is_integral<T>::value;

template<typename T>
inline constexpr bool is_fractional = std::is_floating_point<T>::value;

template<typename T>
inline constexpr bool is_numeric = std::is_arithmetic<T>::value;

template<typename T>
inline constexpr bool is_type_const = std::is_const<T>::value;

template<typename T>
inline constexpr bool is_pointer = std::is_pointer<T>::value;

template<typename T>
inline constexpr bool is_sized = concepts::is_sized_type_impl<T>::value;

}
