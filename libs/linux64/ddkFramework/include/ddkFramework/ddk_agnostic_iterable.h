//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_view_iterable.h"

namespace ddk
{

template<typename Iterable>
class agnostic_iterable : public detail::iterable<detail::type_erasure_iterable_impl<typename Iterable::traits>>
{
public:
    using detail::iterable<detail::type_erasure_iterable_impl<typename Iterable::traits>>::iterable;
    agnostic_iterable(const agnostic_iterable&) = delete;
    agnostic_iterable(agnostic_iterable&) = default;
    agnostic_iterable(agnostic_iterable&&) = default;
};
template<typename Iterable>
agnostic_iterable(detail::iterable<Iterable>&) -> agnostic_iterable<Iterable>;
template<typename Iterable>
agnostic_iterable(detail::iterable<Iterable>&&) -> agnostic_iterable<Iterable>;

template<typename Iterable>
struct iterable_adaptor<agnostic_iterable<Iterable>> : deduced_adaptor<detail::type_erasure_iterable_impl<typename Iterable::traits>>
{
    typedef deduced_adaptor<detail::type_erasure_iterable_impl<typename Iterable::traits>> base_t;

    using base_t::base_t;
};

template<typename Iterable>
struct iterable_adaptor<const agnostic_iterable<Iterable>> : deduced_adaptor<detail::type_erasure_iterable_impl<typename Iterable::traits>>
{
    typedef deduced_adaptor<detail::type_erasure_iterable_impl<typename Iterable::traits>> base_t;

    using base_t::base_t;
};


template<typename ... T>
using const_forward_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_type_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<T&>>,mpl::type_pack<forward_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag>>...>>>;

template<typename ... T>
using const_forward_value_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_value_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<const T&>>,mpl::type_pack<forward_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag>>...>>>;

template<typename ... T>
using forward_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_type_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<T&>,remove_action_tag,add_action_tag<T>>,mpl::type_pack<forward_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag>>...>>>;

template<typename ... T>
using forward_value_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_value_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<const T&>,remove_action_tag,add_action_tag<T>>,mpl::type_pack<forward_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag>>...>>>;

template<typename ... T>
using const_backward_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_type_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<T&>>,mpl::type_pack<backward_action_tag,agnostic_sink_action_tag<const T&>,end_action_tag>>...>>>;

template<typename ... T>
using const_backward_value_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_value_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<const T&>>,mpl::type_pack<backward_action_tag,agnostic_sink_action_tag<const T&>,end_action_tag>>...>>>;

template<typename ... T>
using backward_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_type_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<T&>,remove_action_tag,add_action_tag<T>>,mpl::type_pack<backward_action_tag,agnostic_sink_action_tag<const T&>,end_action_tag>>...>>>;

template<typename ... T>
using backward_value_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_value_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<const T&>,remove_action_tag,add_action_tag<T>>,mpl::type_pack<backward_action_tag,agnostic_sink_action_tag<const T&>,end_action_tag>>...>>>;

template<typename ... T>
using const_bidirectional_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_type_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<T&>>,mpl::type_pack<forward_action_tag,backward_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag,end_action_tag>>...>>>;

template<typename ... T>
using const_bidirectional_value_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_value_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<const T&>>,mpl::type_pack<forward_action_tag,backward_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag,end_action_tag>>...>>>;

template<typename ... T>
using bidirectional_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_type_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<T&>,remove_action_tag,add_action_tag<T>>,mpl::type_pack<forward_action_tag,backward_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag,end_action_tag>>...>>>;

template<typename ... T>
using bidirectional_value_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_value_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<const T&>,remove_action_tag,add_action_tag<T>>,mpl::type_pack<forward_action_tag,backward_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag,end_action_tag>>...>>>;

template<typename ... T>
using const_random_access_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_type_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<T&>>,mpl::type_pack<forward_action_tag,backward_action_tag,displace_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag,end_action_tag>>...>>>;

template<typename ... T>
using const_random_access_value_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_value_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<const T&>>,mpl::type_pack<forward_action_tag,backward_action_tag,displace_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag,end_action_tag>>...>>>;

template<typename ... T>
using random_access_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_type_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<T&>,remove_action_tag,add_action_tag<T>>,mpl::type_pack<forward_action_tag,backward_action_tag,displace_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag,end_action_tag>>...>>>;

template<typename ... T>
using random_access_value_iterable = detail::iterable<detail::type_erasure_iterable_impl<detail::intersection_iterable_traits<detail::iterable_by_value_adaptor<T,mpl::type_pack<agnostic_sink_action_tag<const T&>,remove_action_tag,add_action_tag<T>>,mpl::type_pack<forward_action_tag,backward_action_tag,displace_action_tag,agnostic_sink_action_tag<const T&>,begin_action_tag,end_action_tag>>...>>>;

}