//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_result.h"

namespace ddk
{

enum class IterableError
{
    NotExecuted,
    AlreadyExecuted,
    UserError
};
typedef error<IterableError> iterable_error;
template<typename T>
using iterable_result = result<T,iterable_error>;
template<typename T>
using iterable_payload = typename iterable_result<T>::payload_t;

}