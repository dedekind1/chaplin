//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable_action_tags.h"
#include "ddk_function_template_helper.h"
#include "ddk_tuple_template_helper.h"
#include "ddk_optional.h"
#include "ddk_iterable_action_tag_concepts.h"

namespace ddk
{

template<typename ActionTag, typename ... Adaptors>
inline auto apply_intersection_action(const ActionTag& i_action, Adaptors&& ... i_adaptors);

template<typename Sink, typename ... Adaptors>
inline auto apply_intersection_action(const sink_action_tag<Sink>&, Adaptors&& ... i_adaptors);

template<typename ... Adaptors>
inline auto apply_intersection_action(const size_action_tag&, Adaptors&& ... i_adaptors);

}

#include "ddk_intersection_iterable_action.inl"