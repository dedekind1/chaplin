//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_template_helper.h"
#include <type_traits>

#ifdef __cpp_concepts

#define TEMPLATE(...) \
	template<__VA_ARGS__>
#define REQUIRES(...) \
	requires( ddk::mpl::evaluate_and<__VA_ARGS__>() )
#define REQUIRED(...) \
	REQUIRES(__VA_ARGS__)

#else

#define TEMPLATE(...) \
	template<__VA_ARGS__
#define REQUIRES(...) \
	,typename = typename std::enable_if<ddk::mpl::evaluate_and<__VA_ARGS__>()>::type>
#define REQUIRED(...) \
	,typename>

#endif