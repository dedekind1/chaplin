
#include "ddk_reference_wrapper.h"
#include "ddk_forwarding_iterable_value_callable.h"
#include "ddk_exception_handler.h"

namespace ddk
{
namespace detail
{

template<typename T>
iteration_result<T>::iteration_result()
: m_result(IterableError::NotExecuted)
{
}
template<typename T>
typename iteration_result<T>::reference iteration_result<T>::operator*()
{
	return m_result.get();
}
template<typename T>
typename iteration_result<T>::const_reference iteration_result<T>::operator*() const
{
	return 	m_result.get();
}
template<typename T>
iterable_error iteration_result<T>::error() const
{
	return m_result.error();
}
template<typename T>
iteration_result<T>& iteration_result<T>::set_result(result_t i_result)
{
	m_result.dismiss();

	m_result = std::move(i_result);

	return *this;
}
template<typename T>
iterable_result<T>& iteration_result<T>::get_result()
{
	return m_result;
}
template<typename T>
const iterable_result<T>& iteration_result<T>::get_result() const
{
	return m_result;
}
template<typename T>
iterable_result<T> iteration_result<T>::extract_result()
{
	return std::move(m_result);
}
template<typename T>
void iteration_result<T>::dismiss()
{
	m_result.dismiss();
}

}


template<typename Iterable, typename Sink>
iterable_result<Sink> execute_iteration(iteration<Iterable,Sink>& i_iteration)
{
    return i_iteration.execute(iterable_default_action<Iterable>::default_action());
}

template<typename Iterable, typename Sink>
template<typename IIterable, typename SSink>
constexpr iteration<Iterable,Sink>::iteration(IIterable&& i_iterable, SSink&& i_sink)
: m_iterable(std::forward<IIterable>(i_iterable))
, m_sink(std::forward<SSink>(i_sink))
, m_executable(true)
{
}
template<typename Iterable, typename Sink>
constexpr iteration<Iterable,Sink>::iteration(iteration&& other)
: m_iterable(std::move(other.m_iterable))
, m_sink(std::move(other.m_sink))
, m_executable(true)
{
	other.m_executable = false;
}
template<typename Iterable, typename Sink>
iteration<Iterable,Sink>::~iteration()
{
	if(ddk::atomic_compare_exchange(m_executable,true,false))
	{
		_execute(iterable_default_action<Iterable>::default_action());

		dismiss();
	}
}
template<typename Iterable, typename Sink>
iteration<Iterable,Sink>* iteration<Iterable,Sink>::operator->()
{
	return this;
}
template<typename Iterable, typename Sink>
const iteration<Iterable,Sink>* iteration<Iterable,Sink>::operator->() const
{
	return this;
}
template<typename Iterable, typename Sink>
template<typename Action>
typename iteration<Iterable,Sink>::result_t iteration<Iterable,Sink>::execute(const Action& i_action)
{
	if(ddk::atomic_compare_exchange(m_executable,true,false))
	{
		_execute(i_action);
	}
	else
	{
		set_result(IterableError::AlreadyExecuted);
	}

	return extract_result();
}
template<typename Iterable, typename Sink>
template<typename T>
future<typename iteration<Iterable,Sink>::result_t> iteration<Iterable,Sink>::attach(T&& i_execContext)
{
	return ddk::async([this]()
	{
		return execute_iteration(*this);
	}) -> attach(std::forward<T>(i_execContext));
}
template<typename Iterable, typename Sink>
future<typename iteration<Iterable,Sink>::result_t> iteration<Iterable,Sink>::attach(const detail::this_thread_t&)
{
	return ddk::async([this]()
	{
		return execute_iteration(*this);
	});
}
template<typename Iterable, typename Sink>
iteration<Iterable,Sink>::operator bool() const
{
	if(ddk::atomic_compare_exchange(m_executable,true,false))
	{
		const_cast<iteration<Iterable,Sink>&>(*this)._execute(iterable_default_action<Iterable>::default_action());
	}

	return static_cast<bool>(get_result());
}
template<typename Iterable, typename Sink>
template<typename Action>
constexpr void iteration<Iterable,Sink>::_execute(const Action& i_action)
{
	typedef mpl::remove_qualifiers<Iterable> iterable_t;
	typedef typename iterable_t::traits traits;
	typedef typename traits::reference reference;

	if constexpr (noexcept(ddk::terse_eval(m_sink,std::declval<reference>())))
	{
		m_iterable.iterate_impl(action_sink{ i_action,std::move(m_sink) });

		set_result(success);
	}
	else
	{
		exception_handler::open_scope([&]()
		{
			m_iterable.iterate_impl(action_sink{ i_action,std::move(m_sink) });
		}).on_error([this](result_t i_res) mutable
		{
			set_result(std::move(i_res));
		});
	}
}

}
