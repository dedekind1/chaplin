//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable_action_tags.h"
#include "ddk_iterable_action_tags_template_helper.h"
#include "ddk_function_arguments_template_helper.h"
#include <type_traits>

namespace ddk
{
namespace detail
{

template<typename Value,typename Reference,typename ConstReference>
struct type_traits
{
    typedef Value value_type;
    typedef Reference reference;
    typedef ConstReference const_reference;
    typedef long long difference_type;
};

template<typename Traits>
using const_type_traits = type_traits<const typename Traits::value_type,const typename Traits::const_reference,const typename Traits::const_reference>;

template<typename T>
using by_type_traits = type_traits<T,T&,const T&>;

template<typename T>
using by_value_traits = typename mpl::which_type<std::is_reference<T>::value,type_traits<T,T,T>,type_traits<const T,const T,const T>>::type;

template<typename Adaptor>
using by_adaptor_traits = type_traits<typename Adaptor::value_type,typename Adaptor::reference,typename Adaptor::const_reference>;

template<typename Traits,typename TTraits>
using map_type_traits = type_traits<typename TTraits::value_type,typename TTraits::reference,typename TTraits::const_reference>;

template<typename Traits,typename TTraits>
using reduce_type_traits = type_traits<typename Traits::value_type,typename Traits::reference,typename Traits::const_reference>;

template<typename Value, typename Reference, typename ConstReference, typename Tags, typename ConstTags>
struct iterable_by_type_traits : type_traits<Value,Reference,ConstReference>
{
    using typename type_traits<Value,Reference,ConstReference>::value_type;
    using typename type_traits<Value,Reference,ConstReference>::reference;
    using typename type_traits<Value,Reference,ConstReference>::const_reference;
    typedef mpl::type_pack_union<Tags,ConstTags> tags_t;
    typedef ConstTags const_tags_t;
};

template<typename T, typename Tags, typename ConstTags>
using iterable_by_type_adaptor = iterable_by_type_traits<T,T&,const T&,Tags,ConstTags>;

template<typename T,typename Tags, typename ConstTags>
using iterable_by_value_adaptor = iterable_by_type_traits<T,T,T,Tags,ConstTags>;

template<typename Traits>
using iterable_traits = iterable_by_type_traits<typename Traits::value_type,typename Traits::reference,typename Traits::const_reference,typename Traits::tags_t,typename Traits::const_tags_t>;

template<typename Traits>
using const_iterable_traits = iterable_by_type_traits<const typename Traits::value_type,const typename Traits::const_reference,const typename Traits::const_reference,typename Traits::const_tags_t,typename Traits::const_tags_t>;

template<typename Traits>
using iterable_value_traits = iterable_by_type_traits<typename Traits::value_type,typename Traits::value_type,typename Traits::value_type,typename Traits::const_tags_t,typename Traits::const_tags_t>;

template<typename Adaptor, typename Tags = typename Adaptor::tags_t, typename ConstTags = typename Adaptor::const_tags_t>
using iterable_adaptor_traits = iterable_by_type_traits<typename Adaptor::value_type,typename Adaptor::reference,typename Adaptor::const_reference,Tags,ConstTags>;

template<typename Adaptor>
using adaptor_traits = typename mpl::which_type<mpl::is_const<Adaptor>,typename mpl::remove_qualifiers<Adaptor>::const_traits,typename mpl::remove_qualifiers<Adaptor>::traits>::type;

template<typename Adaptor>
using adaptor_tags = typename mpl::which_type<mpl::is_const<Adaptor>,typename mpl::remove_qualifiers<Adaptor>::const_tags_t,typename mpl::remove_qualifiers<Adaptor>::tags_t>::type;

template<typename Transform, typename Traits>
class transformed_iterable_traits_resolver
{
private:
	typedef decltype(ddk::terse_eval(std::declval<Transform>(),std::declval<typename Traits::reference>())) return_t;
    typedef typename Traits::tags_t::template drop_if<mpl::is_not_type<ddk::agnostic_sink_action_tag<typename Traits::reference>>::template type>::type simplified_tags;

public:
    typedef mpl::remove_qualifiers<return_t> value_type;
    typedef return_t reference;
    typedef typename mpl::add_const<reference> const_reference;
	typedef typename mpl::action_tags_retrait<Traits,by_value_traits<reference>,reduce_type_traits,simplified_tags>::type tags_t;
	typedef typename mpl::action_tags_retrait<Traits,by_value_traits<const_reference>,reduce_type_traits,typename Traits::const_tags_t>::type const_tags_t;
};

template<typename Transform, typename Traits>
using transformed_iterable_traits = iterable_traits<transformed_iterable_traits_resolver<Transform,Traits>>;

template<typename ... Traits>
struct union_iterable_traits_resolver
{
private:
    typedef typename mpl::common_qualifiers<typename Traits::value_type...>::template apply<typename std::common_type<typename Traits::value_type...>::type> common_value_type;
    typedef typename mpl::common_qualifiers<typename Traits::reference...>::template apply<typename std::common_type<typename Traits::reference...>::type> common_reference;
    typedef typename mpl::common_qualifiers<typename Traits::const_reference...>::template apply<typename std::common_type<typename Traits::const_reference...>::type> common_const_reference;

    using union_type_traits = type_traits<common_value_type,common_reference,common_const_reference>;
    using union_iterable_traits = iterable_adaptor_traits<union_type_traits,
                                                            mpl::type_pack_intersection<typename mpl::action_tags_retrait<Traits,union_type_traits,map_type_traits,typename Traits::tags_t>::type...>,
                                                            mpl::type_pack_intersection<typename mpl::action_tags_retrait<Traits,union_type_traits,map_type_traits,typename Traits::const_tags_t>::type...>>;
public:
    typedef iterable_traits<union_iterable_traits> traits;
};

template<typename ... Traits>
using union_iterable_traits = typename union_iterable_traits_resolver<Traits ...>::traits;

template<typename ...>
struct intersection_iterable_traits_resolver;

template<typename Traits>
struct intersection_iterable_traits_resolver<Traits>
{
    typedef Traits traits;
};

template<typename ... Traits>
struct intersection_iterable_traits_resolver
{
private:
    using intersection_type_traits = type_traits<prod_argument<typename Traits::value_type...>,
                                                    prod_argument<typename Traits::reference...>,
                                                    prod_argument<typename Traits::const_reference...>>;
    using intersection_iterable_traits = iterable_adaptor_traits<intersection_type_traits,
                                                                mpl::type_pack_intersection<typename mpl::action_tags_retrait<Traits,intersection_type_traits,reduce_type_traits,typename Traits::tags_t>::type...>,
                                                                mpl::type_pack_intersection<typename mpl::action_tags_retrait<Traits,intersection_type_traits,reduce_type_traits,typename Traits::const_tags_t>::type...>>;

public:
    typedef iterable_traits<intersection_iterable_traits> traits;
};

template<typename ... Traits>
using intersection_iterable_traits = typename intersection_iterable_traits_resolver<Traits...>::traits;

template<typename Traits>
struct ordered_traits_resolver
{
    typedef typename Traits::value_type value_type;
    typedef typename Traits::reference reference;
    typedef typename Traits::const_reference const_reference;
    typedef typename mpl::intersect_type_packs<mpl::static_type_list<mpl::iterable_action_tag>,typename Traits::tags_t>::type tags_t;
    typedef typename mpl::intersect_type_packs<mpl::static_type_list<mpl::iterable_action_tag>,typename Traits::const_tags_t>::type const_tags_t;
};

template<typename Traits>
using ordered_traits = iterable_by_type_traits<typename ordered_traits_resolver<Traits>::value_type,
                                                typename ordered_traits_resolver<Traits>::reference,
                                                typename ordered_traits_resolver<Traits>::const_reference,
                                                typename ordered_traits_resolver<Traits>::tags_t,
                                                typename ordered_traits_resolver<Traits>::const_tags_t>;

}
}
