//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_tuple_impl.h"
#include "ddk_variant_impl.h"

namespace ddk
{

template<typename ... Types>
class prod_argument : public tuple<Types...>
{
public:
    using tuple<Types...>::tuple;
};

template<typename Arg, typename ... Args>
inline prod_argument<Arg,Args...> make_function_arguments(Arg&& i_arg, Args&& ... i_args);

template<typename ... Types>
class sum_argument : public variant<Types...>
{
public:
    using variant<Types...>::variant;
};

}

#include "ddk_function_arguments.inl"