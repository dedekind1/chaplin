
namespace ddk
{

template<typename T, typename ... TT>
TEMPLATE(typename Arg, typename ... Args)
REQUIRED(is_constructible<T,Arg>,is_constructible<TT,Args>...)
constexpr iterable_pipeline<T,TT...>::iterable_pipeline(const Arg& i_arg, const Args& ... i_args)
: tuple<T,TT...>(i_arg,i_args...)
{
}
template<typename T, typename ... TT>
template<typename Functor>
constexpr auto iterable_pipeline<T,TT...>::operator+(const Functor& i_functor) const
{
    typedef typename mpl::make_sequence<0,mpl::num_types<T,TT...>>::type seq_t;

    return append(seq_t{},i_functor);
}
template<typename T, typename ... TT>
template<size_t ... Indexs, typename Iterable>
constexpr auto iterable_pipeline<T,TT...>::resolve(const mpl::sequence<Indexs...>&, Iterable&& i_iterable)
{
    return (this->template get<Indexs>() <<= ...) <<= ( this->template get<0>() <<= std::forward<Iterable>(i_iterable));
}
template<typename T, typename ... TT>
template<size_t ... Indexs, typename Iterable>
constexpr auto iterable_pipeline<T,TT...>::resolve(const mpl::sequence<Indexs...>&, Iterable&& i_iterable) const
{
    return (this->template get<Indexs>() <<= ...) <<= ( this->template get<0>() <<= std::forward<Iterable>(i_iterable));
}
template<typename T, typename ... TT>
template<size_t ... Indexs, typename ... Iterable>
constexpr auto iterable_pipeline<T,TT...>::append(const mpl::sequence<Indexs...>&, Iterable&& ... i_iterables) const
{
    return make_iterable_pipeline(this->template get<Indexs>()...,std::forward<Iterable>(i_iterables)...);
}

template<typename ... Args>
auto constexpr make_iterable_pipeline(const Args& ... i_args)
{
    return iterable_pipeline{i_args...};
}

}