//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable_action_defs.h"

namespace ddk
{

template<typename T>
inline constexpr bool is_iterable_action = std::is_base_of<ddk::action_base,ddk::mpl::remove_qualifiers<T>>::value;

template<typename T, typename TT>
inline constexpr bool action_supported = action_tags_supported<T,typename ddk::mpl::remove_qualifiers<TT>::tags_t>;

template<typename T, typename TT>
inline constexpr bool action_not_supported = action_tags_not_supported<T,typename ddk::mpl::remove_qualifiers<TT>::tags_t>;

template<typename T, typename TT>
inline constexpr bool transformed_action_supported = action_tags_supported<T,typename ddk::mpl::aqcuire_callable_return_type<TT>::type::tags_t>;

}