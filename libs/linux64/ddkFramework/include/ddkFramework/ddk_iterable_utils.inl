
#include "ddk_iteration.h"
#include "ddk_iterable_impl.h"
#include "ddk_union_iterable_impl.h"
#include "ddk_intersection_iterable_impl.h"
#include "ddk_async.h"
#include "ddk_forwarding_iterable_value_callable.h"
#include "ddk_one_to_n_action_adapter.h"
#include "ddk_callable.h"
#include "ddk_reference_wrapper.h"

namespace ddk
{

template<typename Traits,typename Iterable,typename Allocator>
auto make_iterable(Iterable&& i_iterable,const Allocator& i_allocator)
{
	typedef mpl::remove_qualifiers<Iterable> iterable_t;
	typedef typename iterable_t::traits from_traits;
	typedef Traits to_traits;

	if constexpr (is_same_class<from_traits,to_traits>)
	{
		return make_distributed_reference<detail::view_iterable<to_traits,iterable_t>>(i_allocator,std::forward<Iterable>(i_iterable));
	}
	else
	{
		typedef detail::transformed_iterable_impl<to_traits,from_traits,detail::iterable<iterable_t>,detail::traits_conversion_callable<from_traits,to_traits>> transformed_iterable;

		detail::traits_conversion_callable<from_traits,to_traits> kk;

		return make_distributed_reference<detail::view_iterable<to_traits,transformed_iterable>>(i_allocator,detail::iterable{ std::forward<Iterable>(i_iterable)},detail::traits_conversion_callable<from_traits,to_traits>());
	}
}
template<typename Traits,typename Iterable>
auto make_iterable(Iterable&& i_iterable)
{
	return make_iterable<Traits>(std::forward<Iterable>(i_iterable),g_system_allocator);
}

TEMPLATE(typename Iterable)
REQUIRED(is_iterable_type<Iterable>)
Iterable&& deduce_iterable(Iterable&& i_iterable)
{
	return std::forward<Iterable>(i_iterable);
}

TEMPLATE(typename Container)
REQUIRED(is_not_iterable_type<Container>,is_iterable_deducible_type<Container>)
resolved_iterable<Container> deduce_iterable(Container&& i_iterable)
{
	return { i_iterable };
}

}

TEMPLATE(typename Function,typename Iterable)
REQUIRED(ddk::is_iterable_deducible_type<Iterable>)
auto operator<<=(const ddk::detail::iterable_transform<Function>& i_lhs, Iterable&& i_rhs)
{
	typedef ddk::resolved_iterable<Iterable> iterable_t;
	typedef typename iterable_t::traits traits;
	typedef typename traits::tags_t tags_t;
	typedef typename traits::const_tags_t const_tags_t;
	typedef decltype(ddk::terse_eval(std::declval<Function>(),std::declval<typename traits::reference>())) return_t;

	if constexpr (ddk::is_returnable<return_t,typename traits::reference>)
	{
		typedef ddk::detail::transformed_iterable_impl<traits,traits,iterable_t,Function> transformed_iterable;

		using namespace ddk;

		return ddk::detail::iterable(transformed_iterable(deduce_iterable(std::forward<Iterable>(i_rhs)),i_lhs));
	}
	else
	{
		typedef ddk::detail::transformed_iterable_impl<ddk::detail::transformed_iterable_traits<Function,traits>,traits,iterable_t,Function> transformed_iterable;

		using namespace ddk;

		return ddk::detail::iterable(transformed_iterable(deduce_iterable(std::forward<Iterable>(i_rhs)),i_lhs));
	}
}
TEMPLATE(typename Function,typename Iterable)
REQUIRED(ddk::is_iterable_deducible_type<Iterable>)
auto operator<<=(const ddk::detail::iterable_filter<Function>& i_lhs,Iterable&& i_rhs)
{
	typedef ddk::resolved_iterable<Iterable> iterable_t;
	typedef ddk::detail::filtered_iterable_impl<iterable_t,Function> filtered_iterable;

	using namespace ddk;

	return ddk::detail::iterable(filtered_iterable(deduce_iterable(std::forward<Iterable>(i_rhs)),i_lhs.get_filter()));
}
TEMPLATE(typename T,typename Iterable)
REQUIRED(ddk::is_iterable_deducible_type<Iterable>)
auto operator<<=(const ddk::detail::iterable_order<T>& i_lhs, Iterable&& i_rhs)
{
	typedef ddk::resolved_iterable<Iterable> iterable_t;
	typedef ddk::detail::ordered_iterable_impl<iterable_t,T> ordered_iterable;

	using namespace ddk;

	return ddk::detail::iterable(ordered_iterable(deduce_iterable(std::forward<Iterable>(i_rhs)),i_lhs.order()));
}
TEMPLATE(typename Function,typename Iterable)
REQUIRED(ddk::is_iterable_deducible_type<Iterable>)
auto operator<<=(const ddk::detail::iterable_constrain<Function>& i_lhs,Iterable&& i_rhs)
{
	typedef ddk::resolved_iterable<Iterable> iterable_t;
	typedef ddk::detail::constrained_iterable_impl<iterable_t,Function> constrained_iterable;

	using namespace ddk;

	return ddk::detail::iterable(constrained_iterable(deduce_iterable(std::forward<Iterable>(i_rhs)),i_lhs.get_constrain()));
}
TEMPLATE(typename Function,typename Iterable)
REQUIRED(ddk::is_iterable_deducible_type<Iterable>)
auto operator<<=(const Function& i_lhs,Iterable&& i_rhs)
{
	using namespace ddk;

	return ddk::iteration{ deduce_iterable(std::forward<Iterable>(i_rhs)),i_lhs };
}

namespace ddk
{

TEMPLATE(typename Iterable)
REQUIRED(is_iterable_deducible_type<Iterable>)
auto concat(Iterable&& i_iterable)
{
	return deduce_iterable(std::forward<Iterable>(i_iterable));
}
TEMPLATE(typename ... Iterables)
REQUIRED(is_iterable_deducible_type<Iterables>...)
auto concat(Iterables&& ... i_iterables)
{
    static_assert(mpl::num_types<Iterables...> != 0, "You shall provider more than 0 iterables");

	return detail::iterable{ detail::union_iterable_impl(deduce_iterable(std::forward<Iterables>(i_iterables)) ...) };
}

TEMPLATE(typename Iterable)
REQUIRED(is_iterable_deducible_type<Iterable>)
auto fusion(Iterable&& i_iterable)
{
	return deduce_iterable(std::forward<Iterable>(i_iterable));
}
TEMPLATE(typename ... Iterables)
REQUIRED(is_iterable_deducible_type<Iterables>...)
auto fusion(Iterables&& ... i_iterables)
{
    static_assert(mpl::num_types<Iterables...> != 0, "You shall provider more than 0 iterables");

	return detail::iterable{ detail::intersection_iterable_impl(deduce_iterable(std::forward<Iterables>(i_iterables)) ...)};
}
//TEMPLATE(typename ... Iterables)
//REQUIRED(IS_ITERABLE(Iterables)...)
//detail::iterable<detail::intersection_iterable_traits<resolved_iterable_traits<Iterables>...>> enumerate(Iterables&& ... i_iterables)
//{
//	static_assert(mpl::num_types<Iterables...> != 0,"You shall provider more than 0 iterables");
//
//	typedef detail::iterable<detail::intersection_iterable_traits<resolved_iterable_traits<Iterables>...>> ret_type;
//
//	return ret_type{ detail::make_iterable_impl<detail::intersection_iterable_impl<iter::one_to_n_enumerate_action_adapter,detail::iterable<resolved_iterable_traits<Iterables>> ...>>(iter::one_to_n_enumerate_action_adapter(std::forward<Iterables>(i_iterables)...),deduce_iterable(std::forward<Iterables>(i_iterables))...) };
//}
//TEMPLATE(typename Adapter,typename ... Iterables)
//REQUIRED(IS_ITERABLE(Iterables)...)
//detail::iterable<detail::intersection_iterable_traits<resolved_iterable_traits<Iterables>...>> combine(Iterables&& ... i_iterables)
//{
//	static_assert(mpl::num_types<Iterables...> != 0,"You shall provider more than 0 iterables");
//
//	typedef detail::iterable<detail::intersection_iterable_traits<resolved_iterable_traits<Iterables>...>> ret_type;
//
//	return ret_type{ detail::make_iterable_impl<detail::intersection_iterable_impl<Adapter,detail::iterable<resolved_iterable_traits<Iterables>> ...>>(Adapter{},deduce_iterable(std::forward<Iterables>(i_iterables))...) };
//}
//TEMPLATE(typename Adapter,typename ... Iterables)
//REQUIRED(IS_ITERABLE(Iterables)...)
//detail::iterable<detail::intersection_iterable_traits<resolved_iterable_traits<Iterables>...>> combine(const Adapter& i_adapter,Iterables&& ... i_iterables)
//{
//	static_assert(mpl::num_types<Iterables...> != 0,"You shall provider more than 0 iterables");
//
//	typedef detail::iterable<detail::intersection_iterable_traits<resolved_iterable_traits<Iterables>...>> ret_type;
//
//	return ret_type{ detail::make_iterable_impl<detail::intersection_iterable_impl<Adapter,detail::iterable<resolved_iterable_traits<Iterables>> ...>>(i_adapter,deduce_iterable(std::forward<Iterables>(i_iterables))...) };
//}

}
