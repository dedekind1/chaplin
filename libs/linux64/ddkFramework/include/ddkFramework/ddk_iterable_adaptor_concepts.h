//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable_traits_concepts.h"

namespace ddk
{
namespace concepts
{

template<typename T>
class is_adaptor_impl
{
private:
    template<typename TT>
    static std::true_type resolve(const iterable_adaptor<TT>&);
    static std::false_type resolve(...);

public:
    static const bool value = decltype(resolve(std::declval<T>()))::value;
};

template<typename T>
struct is_non_const_adaptor_impl
{
private:
    template<typename TT>
    static std::true_type resolve(TT&, typename std::add_pointer<decltype(std::declval<TT>().erase_value())>::type);
    template<typename TT>
    static std::false_type resolve(TT&, ...);

public:
    static const bool value = decltype(resolve(std::declval<T&>(),nullptr))::value;
};

template<typename T>
struct is_forward_adaptor_impl
{
private:
    template<typename TT>
    static std::true_type resolve(TT&, typename std::add_pointer<decltype(std::declval<TT>().forward_next_value())>::type);
    template<typename TT>
    static std::false_type resolve(TT&, ...);

public:
    static const bool value = decltype(resolve(std::declval<T&>(),nullptr))::value;
};

template<typename T>
struct is_backward_adaptor_impl
{
private:
    template<typename TT>
    static std::true_type resolve(TT&, typename std::add_pointer<decltype(std::declval<TT>().forward_prev_value())>::type);
    template<typename TT>
    static std::false_type resolve(TT&, ...);

public:
    static const bool value = decltype(resolve(std::declval<T&>(),nullptr))::value;
};

template<typename T>
struct is_random_access_adaptor_impl
{
private:
    template<typename TT>
    static std::true_type resolve(TT&, typename std::add_pointer<decltype(std::declval<TT>().forward_shift_value(0))>::type);
    template<typename TT>
    static std::false_type resolve(TT&, ...);

public:
    static const bool value = decltype(resolve(std::declval<T&>(),nullptr))::value;
};

template<typename T>
struct is_sized_adaptor_impl
{
private:
    template<typename TT, typename = decltype(&TT::size)>
    static std::true_type resolve(const TT&);
    static std::false_type resolve(...);

public:
    static const bool value = decltype(resolve(std::declval<T>()))::value;
};

template<typename T>
struct is_dimensionable_adaptor_impl
{
private:
    template<typename TT>
    static std::true_type resolve(const TT&,const typename TT::dimension_t*);
    template<typename TT>
    static std::false_type resolve(const TT&,...);

public:
    static const bool value = decltype(resolve(std::declval<T>(),nullptr))::value;
};

}

template<typename T>
inline constexpr bool is_iterable_adaptor = concepts::is_adaptor_impl<mpl::remove_qualifiers<T>>::value;

template<typename T>
inline constexpr bool is_non_iterable_adaptor = concepts::is_adaptor_impl<mpl::remove_qualifiers<T>>::value == false;

template<typename T>
inline constexpr bool is_const_iterable_adaptor = is_iterable_adaptor<T> && concepts::is_non_const_adaptor_impl<T>::value == false;

template<typename T>
inline constexpr bool is_non_const_iterable_adaptor = is_iterable_adaptor<T> && concepts::is_non_const_adaptor_impl<T>::value;

template<typename T>
inline constexpr bool is_forward_iterable_adaptor = is_iterable_adaptor<T> && concepts::is_forward_adaptor_impl<T>::value;

template<typename T>
inline constexpr bool is_const_forward_iterable_adaptor = is_const_iterable_adaptor<T> && is_forward_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_non_const_forward_iterable_adaptor = is_non_const_iterable_adaptor<T> && is_forward_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_backward_iterable_adaptor = is_iterable_adaptor<T> && concepts::is_backward_adaptor_impl<T>::value;

template<typename T>
inline constexpr bool is_bidirectional_iterable_adaptor = is_forward_iterable_adaptor<T> && is_backward_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_const_bidirectional_iterable_adaptor = is_const_iterable_adaptor<T> && is_bidirectional_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_non_const_bidirectional_iterable_adaptor = is_non_const_iterable_adaptor<T> && is_bidirectional_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_random_access_iterable_adaptor = is_bidirectional_iterable_adaptor<T> && concepts::is_random_access_adaptor_impl<T>::value;

template<typename T>
inline constexpr bool is_const_random_access_iterable_adaptor = is_const_iterable_adaptor<T> && is_random_access_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_non_const_random_access_iterable_adaptor = is_non_const_iterable_adaptor<T> && is_random_access_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_exclusive_forward_iterable_adaptor = is_forward_iterable_adaptor<T> && (is_backward_iterable_adaptor<T> == false);

template<typename T>
inline constexpr bool is_const_exclusive_forward_iterable_adaptor = is_const_iterable_adaptor<T> && is_exclusive_forward_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_non_const_exclusive_forward_iterable_adaptor = is_non_const_iterable_adaptor<T> && is_exclusive_forward_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_exclusive_bidirectional_iterable_adaptor = is_forward_iterable_adaptor<T> && is_backward_iterable_adaptor<T> && (is_random_access_iterable_adaptor<T> == false);

template<typename T>
inline constexpr bool is_const_exclusive_bidirectional_iterable_adaptor = is_const_iterable_adaptor<T> && is_exclusive_bidirectional_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_non_const_exclusive_bidirectional_iterable_adaptor = is_non_const_iterable_adaptor<T> && is_exclusive_bidirectional_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_exclusive_random_access_iterable_adaptor = is_random_access_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_const_exclusive_random_access_iterable_adaptor = is_const_iterable_adaptor<T> && is_exclusive_random_access_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_non_const_exclusive_random_access_iterable_adaptor = is_non_const_iterable_adaptor<T> && is_exclusive_random_access_iterable_adaptor<T>;

template<typename T>
inline constexpr bool is_sized_iterable_adaptor = is_iterable_adaptor<T> && concepts::is_sized_adaptor_impl<mpl::remove_qualifiers<T>>::value;

template<typename T>
inline constexpr bool is_dimensionable_iterable_adaptor = is_iterable_adaptor<T> && concepts::is_dimensionable_adaptor_impl<mpl::remove_qualifiers<T>>::value;
    
}
