//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable.h"
#include "ddk_type_erasure_iterable_impl.h"

namespace ddk
{
namespace detail
{

template<typename Traits, typename Iterable>
class view_iterable : public iterable<Iterable>, public iterable_interface<Traits>
{
    typedef iterable_interface<Traits> base_t;

public:
    using typename base_t::traits;
    using typename base_t::const_traits;

    TEMPLATE(typename ... Args)
    REQUIRES(is_constructible<iterable<Iterable>,Args...>)
    view_iterable(Args&& ... i_args);

private:
    using typename base_t::action;
    using typename base_t::const_action;

    void iterate(const action& i_initialAction) override;
    void iterate(const const_action& i_initialAction) const override;
    iterable_adaptor<type_erasure_iterable_impl<Traits>> deduce_owned_adaptor() override;
    iterable_adaptor<const type_erasure_iterable_impl<Traits>> deduce_owned_adaptor() const override;
};

}
}

#include "ddk_view_iterable.inl"