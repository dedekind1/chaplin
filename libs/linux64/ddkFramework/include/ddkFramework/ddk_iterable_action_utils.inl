
namespace ddk
{

TEMPLATE(typename Action,typename Iterable)
REQUIRED(is_iterable_action<Action>,is_iterable_deducible_type<Iterable>)
void operator>>=(Action&& i_action,Iterable&& i_iterable)
{
	ddk::deduce_iterable(std::forward<Iterable>(i_iterable)).iterate_impl(std::forward<Action>(i_action));
}

}