#pragma once

#include "ddk_shared_reference_wrapper.h"

namespace ddk
{
namespace detail
{

template<typename Traits>
class iterable_interface_base
{
public:
    typedef Traits traits;

    virtual ~iterable_interface_base() = default;
};

template<typename Traits>
using iterable_base_dist_ref = distributed_reference_wrapper<iterable_interface_base<Traits>>;
template<typename Traits>
using iterable_base_const_dist_ref = distributed_reference_wrapper<const iterable_interface_base<Traits>>;
template<typename Traits>
using iterable_base_dist_ptr = distributed_pointer_wrapper<iterable_interface_base<Traits>>;
template<typename Traits>
using iterable_base_const_dist_ptr = distributed_pointer_wrapper<const iterable_interface_base<Traits>>;

}
}