#pragma once

#include <type_traits>

namespace ddk
{
namespace concepts
{

template<typename Order, typename Adaptor, typename ActionTag>
struct may_order_handle_action_at_adaptor
{
private:
    template<typename AAdaptor, typename AActionTag, typename T = decltype(std::declval<Order>().template perform_action(std::declval<AAdaptor&>(),std::declval<AActionTag>()))>
    static std::true_type resolve(AAdaptor&& i_adaptor, AActionTag&& i_action, T* = nullptr);
    static std::false_type resolve(...);

public:
    static const bool value =  decltype(resolve(std::declval<Adaptor&>(),std::declval<ActionTag>()))::value;
};

}

template<typename Order, typename Adaptor,typename ActionTag>
constexpr inline bool may_order_can_handle_action = concepts::may_order_handle_action_at_adaptor<Order,Adaptor,ActionTag>::value;

}