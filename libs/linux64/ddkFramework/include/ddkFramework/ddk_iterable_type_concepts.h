//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

namespace ddk
{

template<typename T>
inline constexpr bool is_iterable_deducible_type = (iterable_adaptor<resolved_iterable<T>>::tags_t::empty() == false ||
                                                    iterable_adaptor<resolved_iterable<T>>::const_tags_t::empty() == false);

}