
namespace ddk
{
namespace detail
{

template<typename Reference,typename Transform,typename ActionTag>
iterable_transformed_action<Reference,Transform,ActionTag>::iterable_transformed_action(Transform& i_transform,ActionTag i_actionTag)
: m_transform(i_transform)
, m_actionTag(std::forward<ActionTag>(i_actionTag))
{
}
template<typename Reference,typename Transform,typename ActionTag>
auto iterable_transformed_action<Reference,Transform,ActionTag>::operator*()
{
	return std::forward<ActionTag>(m_actionTag);
}
template<typename Reference,typename Transform,typename ActionTag>
template<typename T>
constexpr Reference iterable_transformed_action<Reference,Transform,ActionTag>::operator()(T&& i_value)
{
	return  ddk::terse_eval(m_transform,std::forward<T>(i_value));
}

template<typename Reference,typename Transform,typename Sink>
template<typename SSink>
iterable_transformed_action<Reference,Transform,sink_action_tag<Sink>>::iterable_transformed_action(Transform& i_transform, SSink&& i_actionTag)
: m_transform(i_transform)
, m_actionTag(std::forward<SSink>(i_actionTag))
{
}
template<typename Reference,typename Transform,typename Sink>
iterable_transformed_action<Reference,Transform,sink_action_tag<Sink>>::~iterable_transformed_action()
{
	m_cache.template destroy<Reference>();
}
template<typename Reference,typename Transform,typename Sink>
auto iterable_transformed_action<Reference,Transform,sink_action_tag<Sink>>::operator*()
{
	return sink_action_tag{ [this](auto&& i_value) mutable
	{
		//transport value into outer result
		m_cache.template construct<Reference>(m_actionTag(ddk::terse_eval(m_transform,std::forward<decltype(i_value)>(i_value))));
	} };
}
template<typename Reference,typename Transform,typename Sink>
template<typename T>
constexpr Reference iterable_transformed_action<Reference,Transform,sink_action_tag<Sink>>::operator()(T&& i_args)
{
	return  m_cache.template get<Reference>();
}

template<typename Transform>
TEMPLATE(typename TTransform)
REQUIRED(is_constructible<Transform,TTransform>)
iterable_transform<Transform>::iterable_transform(TTransform&& i_transform)
: m_transform(std::forward<TTransform>(i_transform))
{
}
template<typename Transform>
template<typename Reference,typename ActionTag>
auto iterable_transform<Transform>::map_action(ActionTag&& i_action)
{
	return iterable_transformed_action<Reference,Transform,ActionTag>{ m_transform,std::forward<ActionTag>(i_action) };
}
template<typename Transform>
template<typename Reference,typename ActionTag>
auto iterable_transform<Transform>::map_action(ActionTag&& i_action) const
{
	return iterable_transformed_action<Reference,const Transform,ActionTag>{ m_transform,std::forward<ActionTag>(i_action) };
}

template<typename FromTraits,typename ToTraits>
typename traits_conversion_callable<FromTraits,ToTraits>::to_reference traits_conversion_callable<FromTraits,ToTraits>::operator()(from_reference i_value) const
{
	return i_value;
}
template<typename FromTraits,typename ToTraits>
TEMPLATE(typename T)
REQUIRED(is_convertible<T,typename ToTraits::const_reference>)
typename traits_conversion_callable<FromTraits,ToTraits>::to_const_reference traits_conversion_callable<FromTraits,ToTraits>::operator()(T&& i_value, ...) const
{
	return i_value;
}

template<typename PublicTraits, typename PrivateTraits, typename Iterable, typename Transform>
TEMPLATE(typename IIterable)
REQUIRED(is_constructible<Iterable,IIterable>)
transformed_iterable_impl<PublicTraits,PrivateTraits,Iterable,Transform>::transformed_iterable_impl(IIterable&& i_iterable, const detail::iterable_transform<Transform>& i_transform)
: base_t(i_iterable,i_transform)
{
}
template<typename PublicTraits, typename PrivateTraits, typename Iterable,typename Transform>
TEMPLATE(typename Action)
REQUIRED(action_supported<PublicTraits,Action>)
void transformed_iterable_impl<PublicTraits,PrivateTraits,Iterable,Transform>::iterate_impl(Action&& i_initialAction)
{
	this->loop(std::forward<Action>(i_initialAction));
}
template<typename PublicTraits, typename PrivateTraits, typename Iterable,typename Transform>
TEMPLATE(typename Action)
REQUIRED(action_supported<detail::const_iterable_traits<PublicTraits>,Action>)
void transformed_iterable_impl<PublicTraits,PrivateTraits,Iterable,Transform>::iterate_impl(Action&& i_initialAction) const
{
	this->loop(std::forward<Action>(i_initialAction));
}

}

template<typename PublicTraits,typename PrivateTraits,typename Iterable,typename Transform>
iterable_adaptor<detail::transformed_iterable_impl<PublicTraits,PrivateTraits,Iterable,Transform>>::iterable_adaptor(Iterable& i_iterable, const detail::iterable_transform<Transform>& i_transform)
: m_adaptor(deduce_adaptor(i_iterable))
, m_transform(i_transform)
{
}
template<typename PublicTraits,typename PrivateTraits,typename Iterable,typename Transform>
TEMPLATE(typename Adaptor, typename ActionTag)
REQUIRED(action_tags_supported<Adaptor,ActionTag>)
constexpr auto iterable_adaptor<detail::transformed_iterable_impl<PublicTraits,PrivateTraits,Iterable,Transform>>::perform_action(Adaptor&& i_adaptor, ActionTag&& i_actionTag)
{
	if constexpr (is_adaptor_representable_by_action<Adaptor,ActionTag>)
	{
		typedef iterable_action_tag_result<detail::adaptor_traits<Adaptor>,ActionTag> transformed_result;

		auto mappedAction = i_adaptor.m_transform.template map_action<typename detail::adaptor_traits<Adaptor>::reference>(std::forward<ActionTag>(i_actionTag));
		if (auto actionRes = deduced_adaptor_t::perform_action(i_adaptor.m_adaptor,*mappedAction))
		{
			return make_result<transformed_result>(mappedAction(actionRes.get()));
		}
		else
		{
			return make_error<transformed_result>(std::forward<ActionTag>(i_actionTag));
		}
	}
	else
	{
		return deduced_adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor).m_adaptor,std::forward<ActionTag>(i_actionTag));
	}
}

template<typename PublicTraits,typename PrivateTraits,typename Iterable,typename Transform>
iterable_adaptor<const detail::transformed_iterable_impl<PublicTraits,PrivateTraits,Iterable,Transform>>::iterable_adaptor(const Iterable& i_iterable,const detail::iterable_transform<Transform>& i_transform)
: m_adaptor(deduce_adaptor(i_iterable))
, m_transform(i_transform)
{
}
template<typename PublicTraits,typename PrivateTraits,typename Iterable,typename Transform>
TEMPLATE(typename Adaptor, typename ActionTag)
REQUIRED(action_tags_supported<Adaptor,ActionTag>)
constexpr auto iterable_adaptor<const detail::transformed_iterable_impl<PublicTraits,PrivateTraits,Iterable,Transform>>::perform_action(Adaptor&& i_adaptor, ActionTag&& i_actionTag)
{
	if constexpr (is_adaptor_representable_by_action<Adaptor,ActionTag>)
	{
		typedef iterable_action_tag_result<traits,ActionTag> transformed_result;

		auto mappedAction = i_adaptor.m_transform.template map_action<typename detail::adaptor_traits<Adaptor>::reference>(std::forward<ActionTag>(i_actionTag));
		if (auto actionRes = deduced_adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor).m_adaptor,*mappedAction))
		{
			return make_result<transformed_result>(mappedAction(actionRes.get()));
		}
		else
		{
			return make_error<transformed_result>(std::forward<ActionTag>(i_actionTag));
		}
	}
	else
	{
		return deduced_adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor).m_adaptor,std::forward<ActionTag>(i_actionTag));
	}
}

}