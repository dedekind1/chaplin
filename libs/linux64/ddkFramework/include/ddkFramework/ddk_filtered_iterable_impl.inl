
#pragma warning(disable: 4102)

namespace ddk
{
namespace detail
{

template<typename Function>
iterable_filter<Function>::iterable_filter(const Function& i_filter)
: m_filter(i_filter)
{
}
template<typename Function>
Function iterable_filter<Function>::get_filter() const
{
	return m_filter;
}

template<typename Iterable, typename Filter>
TEMPLATE(typename IIterable,typename FFilter)
REQUIRED(is_constructible<Iterable,IIterable>,is_constructible<Filter,FFilter>)
filtered_iterable_impl<Iterable,Filter>::filtered_iterable_impl(IIterable&& i_iterable,FFilter&& i_filter)
: base_t(i_iterable,std::forward<FFilter>(i_filter))
{
}
template<typename Iterable,typename Filter>
TEMPLATE(typename Action)
REQUIRED(action_supported<typename Iterable::traits,Action>)
void filtered_iterable_impl<Iterable,Filter>::iterate_impl(Action&& i_initialAction)
{
    this->loop(std::forward<Action>(i_initialAction));
}
template<typename Iterable,typename Filter>
TEMPLATE(typename Action)
REQUIRED(action_supported<detail::const_iterable_traits<typename Iterable::traits>,Action>)
void filtered_iterable_impl<Iterable,Filter>::iterate_impl(Action&& i_initialAction) const
{
    this->loop(std::forward<Action>(i_initialAction));
}

}

template<typename Iterable,typename Filter>
iterable_adaptor<detail::filtered_iterable_impl<Iterable,Filter>>::iterable_adaptor(Iterable& i_iterable,const Filter& i_filter)
: m_adaptor(deduce_adaptor(i_iterable))
, m_filter(i_filter)
{
}
template<typename Iterable,typename Filter>
TEMPLATE(typename Adaptor, typename ActionTag)
REQUIRED(action_tags_supported<Adaptor,ActionTag>)
constexpr auto iterable_adaptor<detail::filtered_iterable_impl<Iterable,Filter>>::perform_action(Adaptor&& i_adaptor, ActionTag&& i_actionTag)
{
    if (auto actionRes = perform_action(std::forward<Adaptor>(i_adaptor),filtered_iterable_action{ std::forward<ActionTag>(i_actionTag),i_adaptor.m_filter }))
    {
        return make_result<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,ActionTag>>(actionRes);
    }
    else
    {
        return make_error<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,ActionTag>>(std::move(actionRes).error());
    }
}
template<typename Iterable,typename Filter>
template<typename Adaptor, typename ActionTag>
constexpr auto iterable_adaptor<detail::filtered_iterable_impl<Iterable,Filter>>::perform_action(Adaptor&& i_adaptor, filtered_iterable_action<ActionTag,Filter> i_actionTag)
{
    typedef typename mpl::which_type<mpl::is_const<Adaptor>,const deduced_adaptor<Iterable>,deduced_adaptor<Iterable>>::type adaptor_t;
    typedef filtered_iterable_action_result<adaptor_t,ActionTag,Filter> filtered_action_result;
    typedef typename filtered_action_result::error_t filtered_action_error;

    while(true)
    {
        if (filtered_action_result applyRes = i_actionTag.apply(std::forward<Adaptor>(i_adaptor).m_adaptor))
        {
            return make_result<filtered_action_result>(applyRes);
        }
        else
        {
            typedef typename filtered_action_error::recovery_tag recovery_tag;
            typedef filtered_iterable_action<ActionTag,Filter> filtered_action_tag;

            filtered_action_error applyError = std::move(applyRes).error();

            if constexpr (is_same_class<recovery_tag,filtered_action_tag> && is_move_assignable<filtered_action_tag>)
            {
                if (applyError)
                {
                    i_actionTag = std::move(applyError).recovery();

                    continue;
                }
            }
            else
            {
                if (applyError)
                {
                    if (auto recoveryRes = perform_action(std::forward<Adaptor>(i_adaptor),std::move(applyError).recovery()))
                    {
                        return make_result<filtered_action_result>(recoveryRes);
                    }
                    else
                    {
                        return make_error<filtered_action_result>(std::move(recoveryRes).error());
                    }
                }
            }

            return make_error<filtered_action_result>(std::move(applyError));
        }
    }
}

template<typename Iterable,typename Filter>
iterable_adaptor<const detail::filtered_iterable_impl<Iterable,Filter>>::iterable_adaptor(const Iterable& i_iterable,const Filter& i_filter)
: m_adaptor(deduce_adaptor(i_iterable))
, m_filter(i_filter)
{
}
template<typename Iterable,typename Filter>
TEMPLATE(typename Adaptor, typename ActionTag)
REQUIRED(action_tags_supported<Adaptor,ActionTag>)
constexpr auto iterable_adaptor<const detail::filtered_iterable_impl<Iterable,Filter>>::perform_action(Adaptor&& i_adaptor, ActionTag&& i_actionTag)
{
    if (auto actionRes = perform_action(std::forward<Adaptor>(i_adaptor),filtered_iterable_action{ std::forward<ActionTag>(i_actionTag),i_adaptor.m_filter }))
    {
        return make_result<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,ActionTag>>(actionRes);
    }
    else
    {
        return make_error<iterable_action_tag_result<detail::adaptor_traits<Adaptor>,ActionTag>>(actionRes.error());
    }
}
template<typename Iterable,typename Filter>
template<typename Adaptor, typename ActionTag>
constexpr auto iterable_adaptor<const detail::filtered_iterable_impl<Iterable,Filter>>::perform_action(Adaptor&& i_adaptor, filtered_iterable_action<ActionTag,Filter> i_actionTag)
{
    typedef typename mpl::which_type<mpl::is_const<Adaptor>,const deduced_adaptor<const Iterable>,deduced_adaptor<const Iterable>>::type adaptor_t;
    typedef filtered_iterable_action_result<adaptor_t,ActionTag,Filter> filtered_action_result;
    typedef typename filtered_action_result::error_t filtered_action_error;

    while(true)
    {
        if (filtered_action_result applyRes = i_actionTag.apply(std::forward<Adaptor>(i_adaptor).m_adaptor))
        {
            return make_result<filtered_action_result>(applyRes);
        }
        else
        {
            typedef typename filtered_action_error::recovery_tag recovery_tag;
            typedef filtered_iterable_action<ActionTag,Filter> filtered_action_tag;

            filtered_action_error applyError = applyRes.error();

            if constexpr (is_same_class<recovery_tag,filtered_action_tag> && is_move_assignable<filtered_action_tag>)
            {
                if (applyError)
                {
                    i_actionTag = std::move(applyError).recovery();
                }
                
                continue;
            }
            else
            {
                if (applyError)
                {
                    if (auto recoveryRes = perform_action(std::forward<Adaptor>(i_adaptor),std::move(applyError).recovery()))
                    {
                        return make_result<filtered_action_result>(recoveryRes);
                    }
                    else
                    {
                        return make_error<filtered_action_result>(recoveryRes.error());
                    }
                }

                return make_error<filtered_action_result>(applyError);
            }
        }
    }
}

}

#pragma warning(default: 4102)