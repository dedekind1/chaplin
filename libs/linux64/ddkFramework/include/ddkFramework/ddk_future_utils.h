//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_future_concepts.h"
#include "ddk_tuple_values.h"
#include "ddk_array_values.h"

namespace ddk
{

TEMPLATE(typename ... Futures)
REQUIRES(is_num_of_args_greater<0,Futures...>,is_future<Futures>...)
inline auto compose(Futures&& ... i_components);

}