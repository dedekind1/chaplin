
#include "ddk_iterable_order_concepts.h"

namespace ddk
{
namespace detail
{

template<typename T>
iterable_order<T>::iterable_order(const T& i_order)
: m_order(i_order)
{
}
template<typename T>
const T& iterable_order<T>::order() const
{
	return m_order;
}

template<typename Order>
iterable_order_impl<Order>::iterable_order_impl(const Order& i_order)
: Order(i_order)
{
}
template<typename Order>
TEMPLATE(typename Adaptor,typename ActionTag)
REQUIRED(action_tags_supported<Adaptor,ActionTag>)
constexpr auto iterable_order_impl<Order>::perform_action(Adaptor&& i_adaptor, ActionTag&& i_action) const
{
	if constexpr (may_order_can_handle_action<Order,Adaptor,ActionTag>)
	{
		return Order::perform_action(std::forward<Adaptor>(i_adaptor),std::forward<ActionTag>(i_action));
	}
	else
	{
		typedef mpl::remove_qualifiers<Adaptor> adaptor_t;

		return adaptor_t::perform_action(std::forward<Adaptor>(i_adaptor),std::forward<ActionTag>(i_action));
	}
}

template<typename Iterable,typename Order>
TEMPLATE(typename IIterable,typename OOrder)
REQUIRED(is_constructible<Iterable,IIterable>,is_constructible<Order,OOrder>)
ordered_iterable_impl<Iterable,Order>::ordered_iterable_impl(IIterable&& i_iterable, OOrder&& i_order)
: iterable_visitor<ordered_iterable_impl<Iterable,Order>>(i_iterable,i_order)
{
}
template<typename Iterable,typename Order>
TEMPLATE(typename Action)
REQUIRED(action_supported<typename Iterable::traits,Action>)
void ordered_iterable_impl<Iterable,Order>::iterate_impl(Action&& i_initialAction)
{
	this->loop(std::forward<Action>(i_initialAction));
}
template<typename Iterable,typename Order>
TEMPLATE(typename Action)
REQUIRED(action_supported<const_iterable_traits<typename Iterable::traits>,Action>)
void ordered_iterable_impl<Iterable,Order>::iterate_impl(Action&& i_initialAction) const
{
	this->loop(std::forward<Action>(i_initialAction));
}

}

template<typename Iterable,typename Order>
iterable_adaptor<detail::ordered_iterable_impl<Iterable,Order>>::iterable_adaptor(Iterable& i_iterable, const Order& i_order)
: m_adaptor(deduce_adaptor(i_iterable))
, m_order(i_order)
{
}
template<typename Iterable,typename Order>
iterable_adaptor<detail::ordered_iterable_impl<Iterable,Order>>::iterable_adaptor(detail::ordered_iterable_impl<Iterable,Order>& i_iterable)
{
}
template<typename Iterable,typename Order>
TEMPLATE(typename Adaptor, typename ActionTag)
REQUIRED(action_tags_supported<Adaptor,ActionTag>)
constexpr auto iterable_adaptor<detail::ordered_iterable_impl<Iterable,Order>>::perform_action(Adaptor&& i_adaptor, ActionTag&& i_actionTag)
{
	return std::forward<Adaptor>(i_adaptor).m_order.perform_action(std::forward<Adaptor>(i_adaptor).m_adaptor,std::forward<ActionTag>(i_actionTag));
}

template<typename Iterable,typename Order>
iterable_adaptor<const detail::ordered_iterable_impl<Iterable,Order>>::iterable_adaptor(const Iterable& i_iterable, const Order& i_order)
: m_adaptor(deduce_adaptor(i_iterable))
, m_order(i_order)
{
}
template<typename Iterable,typename Order>
TEMPLATE(typename Adaptor, typename ActionTag)
REQUIRED(action_tags_supported<Adaptor,ActionTag>)
constexpr auto iterable_adaptor<const detail::ordered_iterable_impl<Iterable,Order>>::perform_action(Adaptor&& i_adaptor, ActionTag&& i_actionTag)
{
	return std::forward<Adaptor>(i_adaptor).m_order.perform_action(std::forward<Adaptor>(i_adaptor).m_adaptor,std::forward<ActionTag>(i_actionTag));
}

}