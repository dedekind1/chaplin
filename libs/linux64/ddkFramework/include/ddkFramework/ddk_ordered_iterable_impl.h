//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable_visitor.h"
#include "ddk_iterable_action_concepts.h"
#include "ddk_iterable_pipeline.h"

namespace ddk
{
namespace detail
{

template<typename T>
class iterable_order
{
	ITERABLE_PIPELINE(iterable_order)

public:
	iterable_order(const T& i_order);

	const T& order() const;

private:
	const T m_order;
};

template<typename Order>
class iterable_order_impl : public Order
{
public:
	iterable_order_impl(const Order& i_order);
	using Order::perform_action;

	TEMPLATE(typename Adaptor,typename ActionTag)
	REQUIRES(action_tags_supported<Adaptor,ActionTag>)
	constexpr inline auto perform_action(Adaptor&& i_adaptor, ActionTag&& i_action) const;
};

template<typename Iterable,typename Order>
class ordered_iterable_impl : public iterable_impl_interface<typename Iterable::traits>, public iterable_visitor<ordered_iterable_impl<Iterable,Order>>
{
public:
	typedef typename Iterable::traits traits;
	typedef const_iterable_traits<traits> const_traits;

	TEMPLATE(typename IIterable,typename OOrder)
	REQUIRES(is_constructible<Iterable,IIterable>,is_constructible<Order,OOrder>)
	ordered_iterable_impl(IIterable&& i_iterable,OOrder&& i_Order);

	TEMPLATE(typename Action)
	REQUIRES(action_supported<traits,Action>)
	void iterate_impl(Action&& i_initialAction);
	TEMPLATE(typename Action)
	REQUIRES(action_supported<const_traits,Action>)
	void iterate_impl(Action&& i_initialAction) const;
};

}

template<typename Iterable,typename Order>
class iterable_adaptor<detail::ordered_iterable_impl<Iterable,Order>>
{
public:
	typedef typename Iterable::traits traits;
	typedef detail::const_iterable_traits<traits> const_traits;
	typedef typename traits::tags_t tags_t;
	typedef typename traits::const_tags_t const_tags_t;

	iterable_adaptor(Iterable& i_iterable,const Order& i_Order);
	iterable_adaptor(detail::ordered_iterable_impl<Iterable,Order>& i_iterable);

	TEMPLATE(typename Adaptor, typename ActionTag)
	REQUIRES(action_tags_supported<Adaptor,ActionTag>)
	static constexpr auto perform_action(Adaptor&& i_adaptor, ActionTag&& i_actionTag);

private:
	deduced_adaptor<Iterable> m_adaptor;
	const detail::iterable_order_impl<Order> m_order;
};

template<typename Iterable,typename Order>
class iterable_adaptor<const detail::ordered_iterable_impl<Iterable,Order>>
{
public:
	typedef typename Iterable::traits traits;
	typedef detail::const_iterable_traits<traits> const_traits;
	typedef typename traits::tags_t tags_t;
	typedef typename traits::const_tags_t const_tags_t;

	iterable_adaptor(const Iterable& i_iterable,const Order& i_Order);

	TEMPLATE(typename Adaptor, typename ActionTag)
	REQUIRES(action_tags_supported<Adaptor,ActionTag>)
	static constexpr auto perform_action(Adaptor&& i_adaptor, ActionTag&& i_actionTag);

private:
	deduced_adaptor<const Iterable> m_adaptor;
	const detail::iterable_order_impl<Order> m_order;
};

}

#include "ddk_ordered_iterable_impl.inl"