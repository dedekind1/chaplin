//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable_interface_base.h"
#include "ddk_iterable_supported_action.h"

namespace ddk
{
namespace detail
{

template<typename Traits>
class iterable_interface : public iterable_interface_base<Traits>
{
public:
    typedef Traits traits;
    typedef detail::const_iterable_traits<traits> const_traits;
    typedef supported_action<traits> action;
    typedef const_supported_action<traits> const_action;

    virtual void iterate(const action& i_initialAction) = 0;
    virtual void iterate(const const_action& i_initialAction) const = 0;
    virtual iterable_adaptor<type_erasure_iterable_impl<Traits>> deduce_owned_adaptor() = 0;
    virtual iterable_adaptor<const type_erasure_iterable_impl<Traits>> deduce_owned_adaptor() const = 0;
};

template<typename Traits>
using iterable_dist_ref = distributed_reference_wrapper<iterable_interface<Traits>>;
template<typename Traits>
using iterable_const_dist_ref = distributed_reference_wrapper<const iterable_interface<Traits>>;
template<typename Traits>
using iterable_dist_ptr = distributed_pointer_wrapper<iterable_interface<Traits>>;
template<typename Traits>
using iterable_const_dist_ptr = distributed_pointer_wrapper<const iterable_interface<Traits>>;

}
}