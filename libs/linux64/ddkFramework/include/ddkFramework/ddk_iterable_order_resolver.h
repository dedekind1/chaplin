//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable_action.h"
#include "ddk_union_iterable_impl.h"
#include "ddk_intersection_iterable_impl.h"

namespace ddk
{
namespace detail
{

class backward_order_resolver
{
public:
	constexpr backward_order_resolver() = default;

	TEMPLATE(typename Adaptor)
	REQUIRES(action_tags_supported<Adaptor,end_action_tag,backward_action_tag>)
	constexpr inline auto perform_action(Adaptor&& i_adaptor, const begin_action_tag& i_action) const;
	TEMPLATE(typename Adaptor)
	REQUIRES(action_tags_supported<Adaptor,begin_action_tag>)
	constexpr inline auto perform_action(Adaptor&& i_adaptor, const end_action_tag& i_action) const;
	TEMPLATE(typename Adaptor)
	REQUIRES(action_tags_supported<Adaptor,backward_action_tag>)
	constexpr inline auto perform_action(Adaptor&& i_adaptor, const forward_action_tag& i_action) const;
	TEMPLATE(typename Adaptor)
	REQUIRES(action_tags_supported<Adaptor,forward_action_tag>)
	constexpr inline auto perform_action(Adaptor&& i_adaptor, const backward_action_tag& i_action) const;
	TEMPLATE(typename Adaptor)
	REQUIRES(action_tags_supported<Adaptor,displace_action_tag>)
	constexpr inline auto perform_action(Adaptor&& i_adaptor, const displace_action_tag& i_action) const;
};

class interleaved_order_resolver
{
	template<typename ActionTag>
	struct interleaved_action_tag
	{
	private:
		template<typename ... Adaptors>
		constexpr inline friend auto apply_intersection_action(const interleaved_action_tag& i_action, Adaptors&& ... i_adaptors)
		{
			typedef detail::intersection_iterable_traits<detail::adaptor_traits<Adaptors>...> intersection_traits;
			typedef typename mpl::make_sequence<0,mpl::num_types<Adaptors...>>::type sequence_t;

			return i_action.apply_action<intersection_traits>(sequence_t{},static_cast<size_t>(((i_action.m_currIndex % static_cast<ssize_t>(mpl::num_types<Adaptors...>)) + mpl::num_types<Adaptors...>) % mpl::num_types<Adaptors...>),std::forward<Adaptors>(i_adaptors)...);
		}

	public:
		template<typename Traits>
		using return_type = typename Traits::reference;

		interleaved_action_tag(const ActionTag& i_action);
		interleaved_action_tag(const ActionTag& i_action, ssize_t i_currIndex);

	private:
		template<typename Traits, size_t ... Indexs, typename ... Adaptors>
		constexpr inline auto apply_action(const mpl::sequence<Indexs...>&, size_t i_currIndex, Adaptors&& ... i_adaptors) const;
		template<typename Traits, size_t Index, typename ... Adaptors>
		static constexpr inline auto _apply_action(const ActionTag& i_action, Adaptors&& ... i_adaptors);

		ActionTag m_action;
		ssize_t m_currIndex = 0;
	};
	
public:
	constexpr interleaved_order_resolver() = default;

	TEMPLATE(typename ... Iterables)
	REQUIRES(action_tags_supported<deduced_adaptor<Iterables>,forward_action_tag>...)
	constexpr inline auto perform_action(const iterable_adaptor<detail::intersection_iterable_impl<Iterables...>>& i_adaptor, const forward_action_tag& i_action) const;
	TEMPLATE(typename ... Iterables)
	REQUIRES(action_tags_supported<deduced_adaptor<Iterables>,backward_action_tag>...)
	constexpr inline auto perform_action(const iterable_adaptor<detail::intersection_iterable_impl<Iterables...>>& i_adaptor, const backward_action_tag& i_action) const;
	TEMPLATE(typename ... Iterables)
	REQUIRES(action_tags_supported<deduced_adaptor<Iterables>,forward_action_tag>...)
	constexpr inline auto perform_action(iterable_adaptor<detail::intersection_iterable_impl<Iterables...>>& i_adaptor, const forward_action_tag& i_action) const;
	TEMPLATE(typename ... Iterables)
	REQUIRES(action_tags_supported<deduced_adaptor<Iterables>,backward_action_tag>...)
	constexpr inline auto perform_action(iterable_adaptor<detail::intersection_iterable_impl<Iterables...>>& i_adaptor, const backward_action_tag& i_action) const;
	TEMPLATE(typename ... Iterables)
	REQUIRES(action_tags_supported<deduced_adaptor<Iterables>,begin_action_tag,forward_action_tag>...)
	constexpr inline auto perform_action(const iterable_adaptor<detail::union_iterable_impl<Iterables...>>& i_adaptor, const forward_action_tag& i_action) const;
	TEMPLATE(typename ... Iterables)
	REQUIRES(action_tags_supported<deduced_adaptor<Iterables>,backward_action_tag,end_action_tag>...)
	constexpr inline auto perform_action(const iterable_adaptor<detail::union_iterable_impl<Iterables...>>& i_adaptor, const backward_action_tag& i_action) const;
	TEMPLATE(typename ... Iterables)
	REQUIRES(action_tags_supported<deduced_adaptor<Iterables>,begin_action_tag,forward_action_tag>...)
	constexpr inline auto perform_action(iterable_adaptor<detail::union_iterable_impl<Iterables...>>& i_adaptor, const forward_action_tag& i_action) const;
	TEMPLATE(typename ... Iterables)
	REQUIRES(action_tags_supported<deduced_adaptor<Iterables>,backward_action_tag,end_action_tag>...)
	constexpr inline auto perform_action(iterable_adaptor<detail::union_iterable_impl<Iterables...>>& i_adaptor, const backward_action_tag& i_action) const;

private:
	mutable ssize_t m_index = 0;
};

class cumulative_order_resolver
{
	struct cumulative_forward_action_tag
	{
	private:
		template<typename ... Adaptors>
		constexpr inline friend auto apply_intersection_action(const cumulative_forward_action_tag& i_action, Adaptors&& ... i_adaptors)
		{
			typedef detail::intersection_iterable_traits<detail::adaptor_traits<Adaptors>...> intersection_traits;

			return i_action.apply_action<intersection_traits,0>(std::forward<Adaptors>(i_adaptors)...);
		}

	public:
		template<typename Traits>
		using return_type = typename Traits::reference;

		cumulative_forward_action_tag(const forward_action_tag& i_action);

	private:
		template<typename Traits, size_t Index, typename ... Adaptors>
		constexpr inline auto apply_action(Adaptors&& ... i_adaptors) const;

		forward_action_tag m_action;
	};

public:
	cumulative_order_resolver() = default;

	TEMPLATE(typename Adaptor)
	REQUIRES(action_tags_supported<Adaptor,forward_action_tag,begin_action_tag>)
	constexpr inline auto perform_action(Adaptor&& i_adaptor, const forward_action_tag& i_action) const;
};

class transpose_multi_dimensional_order_resolver
{
public:
	constexpr transpose_multi_dimensional_order_resolver() = default;

	TEMPLATE(typename Adaptor)
	REQUIRES(action_tags_supported<Adaptor,forward_action_tag>,is_dimensionable_iterable_adaptor<Adaptor>)
	constexpr inline auto perform_action(Adaptor&& i_adaptor, const forward_action_tag& i_action) const;
	TEMPLATE(typename Adaptor)
	REQUIRES(action_tags_supported<Adaptor,backward_action_tag>,is_dimensionable_iterable_adaptor<Adaptor>)
	constexpr inline auto perform_action(Adaptor&& i_adaptor, const backward_action_tag& i_action) const;
	TEMPLATE(typename Adaptor)
	REQUIRES(action_tags_supported<Adaptor,displace_action_tag>,is_dimensionable_iterable_adaptor<Adaptor>)
	constexpr inline auto perform_action(Adaptor&& i_adaptor, const displace_action_tag& i_action) const;

private:
	template<typename Adaptor>
	static constexpr inline auto _perform_action(Adaptor&& i_adaptor, typename mpl::remove_qualifiers<Adaptor>::traits::difference_type i_displacement);
};

}

extern const detail::backward_order_resolver reverse_order;
extern const detail::interleaved_order_resolver interleaved_order;
extern const detail::cumulative_order_resolver cumulative_order;
extern const detail::transpose_multi_dimensional_order_resolver transponse_dimension_order;

}