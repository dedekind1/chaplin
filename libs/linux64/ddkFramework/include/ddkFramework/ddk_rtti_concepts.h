//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

namespace ddk
{
namespace concepts
{

template<typename T>
struct is_rtti_available
{
private:
	template<typename TT, typename = typename TT::rtti_tag*>
	static std::true_type _checker(const TT&);
	template<typename ... TT>
	static std::false_type _checker(const TT& ...);

public:
	static const bool value = decltype(_checker(std::declval<T>()))::value;
};

}

template<typename T>
inline constexpr bool is_rtti = concepts::is_rtti_available<T>::value;

}