
namespace ddk
{
namespace detail
{

template<typename Iterable>
template<typename ... Args>
iterable<Iterable>::iterable(Args&& ... i_args)
: m_iterableImpl(std::forward<Args>(i_args)...)
{
}
template<typename Iterable>
TEMPLATE(typename Action)
REQUIRED(action_supported<typename Iterable::traits,Action>)
void iterable<Iterable>::iterate_impl(Action&& i_initialAction)
{
	m_iterableImpl.iterate_impl(std::forward<Action>(i_initialAction));
}
template<typename Iterable>
TEMPLATE(typename Action)
REQUIRED(action_supported<const_iterable_traits<typename Iterable::traits>,Action>)
void iterable<Iterable>::iterate_impl(Action&& i_initialAction) const
{
	m_iterableImpl.iterate_impl(std::forward<Action>(i_initialAction));
}
template<typename Iterable>
Iterable& iterable<Iterable>::get()
{
	return m_iterableImpl;
}
template<typename Iterable>
const Iterable& iterable<Iterable>::get() const
{
	return m_iterableImpl;
}
template<typename Iterable>
Iterable&& iterable<Iterable>::extract() &&
{
	return std::move(m_iterableImpl);
}

}
}
