
namespace ddk
{
namespace detail
{

template<typename Traits>
TEMPLATE(typename Iterable)
REQUIRED(are_iterable_traits_compatible<typename Iterable::traits,Traits>)
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(const iterable<Iterable>& i_iterable)
: m_iterable(make_iterable<Traits>(i_iterable.get()))
{
}
template<typename Traits>
template<typename Iterable>
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(iterable<Iterable>& i_iterable)
: m_iterable(make_iterable<Traits>(i_iterable.get()))
{
}
template<typename Traits>
TEMPLATE(typename Iterable)
REQUIRED(are_iterable_traits_compatible<typename Iterable::traits,Traits>)
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(iterable<Iterable>&& i_iterable)
: m_iterable(make_iterable<Traits>(std::move(i_iterable).extract()))
{
}
template<typename Traits>
TEMPLATE(typename Iterable,typename Allocator)
REQUIRED(are_iterable_traits_compatible<typename Iterable::traits,Traits>,is_allocator<Allocator>)
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(const iterable<Iterable>& i_iterable,const Allocator& i_allocator)
: m_iterable(make_iterable<Traits>(i_allocator,i_iterable.get(),i_allocator))
{
}
template<typename Traits>
TEMPLATE(typename Iterable,typename Allocator)
REQUIRED(are_iterable_traits_compatible<typename Iterable::traits,Traits>,is_allocator<Allocator>)
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(iterable<Iterable>& i_iterable,const Allocator& i_allocator)
: m_iterable(make_iterable<Traits>(i_allocator,i_iterable.get(),i_allocator))
{
}
template<typename Traits>
TEMPLATE(typename Iterable,typename Allocator)
REQUIRED(are_iterable_traits_compatible<typename Iterable::traits,Traits>,is_allocator<Allocator>)
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(iterable<Iterable>&& i_iterable, const Allocator& i_allocator)
: m_iterable(make_iterable<Traits>(std::move(i_iterable).extract(),i_allocator))
{
}
template<typename Traits>
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(const iterable<type_erasure_iterable_impl<Traits>>& i_iterable)
: m_iterable(i_iterable.get().m_iterable)
{
}
template<typename Traits>
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(iterable<type_erasure_iterable_impl<Traits>>& i_iterable)
: m_iterable(i_iterable.get().m_iterable)
{
}
template<typename Traits>
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(iterable<type_erasure_iterable_impl<Traits>>&& i_iterable)
: m_iterable(std::move(i_iterable).extract().m_iterable)
{
}
template<typename Traits>
TEMPLATE(typename TTraits)
REQUIRED(are_iterable_traits_compatible<TTraits,Traits>)
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(const iterable<type_erasure_iterable_impl<TTraits>>& i_iterable)
: m_iterable(make_iterable<Traits>(i_iterable.get()))
{
}
template<typename Traits>
TEMPLATE(typename TTraits)
REQUIRED(are_iterable_traits_compatible<TTraits,Traits>)
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(iterable<type_erasure_iterable_impl<TTraits>>& i_iterable)
: m_iterable(make_iterable<Traits>(i_iterable.get()))
{
}
template<typename Traits>
TEMPLATE(typename TTraits)
REQUIRED(are_iterable_traits_compatible<TTraits,Traits>)
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(iterable<type_erasure_iterable_impl<TTraits>>&& i_iterable)
: m_iterable(make_iterable<Traits>(std::move(i_iterable).extract()))
{
}
template<typename Traits>
TEMPLATE(typename TTraits, typename Allocator)
REQUIRED(are_iterable_traits_compatible<TTraits,Traits>,is_allocator<Allocator>)
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(const iterable<type_erasure_iterable_impl<TTraits>>& i_iterable, const Allocator& i_allocator)
: m_iterable(make_iterable<Traits>(i_iterable.get(),i_allocator))
{
}
template<typename Traits>
TEMPLATE(typename TTraits,typename Allocator)
REQUIRED(are_iterable_traits_compatible<TTraits,Traits>,is_allocator<Allocator>)
type_erasure_iterable_impl<Traits>::type_erasure_iterable_impl(iterable<type_erasure_iterable_impl<TTraits>>&& i_iterable,const Allocator& i_allocator)
: m_iterable(make_iterable<Traits>(std::move(i_iterable).extract(),i_allocator))
{
}
template<typename Traits>
template<typename Action>
void type_erasure_iterable_impl<Traits>::iterate_impl(const Action& i_initialAction)
{
	m_iterable->iterate(i_initialAction);
}
template<typename Traits>
template<typename Action>
void type_erasure_iterable_impl<Traits>::iterate_impl(const Action& i_initialAction) const
{
	m_iterable->iterate(i_initialAction);
}
template<typename Traits>
iterable_dist_ref<Traits> type_erasure_iterable_impl<Traits>::get_iterable() const
{
	return m_iterable;
}

}
}