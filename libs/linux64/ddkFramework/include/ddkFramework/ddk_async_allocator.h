#pragma once

#include "ddk_allocator.h"

namespace ddk
{

class async_allocator
{
public:
	typedef async_allocator allocator;
	typedef void type;
	typedef type* pointer;
	typedef const type* const_pointer;
	typedef std::ptrdiff_t difference_type;

    async_allocator(allocator_const_lent_ptr i_allocator);

	void* allocate(size_t i_size = 1) const;
	void* reallocate(void*,size_t) const;
	template<typename T, typename TT>
	inline void deallocate(detail::embedded_private_async_state<T,TT>* i_ptr) const;
	void deallocate(void* i_ptr) const;

private:
    mutable allocator_const_lent_ptr m_allocator;
};

}

#include "ddk_async_allocator.inl"