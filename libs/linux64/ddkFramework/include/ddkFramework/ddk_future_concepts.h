#pragma once

#define IS_FUTURE(_TYPE) \
    ddk::concepts::is_future<_TYPE>

namespace ddk
{
namespace concepts
{

template<typename T>
struct is_future_impl
{
private:
    template<typename TT, typename = TT::future_tag>
    static std::true_type resolve(const TT&);
    static std::false_type resolve(...);

public:
    static const bool value = decltype(resolve(std::declval<T>()))::value;
};

}

template<typename T>
inline constexpr bool is_future = concepts::is_future_impl<T>::value; 

}