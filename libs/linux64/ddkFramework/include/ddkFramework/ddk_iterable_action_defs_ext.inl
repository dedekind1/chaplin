
namespace ddk
{

template<typename ... Actions>
TEMPLATE(typename AAction)
REQUIRED(is_among_constructible_types<AAction,Actions...>)
constexpr any_action<Actions...>::any_action(AAction&& i_action, bool i_valid)
: action_base(i_valid)
, m_actions(std::forward<AAction>(i_action))
{
}
template<typename ... Actions>
TEMPLATE(typename Adaptor)
REQUIRED(action_tags_supported<Adaptor,typename any_action<Actions...>::tags_t>)
constexpr auto any_action<Actions...>::apply(Adaptor&& i_adaptor) const
{
	return visit([&](auto&& i_action)
	{
		return i_action.apply(std::forward<Adaptor>(i_adaptor));
	},m_actions);
}

constexpr swap_action::swap_action(bool i_valid)
: action_base(i_valid)
{
}

}