
namespace ddk
{
namespace detail
{

template<typename Traits, typename Iterable>
TEMPLATE(typename ... Args)
REQUIRED(is_constructible<iterable<Iterable>,Args...>)
view_iterable<Traits,Iterable>::view_iterable(Args&& ... i_args)
: iterable<Iterable>(std::forward<Args>(i_args)...)
{
}
template<typename Traits, typename Iterable>
void view_iterable<Traits,Iterable>::iterate(const action& i_initialAction)
{
	this->iterate_impl(i_initialAction);
}
template<typename Traits, typename Iterable>
void view_iterable<Traits,Iterable>::iterate(const const_action& i_initialAction) const
{
	this->iterate_impl(i_initialAction);
}
template<typename Traits, typename Iterable>
iterable_adaptor<type_erasure_iterable_impl<Traits>> view_iterable<Traits,Iterable>::deduce_owned_adaptor()
{
	return deduce_adaptor(this->get());
}
template<typename Traits, typename Iterable>
iterable_adaptor<const type_erasure_iterable_impl<Traits>> view_iterable<Traits,Iterable>::deduce_owned_adaptor() const
{
	return deduce_adaptor(this->get());
}

}
}