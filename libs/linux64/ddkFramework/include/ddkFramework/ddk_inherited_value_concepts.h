//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_template_helper.h"
#include <type_traits>

namespace ddk
{

template<typename>
class dynamic_visitor;

namespace concepts
{

template<typename T>
struct is_inherited_value_impl
{
private:
    template<typename TT>
    static std::true_type resolve(const distributed_object<TT>&);
    template<typename TT>
    static std::true_type resolve(const distributed_value<TT>&);
    template<typename TT>
    static std::true_type resolve(const unique_object<TT>&);
    template<typename TT>
    static std::true_type resolve(const unique_value<TT>&);
    template<typename TT>
    static std::true_type resolve(const lent_object<TT>&);
    template<typename TT>
    static std::true_type resolve(const lent_value<TT>&);
    template<typename ... TT>
    static std::false_type resolve(const TT& ...);

public:
    static const bool value = decltype(resolve(std::declval<T>()))::value;
};

template<typename T>
struct is_dynamic_visitor_impl;

template<typename T>
struct is_dynamic_visitor_impl<dynamic_visitor<T>> : public std::true_type
{
};
template<typename T>
struct is_dynamic_visitor_impl : public std::false_type
{
};

template<typename T>
struct is_base_of_dynamic_visitor_impl
{
private:
    template<typename TT>
    static std::is_base_of<dynamic_visitor<typename TT::type_interface>,TT> resolve(const TT&);
    static std::false_type resolve(...);

public:
    static const bool value = decltype(resolve(std::declval<T>()))::value;
};

}

template<typename T>
inline constexpr bool is_inherited_value = concepts::is_inherited_value_impl<T>::value;

template<typename T>
inline constexpr bool is_not_inherited_value = concepts::is_inherited_value_impl<T>::value == false;

template<typename T>
inline constexpr bool is_dynamic_visitor = concepts::is_dynamic_visitor_impl<T>::value;

template<typename T>
inline constexpr bool is_not_dynamic_visitor = concepts::is_dynamic_visitor_impl<T>::value == false;

template<typename T>
inline constexpr bool is_base_of_dynamic_visitor = concepts::is_base_of_dynamic_visitor_impl<T>::value;

}
