#pragma once

namespace ddk
{
namespace iter
{

template<typename T>
inline void yield(T&& i_value);
TEMPLATE(typename T, typename ... Args)
REQUIRES(is_constructible<T,Args...>)
inline void yield(Args&& ... i_args);

template<typename T = void>
inline void stop();

TEMPLATE(typename T, typename ... Args)
REQUIRES(is_constructible<iterable_error,Args...>)
inline void suspend(Args&& ... i_args);

}
}

#include "ddk_iterable_flow.inl"