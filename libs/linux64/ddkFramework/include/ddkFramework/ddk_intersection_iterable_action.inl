
#include "ddk_function_arguments_template_helper.h"
#include "ddk_iterable_action_tag_result.h"

namespace ddk
{

template<typename Sink, typename ... Adaptors>
auto apply_intersection_action(const sink_action_tag<Sink>& i_action, Adaptors&& ... i_adaptors)
{
	typedef tuple<iterable_action_tag_result<detail::adaptor_traits<Adaptors>,k_iterable_empty_sink>...> adaptors_result;
	typedef detail::intersection_iterable_traits<detail::adaptor_traits<Adaptors>...> intersection_traits;
	typedef typename intersection_traits::reference reference;
	typedef iterable_action_tag_result<intersection_traits,sink_action_tag<Sink>> intersection_result;

	const auto create_reference = [](auto&& ... i_result) -> optional<reference>
	{
		if ((static_cast<bool>(i_result) && ...))
		{
			return reference{ i_result.get() ... };
		}
		else
		{
			return none;
		}
	};

	if(auto actionRes = create_reference(mpl::remove_qualifiers<Adaptors>::perform_action(std::forward<Adaptors>(i_adaptors),k_iterableEmptySink) ...))
	{
		return make_result<intersection_result>(i_action(*actionRes));
	}
	else
	{
		return make_error<intersection_result>(std::move(i_action));
	}
}

template<typename ... Adaptors>
auto apply_intersection_action(const size_action_tag& i_action, Adaptors&& ... i_adaptors)
{
	typedef detail::intersection_iterable_traits<detail::adaptor_traits<Adaptors>...> intersection_traits;
	typedef iterable_action_tag_result<intersection_traits,size_action_tag> intersection_result;

	const auto get_min = [](auto&& ... i_result) -> optional<size_t>
	{
		if((static_cast<bool>(i_result) && ...))
		{
			return mpl::get_min(i_result.get() ...);
		}
		else
		{
			return none;
		}
	};

	if(auto actionRes = get_min(mpl::remove_qualifiers<Adaptors>::perform_action(std::forward<Adaptors>(i_adaptors),i_action) ...))
	{
		return make_result<intersection_result>(*actionRes);
	}
	else
	{
		return make_error<intersection_result>(i_action);
	}
}

template<typename ActionTag, typename ... Adaptors>
auto apply_intersection_action(const ActionTag& i_action, Adaptors&& ... i_adaptors)
{
	if constexpr ((is_adaptor_representable_by_action<Adaptors,ActionTag> && ...))
	{
		typedef detail::intersection_iterable_traits<detail::adaptor_traits<Adaptors>...> intersection_traits;
		typedef typename intersection_traits::reference reference;
		typedef tuple<iterable_action_tag_result<detail::adaptor_traits<Adaptors>,ActionTag>...> adaptors_result;
		typedef iterable_action_tag_result<intersection_traits,ActionTag> intersection_result;

		const auto create_reference = [](auto&& ... i_result) -> optional<reference>
		{
			if ((static_cast<bool>(i_result) && ...))
			{
				return reference{ i_result.get() ... };
			}
			else
			{
				return none;
			}
		};

		if(auto actionRes = create_reference(mpl::remove_qualifiers<Adaptors>::perform_action(std::forward<Adaptors>(i_adaptors),i_action).dismiss() ...))
		{
			return make_result<intersection_result>(*actionRes);
		}
		else
		{
			return make_error<intersection_result>(std::move(i_action));
		}
	}
	else
	{
		typedef iterable_action_tag_result<detail::intersection_iterable_traits<detail::adaptor_traits<Adaptors>...>,ActionTag> intersection_result;

		if ((mpl::remove_qualifiers<Adaptors>::perform_action(std::forward<Adaptors>(i_adaptors),i_action) && ...))
		{
			return make_result<intersection_result>(success);
		}
		else
		{
			return make_error<intersection_result>(std::move(i_action));
		}
	}
}


}