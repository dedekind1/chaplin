//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_function_template_helper.h"

namespace ddk
{
namespace concepts
{

template<typename,typename...>
struct is_callable_by;

template<typename T,typename ... Args>
struct is_callable_by
{
private:
	template<typename TT, typename = decltype(std::declval<TT>()(std::declval<Args>()...))>
	static std::true_type resolve(TT&, int);
	template<typename TT>
	static std::false_type resolve(const TT&, float);

public:
	static const bool value = decltype(resolve(std::declval<T&>(),0))::value;
};

}

template<typename T>
inline constexpr bool is_function = mpl::is_function<T>;

template<typename T>
inline constexpr bool is_not_function = mpl::is_function<T> == false;

template<typename T, typename ... Args>
inline constexpr bool is_callable = mpl::is_valid_functor<typename std::remove_reference<T>::type,Args...>;

template<typename T, typename ... Args>
inline constexpr bool is_not_callable = mpl::is_valid_functor<typename std::remove_reference<T>::type,Args...> == false;

template<typename T, typename ... Args>
inline constexpr bool is_callable_by = concepts::is_callable_by<T,Args...>::value;

template<typename T, typename TT>
inline constexpr bool is_return_convertible = std::is_convertible<typename mpl::aqcuire_callable_return_type<T>::type,TT>::value;

template<typename T, typename ... Args>
inline constexpr bool is_callable_not_function = is_callable<T,Args...> && is_not_function<T>;

}