//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterable.h"
#include "ddk_iterable_defs.h"
#include "ddk_function.h"
#include "ddk_future.h"
#include "ddk_function_concepts.h"
#include "ddk_concepts.h"
#include "ddk_callable.h"

namespace ddk
{
namespace detail
{

template<typename>
class iteration_result;

template<>
class iteration_result<void>
{
public:
	typedef iterable_result<void> result_t;
	typedef typename result_t::error_t error_t;

	iteration_result();

	error_t error() const;

protected:
	iteration_result& set_result(result_t i_result);
	result_t& get_result();
	const result_t& get_result() const;
	result_t extract_result();
	void dismiss();

private:
	mutable result_t m_result;
};

template<typename T>
class iteration_result
{
public:
	typedef iterable_result<T> result_t;
	typedef iterable_error error_t;
	typedef typename embedded_type<T>::ref_type reference;
	typedef typename embedded_type<T>::cref_type const_reference;

	iteration_result();

	reference operator*();
	const_reference operator*() const;
	error_t error() const;

protected:
	iteration_result& set_result(result_t i_result);
	result_t& get_result();
	const result_t& get_result() const;
	result_t extract_result();
	void dismiss();

private:
	mutable result_t m_result;
};

}

template<typename Iterable, typename Sink>
class iteration : public detail::iteration_result<typename mpl::aqcuire_callable_return_type<Sink>::type>
{
	template<typename IIterable, typename SSink>
	friend iterable_result<SSink> execute_iteration(iteration<IIterable,SSink>& i_co_iteration);

	typedef detail::iteration_result<typename mpl::aqcuire_callable_return_type<Sink>::type> base_t;
	typedef typename Iterable::traits traits;
	typedef typename Iterable::const_traits const_traits;

public:
	using typename base_t::result_t;
	using typename base_t::error_t;

	template<typename IIterable, typename SSink>
	constexpr iteration(IIterable&& i_iterable, SSink&& i_sink);
	iteration(const iteration&) = delete;
	constexpr iteration(iteration&& other);
	~iteration();

	iteration* operator->();
	const iteration* operator->() const;
	template<typename Action = typename iterable_default_action<Iterable>::type>
	result_t execute(const Action& i_action = iterable_default_action<Iterable>::default_action());
	template<typename T>
	future<result_t> attach(T&& i_execContext);
	future<result_t> attach(const detail::this_thread_t&);
	operator bool() const;

private:
	using base_t::set_result;
	using base_t::get_result;
	using base_t::extract_result;
	using base_t::dismiss;

	template<typename Action>
	constexpr void _execute(const Action& i_action);

	Iterable m_iterable;
	Sink m_sink;
	mutable atomic_bool m_executable;
};
template<typename Iterable, typename Sink>
iteration(Iterable&,const Sink&) -> iteration<Iterable,Sink>;
template<typename Iterable,typename Sink>
iteration(Iterable&,Sink&&)->iteration<Iterable,Sink>;
template<typename Iterable, typename Sink>
iteration(Iterable&&,const Sink&) -> iteration<Iterable,Sink>;
template<typename Iterable,typename Sink>
iteration(Iterable&&,Sink&&)->iteration<Iterable,Sink>;

}

#include "ddk_iteration_utils.h"
#include "ddk_iteration.inl"