
namespace ddk
{

template<typename T>
TEMPLATE(typename ... Args)
REQUIRED(is_constructible<T,Args...>)
lendable<T>::lendable(Args&& ... i_args)
: m_value(T(std::forward<Args>(i_args) ...))
{
}
template<typename T>
lendable<T>::lendable(const lendable& other)
: m_value(other.m_value)
{
}
template<typename T>
lendable<T>::lendable(lendable&& other)
: m_value(std::move(other.m_value))
{
}
template<typename T>
lendable<T>::~lendable()
{
    DDK_ASSERT(m_counter.hasLentReferences() == false, "Still lent references alive while destroying unique reference");
}
template<typename T>
lendable<T>& lendable<T>::operator=(const lendable& other)
{
    m_value = other.m_value;

    return *this;
}
template<typename T>
lendable<T>& lendable<T>::operator=(lendable&& other)
{
    m_value = std::move(other.m_value);

    return *this;
}
template<typename T>
typename lendable<T>::pointer_type lendable<T>::operator->()
{
    return &m_value;
}
template<typename T>
typename lendable<T>::const_pointer_type lendable<T>::operator->() const
{
    return &m_value;
}
template<typename T>
typename lendable<T>::reference lendable<T>::operator*()
{
    return m_value;
}
template<typename T>
typename lendable<T>::const_reference lendable<T>::operator*() const
{
    return m_value;
}
#ifdef DDK_DEBUG
template<typename T>
lent_reference_wrapper<typename lendable<T>::value_type> lendable<T>::ref_from_this()
{
    return lent_reference_wrapper<value_type>(&m_value,&m_counter);
}
template<typename T>
lent_reference_wrapper<typename lendable<T>::const_value_type> lendable<T>::ref_from_this() const
{
    return lent_reference_wrapper<const_value_type>(&m_value,&m_counter);
}
#else
template<typename T>
lent_reference_wrapper<typename lendable<T>::value_type> lendable<T>::ref_from_this()
{
    return lent_reference_wrapper<value_type>(&m_value);
}
template<typename T>
lent_reference_wrapper<typename lendable<T>::const_value_type> lendable<T>::ref_from_this() const
{
    return lent_reference_wrapper<const_value_type>(&m_value);
}
#endif

}