//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_template_helper.h"
#include "ddk_iterable_traits.h"
#include "ddk_iterable_action_recovery_tags.h"

namespace ddk
{
namespace concepts
{

template<typename,typename...>
struct adaptor_supports_actions_impl;

template<typename Adaptor,typename ... Tags>
struct adaptor_supports_actions_impl<Adaptor,mpl::type_pack<Tags...>>
{
private:
    template<typename T,typename TT,typename = decltype(std::declval<T>().perform_action(std::declval<TT>()))>
    static std::true_type resolve(T&&,TT&&);
    static std::false_type resolve(...);

public:
    static const bool value = (decltype(resolve(std::declval<Adaptor>(),std::declval<Tags&&>()))::value && ...);
};

template<typename Adaptor,typename Tag>
struct adaptor_supports_actions_impl<Adaptor,Tag>
{
private:
    template<typename T, typename TT, typename = decltype(std::declval<T>().perform_action(std::declval<TT>()))>
    static std::true_type resolve(T&&,TT&&);
    static std::false_type resolve(...);

public:
    static const bool value = decltype(resolve(std::declval<Adaptor>(),std::declval<Tag&&>()))::value;
};

template<typename ...>
struct adaptor_holds_actions_impl;

template<typename ... Tags,typename ... TTags>
struct adaptor_holds_actions_impl<mpl::type_pack<Tags...>,mpl::type_pack<TTags...>>
{
    static const bool value = ( mpl::is_some_constructible_type<Tags,TTags...> && ... );
};

template<typename Tag,typename ... Tags>
struct adaptor_holds_actions_impl<Tag,mpl::type_pack<Tags...>>
{
    static const bool value = mpl::is_some_constructible_type<Tag,Tags...>;
};

template<typename Adaptor,typename ActionTag>
struct is_adaptor_representable_by_action_impl
{
private:
    typedef iterable_action_return_type<detail::adaptor_traits<Adaptor>,ActionTag> return_type;

public:
    static const bool value = std::is_same<return_type,typename detail::adaptor_traits<Adaptor>::const_reference>::value ||
                              std::is_same<return_type,typename detail::adaptor_traits<Adaptor>::reference>::value;
};

}

template<typename Adaptor, typename ... ActionTags>
inline constexpr bool adaptor_tags_supported = (concepts::adaptor_holds_actions_impl<mpl::remove_qualifiers<ActionTags>,detail::adaptor_tags<Adaptor>>::value && ...);

template<typename FromAdaptor,typename ToAdaptor>
inline constexpr bool adaptor_tags_equivalent = adaptor_tags_supported<FromAdaptor,ddk::detail::adaptor_tags<ToAdaptor>> && adaptor_tags_supported<ToAdaptor,ddk::detail::adaptor_tags<FromAdaptor>>;

template<typename Adaptor, typename ... ActionTags>
inline constexpr bool action_tags_supported = adaptor_tags_supported<Adaptor,ActionTags...>;

template<typename Adaptor, typename ... ActionTags>
inline constexpr bool action_tags_not_supported = adaptor_tags_supported<Adaptor,ActionTags...> == false;

template<typename FromAdaptor,typename ToAdaptor>
inline constexpr bool any_adaptor_action_tags_supported = adaptor_tags_supported<FromAdaptor,ddk::detail::adaptor_tags<ToAdaptor>>;

template<typename Adaptor, typename ... ActionTags>
inline constexpr bool any_action_tags_supported = adaptor_tags_supported<Adaptor,ActionTags...> == false;

template<typename Adaptor, typename Function>
inline constexpr bool transformed_action_tags_supported = action_tags_supported<Adaptor,typename ddk::mpl::aqcuire_callable_return_type<Function>::type::tags_t>;

template<typename ActionTag, typename AActionTag>
inline constexpr bool are_action_tags_contravariant = ddk::mpl::are_types_contravariant<ActionTag,AActionTag>;

template<typename ActionTag>
inline constexpr bool is_sink_action = mpl::is_sink_action_tag<ActionTag>;

template<typename Adaptor, typename ActionTag>
constexpr inline bool is_adaptor_representable_by_action = concepts::is_adaptor_representable_by_action_impl<Adaptor,ActionTag>::value;

template<typename Adaptor, typename ActionTag>
constexpr inline bool is_adaptor_not_representable_by_action = concepts::is_adaptor_representable_by_action_impl<Adaptor,ActionTag>::value == false;

}