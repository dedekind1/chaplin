#pragma once

#include "ddk_tuple.h"
#include "ddk_iterable_type_concepts.h"

#define ITERABLE_PIPELINE(_FUNCTOR)	\
template<typename ... __T> \
friend inline auto operator<<=(const _FUNCTOR& i_lhs, const iterable_pipeline<__T...>& i_rhs) \
{ \
    return i_rhs + i_lhs; \
} \
template<typename __T> \
friend inline auto operator<<=(const __T& i_lhs, const _FUNCTOR& i_rhs) \
{ \
    return i_lhs <<= ddk::iterable_pipeline<_FUNCTOR>{i_rhs}; \
}


namespace ddk
{

template<typename T, typename ... TT>
class iterable_pipeline : public tuple<T,TT...>
{
    TEMPLATE(typename Iterable)
    REQUIRES(is_iterable_deducible_type<Iterable>)
    friend inline constexpr auto operator<<=(const iterable_pipeline& i_lhs, Iterable&& i_rhs)
    {
        typedef typename mpl::make_sequence<0,mpl::num_types<TT...>>::type seq_t;

        return i_lhs.resolve(seq_t{},std::forward<Iterable>(i_rhs));
    }
    TEMPLATE(typename Iterable)
    REQUIRES(is_iterable_deducible_type<Iterable>)
    friend inline constexpr auto operator<<=(iterable_pipeline& i_lhs, Iterable&& i_rhs)
    {
        typedef typename mpl::make_sequence<0,mpl::num_types<TT...>>::type seq_t;

        return i_lhs.resolve(seq_t{},std::forward<Iterable>(i_rhs));
    }

public:
    TEMPLATE(typename Arg, typename ... Args)
    REQUIRES(is_constructible<T,Arg>,is_constructible<TT,Args>...)
    constexpr iterable_pipeline(const Arg& i_arg, const Args& ... i_args);

    template<typename Functor>
    inline constexpr auto operator+(const Functor& i_functor) const;

private:
    template<size_t ... Indexs, typename Iterable>
    inline constexpr auto resolve(const mpl::sequence<Indexs...>&, Iterable&& i_iterable);
    template<size_t ... Indexs, typename Iterable>
    inline constexpr auto resolve(const mpl::sequence<Indexs...>&, Iterable&& i_iterable) const;
    template<size_t ... Indexs, typename ... Iterable>
    inline constexpr auto append(const mpl::sequence<Indexs...>&, Iterable&& ... i_iterables) const;
};
template<typename ... T>
iterable_pipeline(const T& ...) -> iterable_pipeline<T...>;

template<typename ... Args>
inline constexpr auto make_iterable_pipeline(const Args& ... i_args);


}

#include "ddk_iterable_pipeline.inl"