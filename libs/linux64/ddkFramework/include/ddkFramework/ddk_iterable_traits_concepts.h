//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_type_concepts.h"
#include "ddk_iterable_traits.h"

namespace ddk
{
namespace concepts
{

template<typename FromTraits, typename ToTraits>
struct check_iterable_types_compatibility
{
private:
	typedef typename FromTraits::reference from_reference;
	typedef typename ToTraits::reference to_reference;
	typedef typename FromTraits::const_reference from_const_reference;
	typedef typename ToTraits::const_reference to_const_reference;

template<typename T, typename TT>
static inline constexpr bool cond = (is_reference<T> && is_reference<TT> && is_convertible<T,TT>) ||
						  			(is_not_reference<TT> && is_constructible<TT,T>);

public:
	static const bool value = cond<from_reference,to_reference> && cond<from_const_reference,to_const_reference>;
};

template<typename FromTraits, typename ToTraits>
struct check_iterable_actions_compatibility
{
private:
	typedef typename FromTraits::tags_t from_tags_t;
	typedef typename ToTraits::tags_t to_tags_t;
	typedef typename FromTraits::const_tags_t from_const_tags_t;
	typedef typename ToTraits::const_tags_t to_const_tags_t;

public:
	static const bool value = from_tags_t::template projects(to_tags_t{}) &&
							  from_const_tags_t::template projects(to_const_tags_t{});
};

}

template<typename Traits1, typename Traits2>
inline constexpr bool are_iterable_types_compatible = concepts::check_iterable_types_compatibility<Traits1,Traits2>::value;

template<typename Traits1, typename Traits2>
inline constexpr bool are_iterable_types_equivalent = are_iterable_types_compatible<Traits1,Traits2> && are_iterable_types_compatible<Traits2,Traits1>;

template<typename Traits1, typename Traits2>
inline constexpr bool are_iterable_actions_compatible = concepts::check_iterable_actions_compatibility<Traits1,Traits2>::value;

template<typename Traits1, typename Traits2>
inline constexpr bool are_iterable_actions_equivalent = are_iterable_actions_compatible<Traits1,Traits2> && are_iterable_actions_compatible<Traits2,Traits1>;

template<typename Traits1, typename Traits2>
inline constexpr bool are_iterable_traits_compatible = are_iterable_types_compatible<Traits1,Traits2> && are_iterable_actions_compatible<Traits1,Traits2>;

template<typename Traits1,typename Traits2>
inline constexpr bool are_iterable_traits_equivalent = are_iterable_traits_compatible<Traits1,Traits2> && are_iterable_traits_compatible<Traits2,Traits1>;
	
}