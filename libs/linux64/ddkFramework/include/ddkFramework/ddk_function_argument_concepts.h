#pragma once

#include "ddk_function_arguments_template_helper.h"

namespace ddk
{
namespace concepts
{

template<typename T,typename ... Args>
struct is_callable_by<T,prod_argument<Args...>>
{
	static const bool value = false;
};
template<typename T,typename ... Args>
struct is_callable_by<T,prod_argument<Args...>&>
{
	static const bool value = false;
};
template<typename T,typename ... Args>
struct is_callable_by<T,const prod_argument<Args...>&>
{
	static const bool value = false;
};
template<typename T,typename ... Args>
struct is_callable_by<T,prod_argument<Args...>&&>
{
	static const bool value = false;
};
template<typename T,typename ... Args>
struct is_callable_by<T,sum_argument<Args...>>
{
	static const bool value = false;
};
template<typename T,typename ... Args>
struct is_callable_by<T,sum_argument<Args...>&>
{
	static const bool value = false;
};
template<typename T,typename ... Args>
struct is_callable_by<T,const sum_argument<Args...>&>
{
	static const bool value = false;
};
template<typename T,typename ... Args>
struct is_callable_by<T,sum_argument<Args...>&&>
{
	static const bool value = false;
};

}

template<typename T>
inline constexpr bool is_function_argument = mpl::is_function_argument<T>::value;

template<typename T>
inline constexpr bool is_not_function_argument = mpl::is_function_argument<T>::value == false;

}