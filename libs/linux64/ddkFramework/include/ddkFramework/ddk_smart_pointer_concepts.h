//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_shared_reference_wrapper.h"
#include "ddk_unique_reference_wrapper.h"

namespace ddk
{
namespace concepts
{

template<typename>
struct is_shared_reference_impl;

template<typename T>
struct is_shared_reference_impl<shared_reference_wrapper<T>>
{
	static const bool value = true;
};
template<typename>
struct is_shared_reference_impl
{
	static const bool value = false;
};

template<typename>
struct is_shared_pointer_impl;

template<typename T>
struct is_shared_pointer_impl<shared_pointer_wrapper<T>>
{
	static const bool value = true;
};
template<typename>
struct is_shared_pointer_impl
{
	static const bool value = false;
};

template<typename>
struct is_distributed_reference_impl;

template<typename T>
struct is_distributed_reference_impl<distributed_reference_wrapper<T>>
{
	static const bool value = true;
};
template<typename>
struct is_distributed_reference_impl
{
	static const bool value = false;
};

template<typename>
struct is_distributed_pointer_impl;

template<typename T>
struct is_distributed_pointer_impl<distributed_pointer_wrapper<T>>
{
	static const bool value = true;
};
template<typename>
struct is_distributed_pointer_impl
{
	static const bool value = false;
};

template<typename>
struct is_unique_reference_impl;

template<typename T>
struct is_unique_reference_impl<shared_reference_wrapper<T>>
{
	static const bool value = true;
};
template<typename>
struct is_unique_reference_impl
{
	static const bool value = false;
};

template<typename>
struct is_unique_pointer_impl;

template<typename T>
struct is_unique_pointer_impl<unique_pointer_wrapper<T>>
{
	static const bool value = true;
};
template<typename>
struct is_unique_pointer_impl
{
	static const bool value = false;
};

template<typename T>
struct is_lendable_impl
{
private:
	template<typename TT>
	static constexpr std::true_type test(TT&&, decltype(lend(std::declval<TT>()))* = nullptr);
	static constexpr std::false_type test(...);

public:
	static const bool value = decltype(test(std::declval<T>()))::value;
};

template<typename T>
struct _is_lendable_pointer_impl;

template<typename T>
struct _is_lendable_pointer_impl<lent_pointer_wrapper<T>>
{
	static const bool value = true;
};

#ifdef DDK_DEBUG

template<typename T>
struct _is_lendable_pointer_impl<lent_reference_wrapper<T>>
{
	static const bool value = true;
};

#endif

template<typename T>
struct _is_lendable_pointer_impl
{
	static const bool value = false;
};


template<typename T>
struct is_lendable_pointer_impl
{
private:
	template<typename TT>
	static constexpr decltype(lend(std::declval<TT>())) test(TT&&);
	static constexpr std::false_type test(...);

public:
	static const bool value = is_lendable_impl<T>::value && _is_lendable_pointer_impl<decltype(lend(std::declval<T>()))>::value;
};

template<typename T>
struct _is_lendable_reference_impl;

template<typename T>
struct _is_lendable_reference_impl<lent_reference_wrapper<T>>
{
	static const bool value = true;
};
template<typename T>
struct _is_lendable_reference_impl
{
	static const bool value = false;
};

template<typename T>
struct is_lendable_reference_impl
{
private:
	template<typename TT>
	static constexpr decltype(lend(std::declval<TT>())) test(TT&&);
	static constexpr std::false_type test(...);

public:
	static const bool value = is_lendable_impl<T>::value && _is_lendable_reference_impl<decltype(lend(std::declval<T>()))>::value;
};

}

template<typename T>
constexpr bool is_shared_reference = concepts::is_shared_reference_impl<T>::value;

template<typename T>
constexpr bool is_not_shared_reference = concepts::is_shared_reference_impl<T>::value == false;

template<typename T>
constexpr bool is_shared_pointer = concepts::is_shared_pointer_impl<T>::value;

template<typename T>
constexpr bool is_not_shared_pointer = concepts::is_shared_pointer_impl<T>::value == false;

template<typename T>
constexpr bool is_distributed_reference = concepts::is_distributed_reference_impl<T>::value;

template<typename T>
constexpr bool is_not_distributed_reference = concepts::is_distributed_reference_impl<T>::value == false;

template<typename T>
constexpr bool is_distributed_pointer = concepts::is_distributed_pointer_impl<T>::value;

template<typename T>
constexpr bool is_not_distributed_pointer = concepts::is_distributed_pointer_impl<T>::value == false;

template<typename T>
constexpr bool is_unique_reference = concepts::is_unique_reference_impl<T>::value;

template<typename T>
constexpr bool is_not_unique_reference = concepts::is_unique_reference_impl<T>::value == false;

template<typename T>
constexpr bool is_unique_pointer = concepts::is_unique_pointer_impl<T>::value;

template<typename T>
constexpr bool is_not_unique_pointer = concepts::is_unique_pointer_impl<T>::value == false;

template<typename T>
constexpr bool is_lendable = concepts::is_lendable_impl<T>::value;

template<typename T>
constexpr bool is_lendable_pointer = concepts::is_lendable_pointer_impl<T>::value;

template<typename T>
constexpr bool is_lendable_reference = concepts::is_lendable_reference_impl<T>::value;

}
