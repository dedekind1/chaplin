//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

namespace ddk
{
namespace concepts
{

template<typename T>
struct has_iterator_defined_impl
{
private:
    template<typename TT, typename = typename TT::iterator>
    static std::true_type resolve(const TT&);
    static std::false_type resolve(...);

public:
    static const bool value = decltype(resolve(std::declval<T>()))::value;
};

}

template<typename T>
inline constexpr bool has_iterator_defined = concepts::has_iterator_defined_impl<mpl::remove_qualifiers<T>>::value;

template<typename T>
inline constexpr bool is_const_iterator = mpl::is_const<decltype(std::declval<T>().operator*())>;

template<typename T>
inline constexpr bool is_non_const_iterator = mpl::is_const<decltype(std::declval<T>().operator*())> == false;

}
