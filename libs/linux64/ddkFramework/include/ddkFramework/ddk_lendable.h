//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_lent_reference_wrapper.h"
#include "ddk_reference_counter.h"
#include <type_traits>

namespace ddk
{

template<typename T>
class lendable
{
	template<typename TT>
	friend inline lent_reference_wrapper<TT> lend(lendable<TT>&);
	template<typename TT>
	friend inline lent_reference_wrapper<const TT> lend(const lendable<TT>&);
    typedef T value_type;
    typedef typename std::add_const<T>::type const_value_type;
    typedef typename std::add_lvalue_reference<value_type>::type reference;
    typedef typename std::add_lvalue_reference<const_value_type>::type const_reference;
    typedef typename std::add_pointer<value_type>::type pointer_type;
    typedef typename std::add_pointer<const_value_type>::type const_pointer_type;

public:
	lendable() = default;
	TEMPLATE(typename ... Args)
	REQUIRES(is_constructible<T,Args...>)
	lendable(Args&& ... i_args);
	lendable(const lendable& other);
	lendable(lendable&& other);
	~lendable();
	lendable& operator=(const lendable& other);
	lendable& operator=(lendable&& other);
	pointer_type operator->();
	const_pointer_type operator->() const;
	reference operator*();
	const_reference operator*() const;

private:
#ifdef DDK_DEBUG
	inline lent_reference_wrapper<value_type> ref_from_this();
	inline lent_reference_wrapper<const_value_type> ref_from_this() const;
#else
	inline lent_reference_wrapper<value_type> ref_from_this();
	inline lent_reference_wrapper<const_value_type> ref_from_this() const;
#endif

	T m_value;
#ifdef DDK_DEBUG
	mutable lent_reference_counter m_counter;
#endif
};

}

#include "ddk_lendable.inl"