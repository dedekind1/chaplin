//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_iterator_concepts.h"
#include "ddk_container_concepts.h"

namespace ddk
{
namespace concepts
{

template<typename IterableValue,typename T,size_t ... Indexs>
typename mpl::static_if<mpl::holds_type_for_some_type<std::is_constructible,IterableValue&&,typename T::template nth_type<Indexs>...>(),std::true_type,std::false_type>::type _resolve_iterable_valued_function(const mpl::sequence<Indexs...>&);
template<typename Iterable,typename Function,typename T = typename mpl::aqcuire_callable_args_type<Function>::type>
decltype(_resolve_iterable_valued_function<typename Iterable::iterable_value,T>(typename mpl::make_sequence<0,T::size()>::type{})) resolve_iterable_valued_function(const Iterable&,Function&);
template<typename Iterable,typename Function,typename = decltype(std::declval<Function>()(std::declval<typename Iterable::iterable_value>()))>
std::true_type resolve_iterable_valued_function(const Iterable&,const Function&);
template<typename ... T>
std::false_type resolve_iterable_valued_function(const T& ...);

template<typename Iterable,typename Function>
inline constexpr bool is_iterable_valued_function = decltype(resolve_iterable_valued_function(std::declval<Iterable>(),std::declval<mpl::remove_qualifiers<Function>&>()))::value;

template<typename T>
struct is_iterable_type_impl;

template<typename T>
struct is_iterable_type_impl<ddk::detail::iterable<T>>
{
    static const bool value = true;
};
template<typename T>
struct is_iterable_type_impl
{
private:
    template<typename Iterable>
    static std::true_type resolve(const ddk::detail::iterable<Iterable>&);
    static std::false_type resolve(...);

public:
    static const bool value = decltype(resolve(std::declval<T>()))::value;
};

template<typename T>
struct holds_iterable_type_impl
{
private:
    template<typename TT, typename = typename TT::iterable_type>
    static std::true_type resolve(const TT&);
    static std::false_type resolve(...);

public:
    static const bool value = decltype(resolve(std::declval<T>()))::value;
};

}

template<typename T>
inline constexpr bool holds_iterable_type = concepts::holds_iterable_type_impl<mpl::remove_qualifiers<T>>::value;

template<typename T>
inline constexpr bool is_iterable_type = concepts::is_iterable_type_impl<mpl::remove_qualifiers<T>>::value;

template<typename T>
inline constexpr bool is_not_iterable_type = is_iterable_type<T> == false && holds_iterable_type<T> == false;

template<typename T>
inline constexpr bool is_const_iterable = has_iterator_defined<T> && (is_type_const<T> || is_const_iterator<typename T::iterator>);

template<typename T>
inline constexpr bool is_non_const_iterable = has_iterator_defined<T> && ((is_type_const<T> == false) || (is_const_iterator<typename T::iterator> == false));

}
