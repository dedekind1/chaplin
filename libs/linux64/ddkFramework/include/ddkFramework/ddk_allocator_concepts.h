//////////////////////////////////////////////////////////////////////////////
//
// Author: Jaume Moragues
// Distributed under the GNU Lesser General Public License, Version 3.0. (See a copy
// at https://www.gnu.org/licenses/lgpl-3.0.ca.html)
//
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ddk_type_concepts.h"

namespace ddk
{
namespace concepts
{

template<typename T>
struct is_allocator_impl
{
private:
	template<typename TT, typename = decltype(std::declval<TT>().allocate(1)), typename = decltype(std::declval<TT>().deallocate(static_cast<void*>(nullptr)))>
	static std::true_type test(const TT&);
	static std::false_type test(...);

public:
	static const bool value = decltype(test(std::declval<T>()))::value;
};

}

template<typename T>
inline constexpr bool is_allocator = concepts::is_allocator_impl<const T>::value;

template<typename T>
inline constexpr bool is_not_allocator = concepts::is_allocator_impl<const T>::value == false;

}